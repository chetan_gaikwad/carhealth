package com.android.kycar.carhealth.entities.trip_transaction;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 1/17/2016.
 */
@RealmClass
public class ConvoyJoinee extends RealmObject {

    @PrimaryKey
    private String id;

    private String name;

    private String number;

    public ConvoyJoinee() {
    }

    public ConvoyJoinee(String id, String name, String number) {
        this.id = id;
        this.name = name;
        this.number = number;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
