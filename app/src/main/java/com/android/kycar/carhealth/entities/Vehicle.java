package com.android.kycar.carhealth.entities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by krishna on 6/12/15.
 */
@RealmClass
public class Vehicle extends RealmObject {

    @PrimaryKey
    private String id;

    private String name;

    private String nickName;

    private String year;

    private String userId;

    private String variantId;

    private String regNumber;

    private String vin;

    private String vehicleImage;

    private String encodedVin;

    private Date puc;

    private Date insurance;

    private Date rc;

    private boolean isActive;

    private int maxSpeed;

    private int maxOilTemp;

    private boolean isSync;

    public Vehicle() {
    }

    public Vehicle(JSONObject vehicleJson) {
        try {
            if (vehicleJson.has("id")) { this.id = vehicleJson.getString("id"); }
            if (vehicleJson.has("name")) { this.name = vehicleJson.getString("name"); }
            if (vehicleJson.has("nickName")) { this.nickName = vehicleJson.getString(""); }
            if (vehicleJson.has("year")) { this.nickName = vehicleJson.getString("year"); }
            if (vehicleJson.has("userId")) { this.nickName = vehicleJson.getString("userId"); }
            if (vehicleJson.has("variantId")) { this.nickName = vehicleJson.getString("variantId"); }
            if (vehicleJson.has("regNumber")) { this.nickName = vehicleJson.getString("regNumber"); }
            if (vehicleJson.has("vin")) { this.nickName = vehicleJson.getString("vin"); }
            if (vehicleJson.has("vehicleImage")) { this.nickName = vehicleJson.getString("vehicleImage"); }
            if (vehicleJson.has("encodedVin")) { this.nickName = vehicleJson.getString("encodedVin"); }
            if (vehicleJson.has("puc")) { this.nickName = vehicleJson.getString("puc"); }
            if (vehicleJson.has("insurance")) { this.nickName = vehicleJson.getString("insurance"); }
            if (vehicleJson.has("rc")) { this.nickName = vehicleJson.getString("rc"); }
            if (vehicleJson.has("isActive")) { this.nickName = vehicleJson.getString("isActive"); }
            if (vehicleJson.has("isSync")) { this.nickName = vehicleJson.getString("isSync"); }
            if (vehicleJson.has("maxSpeed")) { this.nickName = vehicleJson.getString("maxSpeed"); }
            if (vehicleJson.has("maxOilTemp")) { this.nickName = vehicleJson.getString("maxOilTemp"); }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Vehicle(Vehicle vehicle) {
        this.id = vehicle.getId();
        this.name = vehicle.getName();
        this.nickName = vehicle.getNickName();
        this.year = vehicle.getYear();
        this.userId = vehicle.getUserId();
        this.variantId = vehicle.getVariantId();
        this.regNumber = vehicle.getRegNumber();
        this.vin = vehicle.getVin();
        this.vehicleImage = vehicle.getVehicleImage();
        this.encodedVin = vehicle.getEncodedVin();
        this.puc = vehicle.getPuc();
        this.insurance = vehicle.getInsurance();
        this.rc = vehicle.getRc();
        this.isActive = vehicle.isActive();
        this.isSync = vehicle.isSync();
        this.maxSpeed = vehicle.getMaxSpeed();
        this.maxOilTemp = vehicle.getMaxOilTemp();
    }

    public Vehicle(String id, String name, String nickName, String year, String userId, String variantId, String regNummber, String vin, String encodedVin, Date puc, Date insurence, Date rc, Boolean isActive) {
        this.id = id;
        this.name = name;
        this.nickName = nickName;
        this.year = year;
        this.userId = userId;
        this.variantId = variantId;
        this.regNumber = regNummber;
        this.vin = vin;
        this.encodedVin = encodedVin;
        this.puc = puc;
        this.insurance = insurence;
        this.rc = rc;
        this.isActive = isActive;
        this.isSync = false;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getMaxOilTemp() {
        return maxOilTemp;
    }

    public void setMaxOilTemp(int maxOilTemp) {
        this.maxOilTemp = maxOilTemp;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getVehicleImage() {
        return vehicleImage;
    }

    public void setVehicleImage(String vehicleImage) {
        this.vehicleImage = vehicleImage;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getEncodedVin() {
        return encodedVin;
    }

    public void setEncodedVin(String encodedVin) {
        this.encodedVin = encodedVin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getVariantId() {
        return variantId;
    }

    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Date getPuc() {
        return puc;
    }

    public void setPuc(Date puc) {
        this.puc = puc;
    }

    public Date getInsurance() {
        return insurance;
    }

    public void setInsurance(Date insurance) {
        this.insurance = insurance;
    }

    public Date getRc() {
        return rc;
    }

    public void setRc(Date rc) {
        this.rc = rc;
    }
}
