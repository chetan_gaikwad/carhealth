package com.android.kycar.carhealth.utils;

/**
 * Created by 638 on 9/18/2016.
 */
public class CustomDialogPojo {
    public int eventId;

    public CustomDialogPojo(int eventId) {
        this.eventId = eventId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }
}
