package com.android.kycar.carhealth.services.connection_services;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import android.view.View;


import com.android.kycar.carhealth.exceptions.ConnectionException;
import com.android.kycar.carhealth.exceptions.ObdDeviceNotFoundException;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.UUID;

/**
 * Created by krishna on 5/12/15.
 */
public class BluetoothConnection {


    public Boolean doBluetoothConnection(View view, BluetoothAdapter bluetoothAdapter) throws ConnectionException, ObdDeviceNotFoundException {

        Log.d("Do BT", "In doBluetooth connection");

        String xsecureDeviceMac = "20:15:08:13:27:98";
        //String obdMac = "00:18:56:68:AE:08";

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();


        BluetoothDevice bt = null;//= bluetoothAdapter.getRemoteDevice("20:15:08:13:27:98");
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter
                .getBondedDevices();

        if (pairedDevices.isEmpty()) {
            throw new ObdDeviceNotFoundException("Device not found");
        } else {

            Log.d("Total device", "" + pairedDevices.size());
            for (BluetoothDevice device : pairedDevices) {
                Log.d("", "Device : address : " + device.getAddress() + " name :"
                        + device.getName());
                if (xsecureDeviceMac.equals(device.getAddress())) {
                    bt = device;
                    break;
                }
            }
        }

        BluetoothSocket btSocket = null;
        BluetoothSocket mmSocket = null;

        UUID uuid = UUID.fromString("de305d54-75b4-431b-adb2-eb6b9e546014");

        // Get a BluetoothSocket for a connection with the
        // given BluetoothDevice
        try {
            btSocket = bt.createRfcommSocketToServiceRecord(uuid);
            Method m = bt.getClass().getMethod("createRfcommSocket", int.class);
            btSocket = (BluetoothSocket) m.invoke(bt, 1);

            btSocket.connect();

            Log.d("Bluetooth connection", "" + btSocket.isConnected());

            InputStream inputStream = btSocket.getInputStream();
            while (true) {
                int command = inputStream.read();

                if (command == -1) {
                    System.out.println("finish process");
                    break;
                }

                int bufferSize = 2048;
                StringBuilder message = new StringBuilder();
                byte[] buffer = new byte[bufferSize];
                int bytesRead = inputStream.read();

                //message = message + new String(buffer, 0, bytesRead);


                bytesRead = inputStream.read(buffer);
                String readed = new String(buffer, 0, bytesRead);
                message.append(readed);

                if (readed.contains("\n")) {

                    // mHandler.obtainMessage(DeviceControlActivity.MESSAGE_READ, bytes, -1, readMessage.toString()).sendToTarget();
                    // readMessage.setLength(0);
                }

                //bytesRead = inputStream.read(buffer);


                    /*while ((bytesRead==bufferSize)&&(buffer[bufferSize-1] != 0)) {

                    }*/
                //   message = message + new String(buffer, 0, bytesRead - 1);
                //handler.post(new MessagePoster(textView, message));


                if (message.toString().contains("GX")) {

                    Log.d("GX", message.toString());
                }
                if (message.toString().contains("GY")) {

                    Log.d("GY", message.toString());
                }
                if (message.toString().contains("GZ")) {

                    Log.d("GZ", message.toString());
                }

                btSocket.getInputStream();
            }

            /*int bytesRead = -1;
            String message = "";
            InputStream instream = btSocket.getInputStream();


            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];

            bytesRead = instream.read(buffer);


            if (bytesRead != -1) {
                while ((bytesRead==bufferSize)&&(buffer[bufferSize-1] != 0)) {
                    message = message + new String(buffer, 0, bytesRead);
                    bytesRead = instream.read(buffer);
                }
                message = message + new String(buffer, 0, bytesRead - 1);
                //handler.post(new MessagePoster(textView, message));

                Log.d("Bt", message);
                btSocket.getInputStream();
            }
*/


        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnectionException("Bluethooth connection exception occured");
        }
        mmSocket = btSocket;
        return  true;
    }



}
