package com.android.kycar.carhealth.repositories.user;

import android.content.Context;


import com.android.kycar.carhealth.entities.UserContactSetting;
import com.android.kycar.carhealth.repositories.Repository;

import io.realm.exceptions.RealmException;

/**
 * Created by Krishna on 2/1/2016.
 */
public class UserContactSettingRepo extends Repository {
    public UserContactSettingRepo(Context context, Class _aClass) {
        super(context, _aClass);
    }

    public UserContactSetting findByUserContactId(String userContactId)throws RealmException {
        return realm.where(UserContactSetting.class).equalTo("userContactId", userContactId).findFirst();
    }
}
