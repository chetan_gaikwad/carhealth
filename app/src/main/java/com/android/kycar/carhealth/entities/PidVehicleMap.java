package com.android.kycar.carhealth.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 1/17/2016.
 */
@RealmClass
public class PidVehicleMap extends RealmObject {

    @PrimaryKey
    private String id;

    private String vehicleId;

    private String pid;

    public PidVehicleMap() {
    }

    public PidVehicleMap(String id, String vehicleId, String pid) {
        this.id = id;
        this.vehicleId = vehicleId;
        this.pid = pid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
}
