package com.android.kycar.carhealth.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 1/17/2016.
 */
@RealmClass
public class SpeedPerformanceParameter extends RealmObject {

    @PrimaryKey
    private String id;

    private int start;

    private int stop;

    public SpeedPerformanceParameter() {
    }

    public SpeedPerformanceParameter(String id, int start, int stop) {
        this.id = id;
        this.start = start;
        this.stop = stop;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getStop() {
        return stop;
    }

    public void setStop(int stop) {
        this.stop = stop;
    }
}
