package com.android.kycar.carhealth.async_task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.kycar.carhealth.entities.trip_transaction.Trip;
import com.android.kycar.carhealth.repositories.trip.TripRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.TripRestService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krishna on 9/12/2016.
 */

public class CreateTripAsyncTask extends AsyncTask<JSONObject, Void, String> {

    private Context context;

    public CreateTripAsyncTask(Context context){
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(JSONObject... jsonObjects) {

        try {
            Log.d(CreateTripSessionAsyncTask.class.getSimpleName(), "Trip called sussccessfully:-"+jsonObjects[0]);
            return new TripRestService().createTrip(jsonObjects[0]);
        } catch (Exception e) {
            e.printStackTrace();
            return "User not found";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        Log.d(CreateTripAsyncTask.class.getSimpleName(), "Result:- "+result);

        if(result!=null) {
            //Toast.makeText(this, "Trip created successfully", Toast.LENGTH_SHORT).show();
            try {
                JSONObject resObject = new JSONObject(result);
                TripRepo tripRepo = new TripRepo(context, Trip.class);
                Trip trip = new Trip((Trip) tripRepo.findById(resObject.getString("tripId")));
                trip.setSync(true);
                tripRepo.saveOrUpdate(trip);
                Log.d(CreateTripAsyncTask.class.getSimpleName(), "Trip Updated");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}