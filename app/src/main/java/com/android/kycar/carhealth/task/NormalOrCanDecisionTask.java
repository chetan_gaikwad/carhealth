package com.android.kycar.carhealth.task;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.kycar.carhealth.config.ConfigActivity;
import com.android.kycar.carhealth.config.ProgessDialogBox;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.Vehicle;
import com.android.kycar.carhealth.repositories.vehicle.VehicleRepo;
import com.android.kycar.carhealth.services.dto.commands.car_type.CarType;
import com.android.kycar.carhealth.services.utils.Logger;
import com.android.kycar.carhealth.ui.activity.DashboardActivity;
import com.android.kycar.carhealth.utils.CarVinStates;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Chetan on 5/29/2016.
 * This class is used to take decision weather the car is normal communication wali or CAN
 */
public class NormalOrCanDecisionTask extends AsyncTask<String,Void,String> {
    private OnTaskCompleted listener;
    private static final String TAG = NormalOrCanDecisionTask.class.getSimpleName();
    private ProgessDialogBox progessDialogBox;
    private Activity activity;

    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private BluetoothDevice dev = null;
    private BluetoothSocket sock = null;
    private BluetoothSocket sockFallback = null;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private Logger logger;
    private MenuItem item;
    private String vin = null;
    private CarVinStates carVinStates;

    public NormalOrCanDecisionTask(Activity activity, OnTaskCompleted listener, MenuItem item) {
        this.listener = listener;
        this.activity = activity;
        this.item = item;
        prefs = PreferenceManager.getDefaultSharedPreferences(activity.getBaseContext());
        editor = prefs.edit();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //logger = new //logger("Response.txt");
        progessDialogBox = new ProgessDialogBox(activity);
        progessDialogBox.showDailog("Checking car compatibility...");
    }


    @Override
    protected String doInBackground(String[] params) {
        try {
            //logger.logging("Service started   /n");
            return startService();
        }catch (IOException e){
            //logger.logging("Failed to start /n");
            return "Failed to connect";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        progessDialogBox.dismissDialog();

        if(result.contains("Failed") || result.contains("gnition") || result.contains("No Bluetooth")) {
            item.setTitle("Start OBD Data");
            DashboardActivity.isLiveData = false;
        } else {
            DashboardActivity.isLiveData = true;
        }
        Toast.makeText(activity,result,Toast.LENGTH_SHORT).show();
        //logger.finalizeFile();
    }

    public String startService() throws IOException {
        Log.d(TAG, "Starting service..");

        final String remoteDevice = prefs.getString(ConfigActivity.BLUETOOTH_LIST_KEY, null);
        if (remoteDevice == null || "".equals(remoteDevice)) {

            DashboardActivity.isLiveData = false;
            item.setTitle("Start OBD Data");
            Log.e(TAG, "No Bluetooth device has been selected.");
            // TODO kill this service gracefully
            return "No Bluetooth device has been selected.";
        } else {

            final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
            dev = btAdapter.getRemoteDevice(remoteDevice);
            Log.d(NormalOrCanDecisionTask.class.getSimpleName(), "StartService() : Stopping Bluetooth discovery.");
            btAdapter.cancelDiscovery();
            try {
                return startObdConnection();
            } catch (Exception e) {
                Log.e(
                        TAG,
                        "StartService() : There was an error while establishing connection. -> "
                                + e.getMessage()
                );
                throw new IOException();
            }
        }
    }

    private String startObdConnection() throws IOException {

        Log.d(TAG, "startObdConnection() : Starting OBD connection..");
        try {
            // Instantiate a BluetoothSocket for the remote device and connect it.
            sock = dev.createRfcommSocketToServiceRecord(MY_UUID);
            sock.connect();
        } catch (Exception e1) {
            Log.e(TAG, "startObdConnection() : There was an error while establishing Bluetooth connection. Falling back..", e1);
            Class<?> clazz = sock.getRemoteDevice().getClass();
            Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};
            try {

                /************Fallback method 1*********************/
                Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                Object[] params = new Object[]{Integer.valueOf(1)};
                sockFallback = (BluetoothSocket) m.invoke(sock.getRemoteDevice(), params);
                sockFallback.connect();

                sock = sockFallback;
                /************Fallback method 1 end*********************/
                //logger.logging("Obd connected  /n");
                Log.e(TAG,"startObdConnection() : Connected");

                /************Fallback method 2*********************/
            } catch (Exception e2) {
                Log.e(TAG, "startObdConnection() : Couldn't fallback while establishing Bluetooth connection. Stopping app..", e2);
                //logger.logging("failed to start "+e2.getMessage()+" /n");
                return "Failed to connect";
            }
        }
        Log.e(TAG,"startObdConnection() : Connected");
        getDeviceDetails();
        getBatteryDetails();
        if(fireCommand()){
            return "Connected";
        } else {

            /*sock.getInputStream().close();
            sock.getOutputStream().close();
            sock.close();*/
            DashboardActivity.isLiveData = false;
            //item.setTitle("Start OBD Data");
            return "Either ignition is off or car is not supported ";
        }
    }

    public boolean fireCommand(){
        String selectProtocol = "ATSP0\r";
        String echoof = "ATE0\r";
        String rpm = "01 0C\r";
        String headerOff = "ATH0\r";

        try {
            Log.i(Util.LOG, "fire command scanning for supported pid");

            sock.getOutputStream().write(echoof.getBytes());
            String response = readRawData(sock.getInputStream());
            //logger.logging("\nechoof = "+response);

            Log.i(Util.LOG, "fire command echo of - " + response);

            sock.getOutputStream().write(selectProtocol.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\nselectProtocol = "+response);
            Log.i(Util.LOG, " fire command select protocol - " + response);

            sock.getOutputStream().write(rpm.getBytes());
            response = readRawData(sock.getInputStream());
            Log.i(Util.LOG, " fire command rpm - " + response);

            String speed = "010D\r";

            sock.getOutputStream().write(speed.getBytes());
            response = readRawData(sock.getInputStream());
            Log.i("logs", "fire command speed - " + response);

            //calculate vin

            String serialNumber = "0902\r";
            String atdp = "ATDP\r";
            String atdpn = "ATDPN\r";
            String headerOn = "ATH1\r";

            sock.getOutputStream().write(atdp.getBytes());
            String detectedProtocol = readRawData(sock.getInputStream());
            if(detectedProtocol.contains("OK"))
                Log.i(Util.LOG, "getDeviceDetails serial number - "+detectedProtocol);

            //logger.logging("\n atdp = "+atdp);

            sock.getOutputStream().write(atdpn.getBytes());
            String tempAtdpn = readRawData(sock.getInputStream());
            //logger.logging("\n atdpn = "+atdpn);



            //Header on
            sock.getOutputStream().write(headerOn.getBytes());
             tempAtdpn = readRawData(sock.getInputStream());

            sock.getOutputStream().write(serialNumber.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\n serialNumber = "+response);
            if(response.contains("OK"))
                Log.i(Util.LOG, "getDeviceDetails serial number - "+response);
            editor.putString(Util.VIN_PREFERENCE,"").commit();
            //calculateVinHeaderOn(response,detectedProtocol);

            //Header off
            sock.getOutputStream().write(headerOff.getBytes());
            tempAtdpn = readRawData(sock.getInputStream());

            if(prefs.contains(Util.CAR_PREFERENCE)) {
                if (vin != null) {
                    editor.putString(ConfigActivity.VEHICLE_ID_KEY, vin).commit();
                    if (currentVinPresentInDatabase()) {
                        if (vin.equals(getCurrentSelectedCarVin())) {
                            //Viola correct car connected
                            //CURRENTLY_SELECTED_CAR_MATCHES_THE_VIN
                            carVinStates = CarVinStates.CURRENTLY_SELECTED_CAR_MATCHES_THE_VIN;
                        } else {
                            //Wrong car selected
                            //VIN_FOUND_AND_PRESENT_IN_DATABASE_BUT_WRONG_CAR_SELECTED
                            carVinStates = CarVinStates.VIN_FOUND_AND_PRESENT_IN_DATABASE_BUT_WRONG_CAR_SELECTED;
                        }
                    } else {
                        //Vin not fount in database
                        //VIN_FOUND_BUT_NO_CAR_WITH_THE_MATCHING_VIN_FOUND_IN_DATABASE
                        //Ask user is the select car and actual car same
                        //if yes update vin, isSync = false
                        //if no Anon car Ask to add
                        carVinStates = CarVinStates.VIN_FOUND_BUT_NO_CAR_WITH_THE_MATCHING_VIN_FOUND_IN_DATABASE;
                    }

                } else {
                    editor.putString(ConfigActivity.VEHICLE_ID_KEY, "NA").commit();
                    //Vin not found
                    carVinStates = CarVinStates.VIN_NOT_FOUND;
                }
            } else {
                //No car selected
                carVinStates = CarVinStates.NO_CAR_SELECTED;
            }

            sock.getOutputStream().write(rpm.getBytes());
            response = readRawData(sock.getInputStream());

            sock.getOutputStream().write(speed.getBytes());
            response = readRawData(sock.getInputStream());

            if(!response.contains("41")) {
                return false;
            }
            //logger.logging("\nrpm = "+response);

            Log.i(Util.LOG, "fire command rpm - " + response);
            Log.i(Util.LOG, "rpm response length - " + response.length());

            sock.getOutputStream().write(rpm.getBytes());
            int supportType = getSupportedType(sock.getInputStream());

            CarType carType = new CarType();
            carType.setCarType(supportType);

            if(carType.getCarType() == Util.isCrud) {
                getCrudCanPrimaryHeader();
            }

            sock.getInputStream().close();
            sock.getOutputStream().close();
            sock.close();
            listener.onTaskCompleted(carType.getCarType(), carVinStates, vin);
        }catch (Exception e){
            e.printStackTrace();
            return false;
            //Util.writeToFile(activity,e.getMessage());
        }
        return true;
    }

    private String getCurrentSelectedCarVin() {
        VehicleRepo vehicleRepo = new VehicleRepo(activity, Vehicle.class);
        Vehicle vehicle = (Vehicle) vehicleRepo.findById(prefs.getString(Util.CAR_PREFERENCE, ""));
        return vehicle.getVin();
    }

    private boolean currentVinPresentInDatabase() {
        VehicleRepo vehicleRepo = new VehicleRepo(activity, Vehicle.class);
        Vehicle vehicle = vehicleRepo.findByVin(vin);
        if(vehicle != null){
            return true;
        } else {
            return false;
        }
    }

    private void getCrudCanPrimaryHeader() throws Exception {
        String reset = "ATZ\r";
        String lineFeed = "ATL0\r";
        String headerOn = "ATH1\r";
        String selectProtocol = "ATSP0\r";
        String echoof = "ATE0\r";

        String rpm = "01 0C\r";
        String eng_cool_temp = "01 05\r";
        String speed = "010D\r";
        String distCovered = "0131\r";
        String fuleType = "0151\r";
        String engOilTemp = "015c\r";
        String maf = "0110\r";
        String load = "0104\r";
        String throttle = "0111\r";

        HashMap<String,Integer> hashHeaderData = new HashMap<>();
        try {
            String response = "";

            sock.getOutputStream().write(reset.getBytes());
            boolean goAhead = false;
            do {

                response = readRawData(sock.getInputStream());
                //logger.logging("\n reset = "+response);
                Util.writeToFile(activity,reset+" - "+response);
                if(response.contains("ELM327"))
                    goAhead=true;

            }while(!goAhead);

            sock.getOutputStream().write(echoof.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\n echoof = "+response);
            if(response.contains("OK"))
                Log.i(Util.LOG, "getDeviceDetails echoo off");
            Util.writeToFile(activity,echoof+" - "+response);

            sock.getOutputStream().write(lineFeed.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\n lineFeed = "+response);
            if(response.contains("OK"))
                Log.i(Util.LOG, "getDeviceDetails line feed off");

            Util.writeToFile(activity,lineFeed+" - "+response);

            sock.getOutputStream().write(selectProtocol.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\n selectProtocol = "+response);
            Util.writeToFile(activity,selectProtocol+" - "+response);

            /*sock.getOutputStream().write(headerOff.getBytes());
            temp = readRawData(sock.getInputStream());
            Util.writeToFile(activity,headerOff+" - "+temp);*/

            sock.getOutputStream().write(headerOn.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\n headerOn = "+response);
            Util.writeToFile(activity,headerOn+" - "+response);

            sock.getOutputStream().write(rpm.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\n rpm = "+response);

            //RPM
            sock.getOutputStream().write(rpm.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\n rpm "+response);

            ArrayList<String> headers = countHeader(response,"410C");
            for (int i=0;i<headers.size();i++){
                if(hashHeaderData.containsKey(headers.get(i))){
                    int count = hashHeaderData.get(headers.get(i));
                    hashHeaderData.put(headers.get(i),count+1);
                } else {
                    hashHeaderData.put(headers.get(i),1);
                }
            }


            //SPEED
            sock.getOutputStream().write(speed.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\n speeed "+response);


            headers = countHeader(response,"410D");
            for (int i=0;i<headers.size();i++){
                if(hashHeaderData.containsKey(headers.get(i))){
                    int count = hashHeaderData.get(headers.get(i));
                    hashHeaderData.put(headers.get(i),count+1);
                } else {
                    hashHeaderData.put(headers.get(i),1);
                }
            }

            //DISTANCE
            sock.getOutputStream().write(distCovered.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\n distance "+response);


            headers = countHeader(response,"4131");
            for (int i=0;i<headers.size();i++){
                if(hashHeaderData.containsKey(headers.get(i))){
                    int count = hashHeaderData.get(headers.get(i));
                    hashHeaderData.put(headers.get(i),count+1);
                } else {
                    hashHeaderData.put(headers.get(i),1);
                }
            }

            //eng_cool_temp
            sock.getOutputStream().write(eng_cool_temp.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\n eng_cool_temp "+response);


            headers = countHeader(response,"4105");
            for (int i=0;i<headers.size();i++){
                if(hashHeaderData.containsKey(headers.get(i))){
                    int count = hashHeaderData.get(headers.get(i));
                    hashHeaderData.put(headers.get(i),count+1);
                } else {
                    hashHeaderData.put(headers.get(i),1);
                }
            }

            //engOilTemp
            sock.getOutputStream().write(engOilTemp.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\n engOilTemp "+response);

            headers = countHeader(response,"415C");
            for (int i=0;i<headers.size();i++){
                if(hashHeaderData.containsKey(headers.get(i))){
                    int count = hashHeaderData.get(headers.get(i));
                    hashHeaderData.put(headers.get(i),count+1);
                } else {
                    hashHeaderData.put(headers.get(i),1);
                }
            }

            //throttle
            sock.getOutputStream().write(throttle.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\n throttle "+response);


            headers = countHeader(response,"4111");
            for (int i=0;i<headers.size();i++){
                if(hashHeaderData.containsKey(headers.get(i))){
                    int count = hashHeaderData.get(headers.get(i));
                    hashHeaderData.put(headers.get(i),count+1);
                } else {
                    hashHeaderData.put(headers.get(i),1);
                }
            }


            //load
            sock.getOutputStream().write(load.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\n throttle "+response);

            headers = countHeader(response,"4104");
            for (int i=0;i<headers.size();i++){
                if(hashHeaderData.containsKey(headers.get(i))){
                    int count = hashHeaderData.get(headers.get(i));
                    hashHeaderData.put(headers.get(i),count+1);
                } else {
                    hashHeaderData.put(headers.get(i),1);
                }
            }

            //fuleType
            sock.getOutputStream().write(fuleType.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\n fuleType "+response);


            headers = countHeader(response,"4151");
            for (int i=0;i<headers.size();i++){
                if(hashHeaderData.containsKey(headers.get(i))){
                    int count = hashHeaderData.get(headers.get(i));
                    hashHeaderData.put(headers.get(i),count+1);
                } else {
                    hashHeaderData.put(headers.get(i),1);
                }
            }

            //maf
            sock.getOutputStream().write(maf.getBytes());
            response = readRawData(sock.getInputStream());
            //logger.logging("\n maf "+response);


            headers = countHeader(response,"4110");
            for (int i=0;i<headers.size();i++){
                if(hashHeaderData.containsKey(headers.get(i))){
                    int count = hashHeaderData.get(headers.get(i));
                    hashHeaderData.put(headers.get(i),count+1);
                } else {
                    hashHeaderData.put(headers.get(i),1);
                }
            }


            Util.writeToFile(activity,"Hash data "+hashHeaderData.toString());
            Log.d("Hash data ",hashHeaderData.toString());
            int maxValueInMap=(Collections.max(hashHeaderData.values()));
            for (Map.Entry<String, Integer> entry : hashHeaderData.entrySet()) {  // Itrate through hashmap
                if (entry.getValue()==maxValueInMap) {

                    editor.putString(Util.DEFAULT_HEADER, entry.getKey());

                    Log.d("Default Header", entry.getKey());
                    Util.writeToFile(activity,"Default Header "+entry.getKey());
                    editor.commit();
                    System.out.println("header is "+entry.getKey());     // Print the key with max value

                }
            }


        }catch (IOException e){
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void obdDtc(String detectedProtocol) throws Exception {

        sock.getOutputStream().write("ATH0\r".getBytes());
        String response = readRawData(sock.getInputStream());
        String errorCode = "03\r";

        sock.getOutputStream().write(errorCode.getBytes());
        response = readRawData(sock.getInputStream());
        Log.d("Error_Result 03 :-", ""+response);
        extractErrorCode(detectedProtocol, response);

        errorCode = "07\r";
        sock.getOutputStream().write(errorCode.getBytes());
        response = readRawData(sock.getInputStream());
        Log.d("Error_Result 07 :-", ""+response);
        extractErrorCode(detectedProtocol, response);
    }

    private void extractErrorCode(String detectedProtocol, String errorCodeResponse){

        Log.d("detected protocol",detectedProtocol);
        Log.d("detected response",errorCodeResponse);

        StringBuilder errorCode = new StringBuilder();
        errorCodeResponse = errorCodeResponse.replace(" ", "");
        String[] responses = errorCodeResponse.split("\r");

        /** ISO */
        if(detectedProtocol.contains("9141") || detectedProtocol.contains("14230")) {

            for(String str: responses ){

                str = str.substring(2, str.length());
                errorCode.append(str);
            }
        } /** CAN */
        else if(detectedProtocol.contains("15765")) {
            for(String str: responses ){

                if(str == responses[0]){continue;}
                if(str.startsWith("0:")||str.startsWith("1:")||str.startsWith("2:")||str.startsWith("3:")||str.startsWith("4:")){
                    str = str.substring(2, str.length());

                    if(str.startsWith("43") || str.startsWith("47")){
                        str = str.substring(4, str.length());
                        errorCode.append(str);
                    }else {
                        errorCode.append(str);
                    }
                } else if(str.startsWith("43") || str.startsWith("47")){
                    if(errorCode.length() % 4 !=0 && errorCode.substring(errorCode.length()-2, errorCode.length()).equals("00")){

                        errorCode.deleteCharAt(errorCode.length()-1);
                        errorCode.deleteCharAt(errorCode.length()-1);
                    }

                    str = str.substring(4, str.length());
                    errorCode.append(str);
                }
            }
        }
        parseErrorCode(errorCode.toString());
    }

    private void parseErrorCode(String detectedErrorCode) {

        HashMap<Character,String> errorCodeHash = new HashMap<>();
        errorCodeHash.put('0', "P0");
        errorCodeHash.put('1', "P1");
        errorCodeHash.put('2', "P2");
        errorCodeHash.put('3', "P3");
        errorCodeHash.put('4', "C0");
        errorCodeHash.put('5', "C1");
        errorCodeHash.put('6', "C2");
        errorCodeHash.put('7', "C3");
        errorCodeHash.put('8', "B0");
        errorCodeHash.put('9', "B1");
        errorCodeHash.put('A', "B2");
        errorCodeHash.put('B', "B3");
        errorCodeHash.put('C', "U0");
        errorCodeHash.put('D', "U1");
        errorCodeHash.put('E', "U2");
        errorCodeHash.put('F', "U3");

        List<String> output = new ArrayList<>();
        Log.d("Error Found :-", detectedErrorCode);
        for (int i = 0; i < detectedErrorCode.length(); i+=4) {
            String str = detectedErrorCode.substring(i, i+4);

            char header = str.charAt(0);
            output.add(errorCodeHash.get(header)+str.substring(1, str.length()));
        }

        for (String str: output){
            Log.d("Error found:-", str.toString());
        }
    }

    public int getSupportedType(InputStream in){
        String headerOn = "ATH1\r";
        //String headerOff = "ATH0\r";
        String rpm = "01 0C\r";
        try {
            String rawData = readRawData(in);

            Pattern p = Pattern.compile("41 0C");
            Matcher m = p.matcher(rawData);
            int count = 0;
            while (m.find()){
                count +=1;
            }

            if(count == 1){
                return Util.isNormal;
            } else  return Util.isCrud;


        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    private String getDeviceDetails(){

        String reset = "ATZ\r";
        String echoof = "ATE0\r";
        String lineFeed = "ATL0\r";

        Util.writeToFile(activity,"-------------------Started------------------");
        try {
            String temp="";
            sock.getOutputStream().write(reset.getBytes());
            boolean goAhead = false;

            do {

                temp = readRawData(sock.getInputStream());
                if(temp.contains("ELM327"))
                    goAhead=true;

            }while(!goAhead);

            sock.getOutputStream().write(echoof.getBytes());
            temp = readRawData(sock.getInputStream());
            if(temp.contains("OK")){
                Log.i(Util.LOG, "getDeviceDetails echoo off");
                //logger.logging("getDeviceDetails echoo off   /n");
            }

            sock.getOutputStream().write(lineFeed.getBytes());
            temp = readRawData(sock.getInputStream());
            if(temp.contains("OK")){
                Log.i(Util.LOG, "getDeviceDetails line feed off");
                //logger.logging("getDeviceDetails line feed off   /n");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return "";
    }
    private String getBatteryDetails() {

        String batteryVoltage = "ATRV\r";

        try {
            sock.getOutputStream().write(batteryVoltage.getBytes());
            String temp = readRawData(sock.getInputStream());
            Log.i(Util.LOG, "getBatteryDetails battery voltage - " + temp);
            //logger.logging("Got battery voltage "+temp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String readRawData(InputStream in) throws IOException {
        byte b = 0;
        StringBuilder res = new StringBuilder();

        // read until '>' arrives OR end of stream reached
        char c;
        while(true)
        {
            b = (byte) in.read();
            if(b == -1) // -1 if the end of the stream is reached
            {
                break;
            }
            c = (char)b;
            if(c == '>') // read until '>' arrives
            {
                break;
            }
            res.append(c);
        }

    /*
     * Imagine the following response 41 0c 00 0d.
     *
     * ELM sends strings!! So, ELM puts spaces between each "byte". And pay
     * attention to the fact that I've put the word byte in quotes, because 41
     * is actually TWO bytes (two chars) in the socket. So, we must do some more
     * processing..
     */
        String rawData = res.toString().replaceAll("SEARCHING", "");

    /*
     * Data may have echo or informative cost like "INIT BUS..." or similar.
     * The response ends with two carriage return characters. So we need to take
     * everything from the last carriage return before those two (trimmed above).
     */
        //kills multiline.. rawData = rawData.substring(rawData.lastIndexOf(13) + 1);
        rawData = rawData.replaceAll("\\s", "");//removes all [ \t\n\x0B\f\r]
        return res.toString();
    }
    public boolean isPIDSupported(InputStream in){
        boolean rpm = false;
        try {
            String rawData = readRawData(in);
            if(rawData.contains("41")){
                rpm=true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return rpm;
    }


    public void calculateVinHeaderOn(String response, String protocol) {

        //logger.logging("\n In calculateVinHeaderOn "+response+", Protocol is "+protocol);
        editor.putString(Util.DEFAULT_PROTOCOL, protocol).commit();
        response = response.replace(" ", "");
        //logger.logging("\n response = "+response);
        String[] responses = response.split("\r");

        StringBuilder rawEncodedVin = new StringBuilder();

        if(protocol.contains("9141") || protocol.contains("14230")) {

             /*String header = responses[0].substring(4,6);
             editor.putString(Util.DEFAULT_HEADER, header);

             Log.d("Default Header", header);
             //logger.logging("\n Default header "+header);
             editor.commit();*/

            for(String str: responses ){

                str = str.substring(12, str.length()-2);
                rawEncodedVin.append(str);
            }

        } else if(protocol.contains("15765")) {

            if(protocol.contains("11/")){

                 /*editor.putString(Util.DEFAULT_HEADER, responses[0].substring(0,3));
                 editor.commit();

                 Log.d("Default header", responses[0].substring(0,3));*/
                for(String str: responses ){
                    if(responses[0] == str){
                        str = str.substring(13, str.length());
                        rawEncodedVin.append(str);
                    } else {
                        str = str.substring(5, str.length());
                        rawEncodedVin.append(str);
                    }
                }

            } else if(protocol.contains("29/")){
                /* editor.putString(Util.DEFAULT_HEADER, responses[0].substring(6,8));
                 Log.d("Default header", responses[0].substring(6,8));
                 editor.commit();*/

                for(String str: responses ){
                    if(responses[0] == str){
                        str = str.substring(str.length()-6, str.length());
                        rawEncodedVin.append(str);
                    } else {
                        str = str.substring(str.length()-14, str.length());
                        rawEncodedVin.append(str);
                    }
                }
            }
        }
        hexToAscii(rawEncodedVin.toString());
    }

    public void hexToAscii(String hex){
        hex = hex.substring(hex.length()-34, hex.length());
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < hex.length(); i+=2) {
            String str = hex.substring(i, i+2);
            output.append((char)Integer.parseInt(str, 16));
        }
        /*
        if( checkIfThisVinExist()){
            if(checkIfCurrentVinIsSelected()){

            }else{
                //Current car exist so select current car and vin
                editor.putString(Util.VIN_PREFERENCE,output.toString()).commit();
            }
        } else {
            //This car dosent exist in you cars
        }
        */
        editor.putString(Util.VIN_PREFERENCE,output.toString()).commit();
        vin = output.toString();
        Log.d("Final output vin :-", output.toString());
    }

    public void calculateVinHeaderOff(String response, String protocol) {

        String temp;
        String refactored = "";

        if(protocol.contains("A7")) {
            temp = response.replace("\r", "").replace("0:", "").replace("1:", "").replace("2:", "").replace(" ", "");
            refactored = temp.substring(9);
        }
        hexToAscii(refactored);
    }

    private ArrayList<String> countHeader(String response, String pattern) {
        try {
            Util.writeToFile(activity, "Pattern : " + pattern + " ---- Response : " + response);
            response = response.replace(" ", "");
            pattern = pattern.replace(" ", "");
            pattern = pattern.replace("\r", "");
            ArrayList<String> headerArray = new ArrayList<>();
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(response);
            int count = 0;
            while (m.find()) {
                count += 1;
            }

            headerArray.add(response.substring(0, response.indexOf(pattern) - 2));


            p = Pattern.compile(Pattern.quote("\r") + "(.*?)" + Pattern.quote(pattern));
            m = p.matcher(response);
            while (m.find()) {
                String header = m.group(1);
                System.out.println(header);
                headerArray.add(header.substring(0, header.length() - 2));
            }

            Util.writeToFile(activity, "Headers is " + headerArray.toString());
            Log.d(TAG, "Count is " + count);
            Log.d(TAG, "Headers is " + headerArray.toString());
            return headerArray;
        }catch (Exception e){
            Util.writeToFile(activity, "Exception is " + e.getMessage());
        }
        return new ArrayList<String>();
    }
}
