package com.android.kycar.carhealth.beans;

import android.location.Location;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by krishna on 5/12/15.
 */
public class Coordinates {

    private double latitude;
    private double longitude;
    private double accuracy;
    private float gpsBearing;
    private float gpsSpeed;

    public Coordinates() {
        super();
        latitude = -1;
        longitude = -1;
        accuracy = 0.0;
    }

    public Coordinates(String json) {

        try {
            JSONObject jsonObject = new JSONObject(json);
            this.latitude = jsonObject.getLong("latitude");
            this.longitude = jsonObject.getLong("longitude");
            this.accuracy = jsonObject.getLong("accuracy");
            this.gpsBearing = jsonObject.getLong("gpsBearing");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Coordinates(Location location) {

        if(location != null) {
            this.latitude = location.getLatitude();
            this.longitude = location.getLongitude();
            this.accuracy = location.getAccuracy();
            this.gpsBearing = location.getBearing();
            this.gpsSpeed = location.getSpeed();
        }
    }

    public Coordinates(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public JSONObject toJSON() throws JSONException {

        if(latitude == -1 || longitude == -1) {
            return null;
        }

        JSONObject coordinats = new JSONObject();
        coordinats.put("lat", latitude);
        coordinats.put("lng", longitude);
        coordinats.put("acc", accuracy);
        return coordinats;
    }

    public String toString(){
        return " http://maps.google.com/?q="+latitude+","+longitude+"";
    }

    public void setLocation(Location currLocation) {

        latitude = currLocation.getLatitude();
        longitude = currLocation.getLongitude();
        accuracy = currLocation.getAccuracy();
    }
}
