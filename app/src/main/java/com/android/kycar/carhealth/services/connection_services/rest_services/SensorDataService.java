package com.android.kycar.carhealth.services.connection_services.rest_services;

import org.json.JSONObject;

/**
 * Created by Krishna on 9/26/2016.
 */
public class SensorDataService extends RestService {

    public String sendRawSensorData(JSONObject data) {

        String rawSensorData = "/sensor_data/sensor_raw_data";
        return restServiceCall(rawSensorData, "POST", data);
    }
}
