package com.android.kycar.carhealth.async_task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.android.kycar.carhealth.entities.trip_transaction.TripSession;
import com.android.kycar.carhealth.repositories.trip.TripSessionRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.TripRestService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krishna on 9/12/2016.
 */

public class CreateTripSessionAsyncTask extends AsyncTask<JSONObject, Void, String> {

    private Context context;

    public CreateTripSessionAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(JSONObject... tripSessions) {

        try {
            return new TripRestService().createTripSession(tripSessions[0]);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {

        super.onPostExecute(result);
        Log.d(CreateTripSessionAsyncTask.class.getSimpleName(), "Result:- "+result);
        if(result!=null) {
            //Toast.makeText(this, "Trip created successfully", Toast.LENGTH_SHORT).show();
            try {
                JSONObject resObject = new JSONObject(result);
                TripSessionRepo tripSessionRepo = new TripSessionRepo(context, TripSession.class);
                TripSession tripSession = new TripSession((TripSession) tripSessionRepo.findById(resObject.getString("tripSessionId")));
                tripSession.setSync(true);
                tripSessionRepo.saveOrUpdate(tripSession);
                Log.d(CreateTripAsyncTask.class.getSimpleName(), "TripSession Updated");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

