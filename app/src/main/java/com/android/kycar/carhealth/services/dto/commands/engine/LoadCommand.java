package com.android.kycar.carhealth.services.dto.commands.engine;


import com.android.kycar.carhealth.services.dto.commands.PercentageObdCommand;
import com.android.kycar.carhealth.services.dto.enums.AvailableCommandNames;

/**
 * Calculated Engine Load value.
 */
public class LoadCommand extends PercentageObdCommand {

    public LoadCommand() {
        super("01 04");
    }

    /**
     * <p>Constructor for LoadCommand.</p>
     *
     * @param other a {@link LoadCommand} object.
     */
    public LoadCommand(LoadCommand other) {
        super(other);
    }

    /*
     * (non-Javadoc)
     *
     * @see pt.lighthouselabs.obd.commands.ObdCommand#getName()
     */
    @Override
    public String getName() {
        return AvailableCommandNames.ENGINE_LOAD.getValue();
    }

}
