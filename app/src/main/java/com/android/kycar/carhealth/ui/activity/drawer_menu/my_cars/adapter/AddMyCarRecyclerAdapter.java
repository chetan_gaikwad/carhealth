package com.android.kycar.carhealth.ui.activity.drawer_menu.my_cars.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.ProgessDialogBox;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.AppProps;
import com.android.kycar.carhealth.entities.Vehicle;
import com.android.kycar.carhealth.repositories.AppPropsRepo;
import com.android.kycar.carhealth.repositories.vehicle.VehicleRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.VehicleRestService;
import com.android.kycar.carhealth.ui.activity.DashboardActivity;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_cars.CarProfileActivity;

import java.util.List;

public class AddMyCarRecyclerAdapter extends RecyclerView.Adapter<AddMyCarRecyclerAdapter.ViewHolder> {

    private List<Vehicle> vehicles;
    private Activity activity;
    private boolean selectCar;
    private String convoyID;
    private ViewHolder viewHolder;
    private final int SELECT_PHOTO = 100;
    private VehicleRepo vehicleRepo;
    private SharedPreferences preferences;

    public AddMyCarRecyclerAdapter(Activity activity, List<Vehicle> cars, boolean fromSelectCar, String convoyID) {
        this.vehicles = cars;
        this.activity = activity;
        this.selectCar = fromSelectCar;
        this.convoyID = convoyID;
        vehicleRepo = new VehicleRepo(activity.getApplicationContext(), Vehicle.class);
        preferences = PreferenceManager.getDefaultSharedPreferences(activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        //inflate your layout and pass it to view holder
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.item_recycler_vehicle, viewGroup, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder,final int position) {

        Vehicle vehicle = vehicles.get(position);
        viewHolder.imgEditVehicle.setTag(position);
        viewHolder.butDeleteCar.setTag(position);
        viewHolder.butFindMyCar.setTag(position);
        viewHolder.rootLayout.setOnClickListener(onProfileClickListener(position, viewHolder));

        //setting data to view holder elements
        if(vehicle.getName()!=null) {
            viewHolder.tvCarNickName.setText(vehicle.getName());
        }else{
            viewHolder.tvCarNickName.setText(vehicle.getNickName());
        }
        viewHolder.tvCarRegistartionNumber.setText(vehicle.getRegNumber());
        /*if(vehicles.get(position).getVehicleImage() != null && vehicles.get(position).getVehicleImage() != "") {
            viewHolder.imgCarPhoto.setImageBitmap(BitmapFactory.decodeFile(vehicles.get(position).getVehicleImage()));
        }*/

        if(preferences.getString(Util.CAR_PREFERENCE,"").equals(vehicle.getId())){
            //viewHolder.rootLayout.setBackgroundColor(activity.getResources().getColor(R.color.md_amber_100));
        }
        /*viewHolder.cbSelect.setOnCheckedChangeListener(null);
        viewHolder.cbSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unSelectAll(position);
            }
        });*/

        /*if(selectCar) {
            //set on click listener for each element
            viewHolder.rootLayout.setOnClickListener(onClickListener(position));
        } else {
            viewHolder.rootLayout.setOnClickListener(onProfileClickListener(position, viewHolder));
        }*/

        AppPropsRepo appPropsRepo = new AppPropsRepo(viewHolder.butFindMyCar.getContext(), AppProps.class);
        AppProps appProps = appPropsRepo.findByVehicleIdUserId(vehicles.get(position).getId(), preferences.getString(Util.USER_PREFERENCE, ""));
        if(appProps == null) {
            viewHolder.butFindMyCar.setVisibility(View.GONE);
        }
    }

    private void setDataToView(TextView name, TextView model, int position) {
        if(vehicles.get(position).getName()!=null) {
            name.setText(vehicles.get(position).getName());
        }else{
            name.setText(vehicles.get(position).getNickName());
        }
        model.setText(vehicles.get(position).getRegNumber());
    }

    @Override
    public int getItemCount() {
        return (null != vehicles ? vehicles.size() : 0);
    }

    private View.OnClickListener onClickListener(final int position) {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(activity);
                dialog.setContentView(R.layout.item_recycler);
                dialog.setTitle("Selected car");
                dialog.setCancelable(true); // dismiss when touching outside Dialog

                // set the custom dialog components - texts and image
                TextView name = (TextView) dialog.findViewById(R.id.name);
                TextView model = (TextView) dialog.findViewById(R.id.model);
                Button btnJoinConvoy = (Button) dialog.findViewById(R.id.join_convoy);
                Button btnCancel = (Button) dialog.findViewById(R.id.cancel);

                btnJoinConvoy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                        Intent intent = new Intent(activity, DashboardActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(Util.IS_FROM_CONVOY, true);
                        intent.putExtra(Util.CONVOY_ID, convoyID);
                        activity.startActivity(intent);
                        activity.finish();
                    }
                });
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                setDataToView(name, model, position);
                dialog.show();
            }
        };
    }

    private View.OnClickListener onProfileClickListener(final int position , final ViewHolder viewHolder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                /** Select the car */

                new AlertDialog.Builder(v.getContext())
                        .setTitle("Confirm")
                        .setMessage("Do you want select this Car?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                               /** Selecting car */
                                preferences = PreferenceManager.getDefaultSharedPreferences(activity);
                                SharedPreferences.Editor editor = preferences.edit();
                                String vehicleId = vehicles.get(position).getId();
                                editor.putString(Util.CAR_PREFERENCE, vehicleId);
                                editor.commit();
                                activity.onBackPressed();
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
            }
        };
    }
    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgCarPhoto;
        private TextView tvCarNickName;
        private TextView tvCarRegistartionNumber;
        private Button butDeleteCar;
        private Button butFindMyCar;
        private ImageView imgEditVehicle;

        View rootLayout;
        View cardFace;
        View cardBack;
        CheckBox cbSelect;


        public ViewHolder(View view) {
            super(view);
            imgCarPhoto = (ImageView) view.findViewById(R.id.img_vehicle);
            imgCarPhoto.setOnClickListener(itemClicks);
            tvCarNickName = (TextView) view.findViewById(R.id.tv_vehicle_name);
            tvCarRegistartionNumber = (TextView) view.findViewById(R.id.tv_vehicle_number);
            cardFace = view.findViewById(R.id.face);
            rootLayout = view.findViewById(R.id.root_layout);
            cardBack = view.findViewById(R.id.back);

            imgEditVehicle = (ImageView) view.findViewById(R.id.imgEditVehicle);
            imgEditVehicle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String vehicleId = vehicles.get((Integer) v.getTag()).getId();
                    if(v.getTag() == null){return;}
                    activity.startActivity(new Intent(activity, CarProfileActivity.class).putExtra("vehicleId", vehicleId));
                }
            });

            butDeleteCar = (Button) view.findViewById(R.id.butDeleteVehicle);
            butDeleteCar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    new AlertDialog.Builder(v.getContext())
                            .setTitle("Confirm Delete")
                            .setMessage("Do you really want to delete this car?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {

                                    Log.d(AddMyCarRecyclerAdapter.class.getSimpleName(), "WhichButton:-"+whichButton);
                                    if(whichButton == -1){
                                        String vehicleId = vehicles.get((Integer) v.getTag()).getId();
                                        Log.d("delete", "delete tag : "+ v.getTag());
                                        if(v.getTag() == null){return;}
                                        vehicleRepo.deleteById(vehicleId);

                                        vehicles.remove((Integer) v.getTag());
                                        notifyDataSetChanged();

                                        String selectedCarId = preferences.getString(Util.CAR_PREFERENCE,"");
                                        if(selectedCarId.equals(vehicleId)){
                                            Log.d("jdss","Selected car deleted");
                                            preferences.edit().remove(Util.CAR_PREFERENCE).commit();
                                        }
                                        /** Rest call to delete vehicle */
                                            new DeleteVehicleAsyncTask().execute(vehicleId);
                                    }
                                }})
                            .setNegativeButton(android.R.string.no, null).show();
                }
            });

            butFindMyCar = (Button) view.findViewById(R.id.butFindVehicle);
            butFindMyCar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    AppPropsRepo appPropsRepo = new AppPropsRepo(v.getContext(), AppProps.class);

                    AppProps appProps = appPropsRepo.findByVehicleIdUserId(vehicles.get((Integer) v.getTag()).getId(), preferences.getString(Util.USER_PREFERENCE, ""));
                    if(appProps != null) {

                        activity.startActivity(new Intent(activity, DashboardActivity.class).
                                putExtra("lat", appProps.getLastLatitude()).
                                putExtra("long", appProps.getLastLongitude()).
                                putExtra("title", tvCarNickName.getText()));
                                //putExtra("tabNo", 1));

                    } else {
                        Toast.makeText(v.getContext(), "Last location not available", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    View.OnClickListener itemClicks = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.image:
                    /*Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image*//*");
                    activity.startActivityForResult(photoPickerIntent, SELECT_PHOTO);*/
                    break;
                default:break;
            }
        }
    };

    /**
     * Uploading asynchronous task
     */
    private class DeleteVehicleAsyncTask extends AsyncTask<String, Void, Void> {
        private ProgessDialogBox dialogBox;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*dialogBox = new ProgessDialogBox(activity);
            dialogBox.showDailog();*/
        }

        @Override
        protected Void doInBackground(String... cred) {

            Log.d(AddMyCarRecyclerAdapter.class.getSimpleName(), "Request for delete "+cred[0]);
            String response = new VehicleRestService().deleteVehicle(cred[0]);
            Log.d(AddMyCarRecyclerAdapter.class.getSimpleName(), "Response;-"+response);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            /*if(dialogBox.isShowing()){
                dialogBox.dismissDialog();
            }*/
        }
    }
}