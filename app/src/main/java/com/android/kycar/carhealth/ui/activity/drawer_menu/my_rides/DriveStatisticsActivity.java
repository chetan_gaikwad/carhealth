package com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.entities.trip_transaction.TripScore;
import com.android.kycar.carhealth.repositories.trip.TripScoreRepo;


public class DriveStatisticsActivity extends AppCompatActivity {

    public static FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private AnalyticsTabFragment tabFragment;
    public String tripId;
    private TripScoreRepo tripScoreRepo;
    private TripScore tripScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drive_statistics);
        mFragmentManager = getSupportFragmentManager();

        tabFragment = new AnalyticsTabFragment();
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView,tabFragment).commitAllowingStateLoss();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            tripId = extras.getString("tripId");
            tripScoreRepo = new TripScoreRepo(getApplicationContext(), TripScore.class);
            tripScore = new TripScore(tripScoreRepo.findAllByTripId(tripId));
        }
    }

    public TripScore getSensorData() {
        return tripScore;
    }
}
