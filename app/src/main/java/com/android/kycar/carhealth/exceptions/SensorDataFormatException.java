package com.android.kycar.carhealth.exceptions;

/**
 * Created by Krishna on 1/30/2016.
 */
public class SensorDataFormatException extends Exception {

    public SensorDataFormatException() {
        // TODO Auto-generated constructor stub
    }

    public SensorDataFormatException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public SensorDataFormatException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public SensorDataFormatException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }
}
