package com.android.kycar.carhealth.utils;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.android.kycar.carhealth.services.utils.DefaultDataToJson;


/**
 * Created by Krishna on 2/20/2017.
 */

public class DataToJsonBuilder extends AsyncTask<String, Void, String> {

    private Activity context;
    public DataToJsonBuilder(Activity context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... cred) {
        Log.d("Tasker", "Loading default data");
        new DefaultDataToJson().createJson(context);
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }
}

