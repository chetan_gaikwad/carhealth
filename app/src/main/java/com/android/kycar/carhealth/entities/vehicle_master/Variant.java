package com.android.kycar.carhealth.entities.vehicle_master;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by krishna on 6/12/15.
 */
@RealmClass
public class Variant extends RealmObject {

    @PrimaryKey
    private String id;

    private String name;

    private String modelId;

    private String fuelType;

    private String transmissionType;

    private int numOfGears;

    private String supportedObdProtocol;

    public Variant() {
    }

    public Variant(String id, String name, String modelId, String fuelType, String transmissionType, int numOfGears, String supportedObdProtocol) {
        this.id = id;
        this.name = name;
        this.modelId = modelId;
        this.fuelType = fuelType;
        this.transmissionType = transmissionType;
        this.numOfGears = numOfGears;
        this.supportedObdProtocol = supportedObdProtocol;
    }

    public int getNumOfGears() {
        return numOfGears;
    }

    public void setNumOfGears(int numOfGears) {
        this.numOfGears = numOfGears;
    }

    public String getSupportedObdProtocol() {
        return supportedObdProtocol;
    }

    public void setSupportedObdProtocol(String supportedObdProtocol) {
        this.supportedObdProtocol = supportedObdProtocol;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getTransmissionType() {
        return transmissionType;
    }

    public void setTransmissionType(String transmissionType) {
        this.transmissionType = transmissionType;
    }
}
