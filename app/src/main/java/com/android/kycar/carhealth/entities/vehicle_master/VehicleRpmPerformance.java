package com.android.kycar.carhealth.entities.vehicle_master;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 1/16/2016.
 */
@RealmClass
public class VehicleRpmPerformance extends RealmObject {

    @PrimaryKey
    private String id;

    private int gearNumber;

    private int blueRpmFrom;

    private int blueRpmTo;

    private int greenRpmFrom;

    private int greenRpmTo;

    private int redRpmFrom;

    private int redRpmEnd;

    private int maxRpm;

    private String vehicleId;

    private String fuelTypeId;

    public VehicleRpmPerformance() {
    }

    public VehicleRpmPerformance(String id, int gearNumber, int blueRpmFrom, int blueRpmTo, int greenRpmFrom, int greenRpmTo, int redRpmFrom, int redRpmEnd, int maxRpm, String vehicleId, String fuelTypeId) {
        this.id = id;
        this.gearNumber = gearNumber;
        this.blueRpmFrom = blueRpmFrom;
        this.blueRpmTo = blueRpmTo;
        this.greenRpmFrom = greenRpmFrom;
        this.greenRpmTo = greenRpmTo;
        this.redRpmFrom = redRpmFrom;
        this.redRpmEnd = redRpmEnd;
        this.maxRpm = maxRpm;
        this.vehicleId = vehicleId;
        this.fuelTypeId = fuelTypeId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(String fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public int getGearNumber() {
        return gearNumber;
    }

    public void setGearNumber(int gearNumber) {
        this.gearNumber = gearNumber;
    }

    public int getBlueRpmFrom() {
        return blueRpmFrom;
    }

    public void setBlueRpmFrom(int blueRpmFrom) {
        this.blueRpmFrom = blueRpmFrom;
    }

    public int getBlueRpmTo() {
        return blueRpmTo;
    }

    public void setBlueRpmTo(int blueRpmTo) {
        this.blueRpmTo = blueRpmTo;
    }

    public int getGreenRpmFrom() {
        return greenRpmFrom;
    }

    public void setGreenRpmFrom(int greenRpmFrom) {
        this.greenRpmFrom = greenRpmFrom;
    }

    public int getGreenRpmTo() {
        return greenRpmTo;
    }

    public void setGreenRpmTo(int greenRpmTo) {
        this.greenRpmTo = greenRpmTo;
    }

    public int getRedRpmFrom() {
        return redRpmFrom;
    }

    public void setRedRpmFrom(int redRpmFrom) {
        this.redRpmFrom = redRpmFrom;
    }

    public int getRedRpmEnd() {
        return redRpmEnd;
    }

    public void setRedRpmEnd(int redRpmEnd) {
        this.redRpmEnd = redRpmEnd;
    }

    public int getMaxRpm() {
        return maxRpm;
    }

    public void setMaxRpm(int maxRpm) {
        this.maxRpm = maxRpm;
    }
}

