package com.android.kycar.carhealth.services.reciever_services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.utils.BusProvider;


public class IncomingSms extends BroadcastReceiver {

    private Context mContext;
    // Get the object of SmsManager
    final SmsManager sms = SmsManager.getDefault();

    public void onReceive(Context context, Intent intent) {

        mContext = context;
        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

                    Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);


                   // Show Alert
                    int duration = Toast.LENGTH_LONG;
                    /*Toast toast = Toast.makeText(context,
                            "senderNum: " + senderNum + ", message: " + message, duration);
                    toast.show();*/
                    if(message.contains("nextgizmo locations")){
                        String sms = "your lat is 18.55825026 long is 73.80093366";
                        String[] data = sms.split(" ");
                        Log.i("Location","location "+data[3]+","+data[6]);
                        Log.i(Util.LOG,"Location detected from SMS");
                        launchMap(data[3],data[6]);
                    }


                    //Voice call to ask user
                    BusProvider.getInstance().post(new SmsContent(senderNum, message));

                } // end for loop
              } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);
        }
    }

    private void launchMap(String lat, String longi) {
        double latitude = Double.parseDouble(lat);
        double longitude = Double.parseDouble(longi);
        String label = "Location";
        String uriBegin = "geo:" + latitude + "," + longitude;
        String query = latitude + "," + longitude + "(" + label + ")";
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        mContext.startActivity(intent);
    }
}