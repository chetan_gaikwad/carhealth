package com.android.kycar.carhealth.customfonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.android.kycar.carhealth.utils.FontCache;


public class MyTextViewHeader extends TextView {

    public MyTextViewHeader(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextViewHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextViewHeader(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = FontCache.get("", getContext());
            setTypeface(tf);
        }
    }

}