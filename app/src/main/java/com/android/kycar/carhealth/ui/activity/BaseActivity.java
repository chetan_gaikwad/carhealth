package com.android.kycar.carhealth.ui.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.ProgessDialogBox;


public class BaseActivity extends AppCompatActivity {
    //protected String BASE_URL = "http://107.170.87.190:1500";//previous
    protected String BASE_URL = "http://139.59.253.137:1500";
    protected SharedPreferences preferences;
    protected SharedPreferences.Editor editor;
    public ProgessDialogBox progessDialogBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base);
        progessDialogBox = new ProgessDialogBox(this);
        preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        editor = preferences.edit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}