package com.android.kycar.carhealth.utils;

import android.content.Context;
import android.util.Log;

import com.android.kycar.carhealth.async_task.SensorEventDataAsyncTask;
import com.android.kycar.carhealth.services.sensor_services.SensorManagerService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;

/**
 * Created by Krishna on 9/20/2016.
 */
public class SensorEventCloudSync {

    private Context context;
    private File loggerAcc;
    private String fileName;
    private String folderPath;
    private File root;
    private File dir;

    public SensorEventCloudSync(Context context) {
        this.context = context;
        loggerAcc = new File("Logger.csv");
        root = android.os.Environment.getExternalStorageDirectory();
        folderPath = root.getAbsolutePath()+"/nxtauto/logger/rc/";

        //File dir = new File(root.getAbsolutePath() + folderPath + calendar.get(Calendar.YEAR) + calendar.get(Calendar.MONTH) + calendar.get(Calendar.DAY_OF_MONTH));
        //dir = new File(root.getAbsolutePath() + folderPath );
    }

    public File[] finder(String dirName){
        File dir = new File(dirName);

        return dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String filename)
            { return filename.endsWith(".txt"); }
        } );
    }

    public boolean syncData ( String vehicleId) {

        File[] directoryListing = finder(folderPath);
        if (directoryListing != null) {
            for (File file : directoryListing) {
                // Do something with child

                Log.d(SensorManagerService.class.getSimpleName(), "File name :- "+file.getName()+ " Size:- "+file.length());
                if(file.length() == 0) {
                    continue;
                }

                try {
                    syncFileWithCloud(file, vehicleId);
                    file.delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            // Handle the case where dir is not really a directory.
            // Checking dir.isDirectory() above would not be sufficient
            // to avoid race conditions with another process that deletes
            // directories.
        }
        return true;
    }

    private void syncFileWithCloud(File file, String vehicleId) {

        /** Reading content form file */
        BufferedReader br = null;
        try  {
            br = new BufferedReader(new FileReader(file));

            String line = "";
            StringBuilder fileContent = new StringBuilder();
            fileContent.append("[");

            while ((line = br.readLine()) != null) {

                Log.d(SensorEventCloudSync.class.getSimpleName(), line);
                fileContent.append(line);
            }

            fileContent.deleteCharAt(fileContent.length()-1);
            fileContent.append("]");
            Log.d(SensorEventCloudSync.class.getSimpleName(), ""+fileContent.length());

            /** Sending data to server */
            try {
                JSONObject data = new JSONObject();
                data.put("eventData", new JSONArray(fileContent.toString()));
                data.put("vehicleId", vehicleId);
                Log.d(SensorEventCloudSync.class.getSimpleName(), "Data:-"+data);
                new SensorEventDataAsyncTask().execute(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
