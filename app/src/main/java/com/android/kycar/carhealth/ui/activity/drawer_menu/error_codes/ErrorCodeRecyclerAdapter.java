package com.android.kycar.carhealth.ui.activity.drawer_menu.error_codes;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.entities.obd_master.DtcErrorCode;

import java.util.List;

public class ErrorCodeRecyclerAdapter extends RecyclerView.Adapter<ErrorCodeRecyclerAdapter.ViewHolder> {

    private List<DtcErrorCode> dtcErrorCodes;
    private Activity activity;
    private ViewHolder viewHolder;

    public ErrorCodeRecyclerAdapter(Activity activity, List<DtcErrorCode> dtcErrorCodes) {
        this.dtcErrorCodes = dtcErrorCodes;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        //inflate your layout and pass it to view holder
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.error_code_recycler_row, viewGroup, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder,final int position) {

        DtcErrorCode dtcErrorCode = dtcErrorCodes.get(position);
        //setting data to view holder elements
        if(dtcErrorCode.getDtc()!=null) {
            viewHolder.tvPid.setText(dtcErrorCode.getDtc());
        }
        if(dtcErrorCode.getDescription() != null) {
            viewHolder.tvErrorCodeDesc.setText(dtcErrorCode.getDescription());
        }
        viewHolder.btnInfo.setTag(position);
    }


    @Override
    public int getItemCount() {
        return (null != dtcErrorCodes ? dtcErrorCodes.size() : 0);
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvPid;
        private Button btnInfo;
        private TextView tvErrorCodeDesc;

        View rootLayout;

        public ViewHolder(View view) {
            super(view);
            tvPid = (TextView) view.findViewById(R.id.tvNickName);
            rootLayout = view.findViewById(R.id.face);
            btnInfo = (Button) view.findViewById(R.id.butEditCar);
            btnInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String errorCode = dtcErrorCodes.get((Integer) v.getTag()).getId();
                    if(v.getTag() == null){return;}

                }
            });
            tvErrorCodeDesc = (TextView) view.findViewById(R.id.tv_error_code_desc);
        }
    }
}