package com.android.kycar.carhealth.ui.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.speech.RecognizerIntent;

import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.kycar.carhealth.NxtApplication;
import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.async_task.CreateTripAsyncTask;
import com.android.kycar.carhealth.async_task.CreateTripScoreAsyncTask;
import com.android.kycar.carhealth.async_task.CreateTripSessionAsyncTask;
import com.android.kycar.carhealth.async_task.FetchTripsAsyncTask;
import com.android.kycar.carhealth.async_task.FindTripGpsDistanceAsyncTask;
import com.android.kycar.carhealth.beans.MapMarkerBean;
import com.android.kycar.carhealth.beans.ObdConnectionStatus;
import com.android.kycar.carhealth.config.ConfigActivity;
import com.android.kycar.carhealth.config.ObdConfig;
import com.android.kycar.carhealth.config.ProgessDialogBox;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.AppProps;
import com.android.kycar.carhealth.entities.UserContact;
import com.android.kycar.carhealth.entities.UserContactSetting;
import com.android.kycar.carhealth.entities.Vehicle;
import com.android.kycar.carhealth.entities.trip_transaction.Trip;
import com.android.kycar.carhealth.entities.trip_transaction.TripScore;
import com.android.kycar.carhealth.entities.trip_transaction.TripSession;
import com.android.kycar.carhealth.repositories.AppPropsRepo;
import com.android.kycar.carhealth.repositories.trip.LocationRepo;
import com.android.kycar.carhealth.repositories.trip.TripRepo;
import com.android.kycar.carhealth.repositories.trip.TripScoreRepo;
import com.android.kycar.carhealth.repositories.trip.TripSessionRepo;
import com.android.kycar.carhealth.repositories.user.UserContactRepo;
import com.android.kycar.carhealth.repositories.user.UserContactSettingRepo;
import com.android.kycar.carhealth.repositories.vehicle.VehicleRepo;
import com.android.kycar.carhealth.services.connection_services.NetworkUtil;
import com.android.kycar.carhealth.services.connection_services.mqtt_paho.PublishAccident;
import com.android.kycar.carhealth.services.connection_services.mqtt_paho_android_service.Connection;
import com.android.kycar.carhealth.services.dto.commands.ObdCommand;
import com.android.kycar.carhealth.services.dto.commands.car_type.CarType;
import com.android.kycar.carhealth.services.obd_services.AbstractGatewayService;
import com.android.kycar.carhealth.services.obd_services.ObdCommandJob;
import com.android.kycar.carhealth.services.obd_services.ObdGatewayService;
import com.android.kycar.carhealth.services.reciever_services.RemoteControlReceiver;
import com.android.kycar.carhealth.services.reciever_services.ScreenStateReciever;
import com.android.kycar.carhealth.services.reciever_services.SmsContent;
import com.android.kycar.carhealth.services.sensor_services.SensorManagerService;
import com.android.kycar.carhealth.services.sensor_services.TripDataLoggerService;
import com.android.kycar.carhealth.services.utils.LogCSVWriter;
import com.android.kycar.carhealth.services.utils.LoggerTxt;
import com.android.kycar.carhealth.task.NormalOrCanDecisionTask;
import com.android.kycar.carhealth.task.OnTaskCompleted;
import com.android.kycar.carhealth.ui.activity.drawer_menu.AboutUs;
import com.android.kycar.carhealth.ui.activity.drawer_menu.Help;
import com.android.kycar.carhealth.ui.activity.drawer_menu.car_performance.TrackModeActivity2;
import com.android.kycar.carhealth.ui.activity.drawer_menu.error_codes.ErrorCodeScanning;
import com.android.kycar.carhealth.ui.activity.drawer_menu.me.ProfileActivity;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_cars.CalculateGearRatioActivity;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_cars.MyCarsActivity;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_people.MyPeopleActivity;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides.TripListActivity;
import com.android.kycar.carhealth.ui.activity.drawer_menu.settings.SettingActivity;
import com.android.kycar.carhealth.ui.activity.drawer_menu.sign_in.ActivityLogin;
import com.android.kycar.carhealth.ui.fragments.TabFragment;
import com.android.kycar.carhealth.ui.manager.AddressProvider;
import com.android.kycar.carhealth.ui.manager.BluetoothManagerService;
import com.android.kycar.carhealth.utils.BusProvider;
import com.android.kycar.carhealth.utils.CarVinStates;
import com.android.kycar.carhealth.utils.LoadDefaultDataTasker;
import com.android.kycar.carhealth.utils.PreferenceUtils;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.otto.Subscribe;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class DashboardActivity extends BaseActivity implements OnTaskCompleted, NavigationView.OnNavigationItemSelectedListener {

    private AddressProvider addressProvider;
    private static final int REQ_CODE_SPEECH_INPUT = 1000;
    private LocationRepo locationRepo;
    private static final int PERMISSIONS_REQUEST_GPS = 1;
    private static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 2;
    private static final int READ_PHONE_STATE = 3;
    private Intent sensorService;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private View headerLayout;
    public static FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private TabFragment tabFragment;

    public static ObdCommandJob job = null;
    private double mLat, mLong;
    private static final String TAG = Util.LOG + DashboardActivity.class.getName();
    private static final int BLUETOOTH_DISABLED = 1;
    private static final int START_STOP_LIVE_DATA = 2;
    private static final int SETTINGS = 4;
    private static final int SENSOR_HARDWARE = 100;
    private static final int START_STOP_TRIP = 1000;
    private static final int STORE_LOCATION = 987786;
    private static final int CHECK_AUDIO_ALERTS = 89867;
    private static final int CONNECT_TO_MEDIA_TECH = 867;
    private static final int CONNECT_TO_MEDIA_TI = 8987;
    private static final int DICONNECT_TO_MEDIA_TECH = 9890;
    private static final int RESET = 1897;
    private static final int TEST_SUB = 9887;
    private static final int TEST_ERROR_CODE = 9760;
    private static final int TEST_TEXT_TO_SPEECH = 5478;
    private static final int LEAVE_CONVOY = 3000;
    private static final int NO_GPS_SUPPORT = 9;
    private boolean IS_CONVOY = false;
    public static Boolean isLiveData = false;
    private TripRepo tripRepo;
    MenuItem startStopMenu;
    private TripSessionRepo tripSessionRepo;
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private AudioManager mAudioManager;
    private StringBuilder tripID;
    private String convoyID;
    public Map<String, String> commandResult = new HashMap<>();
    boolean mGpsIsStarted = false;
    private LocationManager mLocService;
    private LocationProvider mLocProvider;
    private LogCSVWriter myCSVWriter;
    private PowerManager powerManager;
    private ScreenStateReciever mScreenStateReceiver;
    private boolean isServiceBound;
    private AbstractGatewayService service;
    private TripDataLoggerService tripLogger;
    //private SubscribeConvoy subscribeConvoy;
    private String imeiNumber;
    private String macAddress;
    private String deviceId;
    private String deviceType;
    private String parameters;
    private int currentKmFromObd;

    private SmsContent mSmsContent;

    private GestureDetector gestureDetector;

    //for draggable FAB button
    float dX;
    float dY;
    int lastAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //new DataToJsonBuilder(DashboardActivity.this).execute("");


        /** Fetching all trips */

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String message = bundle.getString("isLoggedInRecently");
            if (message != null && message.equals("yes")) {
                new FetchTripsAsyncTask(getApplicationContext()).execute(preferences.getString(Util.USER_PREFERENCE, ""));
            }
        }


        setContentView(R.layout.dashboard);

        addressProvider = new AddressProvider(this);
        gestureDetector = new GestureDetector(this, new SingleTapConfirm());
        initializeUI();
        new LoadApplicationContext().execute("");
        final IntentFilter dataIntentFilter = new IntentFilter();
        dataIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(newtorkBroadcast, dataIntentFilter);

        /** Starting session under trip */
        if (preferences.contains(Util.TRIP_ID)) {

            TripSession tripSession = new TripSession();
            tripSession.setId(new Date().getTime() + "");
            preferences.edit().putString(Util.TRIP_SESSION_ID, tripSession.getId()).commit();
            if (NxtApplication.currLocation != null) {
                tripSession.setStartLong(NxtApplication.currLocation.getLongitude());
                tripSession.setStartLat(NxtApplication.currLocation.getLatitude());
            }
            tripSession.setObdDistance(((NxtApplication) getApplication()).getDistance());
            tripSession.setStartTime(new Date().getTime());
            if (NxtApplication.currLocation != null) {
                tripSession.setStartLong(NxtApplication.currLocation.getLongitude());
                tripSession.setStartLat(NxtApplication.currLocation.getLatitude());
            }
            tripSessionRepo = new TripSessionRepo(getApplicationContext(), TripSession.class);
            tripSessionRepo.save(tripSession);
            tripSessionRepo.close();

            /** Resetting  values*/
            resetScoreValues();
        }

        /** Loading default data */
        if (!preferences.getBoolean(Util.IS_DEFAULT_DATA_LOADED, false)) {

            editor.putBoolean(Util.IS_DEFAULT_DATA_LOADED, true);
            editor.commit();
            new LoadDefaultDataTasker(DashboardActivity.this).execute("");
        }

        locationRepo = new LocationRepo(getApplicationContext(), com.android.kycar.carhealth.entities.trip_transaction.Location.class);
        if (!preferences.contains(PreferenceUtils.PREFERENCE_APP_UPDATE_CHECK_DATE)) {

            editor.putString(PreferenceUtils.PREFERENCE_APP_UPDATE_CHECK_DATE, Util.getTimeDate()).commit();
            Log.d("HERE", "HERE Date first time setted to : " + Util.getTimeDate());
        }
        createMqttConnection();
    }

    /**
     * Check for app update only once in day
     *
     * @return
     */
    private boolean isDayElapsed() {
        try {

            //Dates to compare
            String CurrentDate = preferences.getString(PreferenceUtils.PREFERENCE_APP_UPDATE_CHECK_DATE, "");
            String FinalDate = Util.getTimeDate();

            Log.d("HERE", "HERE date 1 " + CurrentDate);
            Log.d("HERE", "HERE date 2 " + FinalDate);

            Date date1;
            Date date2;

            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date2 = dates.parse(FinalDate);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);

            //Convert long to String
            String dayDifference = Long.toString(differenceDates);

            if (differenceDates >= 1) {
                return true;
            }
            Log.e("HERE", "HERE: " + dayDifference);

        } catch (Exception exception) {
            Log.e("DIDN'T WORK", "exception " + exception);
        }
        return false;
    }

    private void createMqttConnection() {
        Log.d("createMqttConnection", "createMqttConnection called");
        final MqttConnectOptions options = new MqttConnectOptions();
        final MqttAndroidClient client = new MqttAndroidClient(getApplicationContext(), "tcp://34.73.217.122:1883", MqttClient.generateClientId());
        options.setCleanSession(false);
        Connection.setClient(client);

        try {
            Connection.getClient().connect(options, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    //subscribeUserChannel();
                    Log.e("mqtt", "");

                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    //Toast.makeText(getApplicationContext(), "Connection failed", Toast.LENGTH_SHORT).show();
                    Log.e("mqtt", exception.getMessage());
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void initializeUI() {

        /** Disabled due to runtime permission marshmallow */
       /* TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        imeiNumber = "";//telephonyManager.getDeviceId();

        //BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        macAddress = "";//btAdapter.getAddress();

        deviceId = imeiNumber+"&"+macAddress;
        Log.d("MAC ADDRESS", " "+deviceId);*/

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mNavigationView.setItemIconTintList(null);
        headerLayout = mNavigationView.inflateHeaderView(R.layout.navigation_header);
        LinearLayout profilePictureLayout = (LinearLayout) headerLayout.findViewById(R.id.profile_header);

        profilePictureLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
            }
        });
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        mNavigationView.setNavigationItemSelectedListener(this);

        //manage navigation drawer header
        if (preferences.getBoolean(Util.IS_LOGGED_IN, false)) {
            Menu menu = mNavigationView.getMenu();
            MenuItem signUp = menu.findItem(R.id.nav_sign_in);
            signUp.setTitle("Logout");
            ((TextView) headerLayout.findViewById(R.id.header_username)).setText(preferences.getString(Util.USERNAME_PREFERENCE, ""));
            if (preferences.getString(Util.PROFILE_PREF, null) != null) {
                ImageView imageView = (ImageView) headerLayout.findViewById(R.id.img_profile_picture);
                imageView.setImageBitmap(BitmapFactory.decodeFile(preferences.getString(Util.PROFILE_PREF, null)));
            }
        } else {
            Menu menu = mNavigationView.getMenu();
            MenuItem signUp = menu.findItem(R.id.nav_sign_in);
            signUp.setTitle("Login");
            preferences.edit().remove(Util.PROFILE_PREF).apply();
            preferences.edit().putBoolean(Util.IS_LOGGED_IN, false).apply();
            preferences.edit().remove(Util.USER_PREFERENCE).apply();
            preferences.edit().commit();
        }

        tripID = new StringBuilder();
        toolbar.setTitle("OBD Dashboard");
        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        tripLogger = new TripDataLoggerService(getApplicationContext());
        mScreenStateReceiver = new ScreenStateReciever();
        registerScreenStateReceiver();
    }

    @Override
    public ComponentName startService(Intent service) {
        return super.startService(service);
    }

    private final Runnable mQueueCommands = new Runnable() {
        public void run() {
            if (service != null && service.isRunning() && service.queueEmpty()) {
                queueCommands();
                commandResult.clear();
            }
            // run again in period defined in preferences
            new Handler().postDelayed(mQueueCommands, 1000);
        }
    };
    private PowerManager.WakeLock wakeLock = null;
    private boolean preRequisites = true;
    private ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            isServiceBound = true;
            service = ((AbstractGatewayService.AbstractGatewayServiceBinder) binder).getService();
            service.setContext(DashboardActivity.this);

            Log.d(TAG, "Starting live data");
            try {
                service.startService();
                if (preRequisites) {
                }
            } catch (IOException ioe) {
                Log.e(TAG, "Failure Starting live data");
                doUnbindService();
            }
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        // This method is *only* called when the connection to the service is lost unexpectedly
        // and *not* when the client unbinds (http://developer.android.com/guide/components/bound-services.html)
        // So the isServiceBound attribute should also be set to false when we unbind from the service.
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(TAG, className.toString() + " service is unbound");
            isServiceBound = false;
        }
    };

    private boolean gpsInit() {
        mLocService = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (mLocService != null) {

            mLocProvider = mLocService.getProvider(LocationManager.GPS_PROVIDER);
            if (mLocProvider != null) {
                if (mLocService.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    //gpsStatus.setText(getString(R.string.status_gps_ready));
                    return true;
                }
            }
        }
        //gpsStatus.setText(getString(R.string.status_gps_no_support));
        showDialog(NO_GPS_SUPPORT);
        Log.e(TAG, "Unable to get GPS PROVIDER");
        // todo disable gps controls into Preferences
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        exportDatabase();
        unregisterScreenStateReceiver();
        //Disable drive mode
        editor.putBoolean(Util.IS_DRIVE_MODE, false).commit();
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        ComponentName mReceiverComponent = new ComponentName(this, RemoteControlReceiver.class);
        mAudioManager.unregisterMediaButtonEventReceiver(mReceiverComponent);

        stopService(sensorService);

        releaseWakeLockIfHeld();
        if (isServiceBound) {
            doUnbindService();
        }

        unregisterReceiver(newtorkBroadcast);

        /** Finalize session */
        if (preferences.contains(Util.TRIP_ID) && preferences.contains(Util.TRIP_SESSION_ID)) {

            endTripSession(preferences.getString(Util.TRIP_ID, ""));
            preferences.edit().remove(Util.TRIP_SESSION_ID).commit();
        }

        /** Saving last app-props */
        if (NxtApplication.currLocation != null && NxtApplication.currLocation.getLatitude() != 0 && NxtApplication.currLocation.getLongitude() != 0 && preferences.contains(Util.CAR_PREFERENCE)) {
            AppPropsRepo appPropsRepo = new AppPropsRepo(this, AppProps.class);
            AppProps appProps = new AppProps();
            appProps.setId(new Date().getTime() + "");
            appProps.setLastLatitude(NxtApplication.currLocation.getLatitude());
            appProps.setLastLongitude(NxtApplication.currLocation.getLongitude());
            Log.d(DashboardActivity.class.getSimpleName(), "Lat:- " + NxtApplication.currLocation.getLatitude() + ", Long:- " + NxtApplication.currLocation.getLongitude());
            appProps.setLastTimestamp(new Date().getTime());
            appProps.setUserId(preferences.getString(Util.USER_PREFERENCE, ""));
            if (preferences.contains(Util.CAR_PREFERENCE)) {
                appProps.setVehicleId(preferences.getString(Util.CAR_PREFERENCE, ""));
            }
            appPropsRepo.saveUnique(appProps);
        }

        /** Ending location sharing */
        if (!NxtApplication.shareLocationUserTopics.isEmpty()) {
            for (String topic : NxtApplication.shareLocationUserTopics) {
                try {
                    JSONObject msgObj = new JSONObject();
                    try {
                        msgObj.put("se", true);
                        msgObj.put("nm", preferences.getString(Util.USERNAME_PREFERENCE, ""));
                        msgObj.put("extra", "test");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Connection.getClient().publish(NxtApplication.baseTopic + "/" + topic, msgObj.toString().getBytes(), 0, true);
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
            Toast.makeText(getApplicationContext(), "Location sharing stopped", Toast.LENGTH_SHORT).show();
        }

        UserContactRepo userContactRepo = new UserContactRepo(getApplicationContext(), UserContact.class);
        List<UserContact> userContacts = userContactRepo.findAll();
        NxtApplication.shareLocationUserTopics.clear();
        if (!userContacts.isEmpty()) {
            for (UserContact userContact : userContacts) {
                UserContactSettingRepo userContactSettingRepo = new UserContactSettingRepo(getApplicationContext(), UserContactSetting.class);
                UserContactSetting userContactSetting = userContactSettingRepo.findByUserContactId(userContact.getId());
                if (userContactSetting != null) {

                    UserContactSetting userContactSetting1 = new UserContactSetting(userContactSetting);
                    userContactSetting1.setShareLocation(false);
                    userContactSettingRepo.saveOrUpdate(userContactSetting1);
                }
            }
        }
    }

    private void exportDatabase() {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//com.nxt.autoz//databases//{database name}";
                String backupDBPath = "{database name}";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseWakeLockIfHeld();
        try {
            BusProvider.getInstance().unregister(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * If lock is held, release. Lock will be held when the service is running.
     */
    private void releaseWakeLockIfHeld() {
        if (wakeLock.isHeld())
            wakeLock.release();
    }

    protected void onResume() {

        super.onResume();
        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
                "MyApp::MyWakelockTag");
        BusProvider.getInstance().register(this);
        // get Bluetooth device
        final BluetoothAdapter btAdapter = BluetoothAdapter
                .getDefaultAdapter();

        preRequisites = btAdapter != null && btAdapter.isEnabled();
        if (!preRequisites && preferences.getBoolean(ConfigActivity.ENABLE_BT_KEY, false)) {
            preRequisites = btAdapter.enable();
        }

        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                gpsInit();
            } else {
                getUserPermission(this);
            }
        } else {
            gpsInit();
        }
        if (!preRequisites) {
            showDialog(BLUETOOTH_DISABLED);
        }
    }

    private void getUserPermission(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission(
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_GPS);
        } else {
            gpsInit();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_GPS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    gpsInit();
                } else {
                    Util.showAlert("GPS permission was denied, You can allow it again from setting", DashboardActivity.this);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                verifyStoragePermissions(DashboardActivity.this);
                return;
            }

            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Util.showAlert("Storage permission was denied, You can allow it again from setting", DashboardActivity.this);
                }
                return;
            }

            case READ_PHONE_STATE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Util.showAlert("GPS permission was denied, You can allow it again from setting", DashboardActivity.this);
                }
                return;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, START_STOP_LIVE_DATA, 0, "Start OBD Data");
        //menu.add(0, SENSOR_HARDWARE, 0, "Sensor");
        if (preferences.contains(Util.TRIP_ID)) {
            menu.add(0, START_STOP_TRIP, 0, "Stop Trip");
        } else {
            menu.add(0, START_STOP_TRIP, 0, "Start Trip");
        }
        menu.add(0, TEST_ERROR_CODE, 0, "Test Error code");
        /*menu.add(0, RESET, 0, "Reset OBD Connection");
        menu.add(0, TEST_TEXT_TO_SPEECH, 0, "Sample alert for error");*/
        //menu.add(0, STORE_LOCATION, 0, "Store location");
        //menu.add(0, CHECK_AUDIO_ALERTS, 0 , "Check Audio Alerts");
        menu.add(0, CONNECT_TO_MEDIA_TECH, 0, "Connect Mediatek");
        menu.add(0, DICONNECT_TO_MEDIA_TECH, 0, "Disconnect Mediatek");
        menu.add(0, CONNECT_TO_MEDIA_TI, 0, "Connect TI");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case SENSOR_HARDWARE:
                //Start sensor
                startActivity(new Intent(DashboardActivity.this, SecurityController.class));
                break;
            case START_STOP_LIVE_DATA:
                startStopMenu = item;
                if (!isLiveData) {
                    if (isBluetoothEnabled()) {
                        /** Data fetching process */
                        final String remoteDevice = preferences.getString(ConfigActivity.BLUETOOTH_LIST_KEY, null);
                        if (remoteDevice == null || "".equals(remoteDevice)) {
                            askToSelectBluetooth("Select OBD device from setting?");
                        } else {

                            item.setTitle("Stop OBD Data");

                            /** Creating dialoge box to get trip name */
                            final AlertDialog.Builder alert = new AlertDialog.Builder(this);
                            alert.setTitle("OBD Prerequisite");
                            alert.setMessage("Please make sure OBD device is plugged-in and your car's ignition is ON");
                            alert.setPositiveButton("Yes let's go", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    item.setTitle("Stop OBD Data");
                                    new NormalOrCanDecisionTask(DashboardActivity.this, DashboardActivity.this, item).execute("");
                                }
                            });
                            alert.setNegativeButton("Later",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            dialog.cancel();
                                        }
                                    });
                            alert.show();
                        }

                    } else {
                        //ObdConfig.showDialog(DashboardActivity.this, "Bluetooth disabled", "Please turn on the bluetooth to use the service");
                        Snackbar.make(headerLayout, "Bluetooth is off", Snackbar.LENGTH_LONG)
                                .setAction("Turn on", onClickListener).show();

                    }
                    return true;
                } else {
                    stopLiveData();
                    item.setTitle("Start OBD Data");
                    isLiveData = false;
                }
                return true;
            case SETTINGS:
                return true;
            case START_STOP_TRIP:

                if (!preferences.getBoolean(Util.IS_LOGGED_IN, false)) {
                    Toast.makeText(this, "Please login to create trip", Toast.LENGTH_SHORT).show();
                    break;
                } else if (isServiceBound) {


                    if (preferences.contains(Util.TRIP_ID)) {
                        item.setTitle("Start Trip");
                        Toast.makeText(this, "Trip Stopped", Toast.LENGTH_SHORT).show();
                        endTrip();
                    } else {


                        /** Creating dialoge box to get trip name */
                        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
                        final EditText input = new EditText(this);
                        alert.setView(input);
                        alert.setTitle("Please enter trip name");
                        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String value = input.getText().toString().trim();
                                startTrip(value);
                                item.setTitle("Stop Trip");
                            }
                        });

                        alert.setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.cancel();
                                    }
                                });
                        alert.show();
                    }
                } else {
                    Util.showAlert("Please start obd to collect intial trip data", DashboardActivity.this);
                }
                return true;
            case LEAVE_CONVOY: {

                editor.remove(Util.CONVOY_ID);
                editor.remove(Util.CONVOY_NAME);
                editor.remove(Util.IS_CONVOY);
                editor.commit();
            }
            return true;
            case RESET: {
                Intent restartIntent = new Intent(this, DashboardActivity.class);
                restartIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //SensorManagerService.maxAccXReading = SensorManagerService.maxAccYReading = SensorManagerService.maxAccZReading = 25;
                startActivity(restartIntent);
            }
            return true;
            case TEST_SUB: {
                /*FirebaseMessaging fm = FirebaseMessaging.getInstance();
                fm.send(new RemoteMessage.Builder("890567297876" + "@gcm.googleapis.com")
                        //.setMessageId(Integer.toString(msgId.incrementAndGet()))
                        .setMessageId(Integer.toString(1))
                        .addData("my_message", "Hello World")
                        .addData("my_action","SAY_HELLO")
                        .build());*/

                Toast.makeText(this, "Message sent", Toast.LENGTH_SHORT).show();
            }
            return true;
            case TEST_ERROR_CODE: {
                Toast.makeText(getApplicationContext(), "Testing error code", Toast.LENGTH_SHORT).show();
                Log.d(DashboardActivity.class.getSimpleName(), "Error code clicked");
                startActivity(new Intent(this, ErrorCodeScanning.class));
            }
            return true;
            case TEST_TEXT_TO_SPEECH: {
                //tts.speak("Hello, this is sample error code voice notification", TextToSpeech.QUEUE_FLUSH, null);
            }
            return true;
            case STORE_LOCATION: {
                com.android.kycar.carhealth.entities.trip_transaction.Location location = new com.android.kycar.carhealth.entities.trip_transaction.Location();
                location.setLongitude(NxtApplication.currLocation.getLongitude());
                location.setLatitude(NxtApplication.currLocation.getLatitude());
                location.setBearing(NxtApplication.currLocation.getBearing());
                location.setId(new Date().toString());
                locationRepo.saveOrUpdate(location);
                Toast.makeText(getApplicationContext(), "Location Stored", Toast.LENGTH_SHORT).show();
            }
            return true;
            case CHECK_AUDIO_ALERTS: {
                ((NxtApplication) getApplication()).showDialog(this, "Demo", "This is demo message", R.drawable.alert_icon);
            }
        }
        return false;
    }

    private void askToSelectBluetooth(String message) {
        new BluetoothManagerService(this).askToSelectBluetooth(message);
    }

    OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            mBluetoothAdapter.enable();
        }
    };

    private void subscribeUserChannel() {
        if (Connection.getClient().isConnected() && preferences.getBoolean(Util.IS_LOGGED_IN, false)) {
            try {
                Connection.getClient().subscribe(NxtApplication.baseTopic + "/" + preferences.getString(Util.USER_TOPIC, ""), 0, getApplicationContext(), new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        Log.d(DashboardActivity.class.getSimpleName(), "Sub success");
                        //Toast.makeText(getApplicationContext(), "Sub Success", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                        //Toast.makeText(getApplicationContext(), "Sub Failed", Toast.LENGTH_SHORT).show();
                        Log.e("error", exception.toString());
                    }
                });
            } catch (MqttException e) {
                e.printStackTrace();
            }
            //Connection.getClient().registerResources(this);
            Connection.getClient().setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {

                }

                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    Log.i("sub:" + topic, message.toString());
                    //Toast.makeText(getApplicationContext(), "Message Arrived", Toast.LENGTH_SHORT).show();
                    JSONObject msgObj = new JSONObject(message.toString());
                    if (msgObj.has("se")) {
                        SensorManagerService.SUB_SLEEP_TIME = 20000;
                        MapMarkerBean mapMarkerBean = new MapMarkerBean();
                        mapMarkerBean.setSessionEnd(true);
                        mapMarkerBean.setSessionStart(false);
                        mapMarkerBean.setMarkerTile(msgObj.getString("nm"));
                        BusProvider.getInstance().post(mapMarkerBean);
                    } else if (msgObj.has("acc")) {

                        MapMarkerBean mapMarkerBean = new MapMarkerBean();
                        mapMarkerBean.setAccidentMarker(true);
                        mapMarkerBean.setMarkerTile(msgObj.getString("nm"));
                        double d = msgObj.getDouble("la");
                        mapMarkerBean.setLatitude(d);
                        d = msgObj.getDouble("lo");
                        mapMarkerBean.setLongitude(d);
                        BusProvider.getInstance().post(mapMarkerBean);

                    } else if (msgObj.has("ss")) {
                        MapMarkerBean mapMarkerBean = new MapMarkerBean();
                        double d = msgObj.getDouble("la");
                        mapMarkerBean.setLatitude(d);
                        d = msgObj.getDouble("lo");
                        mapMarkerBean.setLongitude(d);
                        mapMarkerBean.setMarkerTile(msgObj.getString("nm"));
                        mapMarkerBean.setSessionEnd(false);
                        mapMarkerBean.setSessionStart(true);
                        BusProvider.getInstance().post(mapMarkerBean);
                    } else {
                        MapMarkerBean mapMarkerBean = new MapMarkerBean();
                        double d = msgObj.getDouble("la");
                        mapMarkerBean.setLatitude(d);
                        d = msgObj.getDouble("lo");
                        mapMarkerBean.setLongitude(d);
                        mapMarkerBean.setMarkerTile(msgObj.getString("nm"));
                        mapMarkerBean.setSessionEnd(false);
                        mapMarkerBean.setSessionStart(false);
                        BusProvider.getInstance().post(mapMarkerBean);
                    }
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {

                }
            });
        } else {
            //Toast.makeText(getApplicationContext(), "Connection not created. Sub failed", Toast.LENGTH_SHORT).show();
            Log.i(DashboardActivity.class.getSimpleName(), " Connection not created. Sub failed ");
        }
    }

    @Subscribe
    public void getCarType(CarType carType) {
        if (carType.getCarType() == Util.isNormal) {
//            Toast.makeText(DashboardActivity.this,"Normal Car",Toast.LENGTH_SHORT).show();
        }
        startLiveData();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_myrides) {
            if (preferences.getBoolean(Util.IS_LOGGED_IN, false)) {
                startActivity(new Intent(this, TripListActivity.class));
            } else {
                Util.showAlert("Please login to see your trips", DashboardActivity.this);
            }
        } else if (id == R.id.nav_settings) {
            startActivity(new Intent(this, SettingActivity.class));
        } else if (id == R.id.nav_mycar) {
            if (preferences.getBoolean(Util.IS_LOGGED_IN, false)) {
                startActivity(new Intent(this, MyCarsActivity.class));
            } else {
                Util.showAlert("Please login to add a car", DashboardActivity.this);
            }
        } else if (id == R.id.nav_performance) {
            if (isServiceBound) {
                startActivity(new Intent(this, TrackModeActivity2.class));
            } else {
                Util.showAlert("OBD connection is not active, start OBD data", this);
            }
        } else if (id == R.id.nav_sign_in) {
            if (preferences.getBoolean(Util.IS_LOGGED_IN, false)) {
                /** Sign out */
                preferences.edit().remove(Util.IS_LOGGED_IN);
                editor.putBoolean(Util.IS_LOGGED_IN, false);
                editor.remove(Util.USER_PREFERENCE);
                editor.remove(Util.USERNAME_PREFERENCE);
                editor.commit();

                ((TextView) headerLayout.findViewById(R.id.header_username)).setText("User");
                ImageView imageView = (ImageView) headerLayout.findViewById(R.id.img_profile_picture);
                imageView.setImageResource(R.drawable.icon_person);
                Menu menu = mNavigationView.getMenu();
                MenuItem signUp = menu.findItem(R.id.nav_sign_in);
                signUp.setTitle("Login");

            } else {
                startActivity(new Intent(this, ActivityLogin.class));
            }
        } else if (id == R.id.nav_mypeople) {
            if (preferences.getBoolean(Util.IS_LOGGED_IN, false)) {
                startActivity(new Intent(this, MyPeopleActivity.class));
            } else {
                Util.showAlert("Please login in order to add your people", DashboardActivity.this);
            }
        } else if (id == R.id.nav_help) {
            startActivity(new Intent(this, Help.class));
        } else if (id == R.id.nav_about_us) {
            startActivity(new Intent(this, AboutUs.class));
        } else if (id == R.id.nav_safety_feed) {
            //startActivity(new Intent(this, UnfoldableDetailsActivity.class));
        } else if (id == R.id.nav_gear_ration) {
            startActivity(new Intent(this, CalculateGearRatioActivity.class));
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void getTroubleCodes() {
        startActivity(new Intent(this, TroubleCodesActivity.class));
    }

    private void startLiveData() {

        ((NxtApplication) getApplication()).startLoggingObdData();
        Log.d(DashboardActivity.class.getSimpleName(), "Starting live data..");
        doBindService();
        // start all command execution. Query all pid to engine and get response
        new Handler().post(mQueueCommands);

        if (preferences.getBoolean(ConfigActivity.ENABLE_GPS_KEY, false))
            gpsStart();
        else
            wakeLock.acquire();

        if (preferences.getBoolean(ConfigActivity.ENABLE_FULL_LOGGING_KEY, false)) {

            long mils = System.currentTimeMillis();
            SimpleDateFormat sdf = new SimpleDateFormat("_dd_MM_yyyy_HH_mm_ss");

            myCSVWriter = new LogCSVWriter("Log" + sdf.format(new Date(mils)).toString() + ".csv",
                    preferences.getString(ConfigActivity.DIRECTORY_FULL_LOGGING_KEY,
                            getString(R.string.default_dirname_full_logging))
            );
        }
    }

    private void stopLiveData() {
        ((NxtApplication) getApplication()).stopLoggingObdData();
        doUnbindService();
        Log.d(DashboardActivity.class.getSimpleName(), "Stopping live data...");

        releaseWakeLockIfHeld();

        if (myCSVWriter != null) {
            myCSVWriter.closeLogCSVWriter();
        }

        /*if(preferences.contains(Util.TRIP_SESSION_ID)) {
            TripSession tripSession = new TripSession();
            tripSession.setId(preferences.getString(Util.TRIP_SESSION_ID, ""));
            tripSession.setTripId(preferences.getString(Util.TRIP_ID, ""));
            tripSession.setEndTime(new Date().getTime());
            tripSession.setStopLat(updatedLocation.getLatitude());
            tripSession.setStopLong(updatedLocation.getLongitude());
            new CreateTripSessionAsyncTask().execute(tripSession);
            preferences.edit().remove(Util.TRIP_SESSION_ID).commit();
        }*/
    }

    /**
     * Gather all trip info and save in database
     */

    private void startTrip(String tripName) {

        /** Creating file to store offline trip coords */
        LoggerTxt tripTxt = new LoggerTxt("trip", tripName);

        if (NxtApplication.currLocation != null) {
            JSONObject startLoc = new JSONObject();
            try {
                startLoc.put("la", NxtApplication.currLocation.getLatitude());
                startLoc.put("lo", NxtApplication.currLocation.getLongitude());
            } catch (Exception e) {
                e.printStackTrace();
            }
            tripTxt.log(startLoc.toString());
        }

        tripTxt.close();

        tripID = new StringBuilder();
        tripID.append(preferences.getString(Util.USER_PREFERENCE, "USER_ID") + Util.getTimeLong());
        preferences.edit().putString(Util.TRIP_ID, tripID.toString()).commit();

        Trip trip = new Trip();
        trip.setTripName(tripName);
        trip.setId(tripID.toString());
        trip.setStartTime(new Date().getTime());
        if (preferences.contains(Util.CAR_PREFERENCE)) {
            trip.setVehicleId(preferences.getString(Util.CAR_PREFERENCE, "VEHICLE_ID"));
        }
        trip.setUserId(preferences.getString(Util.USER_PREFERENCE, "USER_ID"));
        trip.setStatus(1);
        trip.setConvoy(false);
        //trip.setObdDistance(((NxtApplication)getApplication()).getDistance());
        trip.setObdDistance(preferences.getInt(Util.DISTANCE_COVERED, 0));
        Log.d("dsds", "Trip start km : " + preferences.getInt(Util.DISTANCE_COVERED, 0));
        trip.setStartLocation(addressProvider.getCurrentAddress());

        if (NxtApplication.currLocation != null) {
            trip.setStartLong(NxtApplication.currLocation.getLongitude());
            trip.setStartLat(NxtApplication.currLocation.getLatitude());
        }
        tripRepo = new TripRepo(DashboardActivity.this, Trip.class);
        tripRepo.save(trip);
        tripRepo.close();

        /** Creating trip session */
        tripSessionRepo = new TripSessionRepo(getApplicationContext(), TripSession.class);
        TripSession tripSession = new TripSession();
        tripSession.setTripId(preferences.getString(Util.TRIP_ID, null));
        tripSession.setId(new Date().getTime() + "");
        tripSessionRepo.save(tripSession);
        preferences.edit().putString(Util.TRIP_SESSION_ID, tripSession.getId()).commit();

        resetScoreValues();

        /** Testing for OBD service */
        if (service != null && service.isRunning()) {
            tripLogger.logStartTrip(mLat, mLong, currentKmFromObd);
        } else {
            //  Toast.makeText(this, " Car is not connected", Toast.LENGTH_SHORT).show();
        }
        tripSessionRepo.close();
    }

    private void endTrip() {

        /** Finalizing trip */
        tripRepo = new TripRepo(this, Trip.class);
        Trip trip = new Trip((Trip) tripRepo.findById(preferences.getString(Util.TRIP_ID, null)));

        if (trip != null) {

            endTripSession(preferences.getString(Util.TRIP_ID, ""));

            trip.setTripName(trip.getTripName());
            if (trip.getStartLocation() != null && trip.getStartLocation() != "") {
                trip.setStartLocation(trip.getStartLocation());
            }
            trip.setStopLocation(addressProvider.getCurrentAddress());
            trip.setStatus(3);
            trip.setEndTime(new Date().getTime());
            if (NxtApplication.currLocation != null) {
                trip.setStopLong(NxtApplication.currLocation.getLongitude());
                trip.setStopLat(NxtApplication.currLocation.getLatitude());
            }
            trip.setStopLocation(addressProvider.getCurrentAddress());
            Log.d("Ending distance :- ", "" + ((NxtApplication) getApplication()).getDistance());
            //int dis = ((NxtApplication)getApplication()).getDistance() - trip.getObdDistance();
            int dis = preferences.getInt(Util.DISTANCE_COVERED, trip.getObdDistance() + 1) - trip.getObdDistance();
            Log.d("dsds", "Trip start km : " + preferences.getInt(Util.DISTANCE_COVERED, 0));
            Log.d("dsds", "Dis covered : " + dis);
            trip.setObdDistance((dis > 0) ? dis : 1);

            if (trip.getObdDistance() == 1 && trip.getStartLat() != 0 && trip.getStartLong() != 0 &&
                    trip.getStartLat() != 0 && trip.getStopLong() != 0) {
                Location src = new Location("");
                src.setLatitude(trip.getStartLat());
                src.setLongitude(trip.getStartLong());

                Location desc = new Location("");
                desc.setLatitude(trip.getStopLat());
                desc.setLongitude(trip.getStopLong());

                trip.setObdDistance((int) Util.findDistance(src, desc));
            }

            tripRepo.saveOrUpdate(trip);

            JSONObject tripJson = new JSONObject();
            try {
                tripJson.put("tripName", trip.getTripName());
                tripJson.put("startTime", trip.getStartTime());
                tripJson.put("endTime", trip.getEndTime());
                tripJson.put("vehicleId", trip.getVehicleId());
                tripJson.put("userId", trip.getUserId());
                tripJson.put("status", trip.getStatus());
                tripJson.put("startLocation", trip.getStartLocation());
                tripJson.put("stopLocation", trip.getStopLocation());
                tripJson.put("distance", trip.getObdDistance());
                tripJson.put("engineRpmMax", trip.getEngineRpmMax());
                tripJson.put("maxSpeed", trip.getSpeed());
                tripJson.put("tripId", trip.getId());

                /** need to improve */

                JSONObject startCoords = new JSONObject();
                startCoords.put("latitude", trip.getStartLat());
                startCoords.put("longitude", trip.getStartLong());
                tripJson.put("startCoords", startCoords);

                JSONObject stopCoords = new JSONObject();
                stopCoords.put("latitude", trip.getStartLat());
                stopCoords.put("longitude", trip.getStartLong());
                tripJson.put("stopCoords", stopCoords);

                /* tripJson.put("startLat", trip.getStartLat());
                tripJson.put("startLong", trip.getStartLong());
                tripJson.put("stopLat", trip.getStopLat());
                tripJson.put("stopLong", trip.getStopLong()); */

            } catch (Exception e) {
                e.printStackTrace();
            }

            new CreateTripAsyncTask(getApplicationContext()).execute(tripJson);

            new FindTripGpsDistanceAsyncTask(getApplicationContext()).execute(tripJson);
        }

        preferences.edit().remove(Util.TRIP_ID).commit();
        preferences.edit().remove(Util.TRIP_SESSION_ID).commit();
        resetScoreValues();
    }

    private void queueCommands() {
        if (isServiceBound) {
            for (ObdCommand Command : ObdConfig.getCommands()) {
                if (preferences.getBoolean(Command.getName(), true)) {
                    service.queueJob(new ObdCommandJob(Command));
                } else {
                    Log.i(DashboardActivity.class.getSimpleName(), "not support " + Command.getName());
                }
            }
        }
    }

    private void doBindService() {

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        if (!isServiceBound) {

            Intent serviceIntent = new Intent(this, ObdGatewayService.class);
            bindService(serviceIntent, serviceConn, BIND_AUTO_CREATE);
        }
    }

    private void doUnbindService() {

        DashboardActivity.job = null;
        if (isServiceBound) {
            if (service.isRunning()) {
                service.stopService();
                if (preRequisites) {
                }
                // btStatusTextView.setText(getString(R.string.status_bluetooth_ok));
            }
            Log.d(TAG, "Unbinding OBD service..");
            unbindService(serviceConn);
            isServiceBound = false;
            //obdStatus.setText(getString(R.string.status_obd_disconnected));
        }
        gpsStop();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private synchronized void gpsStart() {
        //if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
        //      || checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
        if (!mGpsIsStarted && mLocProvider != null && mLocService != null && mLocService.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ContextCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ContextCompat#requestPermissions for more details.
                return;
            }
            mLocService.requestLocationUpdates(mLocProvider.getName(), ConfigActivity.getGpsUpdatePeriod(preferences), ConfigActivity.getGpsDistanceUpdatePeriod(preferences), (LocationListener) this);
            mGpsIsStarted = true;
        } else if (mGpsIsStarted && mLocProvider != null && mLocService != null) {
        } else {
            //gpsStatus.setText(getString(R.string.status_gps_no_support));
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private synchronized void gpsStop() {
        if (mGpsIsStarted) {
            if (mLocService != null) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        || checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                }
            }
            mGpsIsStarted = false;
            //gpsStatus.setText(getString(R.string.status_gps_stopped));
        }
    }

    private boolean isBluetoothEnabled() {
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        return btAdapter.isEnabled();
    }

    @Override
    public void
    onTaskCompleted(final Integer carType, final CarVinStates carVinStates, final String vin) {
        final VehicleRepo vehicleRepo = new VehicleRepo(DashboardActivity.this, Vehicle.class);
        final Vehicle vehicle = (Vehicle) vehicleRepo.findById(preferences.getString(Util.CAR_PREFERENCE, ""));

        switch (carVinStates) {
            case CURRENTLY_SELECTED_CAR_MATCHES_THE_VIN:
                startConnectioWithObd(carType);
                break;
            case NO_CAR_SELECTED:
                if (preferences.getBoolean(Util.IS_LOGGED_IN, false)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showAlertWithOption(DashboardActivity.this,
                                    "No car selected",
                                    "Car selection",
                                    "Add/Select now",
                                    "No! Continue",
                                    carVinStates,
                                    vin,
                                    carType
                            );
                        }
                    });

                } else {
                    //Not logged in
                    startConnectioWithObd(carType);
                }
                break;
            case VIN_FOUND_AND_PRESENT_IN_DATABASE_BUT_WRONG_CAR_SELECTED:

                final Vehicle vehicle1 = vehicleRepo.findByVin(vin);
                editor.putString(Util.CAR_PREFERENCE, vehicle1.getId()).commit();
                final String carName = vehicle1.getNickName();
                DashboardActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(DashboardActivity.this, carName + " selected", Toast.LENGTH_LONG).show();
                    }
                });

                startConnectioWithObd(carType);
                break;
            case VIN_FOUND_BUT_NO_CAR_WITH_THE_MATCHING_VIN_FOUND_IN_DATABASE:
                // final String carNickName = vehicle.getNickName();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showAlertWithOption(DashboardActivity.this,
                                "Is this car currently selected car",
                                "Car selection",
                                "Yes",
                                "No",
                                carVinStates,
                                vin, carType);
                    }
                });

                startConnectioWithObd(carType);

                break;
            case VIN_NOT_FOUND:
                startConnectioWithObd(carType);
                break;

            default:
                break;
        }
    }

    public void startConnectioWithObd(Integer carType) {

        editor.putInt(Util.CAR_TYPE, carType).commit();
        DashboardActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                progessDialogBox.showDailog("Connecting with car and Checking for supported Pids");
                startLiveData();
            }
        });
    }

    @Subscribe
    public void changeMessage(final ObdConnectionStatus obdConnectionStatus) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (obdConnectionStatus.getConnectionMessage().equals("Connected")) {
                    progessDialogBox.setMessage(obdConnectionStatus.getConnectionMessage());
                    progessDialogBox.dismissDialog();
                } else if (obdConnectionStatus.getConnectionMessage().equals("Done")) {
                    progessDialogBox.dismissDialog();
                } else {
                    progessDialogBox.setMessage(obdConnectionStatus.getConnectionMessage());
                }
            }
        });


    }

    /**
     * This function will be called if audio next and previous button is clicked
     *
     * @param control
     */
    public void onAudInput(String control, int i) {
        if (control.equals("Next")) {
            tabFragment.switchPage(1);
        } else if (control.equals("Nexth")) {
            tabFragment.switchPageNext(i);
        } else {
            tabFragment.switchPage(0);
        }
    }

    /**
     * Checks if the app has permission to write to device storage
     * <p>
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    /**
     * Checking for the device type (Mobile/Controller)
     */
    private void checkDeviceType() {
        deviceType = "MOBILE";
    }

    /**
     * Checking for the paramters supported by the car
     */
    private void checkParameters() {
        parameters = "0101001010101001001101001010010101001010101001001010100111001010101";
    }


    public class LoadApplicationContext extends AsyncTask<String, Void, String> {

        ProgessDialogBox progessDialogBox;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progessDialogBox = new ProgessDialogBox(DashboardActivity.this);
            //progessDialogBox.showDailog();
        }

        @Override
        protected String doInBackground(String... params) {
            // getUserPermission(DashboardActivity.this);
            checkDeviceType();
            checkParameters();

            mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            ComponentName mReceiverComponent = new ComponentName(DashboardActivity.this, RemoteControlReceiver.class);
            mAudioManager.registerMediaButtonEventReceiver(mReceiverComponent);
            sensorService = new Intent(DashboardActivity.this, SensorManagerService.class);
            startService(sensorService);
            mFragmentManager = getSupportFragmentManager();
            /*if(getIntent().getBooleanExtra(Util.IS_FROM_CONVOY,false)){
                IS_CONVOY = true;
                convoyID = getIntent().getStringExtra(Util.CONVOY_ID);
            }*/
            tabFragment = new TabFragment();
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView, tabFragment).commitAllowingStateLoss();
            //checkConvoy();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Bundle extras = getIntent().getExtras();

                    Log.d(DashboardActivity.class.getSimpleName(), "Got location");
                    if (extras != null) {
                        String title = extras.getString("title");
                        double lat = extras.getDouble("lat");
                        double lon = extras.getDouble("long");

                        if (title != null) {
                            defaultLocation = new Location("");
                            defaultLocation.setLatitude(lat);
                            defaultLocation.setLongitude(lon);
                            defaultLocation.setProvider(title);
                        } else {
                            defaultLocation = null;
                        }
                        tabFragment.switchPage(extras.containsKey("tabNo") ? extras.getInt("tabNo") : 0);

                        Log.d("Notification", " Extra" + extras);
                        if (extras.containsKey("acc")) {
                            PublishAccident publishAccident = new PublishAccident();
                            Toast.makeText(getApplicationContext(), "Got ", Toast.LENGTH_SHORT).show();
                            MqttMessage message = publishAccident.getAccidentMessage(extras.getString("userId"), extras.getDouble("lat"), extras.getDouble("lng"), extras.getDouble("acc"));
                            try {
                                Connection.getClient().publish(NxtApplication.baseTopic + "/" + preferences.getString(Util.USER_TOPIC, ""), message);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        Log.d(DashboardActivity.class.getSimpleName(), "Extra is null");
                    }
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progessDialogBox.isShowing()) {
                progessDialogBox.dismissDialog();
            }
        }
    }

    public void checkConvoy() {
        /** Getting data from sms link */
        Uri data = getIntent().getData();
        if (data != null) {
            String strData = data.toString();
            strData = strData.replace("http://convoyinvitation.com/nxtauto/", "");
            String[] strs = strData.split("/");
            if (strs.length > 1) {
                //Util.showAlert("You got the convoy invitation of "+ strs[0], DashboardActivity.this);
                editor.putBoolean(Util.IS_CONVOY, true);
                editor.putString(Util.CONVOY_NAME, strs[0]);
                editor.commit();
            }
        } else if (preferences.getBoolean(Util.IS_CONVOY, false)) {
            /*subscribeConvoy = new SubscribeConvoy();
            subscribeConvoy.mqttConnect();
            subscribeConvoy.subscribeConvoy(preferences.getString(Util.CONVOY_ID, ""));*/
        }
    }

    BroadcastReceiver newtorkBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int status = NetworkUtil.getConnectivityStatusString(context);

            if (status == 0) {
                Toast.makeText(context, "Connection loss", Toast.LENGTH_LONG).show();
            } else if (status == 1 || status == 2) {
                //createMqttConnection();
            }
        }
    };

    private void resetScoreValues() {

        SensorManagerService.totalRightIntensity = SensorManagerService.totalBrakingIntensity = SensorManagerService.totalLeftIntensity = SensorManagerService.totalAccIntensity = 0;
        SensorManagerService.harshLeftCount = SensorManagerService.harshRightCount = SensorManagerService.harshBrakeCount = SensorManagerService.harshAccCount = 0;
        SensorManagerService.mildBrakeCount = SensorManagerService.mildAccCount = SensorManagerService.mildLeftCount = SensorManagerService.mildRightCount = 0;

        NxtApplication.coolantVeryHot = NxtApplication.coolantHot = NxtApplication.coolantOptimum = NxtApplication.coolantCool = 0;
        NxtApplication.oilVeryHot = NxtApplication.oilHot = NxtApplication.oilOptimum = NxtApplication.coolantCool = 0;
        NxtApplication.rpmAwesome = NxtApplication.rpmHarsh = NxtApplication.rpmHigh = NxtApplication.rpmOk = 0;
        NxtApplication.speedHarsh = NxtApplication.speedAwesome = NxtApplication.rpmHigh = NxtApplication.rpmOk = 0;
    }

    private Location defaultLocation;

    public Location showLocation() {

        return defaultLocation;
    }

    private JSONObject getTripScoreJson(TripScore tripScore) {

        JSONObject tripScoreJson = new JSONObject();
        try {
            tripScoreJson.put("tripScoreId", tripScore.getId());
            tripScoreJson.put("tripId", tripScore.getTripId());
            tripScoreJson.put("tripSessionId", tripScore.getTripSessionId());
            tripScoreJson.put("pedalAccIntensity", tripScore.getPedalAccIntensity());
            tripScoreJson.put("pedalAccFreq", tripScore.getPedalAccFreq());
            tripScoreJson.put("pedalBreakingIntensity", tripScore.getPedalBreakingIntensity());
            tripScoreJson.put("pedalBreakingFreq", tripScore.getPedalBreakingFreq());
            tripScoreJson.put("steeringIntensity", tripScore.getPedalBreakingIntensity());
            tripScoreJson.put("steeringFreq", tripScore.getSteeringFreq());
            tripScoreJson.put("speedHarsh", tripScore.getSpeedHarsh());
            tripScoreJson.put("speedHigh", tripScore.getSpeedHigh());
            tripScoreJson.put("speedOk", tripScore.getSpeedOk());
            tripScoreJson.put("speedAwesome", tripScore.getSpeedAwesome());
            tripScoreJson.put("rpmHarsh", tripScore.getRpmHarsh());
            tripScoreJson.put("rpmHigh", tripScore.getRpmHigh());
            tripScoreJson.put("rpmOk", tripScore.getRpmOk());
            tripScoreJson.put("rpmAwesome", tripScore.getRpmAwesome());
            tripScoreJson.put("oilVeryHot", tripScore.getOilVeryHot());
            tripScoreJson.put("oilHot", tripScore.getOilHot());
            tripScoreJson.put("oilOptimum", tripScore.getOilOptimum());
            tripScoreJson.put("oilCold", tripScore.getOilCold());
            tripScoreJson.put("coolantVeryHot", tripScore.getCoolantHot());
            tripScoreJson.put("coolantHot", tripScore.getCoolantHot());
            tripScoreJson.put("coolantOptimum", tripScore.getCoolantOptimum());
            tripScoreJson.put("coolantCold", tripScore.getCoolantCold());
            tripScoreJson.put("isSync", tripScore.isSync());

        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(DashboardActivity.class.getSimpleName(), "TripScore Json:-" + tripScoreJson);
        return tripScoreJson;
    }

    private JSONObject getTripSessionJson(TripSession tripSession) {
        JSONObject tripSessionJson = new JSONObject();
        try {
            tripSessionJson.put("tripSessionId", tripSession.getId());
            tripSessionJson.put("endTime", tripSession.getEndTime());
            tripSessionJson.put("stopLat", tripSession.getStopLat());
            tripSessionJson.put("stopLong", tripSession.getStopLong());
            tripSessionJson.put("distance", tripSession.getObdDistance());
            tripSessionJson.put("engineRpmMax", tripSession.getEngineRpmMax());
            tripSessionJson.put("maxSpeed", tripSession.getSpeed());
            tripSessionJson.put("tripSessionId", tripSession.getId());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tripSessionJson;
    }

    private boolean endTripSession(String tripId) {

        /** Finalizing trip session */
        tripSessionRepo = new TripSessionRepo(getApplicationContext(), TripSession.class);
        TripSession tripSession = new TripSession((TripSession) tripSessionRepo.findById(preferences.getString(Util.TRIP_SESSION_ID, null)));
        tripSession.setEndTime(new Date().getTime());
        tripSession.setTripId(tripId);

        if (NxtApplication.currLocation != null) {
            tripSession.setStopLat(NxtApplication.currLocation.getLatitude());
            tripSession.setStopLong(NxtApplication.currLocation.getLongitude());
        }

        tripSession.setSync(false);

        /** Distance have to get from OBD/GPS */
        Log.d("Ending se distance :- ", "" + ((NxtApplication) getApplication()).getDistance());
        int sessionDis = ((NxtApplication) getApplication()).getDistance() - tripSession.getObdDistance();
        tripSession.setObdDistance((sessionDis > 0) ? sessionDis : 1);

        tripSessionRepo.saveOrUpdate(tripSession);

        /** finalize trip score */
        /** Driver Score calculation */
        float score = (float) (SensorManagerService.harshAccCount + SensorManagerService.harshBrakeCount + SensorManagerService.harshLeftCount + SensorManagerService.harshRightCount) / tripSession.getObdDistance();
        TripScore tripScore = new TripScore();
        tripScore.setTripId(tripId);
        tripScore.setTripSessionId(tripSession.getId());
        tripScore.setId(new Date().getTime() + "");
        tripScore.setPedalAccFreq((double) SensorManagerService.harshAccCount / tripSession.getObdDistance());
        tripScore.setPedalBreakingFreq((double) SensorManagerService.harshBrakeCount / tripSession.getObdDistance());
        tripScore.setPedalAccIntensity(SensorManagerService.totalAccIntensity / tripSession.getObdDistance());
        tripScore.setPedalBreakingIntensity(SensorManagerService.totalBrakingIntensity / tripSession.getObdDistance());

        try {
            double steeringIntensityIndex = (SensorManagerService.totalLeftIntensity + SensorManagerService.totalRightIntensity) / tripSession.getObdDistance();

            /** Calculating avarge bet left and right */
            tripScore.setSteeringIntensity(steeringIntensityIndex / 2);
        } catch (Exception e) {
        }
        try {
            double steeringFreq = (double) (SensorManagerService.harshLeftCount + SensorManagerService.harshRightCount) / tripSession.getObdDistance();
            /** Calculating avg bet left and right */
            tripScore.setSteeringFreq(steeringFreq / 2);

        } catch (Exception e) {
        }

        tripScore.setCoolantCold((long) NxtApplication.coolantCool);
        tripScore.setCoolantHot((long) NxtApplication.coolantHot);
        tripScore.setCoolantOptimum((long) NxtApplication.coolantOptimum);
        tripScore.setCoolantVeryHot((long) NxtApplication.coolantVeryHot);

        tripScore.setRpmAwesome((long) NxtApplication.rpmAwesome);
        tripScore.setRpmHarsh((long) NxtApplication.rpmHarsh);
        tripScore.setRpmHigh((long) NxtApplication.rpmHarsh);
        tripScore.setRpmOk((long) NxtApplication.rpmOk);

        tripScore.setOilCold((long) NxtApplication.oilCool);
        tripScore.setOilHot((long) NxtApplication.oilHot);
        tripScore.setOilOptimum((long) NxtApplication.oilOptimum);
        tripScore.setOilVeryHot((long) NxtApplication.oilVeryHot);

        tripScore.setSpeedAwesome((long) NxtApplication.speedAwesome);
        tripScore.setSpeedHarsh((long) NxtApplication.speedHarsh);
        tripScore.setSpeedHigh((long) NxtApplication.speedHigh);
        tripScore.setSpeedOk((long) NxtApplication.speedOk);

        Log.d("Final Score:-", " " + score);

        TripScoreRepo tripScoreRepo = new TripScoreRepo(DashboardActivity.this, TripScore.class);
        tripScoreRepo.saveOrUpdate(tripScore);

        new CreateTripSessionAsyncTask(getApplicationContext()).execute(getTripSessionJson(tripSession));
        new CreateTripScoreAsyncTask(getApplicationContext()).execute(getTripScoreJson(tripScore));

        return true;
    }


    private void registerScreenStateReceiver() {
        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
        screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mScreenStateReceiver, screenStateFilter);
    }


    private void unregisterScreenStateReceiver() {
        if (mScreenStateReceiver != null) {
            unregisterReceiver(mScreenStateReceiver);
        }
    }


    void showAlertWithOption(Context ctx, String msg, String title, String positiveMessage,
                             String negativeMessage, final CarVinStates carVinStates, final String vin, final Integer carType) {

        new MaterialStyledDialog.Builder(ctx)
                .setHeaderDrawable(R.color.accent)
                .setIcon(R.mipmap.ic_launcher)
                .withDialogAnimation(true)
                .setTitle(title)
                .setDescription(msg)
                .setPositiveText(positiveMessage)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        switch (carVinStates) {
                            case VIN_FOUND_BUT_NO_CAR_WITH_THE_MATCHING_VIN_FOUND_IN_DATABASE:
                                final VehicleRepo vehicleRepo = new VehicleRepo(DashboardActivity.this, Vehicle.class);
                                final Vehicle vehicle = new Vehicle((Vehicle) vehicleRepo.findById(preferences.getString(Util.CAR_PREFERENCE, "")));
                                vehicle.setVin(vin);
                                vehicleRepo.saveOrUpdate(vehicle);
                                Log.d("d", "Vin updated");
                                break;
                            case VIN_FOUND_AND_PRESENT_IN_DATABASE_BUT_WRONG_CAR_SELECTED:
                                break;
                            case NO_CAR_SELECTED:
                                startActivity(new Intent(DashboardActivity.this, MyCarsActivity.class));
                                break;
                            default:
                                break;
                        }
                    }
                })
                .setNegativeText(negativeMessage)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        switch (carVinStates) {
                            case VIN_FOUND_BUT_NO_CAR_WITH_THE_MATCHING_VIN_FOUND_IN_DATABASE:
                                //You can add this car for now go anon
                                //Todo do we hace to changed the selected car
                                break;
                            case VIN_FOUND_AND_PRESENT_IN_DATABASE_BUT_WRONG_CAR_SELECTED:
                                //If wrong car selected and dont selct write car
                                //Todo do we hace to changed the selected car

                                break;
                            case NO_CAR_SELECTED:
                                //If no car selected and dont want to select car

                                startConnectioWithObd(carType);
                                break;
                            default:
                                break;
                        }
                    }
                })
                .show();

    }


    @Subscribe
    public void smsRecived(SmsContent smsContent) {
        ((NxtApplication) getApplication()).textToSpeech("Message from " + smsContent.getSmsFrom() + "" +
                " say Yes to read or no");
        promptSpeechInput();
        mSmsContent = smsContent;
    }

    /**
     * Showing google speech input dialog
     */
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Want to read sms? Say YES or NO");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    "Speech not supported",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    if (result.get(0).equals("Yes")) {
                        if (mSmsContent != null) {
                            ((NxtApplication) getApplication()).textToSpeech(mSmsContent.getSmsBody());
                        }
                    }
                    //txtSpeechInput.setText(result.get(0));
                }
                break;
            }

        }
    }

    class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            return true;
        }
    }
}