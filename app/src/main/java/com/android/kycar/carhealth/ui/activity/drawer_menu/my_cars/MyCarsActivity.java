package com.android.kycar.carhealth.ui.activity.drawer_menu.my_cars;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.ProgessDialogBox;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.Vehicle;
import com.android.kycar.carhealth.repositories.vehicle.VehicleRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.VehicleRestService;
import com.android.kycar.carhealth.ui.activity.BaseActivity;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_cars.adapter.AddMyCarRecyclerAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

public class MyCarsActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private AddMyCarRecyclerAdapter adapter;
    private FloatingActionButton fab;
    private String convoyID;
    private VehicleRepo vehicleRepo;
    boolean fromSelectCar = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cars);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.recyle_view);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        if(getIntent().getExtras()!=null) {
            fromSelectCar = getIntent().getBooleanExtra(Util.IS_FROM_SELECT_CAR, false);
            convoyID = getIntent().getStringExtra(Util.CONVOY_ID);
        }
        fab.setOnClickListener(onAddingListener());
        vehicleRepo = new VehicleRepo(getApplicationContext(), Vehicle.class);
        Log.d(MyCarsActivity.class.getSimpleName(), "Total vehicle:-"+vehicleRepo.findAll().size());
        getMyCars();
        /** Synchronization with cloud  */
        new SyncVehicleRestCall().execute();
    }

    private View.OnClickListener onAddingListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyCarsActivity.this, AddCarActivity.class));
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMyCars();
    }

    public void  getMyCars(){

        List<Vehicle> vehicles = vehicleRepo.findAllActive();
        adapter = new AddMyCarRecyclerAdapter(MyCarsActivity.this, vehicles, fromSelectCar, convoyID);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Uploading asynchronous task
     */
    private class GetMyCars extends AsyncTask<String, Void, Void> {
        private ProgessDialogBox dialogBox;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogBox = new ProgessDialogBox(MyCarsActivity.this);
            dialogBox.showDailog();
        }

        @Override
        protected Void doInBackground(String... cred) {
            return  null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(dialogBox.isShowing()){
                dialogBox.dismissDialog();
            }
        }
    }

    public void onDeleteCarClick(View view) {

        Vehicle vehicle = (Vehicle) vehicleRepo.findById("");
        vehicle.setActive(false);
        vehicleRepo.saveOrUpdate(vehicle);
        Toast.makeText(this, "Car deleted successfully", Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    /**
     * Sync all unsynced cars to cloud
     */
    class SyncVehicleRestCall extends AsyncTask<Void, Void, Void> {

        private VehicleRepo vehicleRepo;

        @Override
        protected Void doInBackground(Void... params) {

            try {
                vehicleRepo = new VehicleRepo(getApplicationContext(), Vehicle.class);
                List<Vehicle> unsyncVehicles = vehicleRepo.findAllUnsync();

                Iterator<Vehicle> vehicleIterator = unsyncVehicles.iterator();
                while (vehicleIterator.hasNext()) {

                    Vehicle vehicle = new Vehicle(vehicleIterator.next());
                    Log.d(MyCarsActivity.class.getSimpleName(), "Unsynched car :- "+vehicle.getName());

                    String response = new VehicleRestService().createVehicle(vehicle);
                    if(response != null) {
                        JSONObject responseObj = new JSONObject(response);

                        /** Removing old record */
                        vehicleRepo.deleteById(vehicle.getId());

                        /** Updating vehicle id in local db */
                        Vehicle vehicle1 = new Vehicle(responseObj);
                        vehicle1.setSync(true);
                        vehicleRepo.saveOrUpdate(vehicle1);
                        Log.d(AddCarActivity.class.getSimpleName(), "Rest Response:-"+response);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }
}