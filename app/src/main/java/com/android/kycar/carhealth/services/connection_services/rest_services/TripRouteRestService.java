package com.android.kycar.carhealth.services.connection_services.rest_services;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krishna on 10/17/2016.
 */
public class TripRouteRestService extends RestService {

    public String fetchTripRouteLocation(String tripId) {

        JSONObject tripIdJson = new JSONObject();
        try {
            tripIdJson.put("tripId", tripId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  restServiceCall("/coords/findbytripid", "POST", tripIdJson);
    }
}
