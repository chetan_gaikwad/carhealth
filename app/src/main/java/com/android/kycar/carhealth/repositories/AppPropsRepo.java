package com.android.kycar.carhealth.repositories;

import android.content.Context;

import com.android.kycar.carhealth.entities.AppProps;


/**
 * Created by Krishna on 8/12/2016.
 */
public class AppPropsRepo extends Repository<AppProps> {
    public AppPropsRepo(Context context, Class _aClass) {
        super(context, _aClass);
    }

    public boolean saveUnique (AppProps appProps) {

        AppProps appProps1 = realm.where(AppProps.class).equalTo("userId", appProps.getUserId()).equalTo("vehicleId", appProps.getVehicleId()).findFirst();
        if(appProps1 != null) {
            super.deleteById(appProps1.getId());
        }
        super.saveOrUpdate(appProps);
        return true;
    }

    public AppProps findByVehicleIdUserId (String vehicleId, String userId) {
        return realm.where(AppProps.class).equalTo("userId", userId).equalTo("vehicleId", vehicleId).findFirst();
    }
}
