package com.android.kycar.carhealth.services.obd_services;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;


import com.android.kycar.carhealth.NxtApplication;
import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.beans.ObdConnectionStatus;
import com.android.kycar.carhealth.config.ConfigActivity;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.AppProps;
import com.android.kycar.carhealth.repositories.AppPropsRepo;
import com.android.kycar.carhealth.services.dto.commands.SpeedCommand;
import com.android.kycar.carhealth.services.dto.commands.control.DistanceSinceCCCommand;
import com.android.kycar.carhealth.services.dto.commands.control.DtcNumberCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.BatteryCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.LoadCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.MassAirFlowCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.OilTempCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.RPMCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.ThrottlePositionCommand;
import com.android.kycar.carhealth.services.dto.commands.fuel.FindFuelTypeCommand;
import com.android.kycar.carhealth.services.dto.commands.protocol.SelectProtocolCommand;
import com.android.kycar.carhealth.services.dto.commands.temperature.EngineCoolantTemperatureCommand;
import com.android.kycar.carhealth.services.dto.enums.ObdProtocols;
import com.android.kycar.carhealth.services.dto.exceptions.NoDataException;
import com.android.kycar.carhealth.services.dto.exceptions.NonNumericResponseException;
import com.android.kycar.carhealth.services.dto.exceptions.UnsupportedCommandException;
import com.android.kycar.carhealth.services.utils.Logger;
import com.android.kycar.carhealth.utils.BusProvider;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ObdGatewayService extends AbstractGatewayService {

    private static final String TAG = ObdGatewayService.class.getName();
    static BlockingQueue<ObdCommandJob> supportedJobsQueue = new LinkedBlockingQueue<>();
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private Logger logger;
    private BluetoothDevice dev = null;
    private BluetoothSocket sock = null;
    private BluetoothSocket sockFallback = null;
    ArrayList<String> supportedPID = new ArrayList<String>();

    public void startService() throws IOException {
        Log.d(TAG, "Starting service..");
        logger = new Logger("ObdResponse.txt");
        prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        editor = prefs.edit();
        final String remoteDevice = prefs.getString(ConfigActivity.BLUETOOTH_LIST_KEY, null);
        if (remoteDevice == null || "".equals(remoteDevice)) {
            Toast.makeText(ctx, getString(R.string.text_bluetooth_nodevice), Toast.LENGTH_LONG).show();
            Log.e(TAG, "No Bluetooth device has been selected.");
            // TODO kill this service gracefully
            stopService();
            throw new IOException();
        } else {

            final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
            dev = btAdapter.getRemoteDevice(remoteDevice);
             Log.d(TAG, "Stopping Bluetooth discovery.");
            btAdapter.cancelDiscovery();
            try {
                startObdConnection();
            } catch (Exception e) {
                Log.e(
                        TAG,
                        "There was an error while establishing connection. -> "
                                + e.getMessage()
                );

                // in case of failure, stop this service.
                stopService();
                throw new IOException();
            }
        }
    }

    private void startObdConnection() throws IOException {

        Log.d(TAG, "Starting OBD connection..");
        isRunning = true;
        try {
            // Instantiate a BluetoothSocket for the remote device and connect it.
            sock = dev.createRfcommSocketToServiceRecord(MY_UUID);
            sock.connect();
        } catch (Exception e1) {
            Log.e(TAG, "There was an error while establishing Bluetooth connection. Falling back..", e1);
            Class<?> clazz = sock.getRemoteDevice().getClass();
            Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};
            try {

                /************Fallback method 1*********************/
                Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                Object[] params = new Object[]{Integer.valueOf(1)};
                sockFallback = (BluetoothSocket) m.invoke(sock.getRemoteDevice(), params);
                sockFallback.connect();
                sock = sockFallback;
                /************Fallback method 1 end*********************/
                Log.e("","Connected");

                /************Fallback method 2*********************/
            } catch (Exception e2) {
                Log.e(TAG, "Couldn't fallback while establishing Bluetooth connection. Stopping app..", e2);
                stopService();
                BusProvider.getInstance().post(new ObdConnectionStatus("Done"));
                throw new IOException();
            }
        }

        Log.i("obd","Car connected");
        getDeviceDetails();
        getBatteryDetails();
        BusProvider.getInstance().post(new ObdConnectionStatus("Checking for supported commonds"));
        if(prefs.getInt(Util.CAR_TYPE,0) == Util.isNormal ||
                prefs.getInt(Util.CAR_TYPE,0) == Util.isNormal) {

            fireCommand();
        } else {
            mCrudFireCommand();
        }
        BusProvider.getInstance().post(new ObdConnectionStatus("Connected"));
    }

    @Override
    public void queueJob(ObdCommandJob job) {
        job.getCommand().useImperialUnits(prefs.getBoolean(ConfigActivity.IMPERIAL_UNITS_KEY, false));
        super.queueJob(job);
    }

    /**
     * Runs the queue until the service is stopped
     */
    protected void executeQueue() throws InterruptedException,IOException {
        //Log.d(TAG, "Executing queue..");
        while (!Thread.currentThread().isInterrupted()) {
            ObdCommandJob job = null;
            try {
                job = jobsQueue.take();

                if (job.getState().equals(ObdCommandJob.ObdCommandJobState.NEW)) {
                    job.setState(ObdCommandJob.ObdCommandJobState.RUNNING);
                    if(job.getCommand().getName().equals("Battery")){
                        getBatteryDetails();
                    } else {
                        job.getCommand().run(sock.getInputStream(), sock.getOutputStream());
                    }

                    logger.logging("");
                } else
                    Log.e(Util.LOG,
                            "executeQueue Job state was not new, so it shouldn't be in queue. BUG ALERT!");
            } catch (InterruptedException i) {
                Thread.currentThread().interrupt();
            } catch (UnsupportedCommandException u) {
                if (job != null) {
                    job.setState(ObdCommandJob.ObdCommandJobState.NOT_SUPPORTED);
                }
                Log.d(Util.LOG, "executeQueue Command not supported. -> " + u.getMessage());
                Log.d(Util.LOG, "executeQueue removeUnsupportedJob : " + job.getCommand().getName());

            }catch(SocketException se){
                Log.d(Util.LOG, "executeQueue SocketException occured restarting thread");
                t.start();
            }
            catch (NoDataException nde){

                final String protocol = prefs.getString(ConfigActivity.PROTOCOLS_LIST_KEY, "AUTO");

                job = new ObdCommandJob(new SelectProtocolCommand(ObdProtocols.valueOf(protocol)));
                job.setState(ObdCommandJob.ObdCommandJobState.RUNNING);
                job.getCommand().run(sock.getInputStream(), sock.getOutputStream());

                if(t.getState()!=Thread.State.RUNNABLE ) {
                    t.start();
                }

            } catch (Exception e) {

                Log.d(Util.LOG, "error message : " + e.getMessage());

                if(e.getMessage().equals("Broken pipe")){
                    stopService();
                }
                Log.d(Util.LOG, "executeQueue Thread state : " + t.getState());
                if (job != null) {
                    job.setState(ObdCommandJob.ObdCommandJobState.EXECUTION_ERROR);
                }

                if(t.getState()!=Thread.State.RUNNABLE ) {
                    t.start();
                }

                Timer timer = new Timer();
                timer.schedule(new TimerTask() {

                    @Override
                    public void run() {
                        try {
                            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                            r.play();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 2000L);

                saveLastPark();

                Log.e(Util.LOG, "executeQueue Failed to run command. -> " + e.getMessage());
                e.printStackTrace();

            }

            if (job != null) {
                final ObdCommandJob job2 = job;
                /** Need to optimize */
                try {
                    BusProvider.getInstance().post(job2);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void saveLastPark() {
        /** Saving last app-props */
        AppPropsRepo appPropsRepo = new AppPropsRepo(this, AppProps.class);
        AppProps appProps = new AppProps();
        appProps.setId(new Date().getTime()+"");
        if(NxtApplication.currLocation != null) {
            appProps.setLastLatitude(NxtApplication.currLocation.getLatitude());
            appProps.setLastLongitude(NxtApplication.currLocation.getLongitude());
        }

        //Log.d(DashboardActivity.class.getSimpleName(), "Lat:- "+NxtApplication.currLocation.getLatitude()+", Long:- "+NxtApplication.currLocation.getLongitude());

        appProps.setLastTimestamp(new Date().getTime());
        appProps.setUserId(prefs.getString(Util.USER_PREFERENCE, ""));
        appProps.setVehicleId(prefs.getString(Util.CAR_PREFERENCE, ""));

        appPropsRepo.saveUnique(appProps);
        appPropsRepo.close();
    }


    /**
     * Stop OBD connection and queue processing.
     */
    public void stopService() {
        Log.d(Util.LOG, "stopService Stopping service..");
        logger.finalizeFile();
        notificationManager.cancel(NOTIFICATION_ID);
        jobsQueue.removeAll(jobsQueue); // TODO is this safe?
        isRunning = false;

        if (sock != null) {
            // close socket
            try {
                sock.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }

        // kill service
        stopSelf();
    }

    public boolean isRunning() {
        return isRunning;
    }

    public class ObdGatewayServiceBinder extends Binder {
        public ObdGatewayService getService() {
            return ObdGatewayService.this;
        }
    }

    public void fireCommand(){

        supportedPID.clear();
        jobsQueue.removeAll(jobsQueue);
        jobsQueue.clear();
        queueCounter=0L;

        String selectProtocol = "ATSP0\r";
        String echoof = "ATE0\r";
        String rpm = "01 0C\r";
        String eng_cool_temp = "01 05\r";
        String speed = "010D\r";
        String distCovered = "0131\r";
        String fuleType = "0151\r";
        String engOilTemp = "015c\r";
        String maf = "0110\r";
        String load = "0104\r";
        String throttle = "0111\r";
        String fuelLevel = "012f\r";
        String dontKnow = "0189\r";
        String errorCode = "03\r";
        String dtc = "0101\r";
        try {
          /*boolean goAhead = false;
            do {
                sock.getOutputStream().write(reset.getBytes());
                String temp = readRawData(sock.getInputStream());
                Toast.makeText(ctx, temp, Toast.LENGTH_LONG).show();
                if(temp.contains("ELM327"))
                    goAhead=true;

            }while(!goAhead); */

            Log.i(Util.LOG, "fire command scanning for supported pid");

            sock.getOutputStream().write(echoof.getBytes());
            Log.i(Util.LOG, "fire command echo of - " + readRawData(sock.getInputStream()));

            sock.getOutputStream().write(selectProtocol.getBytes());
            Log.i(Util.LOG, " fire command select protocol - " + readRawData(sock.getInputStream()));

            if(prefs.contains(Util.CAR_TYPE)) {
                if (prefs.getInt(Util.CAR_TYPE, 0) == Util.isCan) {
                    String headerOn = "ATH1\r";
                    String setHeader = "ATSH7E0\r";
                    String headerOff = "ATH0\r";

                    sock.getOutputStream().write(headerOn.getBytes());
                    sock.getOutputStream().write(rpm.getBytes());
                    sock.getOutputStream().write(setHeader.getBytes());
                    sock.getOutputStream().write(headerOff.getBytes());
                }
            }

            sock.getOutputStream().write(rpm.getBytes());
            String temp = readRawData(sock.getInputStream());
            Log.i(Util.LOG, "fire command rpm - " + temp);
            Log.i(Util.LOG, "rpm response length - " + temp.length());

           /* String rr = "atsh7e0\r";
            sock.getOutputStream().write(rr.getBytes());
            String p = readRawData(sock.getInputStream());
            Log.i(Util.LOG, "rr - " + p);*/

            sock.getOutputStream().write(rpm.getBytes());


            boolean rpmSupported = isPIDSupported(sock.getInputStream());

            if(rpmSupported){
                Log.i(Util.LOG, "fire command RPM supported");
                queueJob(new ObdCommandJob(new RPMCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new RPMCommand()));
                editor.putBoolean(new RPMCommand().getName(), true);
            }else{
                editor.putBoolean(new RPMCommand().getName(), false);
            }

            sock.getOutputStream().write(eng_cool_temp.getBytes());

            rpmSupported = isPIDSupported(sock.getInputStream());

            if(rpmSupported){
                Log.i(Util.LOG, "fire command eng_cool_temp supported");
                queueJob(new ObdCommandJob(new EngineCoolantTemperatureCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new EngineCoolantTemperatureCommand()));
                editor.putBoolean(new EngineCoolantTemperatureCommand().getName(), true);
            }else{
                editor.putBoolean(new EngineCoolantTemperatureCommand().getName(), false);
            }

            sock.getOutputStream().write(speed.getBytes());

            rpmSupported = isPIDSupported(sock.getInputStream());

            if(rpmSupported){
                Log.i(Util.LOG, "fire command speed supported");
                queueJob(new ObdCommandJob(new SpeedCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new SpeedCommand()));
                editor.putBoolean(new SpeedCommand().getName(), true);
            }else{
                editor.putBoolean(new SpeedCommand().getName(), false);
            }

            sock.getOutputStream().write(distCovered.getBytes());

            rpmSupported = isPIDSupported(sock.getInputStream());

            if(rpmSupported){
                Log.i(Util.LOG, "fire command distance supported");
                queueJob(new ObdCommandJob(new DistanceSinceCCCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new DistanceSinceCCCommand()));
                editor.putBoolean(new DistanceSinceCCCommand().getName(), true);
            }else{
                editor.putBoolean(new DistanceSinceCCCommand().getName(), false);
            }

            sock.getOutputStream().write(engOilTemp.getBytes());
            rpmSupported = isPIDSupported(sock.getInputStream());

            if(rpmSupported){
                Log.i(Util.LOG, "fire command eng oil temp supported");
                queueJob(new ObdCommandJob(new OilTempCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new OilTempCommand()));
                editor.putBoolean(new OilTempCommand().getName(), true);
            }else{
                editor.putBoolean(new OilTempCommand().getName(), false);
            }

            sock.getOutputStream().write(fuleType.getBytes());
            rpmSupported = isPIDSupported(sock.getInputStream());

            if(rpmSupported){
                Log.i(Util.LOG, "fire command eng oil temp supported");
                queueJob(new ObdCommandJob(new FindFuelTypeCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new FindFuelTypeCommand()));
                editor.putBoolean(new FindFuelTypeCommand().getName(), true);
            }else{
                Log.i("jfe", " not support service " + new FindFuelTypeCommand().getName());
                editor.putBoolean(new FindFuelTypeCommand().getName(), false);
            }

            //MAF
            sock.getOutputStream().write(maf.getBytes());
            rpmSupported = isPIDSupported(sock.getInputStream());

            if(rpmSupported){
                Log.i(Util.LOG, "fire command maf supported");
                queueJob(new ObdCommandJob(new MassAirFlowCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new MassAirFlowCommand()));
                editor.putBoolean(new MassAirFlowCommand().getName(), true);
            }else{
                Log.i("jfe", " not support service " + new MassAirFlowCommand().getName());
                editor.putBoolean(new MassAirFlowCommand().getName(), false);
            }


            //load
            sock.getOutputStream().write(load.getBytes());
            rpmSupported = isPIDSupported(sock.getInputStream());

            if(rpmSupported){
                Log.i(Util.LOG, "fire command load supported");
                queueJob(new ObdCommandJob(new LoadCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new LoadCommand()));
                editor.putBoolean(new LoadCommand().getName(), true);
            }else{
                Log.i("jfe", " not support service " + new LoadCommand().getName());
                editor.putBoolean(new LoadCommand().getName(), false);
            }

            //Thrpttle
            sock.getOutputStream().write(throttle.getBytes());
            rpmSupported = isPIDSupported(sock.getInputStream());

            if(rpmSupported){
                Log.i(Util.LOG, "fire command throttle supported");
                queueJob(new ObdCommandJob(new ThrottlePositionCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new ThrottlePositionCommand()));
                editor.putBoolean(new ThrottlePositionCommand().getName(), true);
            }else{
                Log.i("jfe", " not support service " + new ThrottlePositionCommand().getName());
                editor.putBoolean(new ThrottlePositionCommand().getName(), false);
            }

            //DTC
            sock.getOutputStream().write(dtc.getBytes());
            rpmSupported = isCurdPIDSupported(sock.getInputStream());
            if(rpmSupported){
                Log.i(Util.LOG, "fire command throttle supported");
                queueJob(new ObdCommandJob(new DtcNumberCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new DtcNumberCommand()));
                editor.putBoolean(new DtcNumberCommand().getName(), true);
            }else{
                Log.i("jfe", " not support service " + new DtcNumberCommand().getName());
                editor.putBoolean(new DtcNumberCommand().getName(), false);
            }


            final String protocol = prefs.getString(ConfigActivity.PROTOCOLS_LIST_KEY, "AUTO");
            queueJob(new ObdCommandJob(new SelectProtocolCommand(ObdProtocols.valueOf(protocol))));

            // Job for returning dummy data
            queueJob(new ObdCommandJob(new RPMCommand()));

            Log.i(Util.LOG, "fire command Queue size " + jobsQueue.size());

            Log.i(Util.LOG, "fire command scanning done");

            Log.i(Util.LOG, "fire command supported pid "+jobsQueue.toString());

            Log.i(Util.LOG, "supported Queue size " + supportedJobsQueue.size());

            editor.commit();
            t.start();

        }catch (IOException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void mCrudFireCommand(){

        supportedPID.clear();
        jobsQueue.removeAll(jobsQueue);
        jobsQueue.clear();
        queueCounter=0L;

        String selectProtocol = "ATSP0\r";
        String echoof = "ATE0\r";
        String rpm = "01 0C\r";
        String eng_cool_temp = "01 05\r";
        String speed = "010D\r";
        String distCovered = "0131\r";
        String fuleType = "0151\r";
        String engOilTemp = "015c\r";
        String maf = "0110\r";
        String load = "0104\r";
        String throttle = "0111\r";
        String dtc = "0101\r";

        try {
            Log.i(Util.LOG, "fire command scanning for supported pid");

            sock.getOutputStream().write(echoof.getBytes());
            Log.i(Util.LOG, "fire command echo of - " + readRawData(sock.getInputStream()));

            sock.getOutputStream().write(selectProtocol.getBytes());
            Log.i(Util.LOG, " fire command select protocol - " + readRawData(sock.getInputStream()));


            //Start header
            String headerOn = "ATH1\r";
            sock.getOutputStream().write(headerOn.getBytes());
            readRawData(sock.getInputStream());

            sock.getOutputStream().write(rpm.getBytes());
            String temp = readRawData(sock.getInputStream());
            Log.i(Util.LOG, "fire command rpm - " + temp);
            Log.i(Util.LOG, "rpm response length - " + temp.length());

            //RPM
            sock.getOutputStream().write(rpm.getBytes());
            boolean rpmSupported = isCurdPIDSupported(sock.getInputStream());

            if(rpmSupported){
                Log.i(Util.LOG, "fire command RPM supported");
                queueJob(new ObdCommandJob(new RPMCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new RPMCommand()));
                editor.putBoolean(new RPMCommand().getName(), true);
            }else{
                editor.putBoolean(new RPMCommand().getName(), false);
            }

            //ENG COOLANT TEMPERATURE
            sock.getOutputStream().write(eng_cool_temp.getBytes());
            rpmSupported = isCurdPIDSupported(sock.getInputStream());
            if(rpmSupported){
                Log.i(Util.LOG, "fire command eng_cool_temp supported");
                queueJob(new ObdCommandJob(new EngineCoolantTemperatureCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new EngineCoolantTemperatureCommand()));
                editor.putBoolean(new EngineCoolantTemperatureCommand().getName(), true);
            }else{
                editor.putBoolean(new EngineCoolantTemperatureCommand().getName(), false);
            }

            //SPEED
            sock.getOutputStream().write(speed.getBytes());
            rpmSupported = isCurdPIDSupported(sock.getInputStream());
            if(rpmSupported){
                Log.i(Util.LOG, "fire command speed supported");
                queueJob(new ObdCommandJob(new SpeedCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new SpeedCommand()));
                editor.putBoolean(new SpeedCommand().getName(), true);
            }else{
                editor.putBoolean(new SpeedCommand().getName(), false);
            }

            //DISTANCE
            sock.getOutputStream().write(distCovered.getBytes());
            rpmSupported = isCurdPIDSupported(sock.getInputStream());
            if(rpmSupported){
                Log.i(Util.LOG, "fire command distance supported");
                queueJob(new ObdCommandJob(new DistanceSinceCCCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new DistanceSinceCCCommand()));
                editor.putBoolean(new DistanceSinceCCCommand().getName(), true);
            }else{
                editor.putBoolean(new DistanceSinceCCCommand().getName(), false);
            }

            //ENG OIL TEMP
            sock.getOutputStream().write(engOilTemp.getBytes());
            rpmSupported = isCurdPIDSupported(sock.getInputStream());
            if(rpmSupported){
                Log.i(Util.LOG, "fire command eng oil temp supported");
                queueJob(new ObdCommandJob(new OilTempCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new OilTempCommand()));
                editor.putBoolean(new OilTempCommand().getName(), true);
            }else{
                editor.putBoolean(new OilTempCommand().getName(), false);
            }

            //FULE TYPE
            sock.getOutputStream().write(fuleType.getBytes());
            rpmSupported = isCurdPIDSupported(sock.getInputStream());
            if(rpmSupported){
                Log.i(Util.LOG, "fire command eng oil temp supported");
                queueJob(new ObdCommandJob(new FindFuelTypeCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new FindFuelTypeCommand()));
                editor.putBoolean(new FindFuelTypeCommand().getName(), true);
            }else{
                Log.i("jfe", " not support service " + new FindFuelTypeCommand().getName());
                editor.putBoolean(new FindFuelTypeCommand().getName(), false);
            }

            //MAF
            sock.getOutputStream().write(maf.getBytes());
            rpmSupported = isCurdPIDSupported(sock.getInputStream());
            if(rpmSupported){
                Log.i(Util.LOG, "fire command maf supported");
                queueJob(new ObdCommandJob(new MassAirFlowCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new MassAirFlowCommand()));
                editor.putBoolean(new MassAirFlowCommand().getName(), true);
            }else{
                Log.i("jfe", " not support service " + new MassAirFlowCommand().getName());
                editor.putBoolean(new MassAirFlowCommand().getName(), false);
            }


            //load
            sock.getOutputStream().write(load.getBytes());
            rpmSupported = isCurdPIDSupported(sock.getInputStream());
            if(rpmSupported){
                Log.i(Util.LOG, "fire command load supported");
                queueJob(new ObdCommandJob(new LoadCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new LoadCommand()));
                editor.putBoolean(new LoadCommand().getName(), true);
            }else{
                Log.i("jfe", " not support service " + new LoadCommand().getName());
                editor.putBoolean(new LoadCommand().getName(), false);
            }

            //Thrpttle
            sock.getOutputStream().write(throttle.getBytes());
            rpmSupported = isCurdPIDSupported(sock.getInputStream());
            if(rpmSupported){
                Log.i(Util.LOG, "fire command throttle supported");
                queueJob(new ObdCommandJob(new ThrottlePositionCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new ThrottlePositionCommand()));
                editor.putBoolean(new ThrottlePositionCommand().getName(), true);
            }else{
                Log.i("jfe", " not support service " + new ThrottlePositionCommand().getName());
                editor.putBoolean(new ThrottlePositionCommand().getName(), false);
            }


            //DTC
            sock.getOutputStream().write(dtc.getBytes());
            rpmSupported = isCurdPIDSupported(sock.getInputStream());
            if(rpmSupported){
                Log.i(Util.LOG, "fire command throttle supported");
                queueJob(new ObdCommandJob(new DtcNumberCommand()));
                supportedJobsQueue.put(new ObdCommandJob(new DtcNumberCommand()));
                editor.putBoolean(new DtcNumberCommand().getName(), true);
            }else{
                Log.i("jfe", " not support service " + new DtcNumberCommand().getName());
                editor.putBoolean(new DtcNumberCommand().getName(), false);
            }


            //limit
            final String protocol = prefs.getString(ConfigActivity.PROTOCOLS_LIST_KEY, "AUTO");
            queueJob(new ObdCommandJob(new SelectProtocolCommand(ObdProtocols.valueOf(protocol))));

            // Job for returning dummy data
            queueJob(new ObdCommandJob(new RPMCommand()));

            Log.i(Util.LOG, "fire command Queue size " + jobsQueue.size());

            Log.i(Util.LOG, "fire command scanning done");

            Log.i(Util.LOG, "fire command supported pid "+jobsQueue.toString());

            Log.i(Util.LOG, "supported Queue size " + supportedJobsQueue.size());

            editor.commit();
            t.start();

        }catch (IOException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private String mCleanCrud(String in) {
        return "";
    }

    public String readRawData(InputStream in) throws IOException {
        byte b = 0;
        StringBuilder res = new StringBuilder();

        // read until '>' arrives OR end of stream reached
        char c;
        while(true)
        {
            b = (byte) in.read();
            if(b == -1) // -1 if the end of the stream is reached
            {
                break;
            }
            c = (char)b;
            if(c == '>') // read until '>' arrives
            {
                break;
            }
            res.append(c);
        }

    /*
     * Imagine the following response 41 0c 00 0d.
     *
     * ELM sends strings!! So, ELM puts spaces between each "byte". And pay
     * attention to the fact that I've put the word byte in quotes, because 41
     * is actually TWO bytes (two chars) in the socket. So, we must do some more
     * processing..
     */
        String rawData = res.toString().replaceAll("SEARCHING", "");

    /*
     * Data may have echo or informative cost like "INIT BUS..." or similar.
     * The response ends with two carriage return characters. So we need to take
     * everything from the last carriage return before those two (trimmed above).
     */
        //kills multiline.. rawData = rawData.substring(rawData.lastIndexOf(13) + 1);
        rawData = rawData.replaceAll("\\s", "");//removes all [ \t\n\x0B\f\r]
        return res.toString();
    }

    ArrayList<Integer> fillBuffer(String rawData) {
        ArrayList<Integer> buffer = null;


        rawData = rawData.replaceAll("\\s", ""); //removes all [ \t\n\x0B\f\r]
        rawData = rawData.replaceAll("(BUS INIT)|(BUSINIT)|(\\.)", "");

        if (!rawData.matches("([0-9A-F])+")) {
            throw new NonNumericResponseException(rawData);
        }

        // read string each two chars
        buffer.clear();
        int begin = 0;
        int end = 2;
        while (end <= rawData.length()) {
            buffer.add(Integer.decode("0x" + rawData.substring(begin, end)));
            begin = end;
            end += 2;
        }
        return buffer;
    }

    public boolean isPIDSupported(InputStream in){
        boolean rpm = false;
        try {
            String rawData = readRawData(in);
            String temp = rawData.substring(0, 2);
            int len = temp.length();
            if(temp.equals("41")){
                rpm=true;
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return rpm;
    }

    public boolean isCurdPIDSupported(InputStream in){
        boolean rpm = false;
        try {
            String rawData = readRawData(in);

            if(rawData.contains("41")){
                rpm=true;
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return rpm;
    }

    private String getBluetoothMac(){
        return prefs.getString(ConfigActivity.BLUETOOTH_LIST_KEY, null);
    }

    private String getDeviceDetails(){

        String reset = "ATZ\r";
        String echoof = "ATE0\r";
        String lineFeed = "ATL0\r";
        String serialNumber = "STSN\r";
        try {
            /*sock.getOutputStream().write(reset.getBytes());
            String temp = readRawData(sock.getInputStream());
            if(temp.contains("ELM327"))
                Log.i(Util.LOG, "getDeviceDetails device connected");*/
            String temp="";
            sock.getOutputStream().write(reset.getBytes());
            boolean goAhead = false;
            do {

                temp = readRawData(sock.getInputStream());
                if(temp.contains("ELM327"))
                    goAhead=true;

            }while(!goAhead);


            sock.getOutputStream().write(echoof.getBytes());
            temp = readRawData(sock.getInputStream());
            if(temp.contains("OK"))
                Log.i(Util.LOG, "getDeviceDetails echoo off");

            sock.getOutputStream().write(lineFeed.getBytes());
            temp = readRawData(sock.getInputStream());
            if(temp.contains("OK"))
                Log.i(Util.LOG, "getDeviceDetails line feed off");

            sock.getOutputStream().write(serialNumber.getBytes());
            temp = readRawData(sock.getInputStream());
            if(temp.contains("OK"))
                Log.i(Util.LOG, "getDeviceDetails serial number - "+temp);
            editor.putString("vin_number_id",temp).commit();

        }catch (Exception e){
            e.printStackTrace();
        }

        return "";
    }

    private String getBatteryDetails() {


        String batteryVoltage = "ATRV\r";


        try {
            sock.getOutputStream().write(batteryVoltage.getBytes());
            String temp = readRawData(sock.getInputStream());
            Log.i(Util.LOG, "getBatteryDetails battery voltage - " + temp);
            if(temp != null){
                NxtApplication.batteryVoltage.delete(0,NxtApplication.batteryVoltage.length());
                NxtApplication.batteryVoltage.append(temp);
                editor.putBoolean(new BatteryCommand().getName(), true);
            }else{
                Log.i("jfe", " not support service " + new BatteryCommand().getName());
                editor.putBoolean(new BatteryCommand().getName(), false);
            }
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private ArrayList<String> countHeader(String response, String pattern) {
        ArrayList<String> headerArray = new ArrayList<>();
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(response);
        int count = 0;
        while (m.find()){
            count +=1;
        }

        headerArray.add(response.substring(0, response.indexOf(pattern)));


        p = Pattern.compile(Pattern.quote("\r\n") + "(.*?)" + Pattern.quote(pattern));
        m = p.matcher(response);
        while (m.find()) {
            System.out.println(m.group(1));
            headerArray.add(m.group(1));
        }

        Log.d(TAG,"Count is "+count);
        Log.d(TAG,"Headers is "+headerArray.toString());
        return headerArray;
    }
}