package com.android.kycar.carhealth.ui.fragments;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.android.kycar.carhealth.NxtApplication;
import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.beans.Gear;
import com.android.kycar.carhealth.config.ConfigActivity;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.Vehicle;
import com.android.kycar.carhealth.entities.vehicle_master.GearRatio;
import com.android.kycar.carhealth.repositories.vehicle.GearRatioRepo;
import com.android.kycar.carhealth.repositories.vehicle.VehicleRepo;
import com.android.kycar.carhealth.services.dto.commands.SpeedCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.OilTempCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.RPMCommand;
import com.android.kycar.carhealth.services.dto.commands.temperature.EngineCoolantTemperatureCommand;
import com.android.kycar.carhealth.services.dto.enums.AvailableCommandNames;
import com.android.kycar.carhealth.services.obd_services.ObdCommandJob;
import com.android.kycar.carhealth.ui.activity.DashboardActivity;
import com.android.kycar.carhealth.utils.BusProvider;
import com.android.kycar.carhealth.utils.FontCache;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Krishna on 2/2/2016.
 */
public class StatsFragmentUpdated extends Fragment {


    private ImageView upRpm;
    private LinearLayout vv;
    private ImageView gearShift;
    private Map<Integer,Integer> dataSheets = new HashMap<>();
    private static final int RPM_CONST = 100;
    private static final int SPEED_CONST = 101;
    private TextView tvGear;
    private Gear gear;
    private VehicleRepo vehicleRepo;
    protected SharedPreferences preferences;
    protected SharedPreferences.Editor editor;
    public static int obdDistance = -1;
    private TextView speedValue;
    private TextView rpmValue;
    private TextView engineOilTempValue;
    private TextView engineCoolantValue;
    private TextView batteryVoltageValue;
    private TextView vin;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        container = (ViewGroup) inflater.inflate(R.layout.stats_fragment_updated, null);
        vv = (LinearLayout)container.findViewById(R.id.vehicle_view);
        preferences = PreferenceManager.getDefaultSharedPreferences(container.getContext());


        upRpm = (ImageView)container.findViewById(R.id.up_rpm);
        speedValue = (TextView)container.findViewById(R.id.speed_value);
        speedValue.setTypeface(FontCache.get("", getActivity()));
        ((TextView)container.findViewById(R.id.speedTxt)).setTypeface(FontCache.get("", getActivity()));

        rpmValue = (TextView)container.findViewById(R.id.rpm_value);
        rpmValue.setTypeface(FontCache.get("", getActivity()));
        ((TextView)container.findViewById(R.id.vin)).setText(preferences.getString(Util.VIN_PREFERENCE,""));
        ((TextView)container.findViewById(R.id.rpmTxt)).setTypeface(FontCache.get("", getActivity()));

        engineOilTempValue = (TextView)container.findViewById(R.id.eot_value);
        engineOilTempValue.setTypeface(FontCache.get("", getActivity()));
        ((TextView)container.findViewById(R.id.eot_txt)).setTypeface(FontCache.get("", getActivity()));

        engineCoolantValue = (TextView)container.findViewById(R.id.ct_value);
        engineCoolantValue.setTypeface(FontCache.get("", getActivity()));
        ((TextView)container.findViewById(R.id.ct_txt)).setTypeface(FontCache.get("", getActivity()));


        batteryVoltageValue = (TextView)container.findViewById(R.id.bv_value);
        batteryVoltageValue.setTypeface(FontCache.get("", getActivity()));
        ((TextView)container.findViewById(R.id.bv_txt)).setTypeface(FontCache.get("", getActivity()));


        vehicleRepo = new VehicleRepo(container.getContext(), Vehicle.class);

        editor = preferences.edit();
        Vehicle vehicle = (Vehicle) vehicleRepo.findById(preferences.getString(Util.CAR_PREFERENCE, ""));
        vehicleRepo.close();

        if(vehicle != null) {
            GearRatioRepo gearRatioRepo = new GearRatioRepo(getActivity(), GearRatio.class);
            List<GearRatio> gearRatios = gearRatioRepo.findGearRatiosByVariantId(vehicle.getVariantId());
            gear = new Gear();
            //gear = new Gear(container.getContext(), vehicle.getVariantId());
            /** By default nano */
        } else {
            //gear = new Gear(container.getContext(), "784");
        }
        //gear = new Gear(getActivity(), preferences.getString("variantId", ""));
        tvGear = (TextView) container.findViewById(R.id.tv_gear);
        tvGear.setTypeface(FontCache.get("", getActivity()));
        //showEvent(new CustomDialogPojo(10));

        /*PublishCoods publishCoods = new PublishCoods();
        MqttMessage message = publishCoods.getCoordData(NxtApplication.currLocation, preferences.getString(Util.TRIP_ID, null));
        try {
            if(Connection.getClient() != null){
                Connection.getClient().publish(NxtApplication.baseTopic+"/coords_data/"+preferences.getString(Util.USER_PREFERENCE, ""), message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        return container;
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void a2DpControl(String event) {
        Log.i("dsk", "Got a2dp event in stat");
        ((DashboardActivity)getActivity()).onAudInput(event,2);
    }

    @Subscribe
    public void stateUpdate(ObdCommandJob job) {
        final String cmdName = job.getCommand().getName();
        String cmdResult = "";
        final String cmdID = LookUpCommand(cmdName);

        if (job.getState().equals(ObdCommandJob.ObdCommandJobState.EXECUTION_ERROR)) {
            cmdResult = job.getCommand().getResult();
            if (cmdResult != null) {
            }
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.NOT_SUPPORTED)) {
            cmdResult = getString(R.string.status_obd_no_support);
        } else {
            cmdResult = job.getCommand().getFormattedResult();

            if (vv.findViewWithTag(cmdID) != null) {
                final TextView existingTV = (TextView) vv.findViewWithTag(cmdID);

                final String result = cmdResult;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(result.length()<15) {
                            existingTV.setText(result);
                        } else {
                            existingTV.setText("-");
                        }
                    }
                });
                if(dataSheets.containsKey("DISTANCE_TRAVELED_AFTER_CODES_CLEARED")) {
                    obdDistance = dataSheets.get("DISTANCE_TRAVELED_AFTER_CODES_CLEARED");
                }
                updateTripStatistic(job, cmdID);
            }
        }
    }

    public void send(View v){
        NxtApplication.sendDate();
    }
    private void updateTripStatistic(final ObdCommandJob job, final String cmdID) {

        if(NxtApplication.batteryVoltage != null){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    batteryVoltageValue.setText(NxtApplication.batteryVoltage);
                }
            });

        }
        if (cmdID.equals(AvailableCommandNames.SPEED.toString())) {
            final SpeedCommand command = (SpeedCommand) job.getCommand();
            if(command.getMetricSpeed() >= 0) {
                dataSheets.put(SPEED_CONST,command.getMetricSpeed());
            }
        } else if (cmdID.equals(AvailableCommandNames.ENGINE_RPM.toString())) {
            final RPMCommand command = (RPMCommand) job.getCommand();
            if(command.getRPM()>=0)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int upValue =
                                Integer.parseInt(preferences.getString
                                        (ConfigActivity.RPM_pref,"2000"));
                        if(command.getRPM() >= upValue){
                            upRpm.setVisibility(View.VISIBLE);
                        } else{
                            upRpm.setVisibility(View.GONE);
                        }
                    }
                });
            dataSheets.put(RPM_CONST, command.getRPM());
        } else if (cmdID.equals(AvailableCommandNames.ENGINE_COOLANT_TEMP.toString())) {
            final EngineCoolantTemperatureCommand command = (EngineCoolantTemperatureCommand) job.getCommand();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(command.getTemperature() < 90){
                        engineCoolantValue.setTextColor(Color.parseColor("#006389"));
                    } else if(command.getTemperature() > 90 && command.getTemperature()< 112){
                        engineCoolantValue.setTextColor(Color.parseColor("#2D8900"));
                    }else if(command.getTemperature() > 112 && command.getTemperature()< 118){
                        engineCoolantValue.setTextColor(Color.parseColor("#B06700"));
                    }else if(command.getTemperature() > 118){
                        engineCoolantValue.setTextColor(Color.parseColor("#BA1919"));
                    }
                }
            });
        }
        else if (cmdID.equals(AvailableCommandNames.ENGINE_OIL_TEMP.toString())) {
            final OilTempCommand command = (OilTempCommand) job.getCommand();

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(command.getOilTemp() < 90){
                        engineOilTempValue.setTextColor(Color.parseColor("#006389"));
                    } else if(command.getOilTemp() > 90 && command.getOilTemp()< 112){
                        engineOilTempValue.setTextColor(Color.parseColor("#2D8900"));
                    }else if(command.getOilTemp() > 112 && command.getOilTemp()< 118){
                        engineOilTempValue.setTextColor(Color.parseColor("#B06700"));
                    }else if(command.getOilTemp() > 118){
                        engineOilTempValue.setTextColor(Color.parseColor("#BA1919"));
                    }
                }
            });
        }

        if(dataSheets.get(RPM_CONST)!=null){

            Log.d("RPM Value", ""+dataSheets.get(RPM_CONST));
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(dataSheets.get(SPEED_CONST)!=null &&
                            dataSheets.get(SPEED_CONST)>0){

                        Double gr = (double)(dataSheets.get(RPM_CONST)) /
                                ((dataSheets.get(SPEED_CONST)));

                        Log.d("Stats", "Gear ratio"+gr);
                        /*if(dataSheets.get(RPM_CONST) < 1400){
                            tvGear.setTextColor(Color.BLUE);
                            //showGearShiftNotification(true, "UP");
                        } else if(dataSheets.get(RPM_CONST) < 2800){
                            tvGear.setTextColor(Color.GREEN);
                            //showGearShiftNotification(false, "NONE");
                        } else if(dataSheets.get(RPM_CONST) > 2800) {
                            tvGear.setTextColor(Color.RED);
                            //showGearShiftNotification(true, "DOWN");
                        }*/
                        /** Need to optimize */


                        try {
                               // Log.d(ValuesFragmentUpdated.class.getSimpleName(), "Gear ration:" + gr + " gear no:-" + gear.getGear(gr));
                                tvGear.setText("Gear " + gear.getGear(gr));

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        /*editor.putString(Util.GEAR_PREF, gear.getGear(gr)+"");
                        editor.commit();*/

                    }
                }
            });
        }
    }


    private void showGearShiftNotification(Boolean isShift, String shiftTo){

        if(isShift) {
            gearShift.setVisibility(View.INVISIBLE);
        } else {
            gearShift.setVisibility(View.VISIBLE);
            if(shiftTo.equals("UP")){
                gearShift.setImageResource(R.drawable.gear_arrow_up);
            } else if(shiftTo.equals("DOWN")) {
                gearShift.setImageResource(R.drawable.gear_arrow_down);
            }
        }
    }

    public static String LookUpCommand(String txt) {
        for (AvailableCommandNames item : AvailableCommandNames.values()) {
            if (item.getValue().equals(txt)) return item.name();
        }
        return txt;
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }
}
