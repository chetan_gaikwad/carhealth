package com.android.kycar.carhealth.exceptions;

/**
 * Created by Krishna on 1/30/2016.
 */
public class DataLoggingException extends Exception {
    public DataLoggingException() {
        // TODO Auto-generated constructor stub
    }

    public DataLoggingException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public DataLoggingException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public DataLoggingException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }
}
