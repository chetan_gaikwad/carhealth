package com.android.kycar.carhealth.services.dto.utils;

/**
 * Created by 638 on 12/6/2015.
 */
public class RPMTest {
    private int rpm;
    private String timeStamp;


    public int getRpm() {
        return rpm;
    }

    public void setRpm(int rpm) {
        this.rpm = rpm;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "RPMTest{" +
                "rpm=" + rpm +
                ", timeStamp='" + timeStamp + '\'' +
                '}';
    }
}
