package com.android.kycar.carhealth.entities.obd_master;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 1/16/2016.
 */
@RealmClass
public class Pid extends RealmObject {

    @PrimaryKey
    private String id;

    private String pidCommand;

    private String pidResult;

    private String pidType;

    private String pidDesc;

    public Pid() {
    }

    public Pid(String id, String pidCommand, String pidResult, String pidType, String pidDesc) {
        this.id = id;
        this.pidCommand = pidCommand;
        this.pidResult = pidResult;
        this.pidType = pidType;
        this.pidDesc = pidDesc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPidCommand() {
        return pidCommand;
    }

    public void setPidCommand(String pidCommand) {
        this.pidCommand = pidCommand;
    }

    public String getPidResult() {
        return pidResult;
    }

    public void setPidResult(String pidResult) {
        this.pidResult = pidResult;
    }

    public String getPidType() {
        return pidType;
    }

    public void setPidType(String pidType) {
        this.pidType = pidType;
    }

    public String getPidDesc() {
        return pidDesc;
    }

    public void setPidDesc(String pidDesc) {
        this.pidDesc = pidDesc;
    }
}
