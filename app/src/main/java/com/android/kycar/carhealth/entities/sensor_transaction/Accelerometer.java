package com.android.kycar.carhealth.entities.sensor_transaction;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by krishna on 8/12/15.
 */
@RealmClass
public class Accelerometer extends RealmObject {

    @PrimaryKey
    private String id;

    private long timestamp;

    private double ax;

    private double ay;

    private double az;

    public Accelerometer() {
    }

    public Accelerometer(String id, long timestamp, double ax, double ay, double az) {
        this.id = id;
        this.timestamp = timestamp;
        this.ax = ax;
        this.ay = ay;
        this.az = az;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public double getAx() {
        return ax;
    }

    public void setAx(double ax) {
        this.ax = ax;
    }

    public double getAy() {
        return ay;
    }

    public void setAy(double ay) {
        this.ay = ay;
    }

    public double getAz() {
        return az;
    }

    public void setAz(double az) {
        this.az = az;
    }
}
