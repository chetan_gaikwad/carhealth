package com.android.kycar.carhealth.utils;

/**
 * Created by Chetan on 2/17/2017.
 */

public class PreferenceUtils {
    public static final String PREFERENCE_AUTO_CONNECT = "pref_auto_connect";
    public static final String PREFERENCE_APP_UPDATE_CHECK_DATE = "pref_auto_update_date";
    public static final String PREFERENCE_APP_UPDATE_CHECK = "pref_auto_update_connect";
    public static final String PREFERENCE_ENABLE_OFFLINE_LOGGING = "pref_offline_logging";
    public static final String PREFERENCE_END_CALL= "pref_end_call";
}
