package com.android.kycar.carhealth.services.connection_services.rest_services;


import com.android.kycar.carhealth.entities.User;
import com.android.kycar.carhealth.entities.UserContact;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krishna on 5/31/2016.
 */
public class UserContactsRestService extends RestService{

    public String createContact(UserContact userContact){

        JSONObject userContactJson = new JSONObject();
        try {
            userContactJson.put("name", userContact.getName());
            userContactJson.put("id", userContact.getId());
            userContactJson.put("number", userContact.getNumber());
            userContactJson.put("userId", userContact.getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String userContactSaveUrl = "/user/userContact/create";
        return restServiceCall(userContactSaveUrl, "POST", userContactJson);
    }

    public String findUserContactTopic(String number) {

        String userContactSaveUrl = "/user/"+number+"/get/topic";
        return restServiceCall(userContactSaveUrl, "GET");
    }

    public String updateContact(UserContact userContact) {

        JSONObject userContactJson = new JSONObject();
        try {
            userContactJson.put("name", userContact.getName());
            userContactJson.put("id", userContact.getId());
            userContactJson.put("number", userContact.getNumber());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String userContactSaveUrl = "/user/userContact/update";
        return restServiceCall(userContactSaveUrl, "PUT", userContactJson);
    }

    public String deleteContact(String id) {

        String userContactSaveUrl = "/user/userContact/delete?id="+id;
        return restServiceCall(userContactSaveUrl, "DELETE");
    }

    public List<User> fetchAllContact(String userId){

        String userContactFindAllUrl = "/user/userContact/findAll";

        String response = restServiceCall(userContactFindAllUrl, "GET");;

        List<User> users = new ArrayList<>();
        try {
            JSONArray userArray = new JSONArray(response);
            for(int counter = 0; counter < userArray.length(); counter ++) {

                JSONObject userObj = (JSONObject) userArray.get(counter);
                users.add(jsonToUser(userObj));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return users;
    }

    private User jsonToUser(JSONObject userObj) {

        User user = new User();
        try {
            user.setId((String) getCheckedAttribute(userObj, "id"));
            user.setEmailId((String) getCheckedAttribute(userObj, "phoneNumber"));
            user.setProfilePicture((String) getCheckedAttribute (userObj,"profilePicture"));
            user.setLicenceNumber((String) getCheckedAttribute(userObj, "licenceNumber"));
            user.setAadharNumber((String) getCheckedAttribute(userObj,"firstName"));
            user.setAddress((String) getCheckedAttribute(userObj,"address"));
            user.setBloodGrp(Integer.valueOf((Integer) getCheckedAttribute(userObj,"bloodGrp")));
            user.setPhoneNumber((String) getCheckedAttribute(userObj,"phoneNumber"));
            user.setPan((String) getCheckedAttribute(userObj,"pan"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }

    private Object getCheckedAttribute(JSONObject jsonObj, String key) throws JSONException {

        if(jsonObj.has(key)){
            return jsonObj.get(key);
        }else {
            return null;
        }
    }
}
