package com.android.kycar.carhealth.repositories.user;

import android.content.Context;


import com.android.kycar.carhealth.entities.User;
import com.android.kycar.carhealth.repositories.Repository;

import io.realm.exceptions.RealmException;

/**
 * Created by Krishna on 2/1/2016.
 */
public class UserRepo extends Repository {
    public UserRepo(Context context, Class _aClass) {
        super(context, _aClass);
    }

    public User findByEmailId(String emailId) throws RealmException {
        return realm.where(User.class).equalTo("emailId", emailId).findFirst();
    }
}
