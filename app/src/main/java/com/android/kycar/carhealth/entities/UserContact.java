package com.android.kycar.carhealth.entities;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 2/1/2016.
 */
@RealmClass
public class UserContact extends RealmObject {

    @PrimaryKey
    private String id;

    private String name;

    private String number;

    private String userContactTopic;

    private String userId;

    private boolean isSync;

    public UserContact() {
    }

    public UserContact(String id, String name, String number, String userContactTopic, String userId) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.userContactTopic = userContactTopic;
        this.isSync = false;
        this.userId = userId;
    }

    public UserContact(UserContact userContact) {
        this.id = userContact.getId();
        this.name = userContact.getName();
        this.number = userContact.getNumber();
        this.userContactTopic = userContact.getUserContactTopic();
        this.isSync = userContact.isSync();
    }

    public UserContact(JSONObject jsonObject){
        try {
            this.id = jsonObject.getString("id");
            this.number = jsonObject.getString("number");
            this.name = jsonObject.getString("name");
            if(jsonObject.has("topic")){
                this.userContactTopic = jsonObject.getString("topic");
            }
            if (jsonObject.has("isSync")) {
                this.isSync = jsonObject.getBoolean("isSync");
            } else {
                this.isSync = false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getUserContactTopic() {
        return userContactTopic;
    }

    public void setUserContactTopic(String userContactTopic) {
        this.userContactTopic = userContactTopic;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
