package com.android.kycar.carhealth.ui.activity;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.kycar.carhealth.R;


public class SensorAcquasitions extends AppCompatActivity {

    private SensorManager sensorManager;

    private LocationManager locationManager;

    private TextView statusText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_acquasitions);

        statusText = (TextView)findViewById(R.id.status);
        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();


        if(btAdapter.isEnabled()){
         //   btStatusTextView.setImageResource(R.drawable.bluetooth_on);
            statusText.setText("BlueToothStatus : ON");
        }else{
            statusText.setText("BlueToothStatus : OFF");
        }


        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
           statusText.append("\nGPS status : ON");
        }else{
            statusText.append("\nGPS status : OFF");
        }


        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);

        startSensors();
        registerReceiver(mReceiver, filter);
    }

    public void dashboard(View v){
        //SensorAcquasitions.this.finish();
        //startActivity(new Intent(SensorAcquasitions.this, AddCar.class));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to add a new car?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SensorAcquasitions.this.finish();
                        //startActivity(new Intent(SensorAcquasitions.this, AddCarActivityDepricated.class));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    private void startSensors() {
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            // success! we have an accelerometer
            //Toast.makeText( (EngineVitalsActivity)context, "Your device support accelerometer", Toast.LENGTH_SHORT).show();
            statusText.append("\nACCELEROMETER sensor supported");
        } else {
            // fai! we dont have an accelerometer!
            // Toast.makeText(((EngineVitalsActivity) context), "Your device don't support accelerometer", Toast.LENGTH_SHORT).show();
            statusText.append("\nACCELEROMETER sensor not supported");
        }

        if (sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null) {
            // success! we have an gyroscope
            // Toast.makeText((EngineVitalsActivity)context, "Your device support GYROSCOPE", Toast.LENGTH_SHORT).show();
            statusText.append("\nGYROSCOPE sensor supported");
        } else {
            // fai! we dont have an gyroscope!
            //Toast.makeText((EngineVitalsActivity)context, "Your device don't support GYROSCOPE", Toast.LENGTH_SHORT).show();
            statusText.append("\nGYROSCOPE sensor not supported");
        }

        if (sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY) != null) {
            // success! we have an gravity
            //Toast.makeText((EngineVitalsActivity)context, "Your device support GRAVITY", Toast.LENGTH_SHORT).show();
            statusText.append("\nGRAVITY sensor supported");
        } else {
            // fai! we dont have an gravity!
            //Toast.makeText((EngineVitalsActivity)context, "Your device don't support TYPE_GRAVITY", Toast.LENGTH_SHORT).show();
            statusText.append("\nGRAVITY sensor not supported");
        }

    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        statusText.append("\nBluetooth started");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        statusText.append("\nBluetooth turned off");
                        break;
                }
            }
        }
    };
    private boolean isBluetoothEnabled(){
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        return btAdapter.isEnabled();
    }
}
