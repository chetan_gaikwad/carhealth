package com.android.kycar.carhealth.async_task;

/**
 * Created by Krishna on 2/1/2017.
 */
import android.os.AsyncTask;


import com.android.kycar.carhealth.services.connection_services.rest_services.FcmRestCallService;

import org.json.JSONObject;

public class SendSosNotificationAsyncTask extends AsyncTask<JSONObject, Void, String> {

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(JSONObject... tokens) {

        try{
            return new FcmRestCallService().sendSosCopNotification(tokens[0]);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {

        super.onPostExecute(result);
        if(result!=null) {
            //  Toast.makeText(DashboardActivity.this, "Trip created successfully", Toast.LENGTH_SHORT).show();
        } else  {
            //  Toast.makeText(DashboardActivity.this, "Trip creation failed", Toast.LENGTH_SHORT).show();
        }
    }
}
