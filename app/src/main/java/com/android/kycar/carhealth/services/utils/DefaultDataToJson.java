package com.android.kycar.carhealth.services.utils;

import android.app.Activity;


import com.android.kycar.carhealth.entities.SpeedPerformance;
import com.android.kycar.carhealth.entities.obd_master.DtcErrorCode;
import com.android.kycar.carhealth.entities.obd_master.Pid;
import com.android.kycar.carhealth.entities.vehicle_master.EngineVitals;
import com.android.kycar.carhealth.entities.vehicle_master.FuelType;
import com.android.kycar.carhealth.entities.vehicle_master.GearRatio;
import com.android.kycar.carhealth.entities.vehicle_master.Make;
import com.android.kycar.carhealth.entities.vehicle_master.Model;
import com.android.kycar.carhealth.entities.vehicle_master.Variant;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Krishna on 2/20/2017.
 */

public class DefaultDataToJson {

    LoggerTxt makeTxt;
    LoggerTxt modelTxt;
    LoggerTxt variantTxt;
    LoggerTxt pidTxt;
    LoggerTxt fuelTypeTxt;
    LoggerTxt gearRatioTxt;
    LoggerTxt speedPerformanceTxt;
    LoggerTxt dtcCodeTxt;
    LoggerTxt enginVitalsTxt;

    public void createJson(Activity context) {

        Realm.init(context);
        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        Realm realm = Realm.getInstance(configuration);

        makeTxt = new LoggerTxt("Default data", "make");
        modelTxt = new LoggerTxt("Default data", "model");
        variantTxt = new LoggerTxt("Default data", "variant");
        pidTxt = new LoggerTxt("Default data", "pid");
        fuelTypeTxt = new LoggerTxt("Default data", "fuel type");
        gearRatioTxt = new LoggerTxt("Default data", "gear ratio");
        speedPerformanceTxt = new LoggerTxt("Default data", "speed performance");
        dtcCodeTxt = new LoggerTxt("Default data", "dtc code");
        enginVitalsTxt = new LoggerTxt("Default data", "engine vitals");
        createData(realm);
    }

    private void createData(Realm realm) {

        createMakeData(realm);
        createModelData(realm);
        createVariantData(realm);
        createPidData(realm);
        createFuelTypeData(realm);
        createGearRatioData(realm);
        createSpeedPerformanceParameterData(realm);
        createDtcErrorCodeData(realm);
        createEngineVitalsData(realm);
    }

    private void createDtcErrorCodeData(Realm realm) {

        List<DtcErrorCode> dtcErrorCodes = realm.where(DtcErrorCode.class).findAll();
        StringBuilder sb = new StringBuilder();

        for (DtcErrorCode dtcErrorCode : dtcErrorCodes) {
            sb.append("{");
            sb.append("\"id\":"+dtcErrorCode.getId()+",");
            sb.append("\"dtc\":"+dtcErrorCode.getDtc()+",");
            sb.append("\"diagnosis\":"+dtcErrorCode.getDiagnosis()+",");
            sb.append("\"description\":"+dtcErrorCode.getDescription());
            sb.append("}");

            dtcCodeTxt.log(sb.toString());
            sb = new StringBuilder();
        }
    }

    private void createSpeedPerformanceParameterData(Realm realm) {

        List<SpeedPerformance> speedPerformances = realm.where(SpeedPerformance.class).findAll();
        StringBuilder sb = new StringBuilder();

        for (SpeedPerformance speedPerformance : speedPerformances) {
            sb.append("{");
            sb.append("\"id\":"+speedPerformance.getId()+",");
            sb.append("\"form\":"+speedPerformance.getFrom()+",");
            sb.append("\"to\":"+speedPerformance.getTo());
            sb.append("}");

            gearRatioTxt.log(sb.toString());
            sb = new StringBuilder();
        }
    }

    private void createGearRatioData(Realm realm) {
        List<GearRatio> gearRatios = realm.where(GearRatio.class).findAll();
        StringBuilder sb = new StringBuilder();

        for (GearRatio gearRatio:gearRatios) {
            sb.append("{");
            sb.append("\"id\":\""+gearRatio.getId()+"\",");
            sb.append("\"gear\":"+gearRatio.getGear()+",");
            sb.append("\"variantId\":\""+gearRatio.getVariantId()+"\",");
            sb.append("\"gearRatioMin\":"+gearRatio.getGearRatioMin()+",");
            sb.append("\"gearRatioMax\":"+gearRatio.getGearRatioMax());
            sb.append("}");

            gearRatioTxt.log(sb.toString());
            sb = new StringBuilder();
        }
    }

    private void createFuelTypeData(Realm realm) {
        List<FuelType> fuelTypes = realm.where(FuelType.class).findAll();
        StringBuilder sb = new StringBuilder();

        for (FuelType fuelType :fuelTypes) {
            sb.append("{");
            sb.append("\"id\":\""+fuelType.getId()+"\",");
            sb.append("\"fuelType\":\""+fuelType.getFuelType()+"\"");
            sb.append("}");
            fuelTypeTxt.log(sb.toString());
            sb = new StringBuilder();
        }
    }

    private void createPidData(Realm realm) {
        List<Pid> pids = realm.where(Pid.class).findAll();
        StringBuilder sb = new StringBuilder();
        for (Pid pid : pids) {
            sb.append("{");
            sb.append("\"id\":"+pid.getId()+",");
            sb.append("\"pidCommand\":"+pid.getPidCommand()+",");
            sb.append("\"pidResult\":"+pid.getPidResult()+",");
            sb.append("\"pidType\":"+pid.getPidType()+",");
            sb.append("\"pidDesc\":"+pid.getPidDesc());
            sb.append("}");
            pidTxt.log(sb.toString());
            sb = new StringBuilder();
        }
    }

    private void createVariantData(Realm realm) {

        List<Variant> variants = realm.where(Variant.class).findAll();
        StringBuilder sb = new StringBuilder();
        for (Variant variant :variants) {
            sb.append("{");
            sb.append("\"id\":\""+variant.getId()+"\",");
            sb.append("\"name\":\""+variant.getName()+"\",");
            sb.append("\"modelId\":\""+variant.getModelId()+"\",");
            sb.append("\"fuelType\":\""+variant.getFuelType()+"\",");
            sb.append("\"transmissionType\":\""+variant.getTransmissionType()+"\",");
            sb.append("\"numOfGears\":"+variant.getNumOfGears()+",");
            sb.append("\"supportedObdProtocol\":\""+variant.getSupportedObdProtocol()+"\"");
            sb.append("}");
            variantTxt.log(sb.toString());
            sb = new StringBuilder();
        }
    }

    private void createModelData(Realm realm) {
        List<Model> models = realm.where(Model.class).findAll();
        StringBuilder sb = new StringBuilder();
        for (Model model : models) {
            sb.append("{");
            sb.append("\"id\":\""+model.getId()+"\",");
            sb.append("\"makeId\":\""+model.getMakeId()+"\",");
            sb.append("\"name\":\""+model.getName()+"\"");
            sb.append("}");
            modelTxt.log(sb.toString());
            sb = new StringBuilder();
        }
    }

    private void createEngineVitalsData(Realm realm) {
        List<EngineVitals> engineVitalses = realm.where(EngineVitals.class).findAll();
        StringBuilder sb = new StringBuilder();
        for (EngineVitals engineVitals : engineVitalses) {
            sb.append("{");
            sb.append("\"id\":"+engineVitals.getId()+",");
            sb.append("\"variantId\":"+engineVitals.getVariantId()+",");
            sb.append("}");
            enginVitalsTxt.log(sb.toString());
            sb = new StringBuilder();
        }
    }

    private void createMakeData(Realm realm) {

        List<Make> makes = realm.where(Make.class).findAll();

        StringBuilder sb = new StringBuilder();
        for (Make make:makes) {
            sb.append("{");
            sb.append("\"id\":"+make.getId()+",");
            sb.append("\"name\":\""+make.getName().toString()+"\",");
            if(make.getOrigin()==null || make.getOrigin().equals("")) {
                sb.append("\"origin\":\"\",");
            } else {
                sb.append("\"origin\":"+make.getOrigin()+",");
            }

            if(make.getVinBegin()==null || make.getVinBegin().equals("")) {
                sb.append("\"vinBegin\":\"\"");
            } else {
                sb.append("\"vinBegin\":"+make.getVinBegin());
            }

            sb.append("}");
            makeTxt.log(sb.toString());
            sb = new StringBuilder();
        }
    }
}
