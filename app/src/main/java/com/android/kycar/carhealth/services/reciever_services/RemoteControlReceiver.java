package com.android.kycar.carhealth.services.reciever_services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.android.kycar.carhealth.NxtApplication;
import com.android.kycar.carhealth.beans.Coordinates;
import com.android.kycar.carhealth.entities.UserContact;
import com.android.kycar.carhealth.entities.UserContactSetting;
import com.android.kycar.carhealth.repositories.user.UserContactRepo;
import com.android.kycar.carhealth.repositories.user.UserContactSettingRepo;
import com.android.kycar.carhealth.services.io_services.SmsAlerts;
import com.android.kycar.carhealth.utils.BusProvider;

import java.util.List;

/**
 * This class will handle all the A2DP and headphone events
 */
public class RemoteControlReceiver extends BroadcastReceiver {
    static int d = 0;

    private Context context;
    public void sendSos(Context context) {
        UserContactRepo userContactRepo = new UserContactRepo(context, UserContact.class);
        List<UserContact> userContacts = userContactRepo.findAll();

        UserContactSettingRepo userContactSettingRepo = new UserContactSettingRepo(context, UserContactSetting.class);
        for(UserContact userContact: userContacts) {
            UserContactSetting userContactSetting =  userContactSettingRepo.findByUserContactId(userContact.getId());
            if(userContactSetting.getIsSos()) {
                SmsAlerts smsAlerts = new SmsAlerts();
                smsAlerts.sendSosMessage(userContact.getNumber(), new Coordinates(NxtApplication.currLocation));
                Toast.makeText(context,"Sos sent", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.d("hhhhjh", "received intent for action : " + intent.getAction());
        this.context = context;
        if (Intent.ACTION_MEDIA_BUTTON.equals(intent.getAction())) {
        }


        final KeyEvent event = intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
        if (event.getAction() != KeyEvent.ACTION_DOWN) return;

        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_MEDIA_STOP:
                Toast.makeText(context, "A2dp is stop", Toast.LENGTH_SHORT).show();
                break;
            case KeyEvent.KEYCODE_HEADSETHOOK:

                d++;
                Handler handler = new Handler();
                Runnable r = new Runnable() {

                    @Override
                    public void run() {
                        // single click *******************************
                        if (d == 1) {
                            Toast.makeText(context, "single click!", Toast.LENGTH_SHORT).show();
                        }
                        // double click *********************************
                        if (d == 2) {
                            Toast.makeText(context, "Double click!!", Toast.LENGTH_SHORT).show();
                        }
                        if (d > 4) {

                            sendSos(context);
                            Toast.makeText(context, "SOS sent", Toast.LENGTH_SHORT).show();
                        }
                        d = 0;
                    }
                };
                if (d == 1) {
                    handler.postDelayed(r, 4000);
                }

                //listener.onAudioInput("Nexth");
                BusProvider.getInstance().post("Nexth");
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                break;
            case KeyEvent.KEYCODE_MEDIA_NEXT:
                Toast.makeText(context, "A2dp is next", Toast.LENGTH_SHORT).show();
                BusProvider.getInstance().post("Nexth");
                //listener.onAudioInput("Next");
                break;
            case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                Toast.makeText(context, "A2dp is previous", Toast.LENGTH_SHORT).show();
               // listener.onAudioInput("Previous");
                break;
        }

    }
}