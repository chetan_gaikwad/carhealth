package com.android.kycar.carhealth.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.ConfigActivity;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.services.utils.LogCSVWriter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;


// Some code taken from https://github.com/barbeau/gpstest

public class NonOBDActivity extends BaseActivity implements LocationListener, GpsStatus.Listener {

    private String MQTTChannel = "9812739N8097E34789247X48209348G420938E8342093N48320984";

    private String tripID= Util.getTime();

    private GoogleMap mMap;
    boolean mGpsIsStarted = false;
    private LocationManager mLocService;
    private LocationProvider mLocProvider;
    private LogCSVWriter myCSVWriter;
    private Location mLastLocation;
    /// the trip log

    double mLat,mLong;


    //private SpeedometerGauge speedometer;

    private Context context;

    private ImageView gpsStatus;

    private boolean gpsInit() {
        mLocService = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (mLocService != null) {
            mLocProvider = mLocService.getProvider(LocationManager.GPS_PROVIDER);
            if (mLocProvider != null) {
                mLocService.addGpsStatusListener(this);
                if (mLocService.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    //gpsStatus.setText(getString(R.string.status_gps_ready));
                    return true;
                }
            }
        }
        return false;
    }
   /* private void uploadToFirebase(int child,String value){

        String key = "";
        String carid = preferences.getString(Util.CAR_PREFERENCE,"");
        Log.d("Car id",carid);
        String payload = "";
        switch (child){
            case RPM_CONST:
                key = MQTTChannel+"/rpm";
                payload="{\"data\":\"rpm\",\"car_id\":\""+carid+"\",\"rpm\":\""+value+"\",\"trip_id\":\""+tripID+"\",\"ts\":\""+Util.getTime()+"\"}";
                break;

            case SPEED_CONST:
                key = MQTTChannel+"/speed";
                payload="{\"data\":\"speed\",\"car_id\":\""+carid+"\",\"speed\":\""+value+"\",\"trip_id\":\""+tripID+"\",\"ts\":\""+Util.getTime()+"\"}";
                break;

            case GPS_CONST:
                key = MQTTChannel+"/gps";
                payload="{\"data\":\"gps\",\"car_id\":\""+carid+"\",\"gps\":\""+value+"\",\"trip_id\":\""+tripID+"\",\"ts\":\""+Util.getTime()+"\"}";
                break;
            default:
                break;
        }
        try {
            // Send a message to a topic
            connection.publish(key, payload.getBytes(), QoS.AT_LEAST_ONCE, false, new Callback<Void>() {
                public void onSuccess(Void v) {
                   // Toast.makeText(EngineVitalsActivity.this,"jfek",Toast.LENGTH_SHORT).show();
                    Log.d("mqtt","Successfully published");

                }

                public void onFailure(Throwable value) {

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        gpsStatus = (ImageView)findViewById(R.id.gps_id);
        gpsStatus.setOnClickListener(clickListener);

        final LocationManager manager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if ( manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            gpsStatus.setImageResource(R.drawable.gps_on);
        }


        configureSpeedtoMeter();

        context = this.getApplicationContext();
        // create a log instance for use by this application
        setUpMapIfNeeded();
    }


    private void configureSpeedtoMeter(){
        //speedometer = (SpeedometerGauge) findViewById(R.id.speedometer);
        //speedometer.setLabelConverter(new SpeedometerGauge.LabelConverter() {
          /*  @Override
            public String getLabelFor(double progress, double maxProgress) {
                return String.valueOf((int) Math.round(progress));
            }
        });
        // configure value range and ticks
        speedometer.setMaxSpeed(300);
        speedometer.setMajorTickStep(30);
        speedometer.setMinorTicks(2);

        // Configure value range colors
        speedometer.addColoredRange(30, 140, Color.GREEN);
        speedometer.addColoredRange(140, 180, Color.YELLOW);
        speedometer.addColoredRange(180, 300, Color.RED);*/

    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mLocService != null) {
            mLocService.removeGpsStatusListener(this);
            mLocService.removeUpdates(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    protected void onResume() {
        super.onResume();

        gpsInit();

    }

    private void updateConfig() {
        startActivity(new Intent(this, ConfigActivity.class));
    }

    public void
    onLocationChanged(Location location) {

        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);


        mLat = latitude;
        mLong = longitude;

        mLastLocation = location;
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        mMap.addMarker(new MarkerOptions().position(latLng).title("").draggable(false)
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.car_top_view))
        );

        float speed = location.getSpeed()*(18/5);

        //uploadToFirebase(GPS_CONST,speed+"");
        //uploadToFirebase(LAT, latitude + "");
        //uploadToFirebase(LONG, longitude + "");

        //draw current taken route
        PolylineOptions options = new PolylineOptions();

        options.color(Color.parseColor("#CC0000FF"));
        options.width(5);
        options.visible(true);

            options.add( new LatLng( latitude,
                    longitude ) );

        mMap.addPolyline(options);

    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onProviderDisabled(String provider) {
    }

    public void onGpsStatusChanged(int event) {

        switch (event) {
            case GpsStatus.GPS_EVENT_STARTED:
                //gpsStatus.setText(getString(R.string.status_gps_started));
                gpsStatus.setImageResource(R.drawable.gps_on);
                break;
            case GpsStatus.GPS_EVENT_STOPPED:
                //gpsStatus.setText(getString(R.string.status_gps_stopped));
                gpsStatus.setImageResource(R.drawable.gps_off);
                break;
            case GpsStatus.GPS_EVENT_FIRST_FIX:
                //gpsStatus.setText(getString(R.string.status_gps_fix));
                break;
            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                break;
        }
    }

    private synchronized void gpsStart() {
        if (!mGpsIsStarted && mLocProvider != null && mLocService != null && mLocService.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            mLocService.requestLocationUpdates(mLocProvider.getName(), 0, 0, this);
            mGpsIsStarted = true;
        } else if (mGpsIsStarted && mLocProvider != null && mLocService != null) {
        } else {
            //gpsStatus.setText(getString(R.string.status_gps_no_support));
        }
    }

    private synchronized void gpsStop() {
        if (mGpsIsStarted) {
            mLocService.removeUpdates(this);
            mGpsIsStarted = false;
            //gpsStatus.setText(getString(R.string.status_gps_stopped));
        }
    }
    /*Map activities*/

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() throws SecurityException {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.

            /*mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();*/
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }

        // mMap = supportMapFragment.getMap();
        mMap.setMyLocationEnabled(true);
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager.getLastKnownLocation(bestProvider);
        if (location != null) {
            onLocationChanged(location);
        }
        locationManager.requestLocationUpdates(bestProvider, 0, 0, this);
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
        //mMap.addMarker(new MarkerOptions().position(new LatLng(-57.821355, 128.496094)).title("Marker").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher)));
    }


    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           if(v.getId() == R.id.gps_id){
                final LocationManager manager = (LocationManager) getSystemService(LOCATION_SERVICE);

                if ( manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                }else{
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            }
        }
    };
}