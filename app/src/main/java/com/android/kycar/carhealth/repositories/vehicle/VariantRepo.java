package com.android.kycar.carhealth.repositories.vehicle;

import android.content.Context;


import com.android.kycar.carhealth.entities.vehicle_master.Variant;
import com.android.kycar.carhealth.repositories.Repository;

import java.util.List;

import io.realm.exceptions.RealmException;

/**
 * Created by Krishna on 2/1/2016.
 */
public class VariantRepo extends Repository {
    public VariantRepo(Context context, Class _aClass) {
        super(context, _aClass);
    }

    public Variant findByName(String name) throws RealmException {
        Variant variant = realm.where(Variant.class).equalTo("name", name).findFirst();
        return variant;
    }

    public List<Variant> findByModelId(String modelId) throws RealmException {

        List<Variant> variants = realm.where(Variant.class).equalTo("modelId", modelId).findAll();
        return variants;
    }

}
