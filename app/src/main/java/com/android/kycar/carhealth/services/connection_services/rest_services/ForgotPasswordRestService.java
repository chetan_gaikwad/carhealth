package com.android.kycar.carhealth.services.connection_services.rest_services;

import org.json.JSONObject;

/**
 * Created by Krishna on 10/25/2016.
 */
public class ForgotPasswordRestService extends RestService {

    public String getForgotPasswordLink(JSONObject jsonObject) {

        String url = "/forget/password";
        return restServiceCall(url, "POST", jsonObject);
    }
}
