package com.android.kycar.carhealth.repositories.vehicle;

import android.content.Context;

import com.android.kycar.carhealth.entities.vehicle_master.EngineVitals;
import com.android.kycar.carhealth.repositories.Repository;

/**
 * Created by Krishna on 10/28/2016.
 */
public class EngineVitalsRepo extends Repository<EngineVitals> {
    public EngineVitalsRepo(Context context, Class _aClass) {
        super(context, _aClass);
    }

    public EngineVitals findByUserAndVariantId(String variantId, String userId) {

        return realm.where(EngineVitals.class).equalTo("variantId", variantId).findFirst();
        //return realm.where(EngineVitals.class).equalTo("variantId", variantId).equalTo("userId", userId).findFirst();
    }
}
