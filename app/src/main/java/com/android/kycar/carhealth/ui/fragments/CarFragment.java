package com.android.kycar.carhealth.ui.fragments;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.beans.MediaTechAngle;
import com.android.kycar.carhealth.utils.BusProvider;
import com.squareup.otto.Subscribe;


/**
 * Created by Krishna on 1/30/2016.
 */
public class CarFragment extends Fragment {

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_car, container,false);
        return view;
    }

    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            /*if(isMyServiceRunning(MediaTekService.class)){
                (view.findViewById(R.id.mediatek_status)).setVisibility(View.GONE);
            } else {
                (view.findViewById(R.id.mediatek_status)).setVisibility(View.VISIBLE);
                ((TextView)view.findViewById(R.id.x_pos)).setText("- - -");
                ((TextView)view.findViewById(R.id.x_neg)).setText("- - -");
                ((TextView)view.findViewById(R.id.y_pos)).setText("- - -");
                ((TextView)view.findViewById(R.id.y_neg)).setText("- - -");
            }*/
        }
    }

    /**
     * Check if mediatek service is running
     * @param serviceClass
     * @return
     */
    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onResume(){
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause(){
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void onMotion(MediaTechAngle s){

        int angleX = s.getAngleX();
        int angley = s.getAngleY();

        if (angleX <= 90) {
            //X positive
            ((TextView)view.findViewById(R.id.x_pos)).setText("" + (90 - angleX));
            ((TextView)view.findViewById(R.id.x_pos))
                    .setTextSize(((90 - angleX)/2)>15 ? ((90 - angleX)/2) : 15);

        } else {
            //X negative
            ((TextView)view.findViewById(R.id.x_neg)).setText("" + (angleX - 90));
            ((TextView)view.findViewById(R.id.x_neg))
                    .setTextSize(((angleX - 90)/2)>15 ? ((angleX - 90)/2) : 15);
        }

        if (angley <= 90) {
            //Y positive
            ((TextView)view.findViewById(R.id.y_pos)).setText("" + (90 - angley));
            ((TextView)view.findViewById(R.id.y_pos))
                    .setTextSize(((90 - angley)/2)>15 ? ((90 - angley)/2) : 15);
        } else {
            //Y negative
            ((TextView)view.findViewById(R.id.y_neg)).setText("" + (angley - 90));
            ((TextView)view.findViewById(R.id.y_neg))
                    .setTextSize(((angley - 90)/2)>15 ? ((angley - 90)/2) : 15);
        }
    }
}
