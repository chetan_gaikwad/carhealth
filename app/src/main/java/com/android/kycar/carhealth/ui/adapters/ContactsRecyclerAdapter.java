package com.android.kycar.carhealth.ui.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.beans.ContactData;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.repositories.user.ContactBean;

import java.util.ArrayList;
import java.util.List;

public class ContactsRecyclerAdapter extends RecyclerView.Adapter<ContactsRecyclerAdapter.ViewHolder> {

    private List<ContactBean> contactBeanList;
    private Activity activity;
    private ViewHolder viewHolder;
    public ContactsRecyclerAdapter(Activity activity, List<ContactBean> contactBeanList) {
        this.contactBeanList = contactBeanList;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        //inflate your layout and pass it to view holder
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.contact_selector_row, viewGroup, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder,final int position) {

        //setting data to view holder elements
        if(contactBeanList.get(position).getName()!=null) {
            viewHolder.tvContactName.setText(contactBeanList.get(position).getName());
        }
        viewHolder.tvContactNumber.setText(contactBeanList.get(position).getNumber());
        viewHolder.llRow.setTag(position);
    }

    @Override
    public int getItemCount() {
        return (null != contactBeanList ? contactBeanList.size() : 0);
    }

    public void returnData(ContactBean contactBean) {
        Intent result = new Intent();
        ContactData contactData = new ContactData("","","",contactBean.getName(),contactBean.getNumber(),"",false);
        ArrayList<ContactData> results = new ArrayList<ContactData>();
        results.add(contactData);
        result.putParcelableArrayListExtra(ContactData.CONTACTS_DATA, results);
        activity.setResult(Activity.RESULT_OK, result);
        activity.finish();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvContactName;
        private TextView tvContactNumber;
        private LinearLayout llRow;

        public ViewHolder(View view) {
            super(view);
            tvContactName = (TextView) view.findViewById(R.id.tvName);
            tvContactNumber = (TextView) view.findViewById(R.id.tvNumber);
            llRow = (LinearLayout) view.findViewById(R.id.row);

            llRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    new AlertDialog.Builder(v.getContext())
                            .setTitle("Confirm ")
                            .setMessage("Do you really want to add this contact to your people?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    if(contactBeanList
                                            .get(Integer.parseInt(llRow.getTag().toString()))
                                            .getNumber().length() >= 10) {
                                        returnData(contactBeanList.get(Integer.parseInt(llRow.getTag().toString())));
                                    }else{
                                        Util.showAlert("Mobile number should be greater than or equal to 10 digit",activity);
                                    }
                                }})
                            .setNegativeButton(android.R.string.no, null).show();
                }
            });
        }
    }
}