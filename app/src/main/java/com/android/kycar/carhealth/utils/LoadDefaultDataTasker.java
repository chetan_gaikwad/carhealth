package com.android.kycar.carhealth.utils;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.services.utils.LoadDefaultData;

/**
 * Created by Krishna on 1/31/2016.
 */
public class LoadDefaultDataTasker extends AsyncTask<String, Void, String> {

    private Activity context;
    public LoadDefaultDataTasker(Activity context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... cred) {
        Log.d("Tasker", "Loading default data");
        new LoadDefaultData().loadDefaultData(context);
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        SharedPreferences preferences;
        SharedPreferences.Editor editor;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = preferences.edit();
        editor.putBoolean(Util.IS_DEFAULT_DATA_LOADED, true);
        editor.commit();
        Log.d("Tasker", "Default data loaded");
    }
}

