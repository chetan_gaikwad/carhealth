package com.android.kycar.carhealth.entities.vehicle_master;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 1/16/2016.
 */
@RealmClass
public class FuelType extends RealmObject {

    @PrimaryKey
    private String id;

    private String fuelType;

    public FuelType() {
    }

    public FuelType(String id, String fuelType) {
        this.id = id;
        this.fuelType = fuelType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }
}
