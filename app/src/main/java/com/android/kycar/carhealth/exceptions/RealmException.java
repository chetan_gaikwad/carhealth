package com.android.kycar.carhealth.exceptions;

import android.util.Log;

/**
 * Created by Krishna on 5/10/2016.
 */
public class RealmException extends Exception {

    public RealmException() {
        Log.e("RealmException", "Unknown exception occurred ");
    }

    public RealmException(String detailMessage) {
        super(detailMessage);
        Log.e("RealmException", detailMessage);
    }

    public RealmException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
        Log.e("RealmException", detailMessage);
    }

    public RealmException(Throwable throwable) {
        super(throwable);
        Log.e("RealmException", "Unknown exception occurred ");
    }
}
