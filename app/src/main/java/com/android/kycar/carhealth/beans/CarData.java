package com.android.kycar.carhealth.beans;

public class CarData {
  public int rpm;
  public int speed;

  public CarData(int rpm, int speed) {
    this.rpm = rpm;
    this.speed = speed;
  }

  public int getRpm() {
    return rpm;
  }

  public int getSpeed() {
    return speed;
  }
}