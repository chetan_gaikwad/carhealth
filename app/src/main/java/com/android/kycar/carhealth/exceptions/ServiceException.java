package com.android.kycar.carhealth.exceptions;

/**
 * Created by Krishna on 1/30/2016.
 */
public class ServiceException extends Exception {

    public ServiceException() {
        // TODO Auto-generated constructor stub
    }

    public ServiceException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public ServiceException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }
}
