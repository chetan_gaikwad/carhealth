package com.android.kycar.carhealth.beans;

/**
 * Created by krishna on 6/12/15.
 */
public class People {

    private String id;
    private String name;
    private String number;
    private String photo;
    private boolean sos;
    private boolean autoConnect;
    private boolean shareLocation;

    public boolean isSos() {
        return sos;
    }

    public void setSos(boolean sos) {
        this.sos = sos;
    }

    public boolean isAutoConnect() {
        return autoConnect;
    }

    public void setAutoConnect(boolean autoConnect) {
        this.autoConnect = autoConnect;
    }

    public boolean isShareLocation() {
        return shareLocation;
    }

    public void setShareLocation(boolean shareLocation) {
        this.shareLocation = shareLocation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public People() {

    }

    public People(String id, String name, String number, String photo, boolean sos, boolean shareLocation, boolean autoConnect) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.photo = photo;
        this.sos = sos;
        this.autoConnect = autoConnect;
        this.shareLocation = shareLocation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
