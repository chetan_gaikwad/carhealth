package com.android.kycar.carhealth.ui.activity.drawer_menu.my_people.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.kycar.carhealth.NxtApplication;
import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.beans.Coordinates;
import com.android.kycar.carhealth.beans.People;
import com.android.kycar.carhealth.config.ProgessDialogBox;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.UserContact;
import com.android.kycar.carhealth.entities.UserContactSetting;
import com.android.kycar.carhealth.repositories.user.UserContactRepo;
import com.android.kycar.carhealth.repositories.user.UserContactSettingRepo;
import com.android.kycar.carhealth.services.connection_services.mqtt_paho_android_service.Connection;
import com.android.kycar.carhealth.services.connection_services.rest_services.FcmRestCallService;
import com.android.kycar.carhealth.services.connection_services.rest_services.UserContactsRestService;
import com.android.kycar.carhealth.services.io_services.SmsAlerts;
import com.android.kycar.carhealth.ui.activity.drawer_menu.convoy.ConvoyMapsActivity;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Krishna on 2/1/2016.
 */
public class MyPeopleRecyclerAdapter  extends RecyclerView.Adapter<MyPeopleRecyclerAdapter.ViewHolder> {

    private Context context;
    private ProgessDialogBox dialogBox;
    private List<People> peoples;
    private Activity activity;
    private boolean selectPeople;
    private UserContactSettingRepo userContactSettingRepo;
    private UserContactRepo userContactRepo;
    private SmsAlerts smsAlerts;
    private SharedPreferences preferences;
    private String userContactId;
    private static String[] PERMISSIONS_SMS = {
            Manifest.permission.SEND_SMS
    };

    public MyPeopleRecyclerAdapter(Activity activity, List<People> peoples, boolean formSelectPeople) {
        this.peoples = peoples;
        this.activity = activity;
        this.selectPeople = formSelectPeople;
        userContactSettingRepo = new UserContactSettingRepo(activity.getApplicationContext(), UserContactSetting.class);
        userContactRepo = new UserContactRepo(activity.getApplicationContext(), UserContact.class);
        smsAlerts = new SmsAlerts();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        //inflate your layout and pass it to view holder

        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.item_recycler_contact, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        context = viewGroup.getContext();
        dialogBox = new ProgessDialogBox(viewGroup.getContext());
        preferences = PreferenceManager.getDefaultSharedPreferences(viewGroup.getContext());
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        //setting data to view holder elements
        viewHolder.tvName.setText(peoples.get(position).getName());
        viewHolder.tvNumber.setText(peoples.get(position).getNumber());
        viewHolder.swtSos.setChecked(peoples.get(position).isSos());
        viewHolder.swtSos.setTag(position);
        viewHolder.swtAutoConnect.setChecked(peoples.get(position).isAutoConnect());
        viewHolder.swtAutoConnect.setTag(position);
        viewHolder.butDeleteContacts.setTag(position);

        viewHolder.swtLocationSharing.setChecked(peoples.get(position).isShareLocation());
        viewHolder.swtLocationSharing.setTag(position);
        //viewHolder.container.setOnClickListener(onContactClickListener(position));
    }

    private void setDataToView(TextView name, TextView number, int position) {
        if(peoples.get(position).getName()!=null) {
            name.setText(peoples.get(position).getName());
        }

        number.setText(peoples.get(position).getNumber());
    }

    @Override
    public int getItemCount() {
        return (null != peoples ? peoples.size() : 0);
    }


    private View.OnClickListener onContactClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new android.app.AlertDialog.Builder(v.getContext())
                        .setTitle("Confirm")
                        .setMessage("View "+peoples.get(position).getName()+" on map?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                Intent intent = new Intent(activity, ConvoyMapsActivity.class);
                                intent.putExtra("people_id","");
                                intent.putExtra("people_name",peoples.get(position).getName());
                                activity.startActivity(intent);
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
            }
        };
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgPhoto;
        private TextView tvName;
        private TextView tvNumber;
        private Switch swtSos;
        private Switch swtAutoConnect;
        private Switch swtLocationSharing;
        private Button butDeleteContacts;
        private View container;

        public ViewHolder(final View view) {
            super(view);
            imgPhoto = (ImageView) view.findViewById(R.id.image);
            tvName = (TextView) view.findViewById(R.id.name);
            tvNumber = (TextView) view.findViewById(R.id.number);
            swtSos = (Switch) view.findViewById(R.id.swt_sos);
            swtAutoConnect = (Switch) view.findViewById(R.id.swt_auto_connect);
            swtLocationSharing = (Switch) view.findViewById(R.id.swt_location_share);
            butDeleteContacts = (Button) view.findViewById(R.id.butDeleteContacts);

            swtSos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if(buttonView.getTag() == null){return;}

                    peoples.get((Integer)buttonView.getTag()).setSos(swtSos.isChecked());

                    UserContactSettingRepo userContactSettingRepo1 = new UserContactSettingRepo(activity.getApplicationContext(), UserContactSetting.class);
                    UserContactSetting userContactSetting = userContactSettingRepo1.findByUserContactId(peoples.get((Integer) buttonView.getTag()).getId());

                    UserContactSetting userContactSetting1 = new UserContactSetting(userContactSetting);
                    if(userContactSetting != null){
                        userContactSetting1.setId(userContactSetting.getId());
                    } else {
                        userContactSetting1.setId(Util.getTimeLong()+"");
                    }
                    userContactSetting1.setUserContactId(peoples.get((Integer) buttonView.getTag()).getId());
                    userContactSetting1.setIsSos(swtSos.isChecked());
                    userContactSetting1.setIsAutoConnect(swtAutoConnect.isChecked());
                    userContactSetting1.setShareLocation(swtLocationSharing.isChecked());

                    userContactSettingRepo1.saveOrUpdate(userContactSetting1);

                    /** Confirmation for sending the S*/
                    if(swtSos.isChecked()) {

                        new AlertDialog.Builder(view.getContext())
                                .setTitle("Notify")
                                .setMessage("Do want to notify "+tvName.getText()+" by SMS?")
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        /** Checking sms send permission */
                                        if(ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                                            smsAlerts.sendDummySmsNotificationAlert(tvNumber.getText().toString(), new Coordinates(NxtApplication.currLocation));
                                        } else {
                                            verifySmsPermissions(activity);
                                        }
                                    }})
                                .setNegativeButton(android.R.string.no, null).show();
                    }
                }
            });

            swtAutoConnect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if(buttonView.getTag() == null){return;}

                    peoples.get((Integer)buttonView.getTag()).setAutoConnect(swtAutoConnect.isChecked());

                    UserContactSettingRepo userContactSettingRepo1 = new UserContactSettingRepo(activity.getApplicationContext(), UserContactSetting.class);
                    UserContactSetting userContactSetting = userContactSettingRepo1.findByUserContactId(peoples.get((Integer) buttonView.getTag()).getId());
                    UserContactSetting userContactSetting1 = new UserContactSetting();
                    if(userContactSetting != null){
                        userContactSetting1.setId(userContactSetting.getId());
                    } else {
                        userContactSetting1.setId(Util.getTimeLong()+"");
                    }
                    userContactSetting1.setUserContactId(peoples.get((Integer) buttonView.getTag()).getId());
                    userContactSetting1.setIsAutoConnect(swtAutoConnect.isChecked());
                    userContactSetting1.setIsSos(swtSos.isChecked());
                    userContactSetting1.setShareLocation(swtLocationSharing.isChecked());
                    userContactSettingRepo1.saveOrUpdate(userContactSetting1);
                }
            });

            swtLocationSharing.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    Log.d(MyPeopleRecyclerAdapter.class.getSimpleName(), "shared location clicked");
                    if(buttonView.getTag() == null){return;}

                    Log.d(MyPeopleRecyclerAdapter.class.getSimpleName(), " UserContactSetting saved");

                    if(swtLocationSharing.isChecked()) {
                        preferences.edit().putBoolean(Util.IS_SHARE_LOC, true).commit();
                        UserContactRepo userContactRepo = new UserContactRepo(buttonView.getContext(), UserContact.class);
                        UserContact userContact = (UserContact) userContactRepo.findById(peoples.get((Integer) buttonView.getTag()).getId());
                        userContactId = peoples.get((Integer) buttonView.getTag()).getId();

                        if(userContact.getUserContactTopic() == null || userContact.getUserContactTopic().equals("{}")) {

                            new UserContactTopicRestCallAsycTask().execute(userContact.getNumber());
                            swtLocationSharing.setChecked(false);

                        } else {
                            try {
                                //Toast.makeText(container.getContext(), "Session Started on "+userContact.getUserContactTopic(), Toast.LENGTH_SHORT).show();

                                JSONObject msgObj = new JSONObject();
                                try {
                                    msgObj.put("ss", true);
                                    msgObj.put("nm",preferences.getString(Util.USERNAME_PREFERENCE, "John"));
                                    if(NxtApplication.currLocation == null || NxtApplication.currLocation.getLatitude() == 0) {
                                        msgObj.put("gs", false);
                                        Toast.makeText(container.getContext(), "Your Location not available, Please check GPS", Toast.LENGTH_SHORT).show();
                                    } else {
                                        msgObj.put("la", NxtApplication.currLocation.getLatitude());
                                        msgObj.put("lo", NxtApplication.currLocation.getLongitude());
                                        msgObj.put("ac", NxtApplication.currLocation.getAccuracy());
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                Log.d(MyPeopleRecyclerAdapter.class.getSimpleName()+": ", ""+NxtApplication.currLocation.getLatitude()+", "+NxtApplication.currLocation.getLongitude());
                                Connection.getClient().publish(NxtApplication.baseTopic+"/"+userContact.getUserContactTopic(), msgObj.toString().getBytes(), 0 , true);

                                new sendNotificationAsyncTask().execute(userContact.getUserContactTopic(), preferences.getString(Util.USERNAME_PREFERENCE, ""));

                                new AlertDialog.Builder(view.getContext())
                                        .setTitle("Confirm")
                                        .setMessage("Just to be sure, do want to send SMS to notify "+tvName.getText()+"?")
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                            public void onClick(DialogInterface dialog, int whichButton) {

                                                /** Checking sms send permission */
                                                if(ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                                                    smsAlerts.sentLocationSharingSms(tvNumber.getText().toString());
                                                } else {
                                                    verifySmsPermissions(activity);
                                                }
                                            }})
                                        .setNegativeButton(android.R.string.no, null).show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        UserContactRepo userContactRepo = new UserContactRepo(buttonView.getContext(), UserContact.class);
                        UserContact userContact = (UserContact) userContactRepo.findById(peoples.get((Integer) buttonView.getTag()).getId());
                        NxtApplication.updateShareLocationUsers(buttonView.getContext());
                        try {
                            JSONObject msgObj = new JSONObject();
                            try {
                                msgObj.put("se", true);
                                msgObj.put("nm",preferences.getString(Util.USERNAME_PREFERENCE, ""));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Connection.getClient().publish(NxtApplication.baseTopic+"/"+userContact.getUserContactTopic(), msgObj.toString().getBytes(), 0 , true);
                        } catch (MqttException e) {
                            e.printStackTrace();
                        }
                    }

                    userContactSettingRepo = new UserContactSettingRepo(activity.getApplicationContext(), UserContactSetting.class);
                    UserContactSetting userContactSetting = userContactSettingRepo.findByUserContactId(peoples.get((Integer) buttonView.getTag()).getId());
                    UserContactSetting userContactSetting1 = new UserContactSetting();
                    if(userContactSetting != null){
                        userContactSetting1.setId(userContactSetting.getId());
                    } else {
                        userContactSetting1.setId(Util.getTimeLong()+"");
                    }
                    userContactSetting1.setUserContactId(peoples.get((Integer) buttonView.getTag()).getId());
                    userContactSetting1.setIsAutoConnect(swtAutoConnect.isChecked());
                    userContactSetting1.setIsSos(swtSos.isChecked());
                    userContactSetting1.setShareLocation(swtLocationSharing.isChecked());
                    userContactSettingRepo.saveOrUpdate(userContactSetting1);
                    NxtApplication.updateShareLocationUsers(buttonView.getContext());
                }
            });

            butDeleteContacts.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(v.getTag() == null){return;}
                    final int tag = (Integer) v.getTag();
                    new AlertDialog.Builder(view.getContext())
                            .setTitle("Confirm")
                            .setMessage("Remove contact?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {

                                    UserContactSetting userContactSetting = userContactSettingRepo.findByUserContactId(peoples.get(tag).getId());
                                    if(userContactSetting != null) {
                                        userContactSettingRepo.deleteById(userContactSetting.getId());
                                    }

                                    UserContactRepo userContactRepo1 = new UserContactRepo(activity.getApplicationContext(), UserContact.class);
                                    userContactRepo1.deleteById(peoples.get(tag).getId());
                                    peoples.remove(tag);
                                    notifyDataSetChanged();
                                }})
                            .setNegativeButton(android.R.string.no, null).show();
                }
            });
            container = view.findViewById(R.id.card_view);
        }
    }

    private class UserContactTopicRestCallAsycTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogBox.showDailog();
        }

        @Override
        protected String doInBackground(String... strings) {

            String rowNumber = strings[0].replace("-", "").replace(" ", "").replace("+", "").replace("#", "").replace("*", "").replace(".", "");
            String number = rowNumber.substring(rowNumber.length()-10);
            return new UserContactsRestService().findUserContactTopic(number);
        }

        @Override
        protected void onPostExecute(final String result) {
            super.onPostExecute(result);
            if(dialogBox.isShowing()){
                dialogBox.dismissDialog();
            }

            try {
                final JSONObject resultObj = new JSONObject(result);
                if(resultObj.has("user_topic")) {
                    userContactRepo = new UserContactRepo(context, UserContact.class);
                    UserContact userContact = new UserContact((UserContact) userContactRepo.findById(userContactId));
                    userContact.setUserContactTopic(resultObj.getString("user_topic"));
                    Toast.makeText(context, "Contact Updated, Please try again", Toast.LENGTH_LONG).show();
                    userContactRepo.saveOrUpdate(userContact);
                } else {
                    new AlertDialog.Builder(context)
                            .setTitle("User not found")
                            .setMessage("Please invite this contact to use Nxt Gizmo App")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Log.d("Which button", ""+whichButton);
                                    if(whichButton == -1) {
                                        SmsAlerts smsAlerts = new SmsAlerts();
                                        try {
                                            smsAlerts.sendInvitationSms(resultObj.getString("number"));
                                            Toast.makeText(context, "Invitation sent", Toast.LENGTH_SHORT).show();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }})
                            .setNegativeButton(android.R.string.no, null).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class sendNotificationAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... tokens) {

            try{
                return new FcmRestCallService().sendNotification(tokens[0], tokens[1]);
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            if(result!=null) {
                //  Toast.makeText(DashboardActivity.this, "Trip created successfully", Toast.LENGTH_SHORT).show();
            } else  {
                //  Toast.makeText(DashboardActivity.this, "Trip creation failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean verifySmsPermissions(Activity activity) {
        // Check if we have write permission
        int permission = ContextCompat.checkSelfPermission(activity, Manifest.permission.SEND_SMS);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_SMS,
                    1
            );
            return false;
        } else {
            return true;
        }
    }
}