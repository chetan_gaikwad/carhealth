package com.android.kycar.carhealth.services.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Krishna on 9/28/2016.
 */
public class LoggerTxt {

    private File file;
    private FileOutputStream fileOutputStream;
    private PrintWriter pw;
    private String folderPath = "/nxtauto/logger/";

    public LoggerTxt(String folderName) {

        File root = android.os.Environment.getExternalStorageDirectory();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        //File dir = new File (root.getAbsolutePath() + folderPath + calendar.get(Calendar.YEAR)+calendar.get(Calendar.MONTH)+calendar.get(Calendar.DAY_OF_MONTH));

        File dir = new File(root.getAbsolutePath() + folderPath + folderName);
        dir.mkdirs();

        file = new File(dir, "logger"+new Date().getTime()+".txt");
        try {
            fileOutputStream = new FileOutputStream(file);
            pw = new PrintWriter(fileOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LoggerTxt(String folderName, String fileName) {

        File root = android.os.Environment.getExternalStorageDirectory();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        //File dir = new File (root.getAbsolutePath() + folderPath + calendar.get(Calendar.YEAR)+calendar.get(Calendar.MONTH)+calendar.get(Calendar.DAY_OF_MONTH));

        File dir = new File(root.getAbsolutePath() + folderPath + folderName);
        dir.mkdirs();

        file = new File(dir, fileName+".txt");
        try {
            fileOutputStream = new FileOutputStream(file);
            pw = new PrintWriter(fileOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void log(String data) {

        if(pw!= null){
            pw.print(data+ ",\n");
            //pw.println();
            pw.flush();
        }
    }

    public void finalize() {
        if(file.length() == 0) {
            file.delete();
        }
        if(pw!=null) {
            pw.close();
        }
    }

    public void close() {
        pw.close();
    }
}
