package com.android.kycar.carhealth.repositories.vehicle;

import android.content.Context;


import com.android.kycar.carhealth.entities.vehicle_master.Model;
import com.android.kycar.carhealth.repositories.Repository;

import java.util.List;

import io.realm.exceptions.RealmException;

/**
 * Created by Krishna on 2/1/2016.
 */
public class ModelRepo extends Repository {
    public ModelRepo(Context context, Class _aClass) {
        super(context, _aClass);
    }

    public List<Model> findByMakeId(String makeId) throws RealmException {

        List<Model> models = realm.where(Model.class).equalTo("makeId", makeId).findAll();
        return models;
    }

    public Model findByName(String name) throws RealmException {
        return realm.where(Model.class).equalTo("name", name).findFirst();
    }

}
