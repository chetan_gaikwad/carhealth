package com.android.kycar.carhealth.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 8/12/2016.
 */
@RealmClass
public class AppProps extends RealmObject {

    @PrimaryKey
    private String id;

    private double lastLatitude;

    private double lastLongitude;

    private long lastTimestamp;

    private String vehicleId;

    private String userId;

    public AppProps() {
    }

    public AppProps(String id, double lastLatitude, double lastLongitude, long lastTimestamp, String userId, String vehicleId) {
        this.id = id;
        this.lastLatitude = lastLatitude;
        this.lastLongitude = lastLongitude;
        this.lastTimestamp = lastTimestamp;
        this.userId = userId;
        this.vehicleId = vehicleId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getLastLatitude() {
        return lastLatitude;
    }

    public void setLastLatitude(double lastLatitude) {
        this.lastLatitude = lastLatitude;
    }

    public double getLastLongitude() {
        return lastLongitude;
    }

    public void setLastLongitude(double lastLongitude) {
        this.lastLongitude = lastLongitude;
    }

    public long getLastTimestamp() {
        return lastTimestamp;
    }

    public void setLastTimestamp(long lastTimestamp) {
        this.lastTimestamp = lastTimestamp;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }
}
