package com.android.kycar.carhealth.async_task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.android.kycar.carhealth.entities.trip_transaction.Trip;
import com.android.kycar.carhealth.repositories.trip.TripRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.TripRestService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FetchTripsAsyncTask extends AsyncTask<String, Void, String> {

    private Context context;

    public FetchTripsAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
            super.onPreExecute();
        }

    @Override
    protected String doInBackground(String... strings) {

        return new TripRestService().getAllTripsByUserId(strings[0]);
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        /** Trips to realm */

        try {
            JSONArray resObj = new JSONArray(result);

            TripRepo tripRepo = new TripRepo(context, Trip.class);
                for(int counter = 0; counter<resObj.length(); counter++) {
                    JSONObject tripObj = resObj.getJSONObject(counter);
                    Trip trip = new Trip(tripObj);
                    tripRepo.save(trip);

                    new FetchTripsScoreAsyncTask(context).execute(trip.getId());
                    Log.d(FetchTripsAsyncTask.class.getSimpleName(), " Trip saved ");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }