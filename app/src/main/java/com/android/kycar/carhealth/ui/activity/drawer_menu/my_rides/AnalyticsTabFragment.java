package com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides.DriveStatisticsFragments.SensorFragment;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides.DriveStatisticsFragments.SpeedFragment;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides.DriveStatisticsFragments.TemperatureFragment;
import com.google.android.material.tabs.TabLayout;


public class AnalyticsTabFragment extends Fragment {

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 3 ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        /**
         *Inflate tab_layout and setup Views.
         */
            View x =  inflater.inflate(R.layout.tab_layout,null);
            tabLayout = (TabLayout) x.findViewById(R.id.tabs);
            viewPager = (ViewPager) x.findViewById(R.id.viewpager);

        /**
         *Set an Apater for the View Pager
         */
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                    tabLayout.setupWithViewPager(viewPager);
                   }
        });

        return x;
    }

    class MyAdapter extends FragmentPagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position)
        {
          switch (position){
              case 0 : return new SpeedFragment();
              case 1 : return new TemperatureFragment();
              case 2 : return new SensorFragment();
          }
        return null;
        }

        @Override
        public int getCount() {
            return int_items;
        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0 :
                    return "Speed";
                case 1 :
                    return "Temperature";
                case 2 :
                    return "Sensor";
            }
                return null;
        }
    }
}
