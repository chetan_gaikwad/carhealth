package com.android.kycar.carhealth.services.dto.commands.temperature;


import com.android.kycar.carhealth.services.dto.enums.AvailableCommandNames;

/**
 * Engine Coolant Temperature.
 */
public class EngineCoolantTemperatureCommand extends TemperatureCommand {

    /**
     *
     */
    public EngineCoolantTemperatureCommand() {
        super("01 05 1");
    }

    /**
     * @param other a {@link TemperatureCommand} object.
     */
    public EngineCoolantTemperatureCommand(TemperatureCommand other) {
        super(other);
    }

    @Override
    public String getName() {
        return AvailableCommandNames.ENGINE_COOLANT_TEMP.getValue();
    }

}
