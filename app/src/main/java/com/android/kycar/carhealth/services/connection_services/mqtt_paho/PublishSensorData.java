package com.android.kycar.carhealth.services.connection_services.mqtt_paho;

import android.util.Log;


import com.android.kycar.carhealth.beans.Coordinates;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Krishna on 5/3/2016.
 */
public class PublishSensorData{

    String topic;

    /*public boolean getEventSensorData(String userId, String vehicleId, Coordinates coordinates, double intensity, String eventType, int speed, float gpsSpeed, float gpsBearing, int magBearing){

        topic =  baseTopic+"/imu_event_rc/"+userId;
        JSONObject payload = new JSONObject();
        try {
            payload.put("car_id", vehicleId);
            payload.put("data", "rc_data");
            JSONObject rcData = new JSONObject();
            rcData.put("timestamp", new Date());
            rcData.put("location", coordinates.toJSON());
            rcData.put("speed", speed);
            rcData.put("gps_speed", gpsSpeed);
            rcData.put("intensity", intensity);
            rcData.put("event_type", eventType);
            rcData.put("gps_bearing", gpsBearing);
            rcData.put("mag_bearing", magBearing);
            rcData.put("user_id", userId);
            payload.put("rc_data", rcData);
            //Log.d(PublishSensorData.class.getSimpleName(), "Topic:"+topic+" \n Payload:-"+payload.toString());

            MqttMessage message = new MqttMessage(payload.toString().getBytes());
            message.setQos(qos);
            message.setRetained(true);
            mqttClient.publish(baseTopic+"/imu_event_rc/"+userId, message);

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        } catch (MqttException exception) {
            if(exception.getReasonCode() == 32104){
                super.mqttConnect();
            }
            logException(exception);
            exception.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean publishDriverBehaviorEventSensorData(String userId, String vehicleId, Coordinates coordinates, double intensity, String eventType, int speed, float gpsSpeed, float gpsBearing, int magBearing) {

        JSONObject payload = new JSONObject();
        try {
            payload.put("car_id", vehicleId);
            payload.put("data", "db_data");
            JSONObject event_data = new JSONObject();
            event_data.put("timestamp", new Date());
            event_data.put("location", coordinates.toJSON());
            event_data.put("speed", speed);
            event_data.put("gps_speed", gpsSpeed);
            event_data.put("intensity", intensity);
            event_data.put("event_type", eventType);
            event_data.put("gps_bearing", gpsBearing);
            event_data.put("mag_bearing", magBearing);
            event_data.put("user_id", userId);
            payload.put("event_data", event_data);

            Log.d("Event Data", payload.toString());
            MqttMessage message = new MqttMessage(payload.toString().getBytes());
            message.setQos(qos);
            message.setRetained(true);
            mqttClient.publish(topic+"/imu_event_db"+userId, message);

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        } catch (MqttException exception) {
            logException(exception);
            exception.printStackTrace();
            return false;
        }
        return true;
    }
*/
    public MqttMessage getAccSensorData(double ax, double ay, double az){
        JSONObject payload = new JSONObject();
        try {

            JSONObject accData = new JSONObject();
            accData.put("ax", ax);
            accData.put("ay", ay);
            accData.put("az", az);
            payload.put("acc_data", accData);
            payload.put("data", "acc_data");

            MqttMessage message = new MqttMessage(payload.toString().getBytes());
            return message;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public MqttMessage getEventSensorData(String userId, String vehicleId, Coordinates coordinates, double intensity, String eventType, int speed, float gpsSpeed, float gpsBearing, int magBearing, String tripId, String sensorId){

        JSONObject payload = new JSONObject();
        try {

            payload.put("eventData", getEventSensorDataJSONObject(userId,vehicleId,coordinates,intensity,eventType,speed,gpsSpeed,gpsBearing,magBearing,tripId,sensorId));
            payload.put("vehicleId", vehicleId);
            payload.put("data", "eventData");

            MqttMessage message = new MqttMessage(payload.toString().getBytes());
            return message;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject getEventSensorDataJSONObject(String userId, String vehicleId, Coordinates coordinates, double intensity, String eventType, int speed, float gpsSpeed, float gpsBearing, int magBearing, String tripId, String sensorId){

        JSONObject eventData = new JSONObject();

        try {
            eventData.put("tripId", tripId );
            eventData.put("timestamp", new Date());
            eventData.put("loc", coordinates.toJSON());
            eventData.put("speed", speed);
            eventData.put("gpsSpeed", gpsSpeed);
            eventData.put("intensity", intensity);
            eventData.put("eventType", eventType);
            eventData.put("gpsBearing", gpsBearing);
            eventData.put("magBearing", magBearing);
            eventData.put("userId", userId);
            eventData.put("sensorId", sensorId);
            Log.d(PublishSensorData.class.getSimpleName(), "Payload:-"+eventData.toString());

            return eventData;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }




    public MqttMessage getPotholesEventSensorData(String userId, String vehicleId, Coordinates coordinates, double intensity, int speed, float gpsSpeed, String tripId){

        JSONObject payload = new JSONObject();
        try {
            payload.put("vehicleId", vehicleId);
            payload.put("data", "potholeData");
            JSONObject potholeData = new JSONObject();
            potholeData.put("tripId", tripId );
            potholeData.put("timestamp", new Date());
            potholeData.put("loc", coordinates.toJSON());
            potholeData.put("speed", speed);
            potholeData.put("gpsSpeed", gpsSpeed);
            potholeData.put("intensity", intensity);
            potholeData.put("userId", userId);
            payload.put("potholeData", potholeData);
            //Log.d(PublishSensorData.class.getSimpleName(), "Topic:"+topic+" \n Payload:-"+payload.toString());

            MqttMessage message = new MqttMessage(payload.toString().getBytes());
            return message;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public MqttMessage publishDriverBehaviorEventSensorData(String userId, String vehicleId, Coordinates coordinates, double intensity, String eventType, int speed, float gpsSpeed, float gpsBearing, int magBearing) {

        JSONObject payload = new JSONObject();
        try {
            payload.put("car_id", vehicleId);
            payload.put("data", "db_data");
            JSONObject event_data = new JSONObject();
            event_data.put("timestamp", new Date());
            event_data.put("loc", coordinates.toJSON());
            event_data.put("speed", speed);
            event_data.put("gps_speed", gpsSpeed);
            event_data.put("intensity", intensity);
            event_data.put("event_type", eventType);
            event_data.put("gps_bearing", gpsBearing);
            event_data.put("mag_bearing", magBearing);
            event_data.put("user_id", userId);
            payload.put("event_data", event_data);

            Log.d("Event Data", payload.toString());
            MqttMessage message = new MqttMessage(payload.toString().getBytes());
            return message;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public MqttMessage getSensorDataBehaviorEventSensorData(String userId, String vehicleId, Coordinates coordinates, double ax, double ay, double az, int speed, float gpsSpeed, float gpsBearing, int magBearing) {

        JSONObject payload = new JSONObject();
        try {
            payload.put("car_id", vehicleId);
            payload.put("data", "db_data");
            JSONObject sensorData = new JSONObject();
            sensorData.put("timestamp", new Date());
            sensorData.put("loc", coordinates.toJSON());
            sensorData.put("speed", speed);
            sensorData.put("gps_speed", gpsSpeed);

            JSONObject accData = new JSONObject();
            accData.put("ax", ax);
            accData.put("ay", ay);
            accData.put("az", az);
            sensorData.put("accData", accData);

            /* JSONObject magData = new JSONObject();
            magData.put("mx", mx);
            magData.put("my", my);
            magData.put("mz", mz);
            sensorData.put("magData", magData); */

            sensorData.put("gps_bearing", gpsBearing);
            sensorData.put("mag_bearing", magBearing);
            sensorData.put("user_id", userId);
            payload.put("event_data", sensorData);


            Log.d("Event Data", payload.toString());
            MqttMessage message = new MqttMessage(payload.toString().getBytes());
            return message;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
