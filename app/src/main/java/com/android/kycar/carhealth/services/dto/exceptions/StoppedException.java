package com.android.kycar.carhealth.services.dto.exceptions;

/**
 * Sent when there is a "STOPPED" message.
 */
public class StoppedException extends ResponseException {

    public StoppedException() {
        super("STOPPED");
    }

}
