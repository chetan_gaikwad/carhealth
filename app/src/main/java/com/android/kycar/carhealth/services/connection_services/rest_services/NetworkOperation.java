package com.android.kycar.carhealth.services.connection_services.rest_services;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.android.kycar.carhealth.config.ProgessDialogBox;
import com.android.kycar.carhealth.utils.BusProvider;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkOperation extends AsyncTask<String, Void, StringBuffer> {
    private ProgessDialogBox dialogBox;
    private Context context;
    public NetworkOperation(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialogBox = new ProgessDialogBox(context);
        dialogBox.showDailog();
    }

    @Override
    protected StringBuffer doInBackground(String... cred) {
        Log.d("LoginActivity", "Uploading " + cred.length + " readings..");
        try{

            //cred[0] is the url
            String url = cred[0];
            String method;
            if(cred[1] != null){
                method = cred[1];
            } else {
                method = "GET";
            }

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod(method);

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return new StringBuffer().append((response));
        }catch (java.io.FileNotFoundException e){
            return null;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(StringBuffer result) {
        super.onPostExecute(result);
        if(dialogBox.isShowing()){
            dialogBox.dismissDialog();
        }
        BusProvider.getInstance().post(result);
    }
}
