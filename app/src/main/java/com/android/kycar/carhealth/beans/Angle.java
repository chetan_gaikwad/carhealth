package com.android.kycar.carhealth.beans;

/**
 * Created by Krishna on 1/30/2016.
 */
public class Angle {

    private Integer angleX;

    private Integer angleY;

    private Integer angleZ;

    public Angle() {
    }

    public Angle(Integer angleX, Integer angleY, Integer angleZ) {
        this.angleX = angleX;
        this.angleY = angleY;
        this.angleZ = angleZ;
    }

    public Integer getAngleX() { return angleX; }

    public Integer getAngleY() { return angleY; }

    public Integer getAngleZ() { return angleZ; }
}
