package com.android.kycar.carhealth.services.connection_services.rest_services;

import android.location.Location;

import org.json.JSONObject;

/**
 * Created by Krishna on 5/31/2016.
 */
public class RoadConditionRestService extends RestService{

    public String fetchRoadCondition(Location location){

        JSONObject locationJson = new JSONObject();
        try{
            locationJson.put("lat", location.getLatitude());
            locationJson.put("lng", location.getLongitude());
        }catch (Exception e) {
            e.printStackTrace();
        }
        return  restServiceCall("/undulations/findNearByUndulations", "POST", locationJson);
    }
}
