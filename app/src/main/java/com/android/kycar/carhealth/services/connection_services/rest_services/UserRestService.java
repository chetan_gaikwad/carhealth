package com.android.kycar.carhealth.services.connection_services.rest_services;

import android.util.Log;


import com.android.kycar.carhealth.entities.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Krishna on 5/31/2016.
 */
public class UserRestService extends RestService {

    public String login(String...cred){

        JSONObject loginJson = new JSONObject();
        try {
            loginJson.put("user", cred[0]);
            loginJson.put("password", cred[1]);
            loginJson.put("fcmToken", cred[2]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String loginUrl = "/user/login";
        return restServiceCall(loginUrl, "POST", loginJson);
    }

    public boolean logout() {

        return true;
    }

    public String registration(JSONObject registrationJson) {

        String registerUrl = "/user/register";
        return restServiceCall(registerUrl, "POST", registrationJson);
    }

    public boolean saveUserProfile(JSONObject userJson) {

        String loginURL;
        Log.d(UserRestService.class.getSimpleName(), "Uploading " +userJson);
        try{
            loginURL = "/user/create";

            URL obj = new URL(loginURL);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("POST");
            int responseCode = con.getResponseCode();

            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return true;
        }catch (java.io.FileNotFoundException e){
            return false;
        }catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }

    public String updateUserProfile(User user) {

        String updateUserProfileUrl = "/user/creds/update";
        JSONObject userProfileJson = new JSONObject();
        try{
            userProfileJson.put("id",user.getId());
            userProfileJson.put("firstName", user.getFirstName());
            userProfileJson.put("lastName", user.getLastName());
            userProfileJson.put("emailId", user.getEmailId());
            userProfileJson.put("address", user.getAddress());
            userProfileJson.put("phoneNumber", user.getPhoneNumber());
            userProfileJson.put("profilePicture", user.getProfilePicture());
            userProfileJson.put("bloodGrp", user.getBloodGrp());
            userProfileJson.put("aadharNumber", user.getAadharNumber());
            userProfileJson.put("licenceNumber", user.getLicenceNumber());
            userProfileJson.put("pan", user.getPan());
        }catch (Exception e){
            e.printStackTrace();
        }
        return restServiceCall(updateUserProfileUrl, "PUT", userProfileJson);
    }

    public String updateUserProfilePicture(String imgString, String userId) {

        JSONObject registrationJson = new JSONObject();
        String registerUrl = "/user/uplaoddp";
        try {
            registrationJson.put("userId", userId);
            registrationJson.put("profilePic", imgString);
        }catch (Exception e){
            e.printStackTrace();
        }
        return restServiceCall(registerUrl, "POST", registrationJson);
    }

    public User fetchUserProfile(){

        return null;
    }

    public String deviceRegistration(JSONObject deviceRegistrationJson) {

        return restServiceCall("/user/register_device", "POST", deviceRegistrationJson);
    }
}
