package com.android.kycar.carhealth.entities.vehicle_master;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 1/23/2016.
 */
@RealmClass
public class GearRatio extends RealmObject {

    @PrimaryKey
    private String id;

    private int gear;

    private String variantId;

    private double gearRatioMin;

    private double gearRatioMax;

    public GearRatio() {
    }

    public GearRatio(String id, int gear, String variantId, double gearRatioMin, double gearRatioMax) {
        this.id = id;
        this.gear = gear;
        this.variantId = variantId;
        this.gearRatioMin = gearRatioMin;
        this.gearRatioMax = gearRatioMax;
    }

    public GearRatio(JSONObject jsonObject) {
        try {
            this.id = jsonObject.getString("id");
            this.gear = jsonObject.getInt("gear");
            this.variantId = jsonObject.getString("variantId");
            //this.gearRatioMin = jsonObject.getdouble("gearRatioMin");
            //this.gearRatioMax = jsonObject.getdouble("gearRatioMax");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public double getGearRatioMin() {
        return gearRatioMin;
    }

    public void setGearRatioMin(double gearRatioMin) {
        this.gearRatioMin = gearRatioMin;
    }

    public double getGearRatioMax() {
        return gearRatioMax;
    }

    public void setGearRatioMax(double gearRatioMax) {
        this.gearRatioMax = gearRatioMax;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getGear() {
        return gear;
    }

    public void setGear(int gear) {
        this.gear = gear;
    }

    public String getVariantId() {
        return variantId;
    }

    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }
}
