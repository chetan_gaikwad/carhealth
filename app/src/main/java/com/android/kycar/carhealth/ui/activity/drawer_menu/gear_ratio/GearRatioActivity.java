package com.android.kycar.carhealth.ui.activity.drawer_menu.gear_ratio;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.services.dto.commands.SpeedCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.RPMCommand;
import com.android.kycar.carhealth.services.dto.enums.AvailableCommandNames;
import com.android.kycar.carhealth.services.dto.utils.RPMTest;
import com.android.kycar.carhealth.services.dto.utils.SpeedTest;
import com.android.kycar.carhealth.services.obd_services.ObdCommandJob;
import com.android.kycar.carhealth.services.obd_services.ObdProgressListener;
import com.android.kycar.carhealth.utils.BusProvider;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.Map;

public class GearRatioActivity extends AppCompatActivity implements ObdProgressListener {


    private TextView speed;
    private TextView rpm;
    private TextView txtGearRation;
    private Map<String, String> commandResult = new HashMap<String, String>();
    private final String KEY_RPM = "rpm";
    private final String KEY_SPEED = "SPEED";
    private String varientId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_test);
        speed = (TextView)findViewById(R.id.speed);
        //rpm = (TextView)findViewById(R.id.rpm);
        txtGearRation = (TextView)findViewById(R.id.gear_ratio);
        BusProvider.getInstance().register(this);
    }
    public void startPerformanceTest(View view){
    }
    @Subscribe
    public void stateUpdate(ObdCommandJob job) {
        final String cmdName = job.getCommand().getName();
        String cmdResult = "";
        final String cmdID = LookUpCommand(cmdName);

        if (job.getState().equals(ObdCommandJob.ObdCommandJobState.EXECUTION_ERROR)) {
            cmdResult = job.getCommand().getResult();
            if (cmdResult != null) {
            }
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.NOT_SUPPORTED)) {
            cmdResult = getString(R.string.status_obd_no_support);
        } else {
            cmdResult = job.getCommand().getFormattedResult();
        }

        updateTripStatistic(job, cmdID);
    }
    private void updateTripStatistic(final ObdCommandJob job, final String cmdID) {

        if (cmdID.equals(AvailableCommandNames.SPEED.toString())) {

            final SpeedCommand command = (SpeedCommand) job.getCommand();
            Log.d("Cartest", "Speed " + command.getMetricSpeed());

            GearRatioActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    speed.setText(command.getMetricSpeed() + "");
                }
            });

            SpeedTest speedTest = new SpeedTest();
            speedTest.setSpeed(command.getMetricSpeed());
            speedTest.setTimeStamp(Util.getTime());

            commandResult.put(KEY_SPEED,(command.getMetricSpeed() * (1000/60))+"");

        } else if (cmdID.equals(AvailableCommandNames.ENGINE_RPM.toString())) {
            final RPMCommand command = (RPMCommand) job.getCommand();
            commandResult.put(KEY_RPM, command.getRPM() + "");
            GearRatioActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //rpm.setText(command.getRPM() + "");
                }
            });

            RPMTest rpmTest = new RPMTest();
            Log.d("Cartest","rpm "+command.getRPM());
            /*rpmTest.setRpm(command.getRPM());
            rpmTest.setTimeStamp(Util.getTime());*/
        }

        if(commandResult.get(KEY_RPM)!=null &&
                commandResult.get(KEY_SPEED)!=null &&
                (Integer.parseInt(commandResult.get(KEY_SPEED)))>0 ){
            final float gr = (float)(Integer.parseInt(commandResult.get(KEY_RPM))) /
                    (Integer.parseInt(commandResult.get(KEY_SPEED)));
            Log.i("cartest obd", "(RPM/SPEED) - " + gr);
            GearRatioActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txtGearRation.setText(gr + "");
                }
            });

        }
    }

    public static String LookUpCommand(String txt) {
        for (AvailableCommandNames item : AvailableCommandNames.values()) {
            if (item.getValue().equals(txt)) return item.name();
        }
        return txt;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

