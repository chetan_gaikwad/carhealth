package com.android.kycar.carhealth.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 1/16/2016.
 */
@RealmClass
public class UserSettings extends RealmObject {

    @PrimaryKey
    private String id;

    private String userId;

    private String settingOptionId;

    public UserSettings() {
    }

    public UserSettings(String id, String userId, String settingOptionId) {
        this.id = id;
        this.userId = userId;
        this.settingOptionId = settingOptionId;
    }

    public String getSettingOptionId() {
        return settingOptionId;
    }

    public void setSettingOptionId(String settingOptionId) {
        this.settingOptionId = settingOptionId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
