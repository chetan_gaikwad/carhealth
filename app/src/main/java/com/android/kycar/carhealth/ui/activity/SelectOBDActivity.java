package com.android.kycar.carhealth.ui.activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.ConfigActivity;

import java.util.ArrayList;
import java.util.Set;

public class SelectOBDActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_obd);
    }
    public void selectOBD(View v){
        if(isBluetoothEnabled()){
            selectDevice();
        }else{
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    SelectOBDActivity.this);

            // set title
            alertDialogBuilder.setTitle("Bluetooth disabled");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Please switch on the bluetooth")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intentBluetooth = new Intent();
                            intentBluetooth.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
                            startActivity(intentBluetooth);
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }
    }

    private boolean isBluetoothEnabled(){
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        return btAdapter.isEnabled();
    }
    public void selectDevice(){
        ArrayList deviceStrs = new ArrayList();
        final ArrayList devices = new ArrayList();

        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
        if (pairedDevices.size() > 0)
        {
            for (BluetoothDevice device : pairedDevices)
            {
                deviceStrs.add(device.getName() + "\n" + device.getAddress());
                devices.add(device.getAddress());
            }
        }

        // show list
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.select_dialog_singlechoice,
                deviceStrs.toArray(new String[deviceStrs.size()]));

        alertDialog.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                int position = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                String deviceAddress = (String)devices.get(position);
                editor.putString(ConfigActivity.BLUETOOTH_LIST_KEY,deviceAddress).commit();
                Toast.makeText(SelectOBDActivity.this,"Device selected",Toast.LENGTH_SHORT).show();

                SelectOBDActivity.this.finish();
                //startActivity(new Intent(SelectOBDActivity.this, DrawerActivityDeprecated.class));
            }
        });

        alertDialog.setTitle("Choose Bluetooth device");
        alertDialog.show();
    }
}
