package com.android.kycar.carhealth.services.dto.utils;

/**
 * Created by 638 on 12/6/2015.
 */
public class SpeedTest {
    private int speed;
    private String timeStamp;

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
