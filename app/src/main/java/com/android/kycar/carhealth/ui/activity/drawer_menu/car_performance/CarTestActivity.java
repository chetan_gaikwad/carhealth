package com.android.kycar.carhealth.ui.activity.drawer_menu.car_performance;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.ConfigActivity;
import com.android.kycar.carhealth.config.ObdConfig;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.services.dto.commands.ObdCommand;
import com.android.kycar.carhealth.services.dto.commands.SpeedCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.RPMCommand;
import com.android.kycar.carhealth.services.dto.enums.AvailableCommandNames;
import com.android.kycar.carhealth.services.dto.utils.CarTest;
import com.android.kycar.carhealth.services.dto.utils.RPMTest;
import com.android.kycar.carhealth.services.dto.utils.SpeedTest;
import com.android.kycar.carhealth.services.obd_services.AbstractGatewayService;
import com.android.kycar.carhealth.services.obd_services.ObdCommandJob;
import com.android.kycar.carhealth.services.obd_services.ObdProgressListener;
import com.android.kycar.carhealth.services.sensor_services.SensorManagerService;
import com.android.kycar.carhealth.ui.widgets.CustomGauge;
import com.android.kycar.carhealth.utils.BusProvider;
import com.squareup.otto.Subscribe;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CarTestActivity extends AppCompatActivity implements ObdProgressListener {

    private TextView textTimer;
    private TextView txtSpeedo;
    private CustomGauge rpmMeterGauge;
    private ArrayList<CarTest> tripInfo;
    private ArrayList<SpeedTest> tripSpeedInfo;
    private ArrayList<RPMTest> tripRPMInfo;
    private boolean isServiceBound;
    private AbstractGatewayService service;
    private TextView timeTaken;
    private SharedPreferences prefs;
    private Map<String, String> commandResult = new HashMap<String, String>();
    private final String KEY_RPM = "rpm";
    private final String KEY_SPEED = "SPEED";
    private int limit = 60;
    private Map<Integer,String> dataSheets = new HashMap<>();
    private StringBuffer result = new StringBuffer();

    private boolean startTimer = false;
    private boolean flag0 = true;
    private boolean flag60 = true;
    private boolean flag90 = true;
    private boolean flag100 = true;
    private boolean flag120 = true;
    private boolean flag150 = true;

    private long startTime = 0L;
    private Handler customHandler = new Handler();
    private long timeInMilliseconds = 0L;
    private long timeSwapBuff = 0L;
    private long updatedTime = 0L;

    TableLayout performanceTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_test);
        try {
            BusProvider.getInstance().register(this);
        }catch (Exception e){
            e.printStackTrace();
        }
        timeTaken = (TextView)findViewById(R.id.message);
        tripInfo = new ArrayList<>();
        tripSpeedInfo = new ArrayList<>();
        tripRPMInfo = new ArrayList<>();

        textTimer = (TextView) findViewById(R.id.textTimer);
        txtSpeedo = (TextView) findViewById(R.id.txtSpeedo);
        rpmMeterGauge = (CustomGauge) findViewById(R.id.rpm_meter_gauge);
        performanceTable = (TableLayout) findViewById(R.id.performanceTable);
    }

    public void startPerformanceTest(View view){

        textTimer.setText("00:00:00:00");
       // addPerformanceTableRow(10, 100, 20.10);
        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread, 0);
    }

    public void reset(View v){

        //timeSwapBuff += timeInMilliseconds;
        customHandler.removeCallbacks(updateTimerThread);

        /*flag150 = true;
        flag0 = true;
        flag100 = true;
        flag120 = true;
        flag60 = true;
        flag90 = true;
        timeTaken.setText("");
        result = new StringBuffer();*/
    }

    private final Runnable mQueueCommands = new Runnable() {
        public void run() {
            if (service != null && service.isRunning() && service.queueEmpty()) {
                queueCommands();
            }
            // run again in period defined in preferences
            new Handler().postDelayed(mQueueCommands, ConfigActivity.getObdUpdatePeriod(prefs));
        }
    };

    private void queueCommands() {
        if (isServiceBound) {
            for (ObdCommand Command : ObdConfig.getCommands()) {
                if (prefs.getBoolean(Command.getName(), true))
                    service.queueJob(new ObdCommandJob(Command));
            }
        }
    }

    private void dummyData() {
        tripSpeedInfo = new ArrayList<>();
        for(int i=0;i<155;i++){
            try {
                Thread.sleep(150);
            }catch (InterruptedException e){
                e.printStackTrace();
            }

            Log.i("performance ","Time - " + Util.getTimeMicro() + " Speed - "+i);
            if(i >= 150){
                if (flag150) {
                    flag150 = false;
                    dataSheets.put(150,Util.getTimeMicro());

                    addPerformanceTableRow(120, 150 ,findDifference(dataSheets.get(120), dataSheets.get(150)));

                    result.append("\n120 to 150 -> " + findDifference(dataSheets.get(120), dataSheets.get(150)) + " sec");

                    CarTestActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            timeTaken.setText(result + "\n");
                        }

                    });
                }
            }else if(i >= 120){
                if(flag120){
                    flag120 = false;
                    dataSheets.put(120,Util.getTimeMicro());
                    addPerformanceTableRow(90, 120, findDifference(dataSheets.get(90), dataSheets.get(120)));
                    result.append("\n90 to 120 -> " + findDifference(dataSheets.get(90), dataSheets.get(120)) + " sec");
                    CarTestActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            timeTaken.setText(result + "\n");
                        }

                    });
                }
            }else if(i >= 100){
                if(flag100){
                    flag100 = false;
                    dataSheets.put(100,Util.getTimeMicro());
                    addPerformanceTableRow(0, 100, findDifference(dataSheets.get(0), dataSheets.get(100)));
                    result.append("\n0 to 100 -> " + findDifference(dataSheets.get(0), dataSheets.get(100)) + " sec");
                    CarTestActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            timeTaken.setText(result + "\n");
                        }

                    });
                }
            }else if(i >= 90){
                if(flag90){
                    flag90 = false;
                    dataSheets.put(90,Util.getTimeMicro());
                    result.append("\n60 to 90 -> " + findDifference(dataSheets.get(60), dataSheets.get(90)) + " sec");
                    addPerformanceTableRow(60, 90, findDifference(dataSheets.get(60), dataSheets.get(90)));
                    CarTestActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            timeTaken.setText(result + "\n");
                        }

                    });
                }
            }else if(i >= 60){
                if(flag60){
                    flag60 = false;
                    dataSheets.put(60,Util.getTimeMicro());
                    result.append("\n0 to 60 -> " + findDifference(dataSheets.get(0), dataSheets.get(60)) + " sec");
                    addPerformanceTableRow(0, 60, findDifference(dataSheets.get(0), dataSheets.get(60)));
                    CarTestActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            timeTaken.setText(result + "\n");
                        }

                    });
                }
            }else if(i > 0){
                if(flag0){
                    flag0 = false;
                    dataSheets.put(0, Util.getTimeMicro());
                }
            }
        }
        //performCalculations();
    }

    @Subscribe
    public void stateUpdate(ObdCommandJob job) {
        final String cmdName = job.getCommand().getName();
        String cmdResult = "";
        final String cmdID = LookUpCommand(cmdName);

        if (job.getState().equals(ObdCommandJob.ObdCommandJobState.EXECUTION_ERROR)) {
            cmdResult = job.getCommand().getResult();
            if (cmdResult != null) {
            }
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.NOT_SUPPORTED)) {
            cmdResult = getString(R.string.status_obd_no_support);
        } else {
            cmdResult = job.getCommand().getFormattedResult();
        }

        updateTripStatistic(job, cmdID);
    }

    private void updateTripStatistic(final ObdCommandJob job, final String cmdID) {

        if (cmdID.equals(AvailableCommandNames.SPEED.toString())) {

            final SpeedCommand command = (SpeedCommand) job.getCommand();
            Log.d("Cartest", "Speed " + command.getMetricSpeed());

            CarTestActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txtSpeedo.setText(command.getMetricSpeed() + "");
                }
            });

            SpeedTest speedTest = new SpeedTest();
            speedTest.setSpeed(command.getMetricSpeed());
            speedTest.setTimeStamp(Util.getTimeMicro());
            tripSpeedInfo.add(speedTest);

            commandResult.put(KEY_SPEED,(command.getMetricSpeed() * (1000/60))+"");
            /*if(command.getMetricSpeed() >= 60){

                doUnbindService();
                performCalculations(limit);
                limit = limit+30;
            }*/

            Log.i("performance ","Time - " +Util.getTimeMicro()+" Speed - "+command.getMetricSpeed());
            if(command.getMetricSpeed() >= 150){
                if(flag150){
                    flag150 = false;
                    dataSheets.put(150, Util.getTimeMicro());
                    result.append("\n120 to 150 >>> " + findDifference(dataSheets.get(120), dataSheets.get(150)) + " sec");

                    CarTestActivity.this.runOnUiThread(new Runnable() {
                        public void run() {

                            timeTaken.setText(result + "\n");

                        }

                    });
                }
            }else if(command.getMetricSpeed() >= 120){
                if(flag120){
                    flag120 = false;
                    dataSheets.put(120, Util.getTimeMicro());
                    result.append("\n90 to 120 >>> " + findDifference(dataSheets.get(90), dataSheets.get(120)) + " sec");
                    CarTestActivity.this.runOnUiThread(new Runnable() {
                        public void run() {

                            timeTaken.setText(result + "\n");

                        }

                    });
                }
            }else if(command.getMetricSpeed() >= 100){
                if(flag100){
                    flag100 = false;
                    dataSheets.put(100,Util.getTimeMicro());
                    result.append("\n0 to 100 >>> " + findDifference(dataSheets.get(0), dataSheets.get(100))+" sec");
                    CarTestActivity.this.runOnUiThread(new Runnable() {
                        public void run() {

                            timeTaken.setText(result + "\n");

                        }

                    });
                }
            }else if(command.getMetricSpeed() >= 90){
                if(flag90){
                    flag90 = false;
                    dataSheets.put(90,Util.getTimeMicro());
                    result.append("\n60 to 90 >>> " + findDifference(dataSheets.get(60), dataSheets.get(90))+" sec");
                    CarTestActivity.this.runOnUiThread(new Runnable() {
                        public void run() {

                            //timeTaken.setText(result + "\n");

                        }

                    });
                }
            }else if(command.getMetricSpeed() >= 60){

                if(flag60){
                    flag60 = false;
                    dataSheets.put(60,Util.getTimeMicro());
                    result.append("\n0 to 60 >>> " + findDifference(dataSheets.get(0), dataSheets.get(60)) +" sec");
                    Log.i("ff","\n0 to 60 >>> " + findDifference(dataSheets.get(0), dataSheets.get(60)) +" sec");
                    CarTestActivity.this.runOnUiThread(new Runnable() {
                        public void run() {

                            timeTaken.setText(result + "\n");

                        }

                    });
                }
            }else if(command.getMetricSpeed() > 0){

                if(!startTimer){
                    startTimer = true;
                }
                if(flag0){
                    flag0 = false;

                    dataSheets.put(0, Util.getTimeMicro());
                }
        }
        } else if (cmdID.equals(AvailableCommandNames.ENGINE_RPM.toString())) {
            final RPMCommand command = (RPMCommand) job.getCommand();
            commandResult.put(KEY_RPM, command.getRPM() + "");

            RPMTest rpmTest = new RPMTest();
            Log.d("Cartest","rpm "+command.getRPM());
            CarTestActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    rpmMeterGauge.setValue(command.getRPM());
                }
            });
            rpmTest.setRpm(command.getRPM());
            rpmTest.setTimeStamp(Util.getTimeMicro());
            tripRPMInfo.add(rpmTest);
        }
    }

    /*private void performCalculations(int limit) {
        ArrayList<String> timeStamps = new ArrayList<>();
        int checkpoint = 0;
        int count = tripSpeedInfo.size();
        for (int i=0;i<count;i++){
            Log.i(Util.LOG,"speed at timestamp - "+tripSpeedInfo.get(i).getTimeMicroStamp()
                    +" was "+tripSpeedInfo.get(i).getSpeed());
            if(tripSpeedInfo.get(i).getSpeed() > checkpoint){
                checkpoint = 6000;
                timeStamps.add(tripSpeedInfo.get(i).getTimeMicroStamp());
            }
        }
        timeStamps.add(tripSpeedInfo.get(count-1).getTimeMicroStamp());
        Log.i(Util.LOG,"performCalculations start timestamp - "+timeStamps.get(0).toString());
        Log.i(Util.LOG, "performCalculations stop timestamp - " + timeStamps.get(1).toString());
        double timeTakenInSec = findDifference(timeStamps.get(0).toString(),timeStamps.get(1).toString());
        timeTaken.setText("0 to 30 in " + timeTakenInSec + " sec");
        tripSpeedInfo.toString();
    }*/

    private double findDifference(String s, String s1) {
        Date date1,date2;

        try {

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss.SSS");
            date1 = simpleDateFormat.parse(s);
            date2 = simpleDateFormat.parse(s1);

            long difference = date2.getTime() - date1.getTime();
            double diffInSec = (double)difference/1000;
            return  diffInSec;
        }catch (Exception e){
            e.printStackTrace();
        }
        return  0;
    }

    private void doBindService() {
        if (!isServiceBound) {
           // Intent serviceIntent = new Intent(this, CarPerformanceTesingService.class);
           // bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);
        }
    }

    private void doUnbindService() {
        if (isServiceBound) {
            if (service.isRunning()) {
                service.stopService();
            }
            Log.d(Util.LOG, "Unbinding OBD service..");
            unbindService(serviceConn);
            isServiceBound = false;
        }
        Intent sensorService = new Intent(this, SensorManagerService.class);
        stopService(sensorService);
    }

    private ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            Log.d(Util.LOG, className.toString() + " service is bound");
            isServiceBound = true;
            service = ((AbstractGatewayService.AbstractGatewayServiceBinder) binder).getService();
            service.setContext(CarTestActivity.this);
            Log.d(Util.LOG, "Starting live data");
            try {
                service.startService();
            } catch (IOException ioe) {
                Log.e(Util.LOG, "Failure Starting live data");
                doUnbindService();
            }
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        // This method is *only* called when the connection to the service is lost unexpectedly
        // and *not* when the client unbinds (http://developer.android.com/guide/components/bound-services.html)
        // So the isServiceBound attribute should also be set to false when we unbind from the service.
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(Util.LOG, className.toString() + " service is unbound");
            isServiceBound = false;
        }
    };

    public static String LookUpCommand(String txt) {
        for (AvailableCommandNames item : AvailableCommandNames.values()) {
            if (item.getValue().equals(txt)) return item.name();
        }
        return txt;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        doUnbindService();
    }

    private Runnable updateTimerThread = new Runnable() {

        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;
            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            secs = secs % 60;
            int milliseconds = (int) (updatedTime % 1000);
            textTimer.setText("" + mins + ":"
                            + String.format("%02d", secs) + ":"
                            + String.format("%03d", milliseconds));
            customHandler.postDelayed(this, 0);
        }
    };

    private void addPerformanceTableRow(int from, int to , Double time){
        TableRow row= new TableRow(this);
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        row.setLayoutParams(lp);

        final TextView rowTextView = new TextView(this);
        rowTextView.setTextSize(20);

        // set some properties of rowTextView or something
        rowTextView.setText("   "+from +"         -         "+to + "                " + time);

        // add the textview to the linearlayout
        performanceTable.addView(rowTextView);

        /*TextView txtFrom = new TextView(this);

        txtFrom.setText(from);
        row.addView(txtFrom);*/

        /*TextView txtTo = new TextView(this);
        txtTo.setText(to);
        TextView txtTimeTaken = new TextView(this);
        txtTimeTaken.setText(""+time);
        row.addView(txtTo);
        row.addView(txtTimeTaken);
*/
       // performanceTable.addView(row);
    }
}


