package com.android.kycar.carhealth.async_task;

import android.content.Context;
import android.os.AsyncTask;

import com.android.kycar.carhealth.entities.trip_transaction.Trip;
import com.android.kycar.carhealth.repositories.trip.TripRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.TripRestService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krishna on 10/1/2016.
 */
public class FindTripGpsDistanceAsyncTask extends AsyncTask<JSONObject, Void, String> {

    private Context context;

    private String tripId;

    public FindTripGpsDistanceAsyncTask(Context context){
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(JSONObject... jsonObjects) {

        try {
            this.tripId = jsonObjects[0].getString("tripId");
            return new TripRestService().findTripGpsDistance(jsonObjects[0]);
        } catch (Exception e) {
            e.printStackTrace();
            return "User not found";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if(result!=null) {
            try {
                JSONObject resObject = new JSONObject(result);
                JSONObject route = resObject.getJSONArray("routes").getJSONObject(0);
                JSONObject leg = route.getJSONArray("legs").getJSONObject(0);
                JSONObject distance = leg.getJSONObject("distance");

                TripRepo tripRepo = new TripRepo(context, Trip.class);
                Trip trip = new Trip((Trip) tripRepo.findById(tripId));
                trip.setGpsDistance(distance.getInt("value"));
                tripRepo.saveOrUpdate(trip);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
