package com.android.kycar.carhealth.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.kycar.carhealth.R;


public class CustomDialogClass extends Dialog implements
    View.OnClickListener {

  public Context activity;
  public Dialog dialog;
  public TextView alertText;
  public ImageView alertImage;
  private String alertTextValue;
  private int imageId;


  public CustomDialogClass(Context activity, String alertTextValue, int imageId) {
    super(activity);
    this.activity = activity;
      this.alertTextValue = alertTextValue;
      this.imageId = imageId;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);

    setContentView(R.layout.custom_alerts_layout);
      //getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
    alertText = (TextView) findViewById(R.id.txt_dialog_alert);
    alertImage = (ImageView) findViewById(R.id.img_alert);
    alertText.setText(alertTextValue);
    alertImage.setImageResource(imageId);
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {

    default:
      break;
    }
    dismiss();
  }
}