package com.android.kycar.carhealth.services.obd_services;

import android.content.Context;
import android.util.Log;

import com.android.kycar.carhealth.beans.Coordinates;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.services.dto.enums.AvailableCommandNames;
import com.android.kycar.carhealth.services.utils.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Krishna on 2/21/2016.
 */
public class ObdDataLoggerService {

    Logger loggerObd;

    public ObdDataLoggerService(Context context, String s) {

        loggerObd = new Logger(new Date() + "TrackMode.csv", context);
    }
    public ObdDataLoggerService(Context context){

        loggerObd = new Logger(new Date()+"LoggerObd.csv",context);
        List<String> rtHeaders = new ArrayList<String>();
        rtHeaders.add("timestamp");

        rtHeaders.add(AvailableCommandNames.ENGINE_RPM.getValue());
        rtHeaders.add("rpm_zone");
        rtHeaders.add(AvailableCommandNames.SPEED.getValue());
        rtHeaders.add("speed_zone");

        rtHeaders.add(AvailableCommandNames.MAF.getValue());
        rtHeaders.add(AvailableCommandNames.THROTTLE_POS.getValue());
        rtHeaders.add(AvailableCommandNames.ENGINE_LOAD.getValue());
        rtHeaders.add(AvailableCommandNames.DISTANCE_TRAVELED_AFTER_CODES_CLEARED.getValue());
        rtHeaders.add(AvailableCommandNames.ENGINE_OIL_TEMP.getValue());
        rtHeaders.add("oil_zone");
        rtHeaders.add(AvailableCommandNames.ENGINE_COOLANT_TEMP.getValue());
        rtHeaders.add("coolant_zone");
        rtHeaders.add(AvailableCommandNames.FUEL_TYPE.getValue());

        rtHeaders.add("battery");
        rtHeaders.add("gear_ratio");
        rtHeaders.add("gear");
        rtHeaders.add("vin");
        rtHeaders.add("lat");
        rtHeaders.add("lng");
        rtHeaders.add("spd");
        rtHeaders.add("alt");
        rtHeaders.add("acc");

        rtHeaders.add("userId");
        rtHeaders.add("macId");
        rtHeaders.add("tripId");
        loggerObd.setHeader(rtHeaders);
    }
    public boolean stopSensorDataLoggerService(){

        loggerObd.finalizeFile();
        return true;
    }

    public void logObd(Integer rpm, Integer speed, Integer gear, Float engineLoad, Integer oilTemp,
                       Float coolantTemp, Double maf, String throtalPosition, String absLoad,
                       String airFuelRatio, float airIntkeTemp, float ambientAirTemp, String barometricPressure,
                       String moduleVoltage, String distanceTravledAfterCodeCleared, int distanceTravledMILOn,
                       String milOn, Double equivalentRatio, float fuelLevel, String fuelPressure, String vin,
                       Coordinates coords){
        /** PUBLISH DATA TO MQTTT*/

        List<String > obd = new ArrayList<String>();
        obd.add(Util.getTime());
        obd.add("" + rpm);
        obd.add("" + speed);
        obd.add(""+maf);
        obd.add(throtalPosition);
        obd.add(absLoad);
        obd.add(airFuelRatio);
        obd.add(""+airIntkeTemp);
        obd.add(""+ambientAirTemp);
        obd.add(barometricPressure);
        obd.add(moduleVoltage);
        obd.add(distanceTravledAfterCodeCleared);
        obd.add(""+distanceTravledMILOn);
        obd.add(milOn);
        obd.add(""+equivalentRatio);
        obd.add(""+fuelLevel);
        obd.add(fuelPressure);
        obd.add(vin);
        obd.add("" + engineLoad);
        obd.add("" + oilTemp);
        obd.add("" + coolantTemp);
        obd.add("" + coords.getLatitude());
        obd.add("" + coords.getLongitude());
        obd.add("USER1");
        obd.add("DEVICE1");
        obd.add("VEHICLE_ID");
        obd.add("MAC_ID");
        loggerObd.logging(obd);

        /*mqttImpl.publishObdData(new Date(), rpm, speed, gear, maf, throtalPosition, absLoad, airFuelRatio, airIntkeTemp, ambientAirTemp, barometricPressure, moduleVoltage
                , distanceTravledAfterCodeCleared, distanceTravledMILOn, milOn, equivalentRatio, fuelLevel, fuelPressure, vin, engineLoad,
                oilTemp, coolantTemp, coords, "USER1", "DEVICE1", "VEHICLE_ID", "MAC_ID");*/
        //mqttImpl.publishSensorData(new Date(), ax, ay, az, intencity, direction, speed, coords, "", "", "", "");
        //firebaseConnectionImpl.uploadAccilerationData(new Date(), ax, ay, az, intencity, direction, speed, coords, "", "", "", "");
    }

    public void logData(Map<String, String> s) {

        Log.d("logData","Logging offline");
        List<String > obd = new ArrayList<>();
        obd.add(Util.getTime());


        obd.add(s.get(AvailableCommandNames.ENGINE_RPM.toString().toLowerCase()));
        obd.add(s.get("rpm_zone"));
        obd.add(s.get(AvailableCommandNames.SPEED.toString().toLowerCase()));
        obd.add(s.get("speed_zone"));
        obd.add(s.get(AvailableCommandNames.MAF.toString().toLowerCase()));
        obd.add(s.get(AvailableCommandNames.THROTTLE_POS.toString().toLowerCase()));
        obd.add(s.get(AvailableCommandNames.ENGINE_LOAD.toString().toLowerCase()));
        obd.add(s.get(AvailableCommandNames.DISTANCE_TRAVELED_AFTER_CODES_CLEARED.toString().toLowerCase()));
        obd.add(s.get(AvailableCommandNames.ENGINE_OIL_TEMP.toString().toLowerCase()));
        obd.add(s.get("oil_zone"));
        obd.add(s.get(AvailableCommandNames.ENGINE_COOLANT_TEMP.toString().toLowerCase()));
        obd.add(s.get("coolant_zone"));
        obd.add(s.get(AvailableCommandNames.FUEL_TYPE.toString().toLowerCase()));
        obd.add(s.get("battery").replace("\r", ""));

        obd.add(s.get("rpm/speed"));
        obd.add(s.get("gear"));
        obd.add(s.get("vin"));
        obd.add(s.get("lat"));
        obd.add(s.get("lng"));
        obd.add(s.get("spd"));
        obd.add(s.get("alt"));
        obd.add(s.get("acc"));

        obd.add(s.get("userId"));
        obd.add(s.get("macId"));
        obd.add(s.get("tripId"));
        loggerObd.logging(obd);

    }

    public void logDataTrack(String s) {

        Log.d("logData", "Logging offline");
        loggerObd.loggingString(s);

    }

}
