package com.android.kycar.carhealth.services.connection_services.rest_services;

import android.location.Location;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krishna on 7/30/2016.
 */
public class FcmRestCallService extends RestService {

    public String registerToken(String userTopic, String token) {

        JSONObject tokenObj = new JSONObject();
        try {
            tokenObj.put("user", userTopic);
            tokenObj.put("registration_token", token);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String registerUrl = "/user/registerToken/register";
        Log.d(FcmRestCallService.class.getSimpleName(), tokenObj.toString());
        return restServiceCall(registerUrl, "POST", tokenObj);
    }

    public String sendNotification(String topic, String userName) {

        JSONObject notificationMsgObj = new JSONObject();
        try {
            JSONArray tokens = new JSONArray();
            tokens.put(topic);
            notificationMsgObj.put("topic", topic);

            Log.d("Topic", topic);

            JSONObject notification = new JSONObject();
            notification.put("title" , userName+" wants to travels connected");
            notification.put("body", "Click to view on your map");
            notificationMsgObj.put("notification", notification);

            JSONObject notData = new JSONObject();
            notData.put("userId", "Hello");
            notificationMsgObj.put("not_data", notData);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(FcmRestCallService.class.getSimpleName(), ""+notificationMsgObj);
        String notificationUrl = "/user/sendNotification";
        return restServiceCall(notificationUrl, "POST", notificationMsgObj);
    }

    public String sendAccidentNotification(String topic, String userName, Location location) {

        JSONObject notificationMsgObj = new JSONObject();
        try {
            JSONArray tokens = new JSONArray();
            tokens.put(topic);

            Log.d("Topic", topic);

            notificationMsgObj.put("topic", topic);

            JSONObject notification = new JSONObject();
            notification.put("title" , userName+" has met with an accident");
            notification.put("body", "Click to view on your map");
            notificationMsgObj.put("notification", notification);

            JSONObject notData = new JSONObject();
            notData.put("userId", userName);
            notData.put("acc", true);
            notData.put("lat", location.getLatitude());
            notData.put("lng", location.getLongitude());
            notData.put("acc", location.getAccuracy());
            notData.put("click_action", "DashboardActivity");
            notificationMsgObj.put("not_data", notData);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(FcmRestCallService.class.getSimpleName(), ""+notificationMsgObj);
        String notificationUrl = "/user/sendNotification";
        return restServiceCall(notificationUrl, "POST", notificationMsgObj);
    }

    public String sendSosCopNotification(JSONObject notificationMsgObj) {

        Log.d(FcmRestCallService.class.getSimpleName(), ""+notificationMsgObj);
        String sosNotificationUrl = "/user/sendSosNotificationToCops";
        return restServiceCall(sosNotificationUrl, "POST", notificationMsgObj);
    }
}
