package com.android.kycar.carhealth.entities.user_master;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 1/16/2016.
 */
@RealmClass
public class SettingOption extends RealmObject {

    @PrimaryKey
    private String id;

    private String settingOption;

    private String settingDesc;

    public SettingOption() {
    }

    public SettingOption(String id, String settingOption, String settingDesc) {
        this.id = id;
        this.settingOption = settingOption;
        this.settingDesc = settingDesc;
    }

    public String getSettingDesc() {
        return settingDesc;
    }

    public void setSettingDesc(String settingDesc) {
        this.settingDesc = settingDesc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSettingOption() {
        return settingOption;
    }

    public void setSettingOption(String settingOption) {
        this.settingOption = settingOption;
    }
}
