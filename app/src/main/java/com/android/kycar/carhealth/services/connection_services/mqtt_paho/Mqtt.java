package com.android.kycar.carhealth.services.connection_services.mqtt_paho;

import android.util.Log;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;


/**
 * Created by Krishna on 5/2/2016.
 */
public class Mqtt {

    String baseTopic;
    String content;
    int qos;
    String broker;
    MemoryPersistence persistence;
    int keepalive;
    private String clientId;
    protected MqttClient mqttClient;

    public Mqtt(){

        baseTopic    = "nxtgizmo";
        content      = "Test sending msg";
        qos             = 2;
        broker       = "tcp://34.73.217.122:1883";
        keepalive    = 60000;
        persistence  = new MemoryPersistence();
    }

    /*public boolean mqttConnect(String userId) {

        baseTopic = baseTopic+"/"+userId;
        mqttConnect();
        return true;
    }*/

    public boolean mqttConnect(){

        clientId = MqttClient.generateClientId();

        try {
            Log.d(Mqtt.class.getSimpleName(), "Client id: "+clientId);
            Log.d(Mqtt.class.getSimpleName(), "broker id: "+broker);

            mqttClient = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            Log.d("Status Connecting to: ",""+broker);
            mqttClient.connect(connOpts);
            mqttClient.setTimeToWait(keepalive);
            Log.d(Mqtt.class.getSimpleName(), "Connected");
        } catch (MqttException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean mqttDisconnect(){
        try {
            mqttClient.disconnect();
        } catch (MqttException e) {
            e.printStackTrace();
            return false;
        }
        Log.d("Status","Disconnected");
        return true;
    }

    protected void logException(MqttException me){
        System.out.println("reason "+me.getReasonCode());
        System.out.println("msg "+me.getMessage());
        System.out.println("loc "+me.getLocalizedMessage());
        System.out.println("cause "+me.getCause());
        System.out.println("excep "+me);
        me.printStackTrace();
    }
}

