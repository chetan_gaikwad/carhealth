package com.android.kycar.carhealth.exceptions;

/**
 * Created by Krishna on 1/30/2016.
 */
public class ObdResponseFormatException extends Exception {

    public ObdResponseFormatException() {
        // TODO Auto-generated constructor stub
    }

    public ObdResponseFormatException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public ObdResponseFormatException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public ObdResponseFormatException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }
}
