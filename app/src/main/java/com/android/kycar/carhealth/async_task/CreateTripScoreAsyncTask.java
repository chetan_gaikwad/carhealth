package com.android.kycar.carhealth.async_task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.kycar.carhealth.entities.trip_transaction.TripScore;
import com.android.kycar.carhealth.repositories.trip.TripScoreRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.TripRestService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krishna on 9/12/2016.
 */

public class CreateTripScoreAsyncTask extends AsyncTask<JSONObject, Void, String> {

    private Context context;

    public CreateTripScoreAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(JSONObject... tripScores) {

        try {
            /** Saving all trip score */
            return new TripRestService().createTripScore(tripScores[0]);

        } catch (Exception e) {
            e.printStackTrace();
            return "failed";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if(result!=null) {
            if(result!=null) {
                //Toast.makeText(this, "Trip created successfully", Toast.LENGTH_SHORT).show();
                try {
                    JSONObject resObject = new JSONObject(result);
                    TripScoreRepo tripScoreRepo = new TripScoreRepo(context, TripScore.class);
                    TripScore tripScore = new TripScore((TripScore) tripScoreRepo.findById(resObject.getString("tripScoreId")));
                    tripScore.setSync(true);
                    tripScoreRepo.saveOrUpdate(tripScore);
                    Log.d(CreateTripAsyncTask.class.getSimpleName(), "TripScore Updated");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}