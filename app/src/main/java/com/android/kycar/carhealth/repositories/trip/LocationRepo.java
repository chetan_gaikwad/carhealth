package com.android.kycar.carhealth.repositories.trip;

import android.content.Context;
import android.util.Log;


import com.android.kycar.carhealth.entities.trip_transaction.Location;
import com.android.kycar.carhealth.repositories.Repository;

import java.util.List;

import io.realm.exceptions.RealmException;

/**
 * Created by Krishna on 4/17/2016.
 */
public class LocationRepo extends Repository {

    public LocationRepo(Context context, Class _aClass) {
        super(context, _aClass);
    }

    public int getCount() throws RealmException {
        return realm.where(Location.class).findAll().size();
    }

    public boolean saveUnique(Location location) throws RealmException {

        Log.d("All locations", ""+realm.where(aClass).findAll().size());
        List<Location> locations = realm.where(aClass).equalTo("latitude", location.getLatitude()).equalTo("longitude", location.getLongitude()).findAll();

        Log.d("All locations", ""+locations.size());

        if(locations.size() == 0){
            realm.beginTransaction();
            Location location1 = realm.copyToRealm(location);
            realm.commitTransaction();
            return true;
        } else {
            Log.d("Realm location found", ""+locations.size());
            return false;
        }
    }
    /*@Override
    protected void finalize() throws Throwable {
        try{
            realm.close();
            Log.d("Finalise", "Finalize repo");
        }catch(Throwable t){
            throw t;
        }finally{
            System.out.println("Calling finalize of Super Class");
            super.finalize();
        }
    }*/
}
