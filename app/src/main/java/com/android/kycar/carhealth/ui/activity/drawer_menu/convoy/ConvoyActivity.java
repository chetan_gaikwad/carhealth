package com.android.kycar.carhealth.ui.activity.drawer_menu.convoy;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.beans.ContactData;
import com.android.kycar.carhealth.config.ProgessDialogBox;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.UserContact;
import com.android.kycar.carhealth.services.connection_services.mqtt_paho.SubscribeConvoy;
import com.android.kycar.carhealth.services.connection_services.rest_services.ConvoyRestService;
import com.android.kycar.carhealth.services.io_services.SmsAlerts;
import com.android.kycar.carhealth.ui.activity.ContactSelectorActivity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ConvoyActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 100;
    private EditText etConvoyName;
    private List<UserContact> members;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private SubscribeConvoy subscribeConvoy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convoy_dashboard);

        preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        editor = preferences.edit();
        Uri data = getIntent().getData();
        if(data != null) {
            String strData = data.toString();
            strData = strData.replace("http://convoyinvitation.com/nxtauto/", "");

            String[] strs = strData.split("/");
            editor.putBoolean(Util.IS_CONVOY, true);
            editor.putString(Util.CONVOY_NAME, strs[0]);
            Util.showAlert("You got the convoy invitation of "+ strs[0], ConvoyActivity.this);
            Log.d(ConvoyActivity.class.getSimpleName(), "Data got from sms:"+strs[0]);
            if(strs.length > 1) {
                editor.putString(Util.CONVOY_ID, strs[1]);
                Log.d(ConvoyActivity.class.getSimpleName(), "Data got from sms:"+strs[1]);
            }
        }

        etConvoyName = (EditText)findViewById(R.id.et_convoy_name);
        members = new ArrayList<>();
    }

    public void onCreateConvoyClick(View view) {
        if(etConvoyName.getText().toString().isEmpty()) {
            Util.showAlert("Please enter convoy name", ConvoyActivity.this);
            return;
        }
        if(members.isEmpty()) {
            Util.showAlert("Please select at least one joinee", ConvoyActivity.this);
            return;
        }
        new CreateConvoyAsyncTask().execute(preferences.getString(Util.USER_PREFERENCE, ""), etConvoyName.getText().toString());
    }

    public void onAddJoineeClick(View view) {

        Intent contactPicker = new Intent(this, ContactSelectorActivity.class);
        contactPicker.setPackage("com.github_obd222.pires.obd.reader");
        startActivityForResult(contactPicker, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_CODE) {
            if(resultCode == Activity.RESULT_OK) {
                if(data.hasExtra(ContactData.CONTACTS_DATA)) {
                    if(resultCode == Activity.RESULT_OK) {
                        if(data.hasExtra(ContactData.CONTACTS_DATA)) {
                            ArrayList<ContactData> contacts = data.getParcelableArrayListExtra(ContactData.CONTACTS_DATA);
                            if(contacts != null) {
                                Iterator<ContactData> iterContacts = contacts.iterator();
                                Log.i(ConvoyActivity.class.getSimpleName(),"Iterator size "+contacts.size());
                                while(iterContacts.hasNext()) {

                                    ContactData contact = iterContacts.next();
                                    members.add(new UserContact(Util.getTimeLong() + "", contact.displayName, contact.phoneNmb, "", ""));

                                    /** Showing contact list to screen */


                                }
                            }
                        }
                    }
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private class CreateConvoyAsyncTask extends AsyncTask<String, Void, String> {
        private ProgessDialogBox dialogBox;

        String pin = null;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogBox = new ProgessDialogBox(ConvoyActivity.this);
            dialogBox.showDailog();
        }

        @Override
        protected String doInBackground(String... cred) {
            /** Convoy creation call */

            String response = new ConvoyRestService().create(cred[0], cred[1], members);
            if(response != null) {
                for (UserContact userContact: members) {
                    new SmsAlerts().sendConvoySmsNotification(userContact.getNumber(), cred[1], response);
                }
            }

            Log.d(ConvoyActivity.class.getSimpleName(), "Trip Created:-"+response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(dialogBox.isShowing()){
                dialogBox.dismissDialog();
            }
            /** Convoy creation sharing */

            /*subscribeConvoy = new SubscribeConvoy();
            subscribeConvoy.mqttConnect();
            subscribeConvoy.subscribeConvoy(result);*/

            editor.putBoolean(Util.IS_CONVOY, true);
            editor.putString(Util.CONVOY_ID, result);
            editor.putString(Util.CONVOY_NAME, etConvoyName.getText().toString());
            editor.commit();

            Util.showAlert("Invitation Sent", ConvoyActivity.this);
        }
    }
}
