package com.android.kycar.carhealth.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 1/16/2016.
 */
@RealmClass
public class VehiclePerformance extends RealmObject {

    @PrimaryKey
    private String id;

    private String vehicleId;

    private String fuelTypeId;

    private double maxPower;

    private double maxTorque;

    public VehiclePerformance() {
    }

    public VehiclePerformance(String id, String vehicleId, String fuelTypeId, double maxPower, double maxTorque) {
        this.id = id;
        this.vehicleId = vehicleId;
        this.fuelTypeId = fuelTypeId;
        this.maxPower = maxPower;
        this.maxTorque = maxTorque;
    }

    public double getMaxPower() {
        return maxPower;
    }

    public void setMaxPower(double maxPower) {
        this.maxPower = maxPower;
    }

    public double getMaxTorque() {
        return maxTorque;
    }

    public void setMaxTorque(double maxTorque) {
        this.maxTorque = maxTorque;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(String fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }
}
