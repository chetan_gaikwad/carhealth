package com.android.kycar.carhealth.repositories.trip;

import android.content.Context;
import android.util.Log;


import com.android.kycar.carhealth.entities.trip_transaction.TripScore;
import com.android.kycar.carhealth.repositories.Repository;

import java.util.List;

/**
 * Created by Krishna on 8/3/2016.
 */
public class TripScoreRepo extends Repository {
    public TripScoreRepo(Context context, Class _aClass) {
        super(context, _aClass);
    }

    public TripScore findByTripId(String tripId) {

        return realm.where(TripScore.class).equalTo("tripId", tripId).findFirst();
    }

    public boolean deleteByTripId (String tripId) {

        List<TripScore> tripScores1 = findAll();
        for (TripScore tripScore : tripScores1) {
            Log.d(TripScore.class.getSimpleName(), "Trip score:-"+tripScore.getTripId());
        }

        List<TripScore> tripScores = realm.where(aClass).equalTo("tripId", tripId).findAll();
        Log.d(TripScoreRepo.class.getSimpleName(), "Size :- "+tripScores.size());
        for (int counter = 0; counter < tripScores.size(); counter++) {
            deleteById(tripScores.get(counter).getId());
        }
        return true;
    }

    public boolean deleteByTripSessionId (String tripSessionId) {

        List<TripScore> tripScores = realm.where(aClass).equalTo("tripSessionId", tripSessionId).findAll();
        Log.d(TripScoreRepo.class.getSimpleName(), "Size :-"+tripScores.size());
        for (int counter = 0; counter < tripScores.size(); counter++) {
            deleteById(tripScores.get(counter).getId());
        }
        return true;
    }

    public List<TripScore> findAllByTripId(String tripId) {

        List<TripScore> tripScores = realm.where(TripScore.class).findAll();
        for (TripScore tripScore: tripScores) {
            Log.d("TripId" , tripScore.getTripId());
        }

        return realm.where(TripScore.class).equalTo("tripId", tripId).findAll();
    }

    public List<TripScore> findAllUnSynced() {
        return realm.where(TripScore.class).equalTo("isSync", false).findAll();
    }
}
