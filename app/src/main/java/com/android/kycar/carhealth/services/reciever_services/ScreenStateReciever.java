package com.android.kycar.carhealth.services.reciever_services;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import androidx.core.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;


import com.android.kycar.carhealth.NxtApplication;
import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.beans.People;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.User;
import com.android.kycar.carhealth.entities.UserContact;
import com.android.kycar.carhealth.entities.UserContactSetting;
import com.android.kycar.carhealth.repositories.user.UserContactRepo;
import com.android.kycar.carhealth.repositories.user.UserContactSettingRepo;
import com.android.kycar.carhealth.repositories.user.UserRepo;
import com.android.kycar.carhealth.utils.CleanerOverlay;
import com.android.kycar.carhealth.utils.NotificationView;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.NOTIFICATION_SERVICE;

public class ScreenStateReciever extends BroadcastReceiver {

    private UserRepo userRepo;
    private UserContactRepo userContactRepo;
    private UserContactSettingRepo userContactSettingRepo;

    private Context context;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;

        userRepo = new UserRepo(context, User.class);
        userContactRepo = new UserContactRepo(context, UserContact.class);
        userContactSettingRepo = new UserContactSettingRepo(context, UserContactSetting.class);

        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            Log.i("Check","Screen went OFF");
            if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT &&
                    (NxtApplication.preferences.getString(Util.USER_PREFERENCE, null) != null)){
                setPersonalDataNotification();
                setSosNotification();
            } else {
                if (canDrawOverOtherApps(context)) {
                    if (
                            (NxtApplication.preferences.getBoolean(Util.LOCK_WIDGET_ENABLE, false)) &&
                                    (NxtApplication.preferences.getString(Util.USER_PREFERENCE, null) != null)) {

                        CleanerOverlay cleanerOverlay = new CleanerOverlay(context);
                        cleanerOverlay.show();
                    }
                } else {
                    Log.d("casd", "Cannot draw");
                }
            }
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            Log.i("Check","Screen went ON");
        }
    }

    private void setSosNotification() {
        List<UserContact> userContacts = userContactRepo.findAll();
        ArrayList<People> peoples = new ArrayList<>();
        for (UserContact userContact : userContacts) {

            UserContactSetting userContactSetting = userContactSettingRepo.findByUserContactId(userContact.getId());
            if(userContactSetting.getIsSos()) {
                People people = new People();
                people.setName(userContact.getName());
                people.setNumber(userContact.getNumber());
                peoples.add(people);
            }
        }

        customNotification(
                1,
                peoples.size() >=1 ? peoples.get(0).getName()+"-"+peoples.get(0).getNumber():"Please add sos contacts",
                peoples.size() >= 2 ? peoples.get(1).getName()+"-"+peoples.get(1).getNumber():"",
                peoples.size() >= 3 ? peoples.get(2).getName()+"-"+peoples.get(2).getNumber():"");
    }

    private void setPersonalDataNotification() {
        /*User user = (User) userRepo.findById(NxtApplication.preferences.getString(Util.USER_PREFERENCE, ""));
        String bloodGroup = "";
        if(user.getBloodGrp() != null) {
            switch (user.getBloodGrp()) {
                case 0:
                    bloodGroup = "A+";
                    break;
                case 1:
                    bloodGroup = "A-";
                    break;
                case 2:
                    bloodGroup = "B+";
                    break;
                case 3:
                    bloodGroup = "B-";
                    break;
                case 4:
                    bloodGroup = "O+";
                    break;
                case 5:
                    bloodGroup = "O-";
                    break;
                case 6:
                    bloodGroup = "AB+";
                    break;
                case 7:
                    bloodGroup = "AB-";
                    break;
            }
        }
            customNotification(
                    0,
                    NxtApplication.preferences.getString(Util.USERNAME_PREFERENCE, ""),
                    bloodGroup,
                    user.getOnGoingMedications());*/
    }

    @SuppressLint("NewApi")
    public static boolean canDrawOverOtherApps(Context context) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.N || Settings.canDrawOverlays(context);
    }

    public void customNotification(int notificationId, String title, String data, String data3) {
        Log.i("customNotification","Screen went OFF. Showing custom notification");
        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.noti_dialog);


        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(context, NotificationView.class);
        // Send data to NotificationView Class
        intent.putExtra("title", title);
        intent.putExtra("text", data);
        intent.putExtra("text2", data3);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.drawable.ic_launcher)
                // Set Ticker Message
                .setTicker("Sticker")
                // Dismiss Notification
                .setAutoCancel(true)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Set RemoteViews into Notification
                .setContent(remoteViews)
                .setOngoing(true);

        // Locate and set the Image into customnotificationtext.xml ImageViews
        remoteViews.setImageViewResource(R.id.image,R.drawable.ic_launcher);

        // Locate and set the Text into customnotificationtext.xml TextViews
        remoteViews.setTextViewText(R.id.title,title);
        remoteViews.setTextViewText(R.id.text,data);
        remoteViews.setTextViewText(R.id.text2,data3);

        // Create Notification Manager
        NotificationManager notificationmanager =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(notificationId, builder.build());

    }


}