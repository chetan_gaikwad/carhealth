package com.android.kycar.carhealth.ui.activity;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.repositories.user.ContactBean;
import com.android.kycar.carhealth.ui.adapters.ContactsRecyclerAdapter;

import java.util.ArrayList;

public class ContactSelectorActivity extends AppCompatActivity {
    private EditText edtName;
    private ArrayList<ContactBean> contacts;
    private RecyclerView contactRecycleView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_selector);
        edtName = (EditText) findViewById(R.id.edtName);
        edtName.addTextChangedListener(textWatcher);
        contactRecycleView = (RecyclerView) findViewById(R.id.contactRecycleList) ;
        contactRecycleView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        contactRecycleView.setLayoutManager(layoutManager);
        contacts = new ArrayList<>();
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            Log.d("fss","Filtered text "+s.toString());
            //if(s.toString().length() >2) {
                getFilteredContact(s.toString());
            //}
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private void getFilteredContact(String s) {
        Log.d("dedede","Apply filter for "+s);
        final ContentResolver resolver = getContentResolver();

        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection    = new String[] {ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER};


        final String sa1 = "%"+s+"%";

        Cursor people = resolver.query(uri,
                projection,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " LIKE ?",
                new String[] { sa1 }
                , null);


        int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

        contacts.clear();
        people.moveToFirst();
        do {
            String name = "";
            String number = "";
            try {
                if (indexName != 0) {
                    name = people.getString(indexName);
                    number = people.getString(indexNumber);
                }
            }catch (Exception e){
                e.printStackTrace();
            }



            final ContactBean contactData = new ContactBean("", name, number);
            contacts.add(contactData);

    // Do work...
} while (people.moveToNext());

        Log.d("sfms", "dcd + " + contacts.toString());
        people.close();

        ContactsRecyclerAdapter adapter = new ContactsRecyclerAdapter(ContactSelectorActivity.this, contacts);
        contactRecycleView.setAdapter(adapter);
        }
}
