package com.android.kycar.carhealth.task;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.android.kycar.carhealth.config.ConfigActivity;
import com.android.kycar.carhealth.config.ProgessDialogBox;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.ui.activity.DashboardActivity;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Chetan on 5/29/2016.
 * This class is used to take decision weather the car is normal communication wali or CAN
 */
public class CalculateVinTask extends AsyncTask<String,Void,String> {
    private OnTaskCompletedString listener;
    private static final String TAG = CalculateVinTask.class.getSimpleName();
    private ProgessDialogBox progessDialogBox;
    private Activity activity;

    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private BluetoothDevice dev = null;
    private BluetoothSocket sock = null;
    private BluetoothSocket sockFallback = null;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    public CalculateVinTask(Activity activity, OnTaskCompletedString listener) {
        this.listener = listener;
        this.activity = activity;
        prefs = PreferenceManager.getDefaultSharedPreferences(activity.getBaseContext());
        editor = prefs.edit();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progessDialogBox = new ProgessDialogBox(activity);
        progessDialogBox.showDailog("Checking car compatibility...");
    }


    @Override
    protected String doInBackground(String[] params) {
        try {
            return startService();
        }catch (IOException e){
            return "Failed to connect";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        progessDialogBox.dismissDialog();

        try {
            if(sock != null) {
                sock.getInputStream().close();
                sock.getOutputStream().close();
                sock.close();
            }
        }catch (IOException e){
        }

        listener.onTaskCompleted(result);
        Toast.makeText(activity,result,Toast.LENGTH_SHORT).show();
    }

    public String startService() throws IOException {
        Log.d(TAG, "Starting service..");

        final String remoteDevice = prefs.getString(ConfigActivity.BLUETOOTH_LIST_KEY, null);
        if (remoteDevice == null || "".equals(remoteDevice)) {

            DashboardActivity.isLiveData = false;
            Log.e(TAG, "No Bluetooth device has been selected.");
            // TODO kill this service gracefully
            return "No Bluetooth device has been selected.";
        } else {

            final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
            dev = btAdapter.getRemoteDevice(remoteDevice);
            Log.d(CalculateVinTask.class.getSimpleName(), "StartService() : Stopping Bluetooth discovery.");
            btAdapter.cancelDiscovery();
            try {
                return startObdConnection();
            } catch (Exception e) {
                Log.e(
                        TAG,
                        "StartService() : There was an error while establishing connection. -> "
                                + e.getMessage()
                );
                throw new IOException();
            }
        }
    }

    private String startObdConnection() throws IOException {

        Log.d(TAG, "startObdConnection() : Starting OBD connection..");
        try {
            // Instantiate a BluetoothSocket for the remote device and connect it.
            sock = dev.createRfcommSocketToServiceRecord(MY_UUID);
            sock.connect();
        } catch (Exception e1) {
            Log.e(TAG, "startObdConnection() : There was an error while establishing Bluetooth connection. Falling back..", e1);
            Class<?> clazz = sock.getRemoteDevice().getClass();
            Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};
            try {

                /************Fallback method 1*********************/
                Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                Object[] params = new Object[]{Integer.valueOf(1)};
                sockFallback = (BluetoothSocket) m.invoke(sock.getRemoteDevice(), params);
                sockFallback.connect();

                sock = sockFallback;
                /************Fallback method 1 end*********************/
                Log.e(TAG,"startObdConnection() : Connected");

                /************Fallback method 2*********************/
            } catch (Exception e2) {
                Log.e(TAG, "startObdConnection() : Couldn't fallback while establishing Bluetooth connection. Stopping app..", e2);
                return "Failed to connect";
            }
        }
        Log.e(TAG,"startObdConnection() : Connected");
        getDeviceDetails();
        getBatteryDetails();
        return fireCommand();

    }

    public String fireCommand(){
        String selectProtocol = "ATSP0\r";
        String echoof = "ATE0\r";
        String rpm = "01 0C\r";
        try {
            Log.i(Util.LOG, "fire command scanning for supported pid");

            sock.getOutputStream().write(echoof.getBytes());
            String response = readRawData(sock.getInputStream());

            Log.i(Util.LOG, "fire command echo of - " + response);

            sock.getOutputStream().write(selectProtocol.getBytes());
            response = readRawData(sock.getInputStream());
            Log.i(Util.LOG, " fire command select protocol - " + response);

            sock.getOutputStream().write(rpm.getBytes());
            response = readRawData(sock.getInputStream());


            String headerOn = "ATH1\r";

            sock.getOutputStream().write(headerOn.getBytes());
            response = readRawData(sock.getInputStream());

            //**calculate vin**///

            String serialNumber = "0902\r";
            String atdp = "ATDP\r";
            String atdpn = "ATDPN\r";

            sock.getOutputStream().write(atdp.getBytes());
            String detectedProtocol = readRawData(sock.getInputStream());
            if(detectedProtocol.contains("OK"))
                Log.i(Util.LOG, "getDeviceDetails serial number - "+detectedProtocol);


            sock.getOutputStream().write(atdpn.getBytes());
            String tempAtdpn = readRawData(sock.getInputStream());

            sock.getOutputStream().write(serialNumber.getBytes());
            response = readRawData(sock.getInputStream());
            if(response.contains("OK"))
                Log.i(Util.LOG, "getDeviceDetails serial number - "+response);
            return calculateVinHeaderOn(response,detectedProtocol);

        }catch (Exception e){
            e.printStackTrace();
            return "empty";
            //Util.writeToFile(activity,e.getMessage());
        }
    }


    public int getSupportedType(InputStream in){
        String headerOn = "ATH1\r";
        //String headerOff = "ATH0\r";
        String rpm = "01 0C\r";
        try {
            String rawData = readRawData(in);

            Pattern p = Pattern.compile("41 0C");
            Matcher m = p.matcher(rawData);
            int count = 0;
            while (m.find()){
                count +=1;
            }

            if(count == 1){
                return Util.isNormal;
            } else  return Util.isCrud;


        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    private String getDeviceDetails(){

        String reset = "ATZ\r";
        String echoof = "ATE0\r";
        String lineFeed = "ATL0\r";

        //Util.writeToFile(activity,"-------------------Started------------------");
        try {
            String temp="";
            sock.getOutputStream().write(reset.getBytes());
            boolean goAhead = false;

            do {

                temp = readRawData(sock.getInputStream());
                if(temp.contains("ELM327"))
                    goAhead=true;

            }while(!goAhead);

            sock.getOutputStream().write(echoof.getBytes());
            temp = readRawData(sock.getInputStream());
            if(temp.contains("OK")){
                Log.i(Util.LOG, "getDeviceDetails echoo off");
            }

            sock.getOutputStream().write(lineFeed.getBytes());
            temp = readRawData(sock.getInputStream());
            if(temp.contains("OK")){
                Log.i(Util.LOG, "getDeviceDetails line feed off");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return "";
    }
    private String getBatteryDetails() {

        String batteryVoltage = "ATRV\r";

        try {
            sock.getOutputStream().write(batteryVoltage.getBytes());
            String temp = readRawData(sock.getInputStream());
            Log.i(Util.LOG, "getBatteryDetails battery voltage - " + temp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String readRawData(InputStream in) throws IOException {
        byte b = 0;
        StringBuilder res = new StringBuilder();

        // read until '>' arrives OR end of stream reached
        char c;
        while(true)
        {
            b = (byte) in.read();
            if(b == -1) // -1 if the end of the stream is reached
            {
                break;
            }
            c = (char)b;
            if(c == '>') // read until '>' arrives
            {
                break;
            }
            res.append(c);
        }

    /*
     * Imagine the following response 41 0c 00 0d.
     *
     * ELM sends strings!! So, ELM puts spaces between each "byte". And pay
     * attention to the fact that I've put the word byte in quotes, because 41
     * is actually TWO bytes (two chars) in the socket. So, we must do some more
     * processing..
     */
        String rawData = res.toString().replaceAll("SEARCHING", "");

    /*
     * Data may have echo or informative cost like "INIT BUS..." or similar.
     * The response ends with two carriage return characters. So we need to take
     * everything from the last carriage return before those two (trimmed above).
     */
        //kills multiline.. rawData = rawData.substring(rawData.lastIndexOf(13) + 1);
        rawData = rawData.replaceAll("\\s", "");//removes all [ \t\n\x0B\f\r]
        return res.toString();
    }

    public String calculateVinHeaderOn(String response, String protocol) {

        editor.putString(Util.DEFAULT_PROTOCOL, protocol);
        response = response.replace(" ", "");
        String[] responses = response.split("\r");

        StringBuilder rawEncodedVin = new StringBuilder();

         if(protocol.contains("9141") || protocol.contains("14230")) {

             /*String header = responses[0].substring(4,6);
             editor.putString(Util.DEFAULT_HEADER, header);

             Log.d("Default Header", header);
             logger.logging("\n Default header "+header);
             editor.commit();*/

             for(String str: responses ){

                str = str.substring(12, str.length()-2);
                rawEncodedVin.append(str);
            }

        } else if(protocol.contains("15765")) {

             if(protocol.contains("11/")){

                 /*editor.putString(Util.DEFAULT_HEADER, responses[0].substring(0,3));
                 editor.commit();

                 Log.d("Default header", responses[0].substring(0,3));*/
                 for(String str: responses ){
                     if(responses[0] == str){
                         str = str.substring(13, str.length());
                         rawEncodedVin.append(str);
                     } else {
                         str = str.substring(5, str.length());
                         rawEncodedVin.append(str);
                     }
                 }

             } else if(protocol.contains("29/")){
                /* editor.putString(Util.DEFAULT_HEADER, responses[0].substring(6,8));
                 Log.d("Default header", responses[0].substring(6,8));
                 editor.commit();*/

                 for(String str: responses ){
                     if(responses[0] == str){
                         str = str.substring(str.length()-6, str.length());
                         rawEncodedVin.append(str);
                     } else {
                         str = str.substring(str.length()-14, str.length());
                         rawEncodedVin.append(str);
                     }
                 }
             }
        }
        return hexToAscii(rawEncodedVin.toString());
    }

    public String hexToAscii(String hex){
        hex = hex.substring(hex.length()-34, hex.length());
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < hex.length(); i+=2) {
            String str = hex.substring(i, i+2);
            output.append((char)Integer.parseInt(str, 16));
        }
        Log.d("Final output vin :-", output.toString());
        return output.toString();


    }

    public void calculateVinHeaderOff(String response, String protocol) {

        String temp;
        String refactored = "";

        if(protocol.contains("A7")) {
            temp = response.replace("\r", "").replace("0:", "").replace("1:", "").replace("2:", "").replace(" ", "");
            refactored = temp.substring(9);
        }
        hexToAscii(refactored);
    }

    private ArrayList<String> countHeader(String response, String pattern) {
        try {
            //Util.writeToFile(activity, "Pattern : " + pattern + " ---- Response : " + response);
            response = response.replace(" ", "");
            pattern = pattern.replace(" ", "");
            pattern = pattern.replace("\r", "");
            ArrayList<String> headerArray = new ArrayList<>();
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(response);
            int count = 0;
            while (m.find()) {
                count += 1;
            }

            headerArray.add(response.substring(0, response.indexOf(pattern) - 2));


            p = Pattern.compile(Pattern.quote("\r") + "(.*?)" + Pattern.quote(pattern));
            m = p.matcher(response);
            while (m.find()) {
                String header = m.group(1);
                System.out.println(header);
                headerArray.add(header.substring(0, header.length() - 2));
            }

            //Util.writeToFile(activity, "Headers is " + headerArray.toString());
            Log.d(TAG, "Count is " + count);
            Log.d(TAG, "Headers is " + headerArray.toString());
            return headerArray;
        }catch (Exception e){
            //Util.writeToFile(activity, "Exception is " + e.getMessage());
        }
        return new ArrayList<String>();
    }
}
