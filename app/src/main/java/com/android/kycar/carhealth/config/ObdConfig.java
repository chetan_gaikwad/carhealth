package com.android.kycar.carhealth.config;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.android.kycar.carhealth.services.dto.commands.ObdCommand;
import com.android.kycar.carhealth.services.dto.commands.SpeedCommand;
import com.android.kycar.carhealth.services.dto.commands.control.DistanceSinceCCCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.BatteryCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.LoadCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.MassAirFlowCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.OilTempCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.RPMCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.ThrottlePositionCommand;
import com.android.kycar.carhealth.services.dto.commands.fuel.FindFuelTypeCommand;
import com.android.kycar.carhealth.services.dto.commands.temperature.EngineCoolantTemperatureCommand;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public final class ObdConfig {

    public static ArrayList<ObdCommand> getCommands() {
        ArrayList<ObdCommand> cmds = new ArrayList<>();

        cmds.add(new DistanceSinceCCCommand());
        cmds.add(new RPMCommand());
        cmds.add(new FindFuelTypeCommand());
        cmds.add(new EngineCoolantTemperatureCommand());
        cmds.add(new OilTempCommand());
        cmds.add(new SpeedCommand());
        cmds.add(new LoadCommand());
        cmds.add(new ThrottlePositionCommand());
        cmds.add(new MassAirFlowCommand());
        cmds.add(new BatteryCommand());
        /*
        *
        *
        *
        *
        * */
        // Control
        /*cmds.add(new ModuleVoltageCommand());
        cmds.add(new EquivalentRatioCommand());
        cmds.add(new DistanceMILOnCommand());
        cmds.add(new DtcNumberCommand());
        cmds.add(new DistanceSinceCCCommand());
        cmds.add(new TimingAdvanceCommand());
        cmds.add(new TroubleCodesCommand());
        cmds.add(new VinCommand());

        // Engine
        cmds.add(new LoadCommand());
        cmds.add(new RPMCommand());
        cmds.add(new RuntimeCommand());
        cmds.add(new MassAirFlowCommand());
        cmds.add(new ThrottlePositionCommand());

        // Fuel
        cmds.add(new FindFuelTypeCommand());
        cmds.add(new ConsumptionRateCommand());
         //cmds.add(new AverageFuelEconomyObdCommand());
        //cmds.add(new FuelEconomyCommand());
        cmds.add(new FuelLevelCommand());
        // cmds.add(new FuelEconomyMAPObdCommand());
         //cmds.add(new FuelEconomyCommandedMAPObdCommand());
    *//*    cmds.add(new FuelTrimCommand(FuelTrim.LONG_TERM_BANK_1));
        cmds.add(new FuelTrimCommand(FuelTrim.LONG_TERM_BANK_2));
        cmds.add(new FuelTrimCommand(FuelTrim.SHORT_TERM_BANK_1));
        cmds.add(new FuelTrimCommand(FuelTrim.SHORT_TERM_BANK_2));*//*
        cmds.add(new AirFuelRatioCommand());
        cmds.add(new WidebandAirFuelRatioCommand());
        cmds.add(new OilTempCommand());

        // Pressure
        cmds.add(new BarometricPressureCommand());
        cmds.add(new FuelPressureCommand());
        cmds.add(new FuelRailPressureCommand());
        cmds.add(new IntakeManifoldPressureCommand());

        // Temperature
        cmds.add(new AirIntakeTemperatureCommand());
        cmds.add(new AmbientAirTemperatureCommand());


        // Misc

        cmds.add(new FindFuelTypeCommand());
        cmds.add(new EngineCoolantTemperatureCommand());
        cmds.add(new OilTempCommand());
        cmds.add(new SpeedCommand());

        cmds.add(new FindFuelTypeCommand());
        cmds.add(new EngineCoolantTemperatureCommand());
        cmds.add(new OilTempCommand());
        cmds.add(new SpeedCommand());
*/
        return cmds;
    }

     public static String getDateTime(){
         Date date = new Date();
         SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a");
         String formattedDate = sdf.format(date);

        return formattedDate;
    }
    public static void showDialog(Context c, String title, String msg){
        new AlertDialog.Builder(c)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }
}