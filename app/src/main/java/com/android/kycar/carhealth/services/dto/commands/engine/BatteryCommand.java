package com.android.kycar.carhealth.services.dto.commands.engine;


import com.android.kycar.carhealth.services.dto.commands.ObdCommand;
import com.android.kycar.carhealth.services.dto.enums.AvailableCommandNames;

/**
 * Created by Chetan on 2/28/2017.
 */

public class BatteryCommand extends ObdCommand {

    float batteryLevel = 0;
    public BatteryCommand() {
        super("ATRV");
    }
    public BatteryCommand(String command) {
        super(command);
    }

    @Override
    protected void performCalculations() {
        batteryLevel = buffer.get(0);
    }

    @Override
    public String getFormattedResult() {
        return null;
    }

    @Override
    public String getCalculatedResult() {
        return null;
    }

    @Override
    public String getName() {
        return  AvailableCommandNames.BATTERY.getValue();
    }

    /**
     * @return a int.
     */
    public float getBatteryLevel() {
        return batteryLevel;
    }
}
