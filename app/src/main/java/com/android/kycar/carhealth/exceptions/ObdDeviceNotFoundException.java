package com.android.kycar.carhealth.exceptions;

/**
 * Created by Krishna on 1/30/2016.
 */
public class ObdDeviceNotFoundException extends Exception {

    public ObdDeviceNotFoundException() {
        // TODO Auto-generated constructor stub
    }

    public ObdDeviceNotFoundException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public ObdDeviceNotFoundException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public ObdDeviceNotFoundException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }
}
