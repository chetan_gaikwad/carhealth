package com.android.kycar.carhealth.services.connection_services.rest_services;

import org.json.JSONObject;

/**
 * Created by Krishna on 2/22/2017.
 */

public class FetchVehicleGearRatioRestCall extends RestService{
    public String getGearRatio(JSONObject jsonObject) {
        String url = "/vehicleGearRatio/findVehicleGearRatioByVariantId";
        return restServiceCall(url, "POST", jsonObject);

    }
}
