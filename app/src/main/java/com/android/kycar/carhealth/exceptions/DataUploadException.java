package com.android.kycar.carhealth.exceptions;

/**
 * Created by Krishna on 1/30/2016.
 */
public class DataUploadException extends Exception {
    public DataUploadException() {
        // TODO Auto-generated constructor stub
    }

    public DataUploadException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public DataUploadException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public DataUploadException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }
}
