package com.android.kycar.carhealth.ui.activity.drawer_menu.car_milage;

import android.app.DatePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.services.dto.commands.control.DistanceSinceCCCommand;
import com.android.kycar.carhealth.services.dto.enums.AvailableCommandNames;
import com.android.kycar.carhealth.services.obd_services.AbstractGatewayService;
import com.android.kycar.carhealth.services.obd_services.ObdCommandJob;
import com.android.kycar.carhealth.trips.Average;
import com.android.kycar.carhealth.ui.adapters.AverageAdapter;
import com.google.android.material.tabs.TabLayout;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CarAverage extends AppCompatActivity {
    private boolean isServiceBound;
    private AbstractGatewayService service;
    private static int distanceFromOBD = -1;

    private static final String TAG = "obd";
    /**
     * The {@link androidx.core.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link androidx.core.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_average);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        distanceFromOBD = -1;
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        //Intent serviceIntent = new Intent(this, ObdGetDistanceService.class);
        //bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_car_average, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void dontSupportKM() {
        Util.showAlert("Your car does not support distance command. Please enter the details" +
                " from manual tab", CarAverage.this);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a ManualFragment (defined as a static inner class below).
            if(position == 0) {
                return ManualFragment.newInstance(position + 1);
            }else{
                    return OBD.newInstance(position + 1);
            }
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Manual";
                case 1:
                    return "From OBD";

            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class ManualFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private DatePickerDialog fromDatePickerDialog;
        private EditText date;
        private EditText cost;
        private EditText distance;
        private Button showCalender;
        private Button save;
        private AverageAdapter averageAdapter;
        private ListView averageList;
        private CheckBox fuelFull;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ManualFragment newInstance(int sectionNumber) {
            ManualFragment fragment = new ManualFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public ManualFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_car_average, container, false);

            Log.i("obd","manual hhh");
            averageList = (ListView) rootView.findViewById(R.id.average_list);
            cost = (EditText) rootView.findViewById(R.id.cost);
            date = (EditText) rootView.findViewById(R.id.date_view);
            distance = (EditText) rootView.findViewById(R.id.km_covered);
            fuelFull = (CheckBox) rootView.findViewById(R.id.full_check);
            date.setInputType(InputType.TYPE_NULL);

            //ArrayList<Average> averageArrayList = triplog.getAverageDetails();
            //averageAdapter = new AverageAdapter(getActivity(),averageArrayList,null);
            averageList.setAdapter(averageAdapter);
            showCalender = (Button) rootView.findViewById(R.id.showcalender);
            showCalender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fromDatePickerDialog.show();
                }
            });

            save = (Button) rootView.findViewById(R.id.save);
            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!distance.getText().toString().isEmpty() &&
                            distance.getText().toString() != null &&
                            !cost.getText().toString().isEmpty() &&
                            cost.getText().toString() != null &&
                            !date.getText().toString().isEmpty() &&
                            date.getText().toString() != null) {

                        Average average = new Average();
                        average.setDate(date.getText().toString());
                        average.setDistance(distance.getText().toString());
                        average.setCost(cost.getText().toString());
                        /*if(triplog.insertAverage(average)){
                            distance.setText("");
                            cost.setText("");
                            date.setText("");
                            Util.showAlert("Details saved", getActivity());
                            ArrayList<Average> averageArrayList = triplog.getAverageDetails();
                            averageAdapter = new AverageAdapter(getActivity(),averageArrayList,null);
                            averageList.setAdapter(averageAdapter);

                            if(fuelFull.isChecked()){
                                int totalCost = 0;
                                for(int i = 0;i<averageArrayList.size();i++){
                                    totalCost = totalCost + Integer.parseInt(averageArrayList.get(i).getCost());
                                }
                                int startKM = Integer.parseInt(averageArrayList.get(0).getObdDistance());
                                int endKM = Integer.parseInt(averageArrayList.get(averageArrayList.size()-1).getObdDistance());

                                double averageOfCar = (totalCost/(endKM-startKM));
                                Util.showAlert("Car average = "+averageOfCar, getActivity());
                            }
                        }else{
                            Util.showAlert("Cannot save the details",getActivity());
                        }*/
                    }else{
                        Toast.makeText(getActivity(), "Please enter all the feilds", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            Calendar newCalendar = Calendar.getInstance();
            fromDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                    date.setText(format.format(newDate.getTime()));
                }

            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

            View view = getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            return rootView;
        }
    }

    public static class OBD extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private DatePickerDialog fromDatePickerDialog;
        private EditText date;
        private EditText cost;
        private EditText distance;
        private Button showCalender;
        private Button save;
        private Button getKM;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static OBD newInstance(int sectionNumber) {
            OBD fragment = new OBD();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public OBD() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_car_average_obd, container, false);

            Log.i("obd","obd hhh");
            cost = (EditText) rootView.findViewById(R.id.cost);
            date = (EditText) rootView.findViewById(R.id.date_view);
            distance = (EditText) rootView.findViewById(R.id.km_covered);
            date.setInputType(InputType.TYPE_NULL);

            showCalender = (Button) rootView.findViewById(R.id.showcalender);
            showCalender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fromDatePickerDialog.show();
                }
            });
            getKM = (Button) rootView.findViewById(R.id.getkm);
            getKM.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(distanceFromOBD >= 0){
                        distance.setText(distanceFromOBD);
                    }else{
                        Util.showAlert("Getting distance from OBD please try again",getActivity());
                    }

                }
            });

            save = (Button) rootView.findViewById(R.id.save);
            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!distance.getText().toString().isEmpty() &&
                            distance.getText().toString() != null &&
                            !cost.getText().toString().isEmpty() &&
                            cost.getText().toString() != null &&
                            !date.getText().toString().isEmpty() &&
                            date.getText().toString() != null) {
                        Average average = new Average();
                        average.setDate(date.getText().toString());
                        /*if(triplog.insertAverage(average)){
                            distance.setText("");
                            cost.setText("");
                            date.setText("");
                            Util.showAlert("Details saved",getActivity());
                        }else{
                            Util.showAlert("Cannot save the details",getActivity());
                        }*/
                    }else{
                        Toast.makeText(getActivity(), "Please enter all the feilds", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            Calendar newCalendar = Calendar.getInstance();
            fromDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                    date.setText(format.format(newDate.getTime()));
                }

            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


            return rootView;
        }
    }

    public void stateUpdate(final ObdCommandJob job) {

        final String cmdName = job.getCommand().getName();
        String cmdResult = "";
        final String cmdID = LookUpCommand(cmdName);

        if (job.getState().equals(ObdCommandJob.ObdCommandJobState.EXECUTION_ERROR)) {
            cmdResult = job.getCommand().getResult();
            if (cmdResult != null) {
                //obdStatus.setText(cmdResult.toLowerCase());
                //obdStatus.setImageDrawable(getDrawable(R.drawable.obd_on));
            }
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.NOT_SUPPORTED)) {
            cmdResult = getString(R.string.status_obd_no_support);
        } else {
            cmdResult = job.getCommand().getFormattedResult();
        }
        updateTripStatistic(job, cmdID);
    }
    private void updateTripStatistic(final ObdCommandJob job, final String cmdID) {

            if (cmdID.equals(AvailableCommandNames.DISTANCE_TRAVELED_AFTER_CODES_CLEARED.toString())) {
                DistanceSinceCCCommand command = (DistanceSinceCCCommand) job.getCommand();
                Log.d("speed", "Distance km" + command.getKm());
                if(command.getKm() >= 0){
                    distanceFromOBD = command.getKm();
                    doUnbindService();
                }

            }
    }
    public static String LookUpCommand(String txt) {
        for (AvailableCommandNames item : AvailableCommandNames.values()) {
            if (item.getValue().equals(txt)) return item.name();
        }
        return txt;
    }

    private void doBindService() {

        if (!isServiceBound) {

            //Intent serviceIntent = new Intent(this, ObdGetDistanceService.class);
            //bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);
        }
    }

    private void doUnbindService() {


        if (isServiceBound) {
            if (service.isRunning()) {
                service.stopService();
            }
            Log.d(TAG, "Unbinding OBD service..");
            unbindService(serviceConn);
            isServiceBound = false;
        }

    }
    private ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            Log.d(TAG, className.toString() + " service is bound");
            isServiceBound = true;
            service = ((AbstractGatewayService.AbstractGatewayServiceBinder) binder).getService();
            service.setContext(CarAverage.this);
            Log.d(TAG, "Starting live data");
            try {
                service.startService();
            } catch (IOException ioe) {
                Log.e(TAG, "Failure Starting live data");
                doUnbindService();
            }
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        // This method is *only* called when the connection to the service is lost unexpectedly
        // and *not* when the client unbinds (http://developer.android.com/guide/components/bound-services.html)
        // So the isServiceBound attribute should also be set to false when we unbind from the service.
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(TAG, className.toString() + " service is unbound");
            isServiceBound = false;
        }
    };

}
