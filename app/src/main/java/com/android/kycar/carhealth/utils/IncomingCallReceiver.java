package com.android.kycar.carhealth.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.kycar.carhealth.config.Util;

import java.lang.reflect.Method;
import java.util.ArrayList;

import telephony.ITelephony;

public class IncomingCallReceiver extends BroadcastReceiver {
    private ITelephony telephonyService;
    private String blacklistednumber = "+458664455";

    @Override
    public void onReceive(Context context, Intent intent) {

        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class c = Class.forName(tm.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            ITelephony telephonyService = (ITelephony) m.invoke(tm);
            Bundle bundle = intent.getExtras();
            String phoneNumber = bundle.getString("incoming_number");
            Log.d("INCOMING", phoneNumber);
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            if(preferences.getBoolean(Util.IS_DRIVE_MODE,false)) {
                if ((phoneNumber != null)) {
                    telephonyService.silenceRinger();
                    telephonyService.endCall();
                    Log.e("HANG UP", phoneNumber);

                    SmsManager smsManager = SmsManager.getDefault();
                    ArrayList<String> parts = smsManager.divideMessage("I am driving. Please call me after some time.");
                    smsManager.sendMultipartTextMessage(phoneNumber, null, parts, null, null);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}