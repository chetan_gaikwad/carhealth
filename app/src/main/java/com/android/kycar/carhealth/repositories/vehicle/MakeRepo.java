package com.android.kycar.carhealth.repositories.vehicle;

import android.content.Context;


import com.android.kycar.carhealth.entities.vehicle_master.Make;
import com.android.kycar.carhealth.repositories.Repository;

import io.realm.exceptions.RealmException;

/**
 * Created by Krishna on 2/1/2016.
 */
public class MakeRepo extends Repository {
    public MakeRepo(Context context, Class _aClass) {
        super(context, _aClass);
    }

    public Make findByName(String name) throws RealmException {
        Make make =   realm.where(Make.class).equalTo("name", name).findFirst();
        return make;
    }
}
