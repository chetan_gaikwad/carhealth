package com.android.kycar.carhealth.ui.fragments;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.kycar.carhealth.NxtApplication;
import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.async_task.SendSosNotificationAsyncTask;
import com.android.kycar.carhealth.beans.Coordinates;
import com.android.kycar.carhealth.beans.Obstacle;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.UserContact;
import com.android.kycar.carhealth.entities.UserContactSetting;
import com.android.kycar.carhealth.repositories.user.UserContactRepo;
import com.android.kycar.carhealth.repositories.user.UserContactSettingRepo;
import com.android.kycar.carhealth.services.dto.commands.SpeedCommand;
import com.android.kycar.carhealth.services.dto.commands.control.DistanceSinceCCCommand;
import com.android.kycar.carhealth.services.dto.enums.AvailableCommandNames;
import com.android.kycar.carhealth.services.io_services.SmsAlerts;
import com.android.kycar.carhealth.services.obd_services.ObdCommandJob;
import com.android.kycar.carhealth.ui.activity.DashboardActivity;
import com.android.kycar.carhealth.utils.BusProvider;
import com.android.kycar.carhealth.utils.FontCache;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

/**
 * Created by Ratan on 7/29/2015.
 */
public class ValuesFragmentUpdated extends Fragment implements LocationListener {
    private ImageView imgGpsStatus;
    private ImageView imgOBDStatus;
    private ImageView imgDataStatus;
    private ImageView imgCloudStatus;
    private TextView mGpsSpeed;
    private TextView mObdSpeed;
    protected SharedPreferences preferences;
    protected SharedPreferences.Editor editor;
    private AudioManager am;
    private ImageView sos;
    private ImageView imgDriveMode;

    private ViewGroup cnt;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        container = (ViewGroup) inflater.inflate(R.layout.value_layout_updated, null);
        cnt = container;
        imgGpsStatus = (ImageView) container.findViewById(R.id.gps_status);
        imgGpsStatus.setOnClickListener(clickListener);
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                setupGPS();
            }
        } else {
            setupGPS();
        }

        new Thread() {
            public void run() {
                initUI(cnt);
            }
        }.start();
        return container;
    }



    private void initUI(final ViewGroup container) {
        preferences = PreferenceManager.getDefaultSharedPreferences(container.getContext());
        editor = preferences.edit();

        imgDriveMode = (ImageView) container.findViewById(R.id.img_drive_mode);

        if(preferences.contains(Util.IS_DRIVE_MODE) && preferences.getBoolean(Util.IS_DRIVE_MODE, true)) {
            imgDriveMode.setImageResource(R.drawable.drive_mode_on);
        }

        imgDriveMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(preferences.contains(Util.IS_DRIVE_MODE) && preferences.getBoolean(Util.IS_DRIVE_MODE, true)) {
                    am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                    preferences.edit().putBoolean(Util.IS_DRIVE_MODE, false).commit();
                    imgDriveMode.setImageResource(R.drawable.drive_mode_off);
                } else {
                    am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                    preferences.edit().putBoolean(Util.IS_DRIVE_MODE, true).commit();
                    imgDriveMode.setImageResource(R.drawable.drive_mode_on);
                }
            }
        });

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mGpsSpeed = (TextView) container.findViewById(R.id.gps_speed);
                mGpsSpeed.setTypeface(FontCache.get("", getActivity()));
                mObdSpeed = (TextView) container.findViewById(R.id.obd_speed);
                mObdSpeed.setTypeface(FontCache.get("", getActivity()));
            }
        });


        sos = (ImageView) container.findViewById(R.id.sos);
        sos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(sos, "Long press to send sos", Snackbar.LENGTH_LONG).show();
            }
        });
        sos.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                /*Snackbar.make(sos, "Confirm SOS", Snackbar.LENGTH_LONG)
                        .setAction("Send", onClickListener).show();*/
                NxtApplication.sendDate();
                return true;
            }
        });
        imgOBDStatus = (ImageView) container.findViewById(R.id.obd_status);
        imgOBDStatus.setOnClickListener(clickListener);
        if (!isBluetoothEnabled()) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    imgOBDStatus.setImageResource(R.drawable.bluetooth_off);
                }
            });

        }else{
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    imgOBDStatus.setImageResource(R.drawable.bluetooth_on);
                }
            });
        }
        imgDataStatus = (ImageView) container.findViewById(R.id.data_status);
        imgDataStatus.setOnClickListener(clickListener);
        imgCloudStatus = (ImageView) container.findViewById(R.id.cloud_status);
        imgCloudStatus.setOnClickListener(clickListener);

        if(preferences.getBoolean(Util.IS_UPLOAD_OBD, true)){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    imgCloudStatus.setImageResource(R.drawable.cloud_sync_off);
                }
            });

        }

        am = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);


        IntentFilter filter1 = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        getActivity().registerReceiver(mReceiver, filter1);

        locationManager();
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            JSONObject notificationMsgObj = new JSONObject();
            JSONObject notification = new JSONObject();

            try {
                notification.put("title" , preferences.getString(Util.USERNAME_PREFERENCE, "")+" has met with an accident");
                notification.put("body", "Click to view on your map");
                notification.put("time", new Date().toString());
                notificationMsgObj.put("notification", notification);

                JSONObject notData = new JSONObject();
                notData.put("userId", preferences.getString(Util.USERNAME_PREFERENCE, ""));
                if(NxtApplication.currLocation == null) {
                    notData.put("msg", "Location not available");
                } else {
                    notData.put("lat", NxtApplication.currLocation.getLatitude());
                    notData.put("lng", NxtApplication.currLocation.getLongitude());
                    notData.put("acc", NxtApplication.currLocation.getAccuracy());
                }

                notificationMsgObj.put("not_data", notData);
                JSONArray userSoses = new JSONArray();

                UserContactRepo userContactRepo = new UserContactRepo(getContext(), UserContact.class);
                List<UserContact> userContacts = userContactRepo.findAll();

                UserContactSettingRepo userContactSettingRepo = new UserContactSettingRepo(getContext(), UserContactSetting.class);
                for (UserContact userContact : userContacts) {
                    UserContactSetting userContactSetting = userContactSettingRepo.findByUserContactId(userContact.getId());
                    if (userContactSetting.getIsSos()) {
                        SmsAlerts smsAlerts = new SmsAlerts();
                        smsAlerts.sendSosMessage(userContact.getNumber(), new Coordinates(NxtApplication.currLocation));

                        JSONObject userSos = new JSONObject();
                        userSos.put("name", userContact.getName());
                        userSos.put("number", userContact.getNumber());
                        userSoses.put(userSos);

                        Snackbar.make(sos, "SOS sent", Snackbar.LENGTH_LONG).show();
                    }
                }

                /** Sending sos to cops */
                if(userSoses.length() != 0){
                    notificationMsgObj.put("userSoses", userSoses);
                }
                new SendSosNotificationAsyncTask().execute(notificationMsgObj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };


    @Subscribe
    public void locationUpdate(Location location) {
        if(location != null && mGpsSpeed != null) {
            mGpsSpeed.setText((int)location.getSpeed() + " ");
        }
    }

    @Subscribe
    public void a2DpControl(String event) {
        if (event.equals("cloud")) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    imgCloudStatus.setImageResource(R.drawable.cloud_active);
                }
            });

        } else {
            ((DashboardActivity) getActivity()).onAudInput(event, 0);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //unregister bluetooth state listener
        getActivity().unregisterReceiver(mReceiver);
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (v.getId() == R.id.obd_status) {
                if (isBluetoothEnabled()) {
                } else {
                    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    mBluetoothAdapter.enable();
                }
            } else if (v.getId() == R.id.gps_status) {
                final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

                if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                } else {
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            } else if (v.getId() == R.id.data_status) {
                gotoDataSettings();
            } else if (v.getId() == R.id.cloud_status) {
                if(preferences.getBoolean(Util.IS_UPLOAD_OBD, true)){
                    imgCloudStatus.setImageResource(R.drawable.cloud_sync_off);
                    preferences.edit().putBoolean(Util.IS_UPLOAD_OBD, false).commit();
                    Toast.makeText(getContext(), "Data disable", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Data enable", Toast.LENGTH_SHORT).show();
                    imgCloudStatus.setImageResource(R.drawable.cloud_active);
                    preferences.edit().putBoolean(Util.IS_UPLOAD_OBD, true).commit();
                }
            }
        }
    };

    private boolean isBluetoothEnabled() {
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        return btAdapter.isEnabled();
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imgOBDStatus.setImageResource(R.drawable.bluetooth_off);
                            }
                        });

                        break;
                    case BluetoothAdapter.STATE_ON:
                        imgOBDStatus.setImageResource(R.drawable.bluetooth_on);
                        break;
                }
            }
        }
    };

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
        if (LocationManager.GPS_PROVIDER.equals(s)) {
            imgGpsStatus.setImageResource(R.drawable.location_on);
        }
    }

    @Override
    public void onProviderDisabled(String s) {
        if (LocationManager.GPS_PROVIDER.equals(s)) {
            imgGpsStatus.setImageResource(R.drawable.location_off);
        }
    }

    @Subscribe
    public void subscribeNextObstacle(final Obstacle nextObstacle) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (nextObstacle.getDistance() > 1000) {
                    // tvNextObstacleDistance.setText(""+(float)nextObstacle.getDistance()/1000+" kms");
                } else {
                    // tvNextObstacleDistance.setText(""+nextObstacle.getDistance()+" meters");
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
        final IntentFilter dataIntentFilter = new IntentFilter();
        dataIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        getActivity().registerReceiver(networkBroadcast, dataIntentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
        getActivity().unregisterReceiver(networkBroadcast);
    }

    public void locationManager() {
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        try {
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imgGpsStatus.setImageResource(R.drawable.location_on);
                    }
                });
            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imgGpsStatus.setImageResource(R.drawable.location_off);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupGPS() {
        LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, this);
    }
    public void gotoDataSettings(){
        startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
    }

    @Subscribe
    public void updateTripStatistic(ObdCommandJob job) {

        final String cmdName = job.getCommand().getName();
        final String cmdID = LookUpCommand(cmdName);

        if (cmdID.equals(AvailableCommandNames.SPEED.toString())) {
            final SpeedCommand command = (SpeedCommand) job.getCommand();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mObdSpeed.setText(command.getMetricSpeed() +"");
                }
            });
        } else if (cmdID.equals(AvailableCommandNames.DISTANCE_TRAVELED_AFTER_CODES_CLEARED.toString())) {
            final DistanceSinceCCCommand command = (DistanceSinceCCCommand) job.getCommand();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    /*if(preferences.contains(Util.TRIP_ID)) {
                        tripStartDistance = getTripStartDistance(preferences.getString(Util.TRIP_ID, ""));
                        ((TextView) cnt.findViewById(R.id.current_distance)).setText("Cureent trip distance covered: "+(command.getKm() - tripStartDistance)+"");
                    }*/
                }
            });
        }
    }
    public static String LookUpCommand(String txt) {
        for (AvailableCommandNames item : AvailableCommandNames.values()) {
            if (item.getValue().equals(txt)) return item.name();
        }
        return txt;
    }

    BroadcastReceiver networkBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE );
            NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
            boolean isConnected = activeNetInfo != null && activeNetInfo.isConnectedOrConnecting();
            if(isConnected){
                imgDataStatus.setImageResource(R.drawable.internet_on);
            } else {
                imgDataStatus.setImageResource(R.drawable.internet_off);
                imgCloudStatus.setImageResource(R.drawable.cloud_sync_off);
            }
        }
    };
}
