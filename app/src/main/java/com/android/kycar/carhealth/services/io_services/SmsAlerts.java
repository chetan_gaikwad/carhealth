package com.android.kycar.carhealth.services.io_services;

import android.content.Context;
import android.telephony.SmsManager;
import android.util.Log;
import com.android.kycar.carhealth.beans.Coordinates;
import com.android.kycar.carhealth.entities.UserContact;
import com.android.kycar.carhealth.entities.UserContactSetting;
import com.android.kycar.carhealth.repositories.user.UserContactRepo;
import com.android.kycar.carhealth.repositories.user.UserContactSettingRepo;

import java.util.List;


/**
 * Created by krishna on 5/12/15.
 */
public class SmsAlerts {

    UserContactRepo userContactRepo;

    public void sendSMSAccidentAlert(String vehicleId, float intencity, Coordinates coords, Context context){

        userContactRepo = new UserContactRepo(context, UserContact.class);
        List<UserContact> soses = userContactRepo.findAll();
        userContactRepo.close();

        StringBuilder content = new StringBuilder();

        content.append("This is demo msg...");
        content.append("#NXTALERTS ");

        content.append("Intensity: "+intencity);

        content.append(coords.toString());
        SmsManager smsManager = SmsManager.getDefault();
        Log.d("SMS output", "" + content.toString());
        for (UserContact sos : soses) {
            UserContactSettingRepo userContactSettingRepo = new UserContactSettingRepo(context, UserContactSetting.class);
            UserContactSetting userContactSetting = userContactSettingRepo.findByUserContactId(sos.getId());
            if(userContactSetting.getIsSos() && sos.getNumber() != null) {

                Log.d("Msg Sent", ""+sos.getNumber());
                smsManager.sendTextMessage(sos.getNumber(), null, content.toString(), null, null);
            }
        }
    }

    public boolean sendSms(String senderNumber, String content){

        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(senderNumber, null, content, null, null);
        return  true;
    }

    public boolean sendSosNotificationAlert(String senderNumber) {

        StringBuilder content = new StringBuilder();
        content.append("Hi, I am adding you as my SOS contact.");
        sendSms(senderNumber, content.toString());
        return true;
    }

    public boolean sendSosMessage(String senderNumber, Coordinates coords) {

        StringBuilder content = new StringBuilder();
        content.append("This is a SOS message. ");
        if(coords.getLatitude() == 0 ) {
            content.append("Location not available. Please contact");
        } else {
            content.append("Location :-"+coords.toString());
        }

        sendSms(senderNumber, content.toString());
        return true;
    }




    public boolean sendDummySmsNotificationAlert(String senderNumber, Coordinates coords) {

        StringBuilder content = new StringBuilder();
        content.append("Hi, I have added you as my SOS contact on Nxt Gizmo.");
        content.append("Location is :- "+coords.toString()+" ");
        content.append(" To know more http://nxtgizmo.com");
        sendSms(senderNumber, content.toString());
        return true;
    }

    public boolean sendConvoySmsNotification(String senderNumber, String convoyName, String convoyId) {

        StringBuilder content = new StringBuilder();
        content.append("http://convoyinvitation.com/nxtauto/"+convoyName+"/"+convoyId);
        sendSms(senderNumber, content.toString());

        return true;
    }

    public boolean sendInvitationSms(String senderNumber) {

        StringBuilder content = new StringBuilder();
        content.append("Hi, I want connect with you via NxtGizmo Android application. Please download the app from playstore. ");
        content.append("https://play.google.com/store/apps/details?id=com.nxt.autoz");
        sendSms(senderNumber, content.toString());
        return true;
    }

    public boolean sentLocationSharingSms(String senderNumber) {
        StringBuilder content = new StringBuilder();
        content.append("I have shared my location please open this in Nxt Gizmo app. ");
        content.append("http://nxtgizmo.com");
        sendSms(senderNumber, content.toString());
        return true;
    }
}
