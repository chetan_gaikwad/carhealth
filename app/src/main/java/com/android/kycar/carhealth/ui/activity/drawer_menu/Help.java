package com.android.kycar.carhealth.ui.activity.drawer_menu;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.kycar.carhealth.R;


public class Help extends AppCompatActivity {

    TextView url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        url = (TextView)findViewById(R.id.url);
        url.setText(
                Html.fromHtml(
                        "<a href=\"http://nxtgizmo.com/\">Nxt Gizmo</a> "));
        url.setMovementMethod(LinkMovementMethod.getInstance());

    }
}
