package com.android.kycar.carhealth.utils;

import android.content.Context;

import com.android.kycar.carhealth.async_task.CreateTripAsyncTask;
import com.android.kycar.carhealth.async_task.CreateTripScoreAsyncTask;
import com.android.kycar.carhealth.async_task.CreateTripSessionAsyncTask;
import com.android.kycar.carhealth.entities.trip_transaction.Trip;
import com.android.kycar.carhealth.entities.trip_transaction.TripScore;
import com.android.kycar.carhealth.entities.trip_transaction.TripSession;
import com.android.kycar.carhealth.repositories.trip.TripRepo;
import com.android.kycar.carhealth.repositories.trip.TripScoreRepo;
import com.android.kycar.carhealth.repositories.trip.TripSessionRepo;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by Krishna on 9/17/2016.
 */
public class DataCloudSync {

    private TripScoreRepo tripScoreRepo;
    private TripSessionRepo tripSessionRepo;
    private TripRepo tripRepo;
    private Context context;

    public DataCloudSync(Context context) {
        this.context = context;
        tripRepo = new TripRepo(context, Trip.class);
        tripScoreRepo = new TripScoreRepo(context, TripScore.class);
        tripSessionRepo = new TripSessionRepo(context, TripSession.class);
    }

    public boolean syncData() {

        List<TripScore> tripScores = tripScoreRepo.findAllUnSynced();
        for(final TripScore tripScore : tripScores) {

            JSONObject tripScoreJson = new JSONObject();
            try{
                tripScoreJson.put("tripScoreId", tripScore.getId());
                tripScoreJson.put("tripId", tripScore.getTripId());
                tripScoreJson.put("tripSessionId", tripScore.getTripSessionId());
                tripScoreJson.put("pedalAccIntensity", tripScore.getPedalAccIntensity());
                tripScoreJson.put("pedalAccFreq",tripScore.getPedalAccFreq());
                tripScoreJson.put("pedalBreakingIntensity", tripScore.getPedalBreakingIntensity());
                tripScoreJson.put("pedalBreakingFreq", tripScore.getPedalBreakingFreq());
                tripScoreJson.put("steeringIntensity", tripScore.getPedalBreakingIntensity());
                tripScoreJson.put("steeringFreq", tripScore.getSteeringFreq());
                tripScoreJson.put("speedHarsh", tripScore.getSpeedHarsh());
                tripScoreJson.put("speedHigh", tripScore.getSpeedHigh());
                tripScoreJson.put("speedOk", tripScore.getSpeedOk());
                tripScoreJson.put("speedAwesome", tripScore.getSpeedAwesome());
                tripScoreJson.put("rpmHarsh", tripScore.getRpmHarsh());
                tripScoreJson.put("rpmHigh", tripScore.getRpmHigh());
                tripScoreJson.put("rpmOk",tripScore.getRpmOk());
                tripScoreJson.put("rpmAwesome", tripScore.getRpmAwesome());
                tripScoreJson.put("oilVeryHot", tripScore.getOilVeryHot());
                tripScoreJson.put("oilHot", tripScore.getOilHot());
                tripScoreJson.put("oilOptimum", tripScore.getOilOptimum());
                tripScoreJson.put("oilCold", tripScore.getOilCold());
                tripScoreJson.put("coolantVeryHot", tripScore.getCoolantHot());
                tripScoreJson.put("coolantHot", tripScore.getCoolantHot());
                tripScoreJson.put("coolantOptimum", tripScore.getCoolantOptimum());
                tripScoreJson.put("coolantCold", tripScore.getCoolantCold());
                tripScoreJson.put("isSync", tripScore.isSync());

            } catch (Exception e){
                e.printStackTrace();
            }

            new CreateTripScoreAsyncTask(context).execute(tripScoreJson);
        }

        List<TripSession> tripSessions = tripSessionRepo.findAllUnSynced();
        for (final TripSession tripSession : tripSessions) {

            JSONObject tripSessionJson = new JSONObject();
            try {
                tripSessionJson.put("tripSessionId", tripSession.getId());
                tripSessionJson.put("endTime", tripSession.getEndTime());
                tripSessionJson.put("stopLat", tripSession.getStopLat());
                tripSessionJson.put("stopLong", tripSession.getStopLong());
                tripSessionJson.put("distance", tripSession.getObdDistance());
                tripSessionJson.put("engineRpmMax", tripSession.getEngineRpmMax());
                tripSessionJson.put("maxSpeed", tripSession.getSpeed());

            } catch (Exception e) {
                e.printStackTrace();
            }
            new CreateTripSessionAsyncTask(context).execute(tripSessionJson);
        }

        List<Trip> trips = tripRepo.findAllUnSynced();
        for (final Trip trip : trips) {

            JSONObject tripJson = new JSONObject();
            try {
                tripJson.put("tripName", trip.getTripName());
                tripJson.put("startTime", trip.getStartTime());
                tripJson.put("endTime", trip.getEndTime());
                tripJson.put("vehicleId",trip.getVehicleId());
                tripJson.put("userId", trip.getUserId());
                tripJson.put("status", trip.getStatus());
                tripJson.put("startLocation", trip.getStartLocation());
                tripJson.put("stopLocation", trip.getStopLocation());
                tripJson.put("distance", trip.getObdDistance());
                tripJson.put("engineRpmMax", trip.getEngineRpmMax());
                tripJson.put("maxSpeed", trip.getSpeed());
                tripJson.put("tripId", trip.getId());

            } catch (Exception e) {
                e.printStackTrace();
            }

            new CreateTripAsyncTask(context).execute(tripJson);
        }
        return true;
    }
}
