package com.android.kycar.carhealth.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.ConfigActivity;
import com.android.kycar.carhealth.config.Util;

public class EngineVitalsActivity extends BaseActivity {

    private EditText edtOilTemp;
    private EditText edtCoolantTemp;
    private EditText edtSpeed;
    private EditText edtRPM;
    private TextView edtSpeedUnit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.engine_vital_units_dialog);
        edtOilTemp = (EditText) findViewById(R.id.edt_oil_temp);
        edtCoolantTemp = (EditText) findViewById(R.id.edt_cool_temp);
        edtSpeed = (EditText) findViewById(R.id.edt_speed);
        edtRPM = (EditText) findViewById(R.id.edt_rpm);
        edtSpeedUnit = (TextView) findViewById(R.id.edt_speed_unit);


        boolean empirialUnit = preferences.getBoolean(ConfigActivity.IMPERIAL_UNITS_KEY,false);

        edtOilTemp.setText(preferences.getInt(ConfigActivity.OIL_TEMP_pref, 118)+"");
        editor.putInt(ConfigActivity.OIL_TEMP_pref,118).commit();

        edtCoolantTemp.setText(preferences.getInt(ConfigActivity.COOLANT_TEMP_pref, 105)+"");
        editor.putInt(ConfigActivity.COOLANT_TEMP_pref,105).commit();

        /*if(preferences.getString(ConfigActivity.SPEED_km_pref,null) == null &&
                preferences.getString(ConfigActivity.SPEED_mile_pref,null) == null){
            if(empirialUnit){
                edtSpeed.setText("118");
                edtSpeedUnit.setText(getString(R.string.speed_miles));
                editor.putString(ConfigActivity.SPEED_km_pref,"118").commit();
            }else {
                edtSpeed.setText("118");
                edtSpeedUnit.setText(getString(R.string.speed_km));
                editor.putString(ConfigActivity.SPEED_mile_pref,"118").commit();
            }
        }else{
            if(empirialUnit){
                edtSpeed.setText(preferences.getString(ConfigActivity.SPEED_mile_pref,null));
                edtSpeedUnit.setText(getString(R.string.speed_miles));
            }else {
                edtSpeed.setText(preferences.getString(ConfigActivity.SPEED_km_pref,null));
                edtSpeedUnit.setText(getString(R.string.speed_km));
            }
        }*/

        edtSpeed.setText(preferences.getInt(ConfigActivity.SPEED_km_pref,110)+"");
        edtSpeedUnit.setText(getString(R.string.speed_km));

        edtRPM.setText(preferences.getInt(ConfigActivity.RPM_pref, 2000)+"");
        editor.putInt(ConfigActivity.RPM_pref,2000).commit();
    }

    public void save(View view) {

        if(!edtOilTemp.getText().toString().isEmpty() &&
                !edtCoolantTemp.getText().toString().isEmpty() &&
                !edtSpeed.getText().toString().isEmpty() &&
                !edtRPM.getText().toString().isEmpty()) {
            boolean empirialUnit = preferences.getBoolean(ConfigActivity.IMPERIAL_UNITS_KEY, false);
            editor.putInt(ConfigActivity.OIL_TEMP_pref, Integer.parseInt(edtOilTemp.getText().toString())).commit();

            editor.putInt(ConfigActivity.COOLANT_TEMP_pref, Integer.parseInt(edtCoolantTemp.getText().toString())).commit();
            if (empirialUnit) {
                editor.putInt(ConfigActivity.SPEED_mile_pref, Integer.parseInt(edtSpeed.getText().toString())).commit();
            } else {
                editor.putInt(ConfigActivity.SPEED_km_pref, Integer.parseInt(edtSpeed.getText().toString())).commit();
            }
            editor.putInt(ConfigActivity.RPM_pref, Integer.parseInt(edtRPM.getText().toString())).commit();
            Util.showAlert("Saved",this);
        }else{
             Util.showAlert("Field cannot be empty",this);
        }
    }
}
