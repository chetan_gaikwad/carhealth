package com.android.kycar.carhealth.services.reciever_services;

/**
 * Created by 638 on 18-May-17.
 */

public class SmsContent {
    String smsFrom;
    String smsBody;

    public SmsContent(String smsFrom, String smsBody) {
        this.smsFrom = smsFrom;
        this.smsBody = smsBody;
    }

    public String getSmsFrom() {
        return smsFrom;
    }

    public String getSmsBody() {
        return smsBody;
    }
}
