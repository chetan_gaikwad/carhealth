package com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides.DriveStatisticsFragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.entities.trip_transaction.TripScore;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides.DriveStatisticsActivity;
import com.android.kycar.carhealth.utils.BusProvider;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;


public class TemperatureFragment extends Fragment {

    private PieChart oilTempChart;
    private PieChart coolantTempChart;
    private boolean isDataAvailable = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        container = (ViewGroup) inflater.inflate(R.layout.temperature_fragment_layout, null);
        final ViewGroup cnt = container;

        TripScore tripScore = ((DriveStatisticsActivity)getActivity()).getSensorData();

        oilTempChart = (PieChart) container.findViewById(R.id.chart_oil_temp);
        oilTempChart.setUsePercentValues(true);
        oilTempChart.setDescription("");
        oilTempChart.setValueTextColor(Color.BLACK);
        oilTempChart.setBackgroundColor(Color.parseColor("#DEDADA"));
        oilTempChart.setHoleRadius(20);
        oilTempChart.setTransparentCircleRadius(35);
        oilTempChart.setRotationAngle(0);
        oilTempChart.animateY(2000);
        oilTempChart.setRotationEnabled(true);

        ArrayList<String> oilTempChartLabels = new ArrayList<>();

        oilTempChartLabels.add("Optimum");
        oilTempChartLabels.add("Hot");
        oilTempChartLabels.add("Very Hot");
        oilTempChartLabels.add("Cold");

        ArrayList<Entry> oilTempEntries = new ArrayList<>();
        oilTempEntries.add(new Entry(tripScore.getOilVeryHot(), 0));
        oilTempEntries.add(new Entry(tripScore.getOilHot(), 1));
        oilTempEntries.add(new Entry(tripScore.getOilOptimum(), 2));
        oilTempEntries.add(new Entry(tripScore.getOilCold(), 3));

        if(tripScore.getOilHot() == 0 && tripScore.getOilOptimum() == 0 && tripScore.getOilVeryHot() == 0 && tripScore.getOilCold() == 0) {
            isDataAvailable = false;
        }

        /*oilTempEntries.add(new Entry(60, 2));
        oilTempEntries.add(new Entry(20, 1));
        oilTempEntries.add(new Entry(12, 0));
        oilTempEntries.add(new Entry(8, 3));*/
        addPieData(oilTempChart, oilTempEntries, oilTempChartLabels, "");

        coolantTempChart = (PieChart) container.findViewById(R.id.chart_coolant_temp);
        coolantTempChart.setUsePercentValues(true);
        coolantTempChart.setDescription("");
        coolantTempChart.setValueTextColor(Color.BLACK);
        coolantTempChart.setBackgroundColor(Color.parseColor("#DEDADA"));
        coolantTempChart.setHoleRadius(20);
        coolantTempChart.setTransparentCircleRadius(35);
        coolantTempChart.setRotationAngle(0);
        coolantTempChart.animateY(2000);
        coolantTempChart.setRotationEnabled(true);

        ArrayList<String> coolantTempChartLabels = new ArrayList<>();

        coolantTempChartLabels.add("Optimum");
        coolantTempChartLabels.add("Hot");
        coolantTempChartLabels.add("Very Hot");
        coolantTempChartLabels.add("Cold");

        ArrayList<Entry> coolantTempEntries = new ArrayList<>();
        coolantTempEntries.add(new Entry(tripScore.getCoolantOptimum(), 2));
        coolantTempEntries.add(new Entry(tripScore.getCoolantHot(), 1));
        coolantTempEntries.add(new Entry(tripScore.getCoolantVeryHot(), 0));
        coolantTempEntries.add(new Entry(tripScore.getCoolantCold(), 3));

        /*coolantTempEntries.add(new Entry(60, 0));
        coolantTempEntries.add(new Entry(25, 1));
        coolantTempEntries.add(new Entry(10, 2));
        coolantTempEntries.add(new Entry(5, 3));*/

        addPieData(coolantTempChart, coolantTempEntries, coolantTempChartLabels, "");

        return container;
    }



    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {



        }
    };



    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    private void addPieData(PieChart engineVitals, ArrayList<Entry> entries, ArrayList<String> labls, String title) {

        PieDataSet pieDataSet = new PieDataSet(entries, title);
        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());
        pieDataSet.setColors(colors);
        PieData pieData = new PieData(labls, pieDataSet);
        engineVitals.setData(pieData);
    }

    @Override
    public void setUserVisibleHint(boolean visible){
        super.setUserVisibleHint(visible);

        if(!isDataAvailable) {
            Toast.makeText(getContext(), "No data logged.", Toast.LENGTH_SHORT).show();
        }

        if (visible && isResumed()){
            oilTempChart.animateXY(2000, 2000);
            coolantTempChart.animateXY(2000, 2000);
        }
    }
}
