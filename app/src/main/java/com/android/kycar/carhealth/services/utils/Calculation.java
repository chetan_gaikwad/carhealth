package com.android.kycar.carhealth.services.utils;

import android.hardware.SensorEvent;

/**
 * Created by krishna on 30/11/15.
 */
public class Calculation {

    private static final double PI = 22/7;

    public double radianPerSecToDegreePerSec(float radPerSec){

        double degreePerSec = (radPerSec* 180)/PI;
        return  degreePerSec;
    }

    public double radianPerSecToDegree(float radPerSec, long timestamp){
       // Log.d("Values are ", ""+radPerSec+"  "+timestamp);

        return  degreePerSecToDegree(radianPerSecToDegreePerSec(radPerSec), timestamp);
    }

    public double degreePerSecToDegree(double degreePerSec, float timestamp){

       // Log.d("Degree is ", ""+degreePerSec*timestamp*0.001);
        return  degreePerSec*timestamp*0.001;
    }


    public int getInclineAngle(SensorEvent sensorEvent){

        float x = sensorEvent.values[0];
        float y = sensorEvent.values[1];
        float z = sensorEvent.values[2];

        double norm_Of_g = Math.sqrt(x * x + y * y + z * z);

        // Normalize the accelerometer vector
        float aX = (float) (x / norm_Of_g);
        float aY = (float) (y / norm_Of_g);
        float aZ = (float) (z / norm_Of_g);

        return  (int) Math.round(Math.toDegrees(Math.acos(aY)));
    }
    public int getInclineAngleX(SensorEvent sensorEvent){

        float x = sensorEvent.values[0];
        float y = sensorEvent.values[1];
        float z = sensorEvent.values[2];

        double norm_Of_g = Math.sqrt(x * x + y * y + z * z);

        // Normalize the accelerometer vector
        float aX = (float) (x / norm_Of_g);
        float aY = (float) (y / norm_Of_g);
        float aZ = (float) (z / norm_Of_g);

        return  (int) Math.round(Math.toDegrees(Math.acos(aX)));
    }
    public int getInclineAngleY(SensorEvent sensorEvent){

        float x = sensorEvent.values[0];
        float y = sensorEvent.values[1];
        float z = sensorEvent.values[2];

        double norm_Of_g = Math.sqrt(x * x + y * y + z * z);

        // Normalize the accelerometer vector
        float aX = (float) (x / norm_Of_g);
        float aY = (float) (y / norm_Of_g);
        float aZ = (float) (z / norm_Of_g);

        return  (int) Math.round(Math.toDegrees(Math.acos(aY)));
    }
    public int getInclineAngleZ(SensorEvent sensorEvent){

        float x = sensorEvent.values[0];
        float y = sensorEvent.values[1];
        float z = sensorEvent.values[2];

        double norm_Of_g = Math.sqrt(x * x + y * y + z * z);

        // Normalize the accelerometer vector
        float aX = (float) (x / norm_Of_g);
        float aY = (float) (y / norm_Of_g);
        float aZ = (float) (z / norm_Of_g);

        return  (int) Math.round(Math.toDegrees(Math.acos(aZ)));
    }

    public int findGear(Double gearRatio){

        if(gearRatio > 107 && gearRatio < 113) {
            return 1;
        }else if(gearRatio > 57 && gearRatio < 62) {
            return 2;
        }else if(gearRatio > 38&& gearRatio < 42) {
            return 3;
        }else if(gearRatio > 27&& gearRatio < 30) {
            return 4;
        }else if(gearRatio > 2.9&& gearRatio < 3.19) {
            return 5;
        }else{
            return 0;
        }
    }
}
