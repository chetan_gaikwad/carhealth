package com.android.kycar.carhealth.entities;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 1/16/2016.
 */
@RealmClass
public class User extends RealmObject {

    @PrimaryKey
    private String id;

    private String firstName;

    private String lastName;

    private String emailId;

    private String address;

    private String phoneNumber;

    private String profilePicture;

    private int bloodGrp;

    private String aadharNumber;

    private String licenceNumber;

    private String pan;

    private String onGoingMedications;

    private String accidentalHistory;

    private String allergies;

    private boolean isSync;

    public User() {
    }

    public User(JSONObject jsonObject) {

        try {
            this.id = jsonObject.getString("id");
            if(jsonObject.has("email")) {
                this.emailId = jsonObject.getString("email");
            }
            if (jsonObject.has("aadharNumber")) {
                this.aadharNumber = jsonObject.getString("aadharNumber");
            }
            if (jsonObject.has("firstName")) {
                this.firstName = jsonObject.getString("firstName");
            }
            if (jsonObject.has("phoneNumber")) {
                this.phoneNumber = jsonObject.getString("phoneNumber");
            }
            if (jsonObject.has("address")) {
                this.address = jsonObject.getString("address");
            }
            if (jsonObject.has("bloodGrp")) {
                this.bloodGrp = jsonObject.getInt("bloodGrp");
            }
            if (jsonObject.has("lastName")) {
                this.lastName = jsonObject.getString("lastName");
            }
            if (jsonObject.has("licenceNumber")) {
                this.licenceNumber = jsonObject.getString("licenceNumber");
            }
            if (jsonObject.has("pan")) {
                this.pan = jsonObject.getString("pan");
            }
            if (jsonObject.has("profilePicture")) {
                this.profilePicture = jsonObject.getString("profilePicture");
            }
            if (jsonObject.has("onGoingMedications")) {
                this.onGoingMedications = jsonObject.getString("onGoingMedications");
            }
            if (jsonObject.has("accidentalHistory")) {
                this.accidentalHistory = jsonObject.getString("accidentalHistory");
            }
            if (jsonObject.has("allergies")) {
                this.allergies = jsonObject.getString("allergies");
            }

            if (jsonObject.has("isSync")) {
                this.isSync = jsonObject.getBoolean("isSync");
            } else {
                this.isSync = false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public User(User user) {
        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.aadharNumber = user.getAadharNumber();
        this.bloodGrp = user.getBloodGrp();
        this.address = user.getAddress();
        this.emailId = user.getEmailId();
        this.licenceNumber = user.getLicenceNumber();
        this.pan = user.getPan();
        this.phoneNumber = user.getPhoneNumber();
        this.profilePicture = user.getProfilePicture();
        this.isSync = user.isSync();
    }

    public User(String aadharNumber, String address, int bloodGrp, String emailId, String firstName, String id, String lastName, String licenceNumber, String pan, String phoneNumber, String profilePicture) {
        this.aadharNumber = aadharNumber;
        this.address = address;
        this.bloodGrp = bloodGrp;
        this.emailId = emailId;
        this.firstName = firstName;
        this.id = id;
        this.lastName = lastName;
        this.licenceNumber = licenceNumber;
        this.pan = pan;
        this.phoneNumber = phoneNumber;
        this.profilePicture = profilePicture;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    public int getBloodGrp() {
        return bloodGrp;
    }

    public void setBloodGrp(int bloodGrp) {
        this.bloodGrp = bloodGrp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    public String getAccidentalHistory() {
        return accidentalHistory;
    }

    public void setAccidentalHistory(String accidentalHistory) {
        this.accidentalHistory = accidentalHistory;
    }

    public String getOnGoingMedications() {
        return onGoingMedications;
    }

    public void setOnGoingMedications(String onGoingMedications) {
        this.onGoingMedications = onGoingMedications;
    }
}
