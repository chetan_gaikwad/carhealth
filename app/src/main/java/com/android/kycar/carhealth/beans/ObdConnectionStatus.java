package com.android.kycar.carhealth.beans;

/**
 * Created by 638 on 3/7/2017.
 */

public class ObdConnectionStatus {
    private String ConnectionMessage;

    public ObdConnectionStatus(String connectionMessage) {
        ConnectionMessage = connectionMessage;
    }

    public String getConnectionMessage() {
        return ConnectionMessage;
    }
}
