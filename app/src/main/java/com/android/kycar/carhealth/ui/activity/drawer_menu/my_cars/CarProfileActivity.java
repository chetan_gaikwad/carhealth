
package com.android.kycar.carhealth.ui.activity.drawer_menu.my_cars;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.ProgessDialogBox;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.Vehicle;
import com.android.kycar.carhealth.repositories.vehicle.VehicleRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.VehicleRestService;
import com.android.kycar.carhealth.task.CalculateVinTask;
import com.android.kycar.carhealth.task.OnTaskCompletedString;

import java.util.Date;

public class CarProfileActivity extends AppCompatActivity implements OnTaskCompletedString {

    private static final int AVATAR_FROM_GALLERY = 100;
    private EditText etCarNickName;
    private EditText etRegNo;
    private EditText etPuc;
    private EditText etInsurance;
    private EditText etRc;
    private Button btnUpdateCarProfile;
    private ImageView ivInsuranceValidTIll;
    private ImageView ivPucVaidTill;
    private ImageView ivRcValidTill;
    private StringBuilder dateBuilder;
    private String vehicleId;
    private VehicleRepo vehicleRepo;
    private ProgessDialogBox dialogBox;
    private ImageView imgCarPic;
    private Vehicle vehicle;
    private EditText edtSpeedAnalytics;
    private EditText edtOilAnalytics;
    private TextView tvGetVinFromCar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_profile);

        dialogBox = new ProgessDialogBox(this);
        vehicleRepo = new VehicleRepo(getApplicationContext(), Vehicle.class);
        etCarNickName = (EditText) findViewById(R.id.etcarnickname);
        etInsurance = (EditText) findViewById(R.id.etinsurance);
        etPuc = (EditText) findViewById(R.id.etpuc);
        etRc  = (EditText) findViewById(R.id.etrc);
        etRegNo = (EditText) findViewById(R.id.etregno);
        tvGetVinFromCar = (TextView) findViewById(R.id.tv_get_vin_from_car2);

        ivInsuranceValidTIll = (ImageView) findViewById(R.id.iv_insurance_valid_till);
        ivPucVaidTill = (ImageView) findViewById(R.id.iv_puc_valid_till);
        ivRcValidTill = (ImageView) findViewById(R.id.iv_rc_valid_till);
        imgCarPic = (ImageView) findViewById(R.id.img_car_picture);
        edtSpeedAnalytics = (EditText) findViewById(R.id.speed_analytics);
        edtOilAnalytics = (EditText) findViewById(R.id.oil_analytics);

        setCarInfo();
    }

    private void setCarInfo(){

        vehicleId = (String) getIntent().getSerializableExtra("vehicleId");
        Log.d(CarProfileActivity.class.getSimpleName(), "VehicleId :-"+vehicleId);
        vehicle = (Vehicle) vehicleRepo.findById(vehicleId);
        etCarNickName.setText(vehicle.getNickName());
        etRegNo.setText(vehicle.getRegNumber());
        if(vehicle.getVehicleImage() != null && vehicle.getVehicleImage().length() > 0) {
            imgCarPic.setImageBitmap(BitmapFactory.decodeFile(vehicle.getVehicleImage()));
        }
        ((EditText)findViewById(R.id.edt_vin)).setText(vehicle.getVin());
    }


    public void getVinFromCar(View view) {
        new CalculateVinTask(CarProfileActivity.this, CarProfileActivity.this).execute("");
    }

    @Override
    public void onTaskCompleted(String vin) {

        //tvGetVinFromCar.setText(vin);
        ((EditText)findViewById(R.id.edt_vin)).setText(vehicle.getVin());
    }

    public void onUpdateCar(View view){


        if (Integer.parseInt(edtOilAnalytics.getText().toString()) > 50
                && Integer.parseInt(edtSpeedAnalytics.getText().toString()) > 50) {

            Vehicle vehicle = new Vehicle((Vehicle) vehicleRepo.findById(vehicleId));

            vehicle.setNickName(etCarNickName.getText().toString().trim());
            vehicle.setInsurance(new Date());
            vehicle.setRegNumber(etRegNo.getText().toString().trim());
            vehicle.setVin(tvGetVinFromCar.getText().toString());
            vehicle.setMaxSpeed(Integer.parseInt(edtSpeedAnalytics.getText().toString()));
            vehicle.setMaxOilTemp(Integer.parseInt(edtOilAnalytics.getText().toString()));
            vehicle.setPuc(new Date());
            vehicle.setRc(new Date());
            vehicle.setActive(true);

            /** Rest Call for vehicle update */
            new UpdateVehicleRestCall().execute(vehicle);
        } else {
            Util.showAlert("Speed and Oil value must be greater than 50",this);
        }
    }

    //@OnClick(R.id.iv_puc_valid_till)
    public void onPucValid(){

        showPucDatePickerDialog();
    }

    //@OnClick(R.id.iv_rc_valid_till)
    public void onRcValidTill(){

        showRcDatePickerDialog();
    }

    //@OnClick(R.id.iv_insurance_valid_till)
    public void onInsurnceValid(){
        showInsuranceDatePickerDialog();
    }

    public void showInsuranceDatePickerDialog() {

       // DialogFragment newFragment = new InsuranceDatePicker();
        //newFragment.show(getSupportFragmentManager(), "datePicker");

    }

    public void showPucDatePickerDialog() {

        //DialogFragment newFragment = new PucDatePicker();
        //newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void showRcDatePickerDialog() {

        //DialogFragment newFragment = new RcDatePicker();
        //newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void onSetGearRatioClick(View view){

        startActivity(new Intent(this, CalculateGearRatioActivity.class).putExtra("variantId", vehicle.getVariantId()));
    }

    public void onUploadCarPic(View view){

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, AVATAR_FROM_GALLERY);
        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Util.PROFILE_PREF, AVATAR_FROM_GALLERY);
        SharedPreferences.Editor editor = preferences.edit();
        editor.commit();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImage,
                filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();

        imgCarPic.setImageBitmap(BitmapFactory.decodeFile(picturePath));

        Vehicle vehicle = (Vehicle) vehicleRepo.findById(vehicleId);
        Vehicle updatedVehicle = new Vehicle(vehicle);
        updatedVehicle.setVehicleImage(picturePath);
        vehicleRepo.saveOrUpdate(updatedVehicle);

        /*editor.putString(Util.PROFILE_PREF, picturePath);
        editor.commit();*/
    }

    private class UpdateVehicleRestCall extends AsyncTask<Vehicle, Void, String> {
        VehicleRepo vehicleRepo;
        @Override
        protected String doInBackground(Vehicle... vehicles) {

            try {

                //Toast.makeText(CarProfileActivity.this, "Car Updated Successfully", Toast.LENGTH_SHORT).show();
                String response =  new VehicleRestService().updateVehicle(vehicles[0]);
                vehicleRepo = new VehicleRepo(getApplicationContext(), Vehicle.class);
                Log.d(CarProfileActivity.class.getSimpleName(), "Id : ="+vehicles[0].getId());
                vehicleRepo.saveOrUpdate(vehicles[0]);
                Log.d(CarProfileActivity.class.getSimpleName(), "Total vehicles:-"+vehicleRepo.findAll().size());
                return response;

            } catch (Exception e) {
                e.printStackTrace();
                return "Vehicle updation failed";
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogBox.showDailog();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(dialogBox.isShowing()){
                dialogBox.dismissDialog();
            }
            onBackPressed();
        }
    }
}
