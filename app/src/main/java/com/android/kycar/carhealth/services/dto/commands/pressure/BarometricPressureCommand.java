package com.android.kycar.carhealth.services.dto.commands.pressure;


import com.android.kycar.carhealth.services.dto.enums.AvailableCommandNames;

/**
 * Barometric pressure.
 */
public class BarometricPressureCommand extends PressureCommand {

    public BarometricPressureCommand() {
        super("01 33");
    }

    /**
     * @param other a {@link PressureCommand} object.
     */
    public BarometricPressureCommand(PressureCommand other) {
        super(other);
    }

    @Override
    public String getName() {
        return AvailableCommandNames.BAROMETRIC_PRESSURE.getValue();
    }

}
