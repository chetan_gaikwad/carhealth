package com.android.kycar.carhealth.ui.activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.kycar.carhealth.NxtApplication;
import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.beans.Coordinates;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.services.connection_services.mqtt_paho.PublishSensorData;
import com.android.kycar.carhealth.services.connection_services.mqtt_paho_android_service.Connection;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class SecurityController extends AppCompatActivity {

    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    //"00001101-0000-1000-8000-00805F9B34FB

    protected SharedPreferences preferences;
    private static final String TAG = SecurityController.class.getName();
    private BluetoothDevice dev = null;
    private BluetoothSocket sock = null;
    private BluetoothSocket sockFallback = null;
    private Context ctx = SecurityController.this;
    private TextView txtSensorValues;
    private TextView txtSensorMaxValues;

    private OutputStream mmOutputStream;
    private InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    private int readBufferPosition;
    private int counter;
    volatile boolean stopWorker;
    private PublishSensorData publishSensorData;
    private float maxX, maxY, maxZ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security_controller);
        txtSensorValues = (TextView)findViewById(R.id.sensorValues);
        txtSensorMaxValues = (TextView) findViewById(R.id.sensorMaxValues);
        preferences = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        publishSensorData = new PublishSensorData();
    }

    public void connect(View v){

        Log.d(TAG, "Starting service..");
        // get the remote Bluetooth device
       // final String remoteDevice = prefs.getString(ConfigActivity.BLUETOOTH_LIST_KEY, null);
        final String remoteDevice = "2C:F7:F1:81:3A:42";//"20:14:04:09:22:46";//
        //final String remoteDevice = "D9:64:7A:52:D8:68";

            /** Need to code for device selection */

            if (remoteDevice == null || "".equals(remoteDevice)) {
                Toast.makeText(ctx, getString(R.string.text_bluetooth_nodevice), Toast.LENGTH_LONG).show();

            // log error
            Log.e(TAG, "No Bluetooth device has been selected.");
        } else {

            final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
            dev = btAdapter.getRemoteDevice(remoteDevice);
            Log.d(TAG, "Stopping Bluetooth discovery.");
            btAdapter.cancelDiscovery();

            try {

                //new BluetoothConnector(dev,false,btAdapter, null);

                startObdConnection();
            } catch (Exception e) {
                Log.e(
                        TAG,
                        "There was an error while establishing connection. -> "
                                + e.getMessage()
                );
            }
        }
    }

    private void startObdConnection() throws IOException {
        Log.d(TAG, "Starting OBD connection..");
        try {
            // Instantiate a BluetoothSocket for the remote device and connect it.
            //sock = dev.createRfcommSocketToServiceRecord(MY_UUID);
            sock = dev.createInsecureRfcommSocketToServiceRecord(MY_UUID);
            sock.connect();
            Toast.makeText(SecurityController.this,"Connected",Toast.LENGTH_LONG).show();
        } catch (Exception e1) {
            Log.e(TAG, "There was an error while establishing Bluetooth connection. Falling back..", e1);
            Class<?> clazz = sock.getRemoteDevice().getClass();
            Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};
            try {

                /************Fallback method 1*********************/
                Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                Object[] params = new Object[]{Integer.valueOf(1)};
                sockFallback = (BluetoothSocket) m.invoke(sock.getRemoteDevice(), params);
                sockFallback.connect();
                sock = sockFallback;
                Log.e("", "SockFallback Connected");
            } catch (Exception e2) {
                Log.e(TAG, "Couldn't fallback while establishing Bluetooth connection. Stopping app..", e2);
                throw new IOException();
            }
        }
        mmOutputStream = sock.getOutputStream();
        mmInputStream = sock.getInputStream();
        beginListenForData();
    }

    public void lock(View v){
        String three = "3";

        try {
            sock.getOutputStream().write(three.getBytes());
            /*Timer t = new Timer();
            t.schedule(new TimerTask() {

                @Override
                public void run() {                   //timer

                    String four = "4\r";
                    try {
                        sock.getOutputStream().write(four.getBytes());
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
            }, 500L);*/
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void unlock(View v){
        String five = "5\r";

        try {
            sock.getOutputStream().write(five.getBytes());
            Timer t = new Timer();
            t.schedule(new TimerTask() {

                @Override
                public void run() {                   //timer

                    String six = "6\r";
                    try {
                        sock.getOutputStream().write(six.getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }, 500L);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void three(View v){
        String message = "3\r";
        try {
            sock.getOutputStream().write(message.getBytes());
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void immobilize(View v) {
        String message = "4\r";
        try {
            sock.getOutputStream().write(message.getBytes());
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (sock != null) {
                sock.close();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private class ReadHardwareValues extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... cred) {

          while (true){
                  runOnUiThread(new Runnable() {

                      @Override
                      public void run() {
                          try {
                              txtSensorValues.append(readRawData(sock.getInputStream())+"\n");
                              Log.d(SecurityController.class.getSimpleName(), " "+txtSensorValues.getText());
                          }catch (IOException e){
                              e.printStackTrace();
                          }
                      }
                  });
          }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    public String readRawData(InputStream in) throws IOException {
        byte b = 0;
        StringBuilder res = new StringBuilder();

        // read until '>' arrives OR end of stream reached
        char c;
        while(true)
        {
            b = (byte) in.read();
            if(b == -1) // -1 if the end of the stream is reached
            {
                break;
            }
            c = (char)b;
            if(c == '>') // read until '>' arrives
            {
                break;
            }
            res.append(c);
        }

    /*
     * Imagine the following response 41 0c 00 0d.
     *
     * ELM sends strings!! So, ELM puts spaces between each "byte". And pay
     * attention to the fact that I've put the word byte in quotes, because 41
     * is actually TWO bytes (two chars) in the socket. So, we must do some more
     * processing..
     */
        String rawData = res.toString().replaceAll("SEARCHING", "");

    /*
     * Data may have echo or informative cost like "INIT BUS..." or similar.
     * The response ends with two carriage return characters. So we need to take
     * everything from the last carriage return before those two (trimmed above).
     */
        //kills multiline.. rawData = rawData.substring(rawData.lastIndexOf(13) + 1);
        rawData = rawData.replaceAll("\\s", "");//removes all [ \t\n\x0B\f\r]
        Log.d(SecurityController.class.getSimpleName(), ""+rawData);
        return res.toString();
    }

    void beginListenForData() {
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character

        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];
        workerThread = new Thread(new Runnable()
        {
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopWorker)
                {
                    try
                    {
                        int bytesAvailable = mmInputStream.available();
                        if(bytesAvailable > 0)
                        {
                            byte[] packetBytes = new byte[bytesAvailable];
                            mmInputStream.read(packetBytes);
                            for(int i=0;i<bytesAvailable;i++)
                            {
                                byte b = packetBytes[i];
                                if(b == delimiter)
                                {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;

                                    handler.post(new Runnable()
                                    {
                                        public void run()
                                        {

                                            /** Actual sensor data*/
                                            Log.d(SecurityController.class.getSimpleName(), "data is ==>"+data);

                                            JSONObject jsonObject = createJsonStructure(data);
                                            if(jsonObject != null) {
                                                txtSensorValues.setText(jsonObject.toString());
                                                if (preferences.getBoolean(Util.IS_UPLOAD_RC, true) && preferences.getBoolean(Util.IS_LOGGED_IN, false)){
                                                    //publishSensorData.getEventSensorData(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAy, event, speed, updatedLocation.getSpeed(), updatedLocation.getBearing(), magBearing);

                                                    /**Need to check threshold*/
                                                    if(false){
                                                        try {
                                                            Log.d(SecurityController.class.getSimpleName(), "Event Data published");
                                                            Connection.getClient().publish(NxtApplication.baseTopic+"/sensor_event_data/"+preferences.getString(Util.USER_PREFERENCE, ""), publishSensorData.getEventSensorData(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), preferences.getString(Util.CAR_PREFERENCE, ""), new Coordinates(), 0.0, "", 0, NxtApplication.currLocation.getSpeed(), NxtApplication.currLocation.getBearing(), 0, preferences.getString(Util.TRIP_ID, ""), preferences.getString(Util.SENSOR_ACC_NAME, "")));
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                                else
                                {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                    }
                    catch (IOException ex)
                    {
                        ex.printStackTrace();
                        stopWorker = true;
                    }
                }
            }
        });
        workerThread.start();
    }

    private JSONObject createJsonStructure(String input){

        try {
            StringBuilder output = new StringBuilder(input);

            output.deleteCharAt(0);
            int index = output.indexOf(",\"g\"");
            if(index != -1 && output.length() > index){
                output.delete(index, output.length()-1);
            }

            //output.delete(output.charAt())
            //input = input.replace(input.substring(input.charAt(66), input.length()), "");
            output.append("}");
            return convertJson(output);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private JSONObject convertJson(StringBuilder output) throws JSONException {
        JSONObject outputJson = new JSONObject(output.toString());

        JSONObject accValue = outputJson.getJSONObject("a");
        int x = accValue.getInt("x");
        int y = accValue.getInt("y");
        int z = accValue.getInt("z");

        float newX = (float) (x/1632.65);
        float newY = (float) (y/1632.65);
        float newZ = (float) (z/1632.65);
        if(maxX < newX) {
            maxX = newX;
            showMax();
        }
        if(maxY < newY) {
            maxY = newY;
            showMax();
        }
        if(maxZ < newZ) {
            maxZ = newZ;
            showMax();
        }

        accValue.put("x", newX);
        accValue.put("y", newY);
        accValue.put("z", newZ);
        outputJson.put("a", accValue);
        return  outputJson;
    }

    private void showMax() {
        txtSensorMaxValues.setText("MaxX: "+maxX+", MaxY: "+maxY+", MaxZ: "+maxZ);
    }
}
