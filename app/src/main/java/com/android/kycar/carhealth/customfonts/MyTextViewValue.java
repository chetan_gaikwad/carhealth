package com.android.kycar.carhealth.customfonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.android.kycar.carhealth.utils.FontCache;


public class MyTextViewValue extends TextView {

    public MyTextViewValue(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextViewValue(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextViewValue(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = FontCache.get("", getContext());
            setTypeface(tf);
        }
    }

}