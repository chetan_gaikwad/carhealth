package com.android.kycar.carhealth.beans;

/**
 * Created by Krishna on 5/22/2016.
 */
public class Obstacle {

    private int distance;

    private int type;

    public Obstacle(int distance, int type) {
        this.distance = distance;
        this.type = type;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
