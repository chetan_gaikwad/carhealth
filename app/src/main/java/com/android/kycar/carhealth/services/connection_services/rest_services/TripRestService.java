package com.android.kycar.carhealth.services.connection_services.rest_services;

import android.content.Context;
import android.util.Log;


import com.android.kycar.carhealth.entities.trip_transaction.Trip;
import com.android.kycar.carhealth.entities.trip_transaction.TripScore;
import com.android.kycar.carhealth.entities.trip_transaction.TripSession;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krishna on 5/30/2016.
 */
public class TripRestService extends RestService{

    public boolean stopTrip(Context context, String tripId){

        try {
            /** Trip creation URL */
//            String url = BASE_REST_URL+"/trip/stop?trip_id="+tripId;
            return true;
        }catch (Exception e){
            //Util.showAlert("Something went wrong. Please try again later", context);
            return false;
        }
    }

    public String startTrip(Trip trip){

        JSONObject tripJson = new JSONObject();
        try{
            tripJson.put("id", trip.getId());
            tripJson.put("tripName", trip.getTripName());
            tripJson.put("startTime", trip.getStartTime());
            tripJson.put("endTime", trip.getEndTime());
            tripJson.put("vehicleId",trip.getVehicleId());
            tripJson.put("userId", trip.getUserId());
            tripJson.put("status", trip.getStatus());
            tripJson.put("startLocation", trip.getStartLocation());
            tripJson.put("stopLocation", trip.getStopLocation());
            tripJson.put("distance", trip.getObdDistance());
            tripJson.put("engineRpmMax", trip.getEngineRpmMax());
            tripJson.put("maxSpeed", trip.getSpeed());

        }catch (Exception e){
            e.printStackTrace();
        }
        return  restServiceCall("/trip/create", "POST", tripJson);
    }

    public Trip fetchAllTrips(Context context){

        return null;
    }

    public String createTripSession(TripSession tripSession) {

        JSONObject tripSessionJson = new JSONObject();
        try{
            tripSessionJson.put("tripSessionId", tripSession.getId());
            tripSessionJson.put("endTime", tripSession.getEndTime());
            tripSessionJson.put("stopLat", tripSession.getStopLat());
            tripSessionJson.put("stopLong", tripSession.getStopLong());
            tripSessionJson.put("distance", tripSession.getObdDistance());
            tripSessionJson.put("engineRpmMax", tripSession.getEngineRpmMax());
            tripSessionJson.put("maxSpeed", tripSession.getSpeed());
            tripSessionJson.put("tripId", tripSession.getTripId());
            tripSessionJson.put("startTime", tripSession.getStartTime());
            tripSessionJson.put("startLat", tripSession.getStartLat());
            tripSessionJson.put("startLong", tripSession.getStartLong());

        }catch (Exception e) {
            e.printStackTrace();
        }
        return  restServiceCall("/Session/create", "POST", tripSessionJson);
    }

    public String createTripSession(JSONObject tripSessionJson) {
        return  restServiceCall("/Session/create", "POST", tripSessionJson);
    }

    public String createTrip(Trip trip){

        JSONObject tripJson = new JSONObject();
        try{
            tripJson.put("tripName", trip.getTripName());
            tripJson.put("startTime", trip.getStartTime());
            tripJson.put("endTime", trip.getEndTime());
            tripJson.put("vehicleId",trip.getVehicleId());
            tripJson.put("userId", trip.getUserId());
            tripJson.put("status", trip.getStatus());
            tripJson.put("startLocation", trip.getStartLocation());
            tripJson.put("stopLocation", trip.getStopLocation());
            tripJson.put("distance", trip.getObdDistance());
            tripJson.put("engineRpmMax", trip.getEngineRpmMax());
            tripJson.put("maxSpeed", trip.getSpeed());
            tripJson.put("tripId", trip.getId());

        }catch (Exception e){
            e.printStackTrace();
        }
        return  restServiceCall("/trip/create", "POST", tripJson);
    }

    public String createTrip(JSONObject tripJson){

        return  restServiceCall("/trip/create", "POST", tripJson);
    }

    public String createTripScore(TripScore tripScore) {

        JSONObject tripScoreJson = new JSONObject();
        try{
            tripScoreJson.put("tripScoreId", tripScore.getId());
            tripScoreJson.put("tripId", tripScore.getTripId());
            tripScoreJson.put("tripSessionId", tripScore.getTripSessionId());
            tripScoreJson.put("pedalAccIntensity", tripScore.getPedalAccIntensity());
            tripScoreJson.put("pedalAccFreq",tripScore.getPedalAccFreq());
            tripScoreJson.put("pedalBreakingIntensity", tripScore.getPedalBreakingIntensity());
            tripScoreJson.put("pedalBreakingFreq", tripScore.getPedalBreakingFreq());
            tripScoreJson.put("steeringIntensity", tripScore.getPedalBreakingIntensity());
            tripScoreJson.put("steeringFreq", tripScore.getSteeringFreq());
            tripScoreJson.put("speedHarsh", tripScore.getSpeedHarsh());
            tripScoreJson.put("speedHigh", tripScore.getSpeedHigh());
            tripScoreJson.put("speedOk", tripScore.getSpeedOk());
            tripScoreJson.put("speedAwesome", tripScore.getSpeedAwesome());
            tripScoreJson.put("rpmHarsh", tripScore.getRpmHarsh());
            tripScoreJson.put("rpmHigh", tripScore.getRpmHigh());
            tripScoreJson.put("rpmOk",tripScore.getRpmOk());
            tripScoreJson.put("rpmAwesome", tripScore.getRpmAwesome());
            tripScoreJson.put("oilVeryHot", tripScore.getOilVeryHot());
            tripScoreJson.put("oilHot", tripScore.getOilHot());
            tripScoreJson.put("oilOptimum", tripScore.getOilOptimum());
            tripScoreJson.put("oilCold", tripScore.getOilCold());
            tripScoreJson.put("coolantVeryHot", tripScore.getCoolantHot());
            tripScoreJson.put("coolantHot", tripScore.getCoolantHot());
            tripScoreJson.put("coolantOptimum", tripScore.getCoolantOptimum());
            tripScoreJson.put("coolantCold", tripScore.getCoolantCold());
            tripScoreJson.put("isSync", tripScore.isSync());

        } catch (Exception e){
            e.printStackTrace();
        }
        Log.d(TripRestService.class.getSimpleName(), " :-"+tripScoreJson);
        return  restServiceCall("/tripScore/create", "POST", tripScoreJson);
    }

    public String createTripScore(JSONObject tripScoreJson) {
        return  restServiceCall("/tripScore/create", "POST", tripScoreJson);
    }

    public String findTripGpsDistance(JSONObject tripScoreJson) {

        String url = null;
        try {
            url = "http://maps.googleapis.com/maps/api/directions/json?origin="+tripScoreJson.get("startLat")+","+ tripScoreJson.get("startLong")+
                    "&destination="+tripScoreJson.get("stopLat")+","+tripScoreJson.get("stopLong") +
                    "&sensor=false&mode=%22DRIVING%22";
            return externalRestServiceCall(url, "GET");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getAllTripsByUserId(String userId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  restServiceCall("/trip/findByUserId", "POST", jsonObject);
    }

    public String getTripScoreByTripId(String tripId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("tripId", tripId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  restServiceCall("/tripScore/findByTripId", "POST", jsonObject);
    }
}
