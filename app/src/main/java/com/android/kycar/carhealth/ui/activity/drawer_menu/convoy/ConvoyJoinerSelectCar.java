package com.android.kycar.carhealth.ui.activity.drawer_menu.convoy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_cars.MyCarsActivity;

public class ConvoyJoinerSelectCar extends AppCompatActivity {

    private static final int RESULT_PICK_CONTACT = 1;
    private String convoyID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convoy_joiner_select_car);
        convoyID = getIntent().getStringExtra(Util.CONVOY_ID);
    }

    public void selectCar(View view){
        startActivity(new Intent(ConvoyJoinerSelectCar.this,MyCarsActivity.class)
                .putExtra(Util.IS_FROM_SELECT_CAR, true).putExtra(Util.CONVOY_ID, convoyID));
    }
}
