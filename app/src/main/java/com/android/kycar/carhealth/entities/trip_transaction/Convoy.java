package com.android.kycar.carhealth.entities.trip_transaction;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 1/17/2016.
 */
@RealmClass
public class Convoy extends RealmObject {

    @PrimaryKey
    private String id;

    private String convoyName;

    private String status;

    private String userId;

    private String vehicleId;

    public Convoy() {
    }

    public Convoy(String id, String convoyName, String status, String userId, String vehicleId) {
        this.id = id;
        this.convoyName = convoyName;
        this.status = status;
        this.userId = userId;
        this.vehicleId = vehicleId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getConvoyName() {
        return convoyName;
    }

    public void setConvoyName(String convoyName) {
        this.convoyName = convoyName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }
}
