package com.android.kycar.carhealth.services.connection_services.mqtt_paho_android_service;

import org.eclipse.paho.android.service.MqttAndroidClient;

/**
 * Created by ameykshirsagar on 21/07/16.
 */
public class Connection {
    private static MqttAndroidClient client;

    public static void setClient(MqttAndroidClient cl){
        client = cl;
    }
    public static MqttAndroidClient getClient(){
        return client;
    }
}
