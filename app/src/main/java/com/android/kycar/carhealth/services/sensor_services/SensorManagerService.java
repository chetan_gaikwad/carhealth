package com.android.kycar.carhealth.services.sensor_services;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.kycar.carhealth.NxtApplication;
import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.async_task.SendAccidentNotificationAsyncTask;
import com.android.kycar.carhealth.beans.Angle;
import com.android.kycar.carhealth.beans.Coordinates;
import com.android.kycar.carhealth.beans.Obstacle;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.UserContact;
import com.android.kycar.carhealth.entities.UserContactSetting;
import com.android.kycar.carhealth.entities.sensor_transaction.Accelerometer;
import com.android.kycar.carhealth.repositories.user.UserContactRepo;
import com.android.kycar.carhealth.repositories.user.UserContactSettingRepo;
import com.android.kycar.carhealth.services.connection_services.mqtt_paho.PublishAccident;
import com.android.kycar.carhealth.services.connection_services.mqtt_paho.PublishSensorData;
import com.android.kycar.carhealth.services.connection_services.mqtt_paho_android_service.Connection;
import com.android.kycar.carhealth.services.connection_services.rest_services.RoadConditionRestService;
import com.android.kycar.carhealth.services.io_services.SmsAlerts;
import com.android.kycar.carhealth.services.utils.LoggerTxt;
import com.android.kycar.carhealth.ui.activity.DashboardActivity;
import com.android.kycar.carhealth.utils.BusProvider;
import com.android.kycar.carhealth.utils.PreferenceUtils;
import com.github.nisrulz.sensey.Sensey;
import com.github.nisrulz.sensey.WaveDetector;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class SensorManagerService extends Service implements SensorEventListener, LocationListener {

    private Coordinates coords = new Coordinates();
    private LoggerTxt loggerTxt;
    private SensorManager sensorManager;
    private LocationManager locationManager;
    private Sensor accelerometer;
    private Toast nxtObstacleToast;
   /*
   //Chetan : Commented since not currently in use
    //Dont delete it
    private Sensor gyroscope;
    private Sensor gravity;
    private Sensor magnetometer;*/
    private float[] mGravity;
    private float[] mGeomagnetic;
    public static double maxAccXReading;
    public static double maxAccYReading;
    public static double maxAccZReading;

    private double detectedAy, detectedAz, detectedAx;

    /** newX:z, newY:x, newZ:y */

    public static long harshBrakeCount = 0;
    public static long harshAccCount = 0;
    public static long harshLeftCount = 0;
    public static long harshRightCount = 0;

    public static double totalAccIntensity = 0;
    public static double totalBrakingIntensity = 0;
    public static double totalRightIntensity = 0;
    public static double totalLeftIntensity = 0;

    public static long mildBrakeCount = 0;
    public static long mildAccCount = 0;
    public static long mildLeftCount = 0;
    public static long mildRightCount = 0;

    private Integer speed;
    protected SharedPreferences preferences;
    protected SharedPreferences.Editor editor;
    private String vehicleId;
    private HashMap<String, Double> harshValues;
    private HashMap<String, Double> mildValues;
    private int magBearing;
    private PublishAccident publishAccident;
    private PublishSensorData publishSensorData;
    private SmsAlerts smsAlerts;
    private List<Location> undulationsLocationArray;
    private UserContactRepo userContactRepo;
    private UserContactSettingRepo userContactSettingRepo;
    static final float ALPHA = 0.25f;
    SensorDataLoggerService sensorDataLoggerService;
    public static int SUB_SLEEP_TIME = 20000;

    @Override
    public void onCreate() {

        preferences = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        editor = preferences.edit();
        NxtApplication.currLocation = new Location("");

        loggerTxt = new LoggerTxt("rc");

        undulationsLocationArray = new ArrayList<>();

        /** Used to detect accident for nexus*/
        maxAccXReading = preferences.getFloat(Util.SENSOR_CALIBRATION_VALUE_X, 20);
        maxAccYReading = preferences.getFloat(Util.SENSOR_CALIBRATION_VALUE_Y, 20);
        maxAccZReading = preferences.getFloat(Util.SENSOR_CALIBRATION_VALUE_Z, 20);
        userContactRepo = new UserContactRepo(getApplicationContext(), UserContact.class);

        /** Locations reading from CSV file */

        /*File root = android.os.Environment.getExternalStorageDirectory();
        String folder = "/nxtauto/logger/KothrudCNG";

        File dir = new File (root.getAbsolutePath() + folder );
        File file = new File(dir, "Logger1.csv");

        localLocations = new ArrayList<>();
        InputStream inputStream = null;
        try {

            inputStream = new FileInputStream(file);
            CsvReaderUtil csvFile = new CsvReaderUtil(inputStream);
            List scoreList = csvFile.read();

            for (int i = 1 ; i< scoreList.size(); i++) {

                Location location = new Location("");
                String [] datas = (String[]) scoreList.get(i);

                location.setLatitude(Double.parseDouble(datas[3]));
                location.setLongitude(Double.parseDouble(datas[4]));
                localLocations.add(location);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }*/

        /** Initialised on main thread for maintaining sequence */
        publishAccident = new PublishAccident();
        publishSensorData = new PublishSensorData();

//        Log.d("Location---", "" + locationArray.size());

        nxtObstacleToast = new Toast(getApplicationContext());
        initializeLocation();

        if(NxtApplication.currLocation != null) {
            //new FetchRoadConditionAsyncTask().execute("");
        }

        Sensey.getInstance().init(this);

        Sensey.getInstance().startWaveDetection(waveListener);
        sensorDataLoggerService = new SensorDataLoggerService(this);
    }

    WaveDetector.WaveListener waveListener=new WaveDetector.WaveListener() {
        @Override
        public void onWave() {
            // Wave of hand gesture detected
            Log.d("Sensor manager", "Wave detected");
            //If end call gesture is set to true from setting then only end call on gesture detection
            if(preferences.getBoolean(PreferenceUtils.PREFERENCE_END_CALL, false)) {
                killCall(SensorManagerService.this);
            }

        }
    };

    public boolean killCall(Context context) {
        Log.d("dfs","Kill call");
        try {
            // Get the boring old TelephonyManager
            TelephonyManager telephonyManager =
                    (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            // Get the getITelephony() method
            Class classTelephony = Class.forName(telephonyManager.getClass().getName());
            Method methodGetITelephony = classTelephony.getDeclaredMethod("getITelephony");

            // Ignore that the method is supposed to be private
            methodGetITelephony.setAccessible(true);

            // Invoke getITelephony() to get the ITelephony interface
            Object telephonyInterface = methodGetITelephony.invoke(telephonyManager);

            // Get the endCall method from ITelephony
            Class telephonyInterfaceClass =
                    Class.forName(telephonyInterface.getClass().getName());
            Method methodEndCall = telephonyInterfaceClass.getDeclaredMethod("endCall");

            // Invoke endCall()
            methodEndCall.invoke(telephonyInterface);

        } catch (Exception ex) { // Many things can go wrong with reflection calls
            Log.d("dfs","PhoneStateReceiver **" + ex.toString());
            return false;
        }
        return true;
    }

    public SensorManagerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.i(SensorManagerService.class.getSimpleName(), "Service onStartCommand");
        Thread thread = new Thread() {
            @Override
            public void run() {
                startSensors();
            }
        };
        //thread.start();
        //BusProvider.getInstance().register(this);
        return Service.START_STICKY;
    }

    private void initializeLocation() {

        //Log.d(SensorManagerService.class.getSimpleName(), "Location found:-"+NxtApplication.currLocation);
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, true);
        NxtApplication.currLocation = locationManager.getLastKnownLocation(bestProvider);

        if (NxtApplication.currLocation != null) {
            /** Temporory solution */
            onLocationChanged(NxtApplication.currLocation);
            //coords.setLocation(updatedLocation);
        } else{
            NxtApplication.currLocation = new Location("");
        }
        locationManager.requestLocationUpdates(bestProvider, 0, 0, this);
    }

    public void onLocationChanged(Location location) {

        NxtApplication.currLocation = location;
        coords.setLocation(NxtApplication.currLocation);

        /** Publishing real-time coordinates for tracking */


        /*if(*//*preferences.getBoolean(Util.IS_SHARE_LOC, false) && *//*preferences.getBoolean(Util.IS_LOGGED_IN, false)) {
            if(!NxtApplication.shareLocationUserTopics.isEmpty()) {
                MqttMessage message = publishCoods.getCoordData(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, "");
                for ( String topic : NxtApplication.shareLocationUserTopics ) {
                    try {
                        NxtApplication.mqttAndroidClient.publish(NxtApplication.baseTopic+"/"+topic, message);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }
            }
        }*/

        if(location != null) {
            BusProvider.getInstance().post(location);
        }
        if(undulationsLocationArray != null);
        //Log.d(SensorManagerService.class.getSimpleName(), "Location found:-"+NxtApplication.currLocation);
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onDestroy() {
        //BusProvider.getInstance().unregister(this);
        try {
            sensorManager.unregisterListener(this, accelerometer);
            sensorManager = null;
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                stopGps();
            }
            userContactRepo.close();
        } catch (Exception w) {
            w.printStackTrace();
        }

        /** Deleting empty files */
        loggerTxt.finalize();
    }

    private void stopGps() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ContextCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ContextCompat#requestPermissions for more details.
            return;
        }
        locationManager.removeUpdates(this);
        locationManager = null;
    }

    private void initLocalVariables(){

        vehicleId = preferences.getString(Util.CAR_PREFERENCE, "1");
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses;

        try {
            addresses = geocoder.getFromLocation(
                    coords.getLatitude(),
                    coords.getLongitude(),
                    // In this sample, get just a single address.
                    1);
            for (Address address : addresses){
                //Log.d("Address", ""+address.getLocality()+", "+address.getAddressLine(0)+", "+address.getAdminArea());
                address.getSubLocality();
            }
        } catch (Exception e) {
            // Catch network or other I/O problems.
            //errorMessage = getString(R.string.service_not_available);
            //Log.d("Locaion error", ""+e.getMessage());
            e.printStackTrace();
        }
        speed = 0;

        mildValues = new HashMap<String, Double>();
        mildValues.put("ACC", 4.5);
        mildValues.put("BREAKING", -4.5);
        mildValues.put("LEFT", -4.5);
        mildValues.put("RIGHT", 4.5);
        mildValues.put("UP", -0.15);
        mildValues.put("DOWN",11.2 );

        harshValues= new HashMap<String, Double>();
        harshValues.put("ACC", 6.0);
        harshValues.put("BREAKING", -6.0);
        harshValues.put("LEFT", -6.0);
        harshValues.put("RIGHT", 6.0);
        harshValues.put("UP", -5.0);
        harshValues.put("DOWN", 18.0);

        /** values for mi */
        //harshValues.put("DOWN", 15.0);
        //harshValues.put("UP", 5.0);

        magBearing = 0;
    }

    private void startSensors() {

        initLocalVariables();
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);

            Log.d("Range :-", ""+accelerometer.getMaximumRange());
            accelerometer.getMinDelay();

        } else {
            // Toast.makeText(((EngineVitalsActivity) context), "Your device don't support accelerometer", Toast.LENGTH_SHORT).show();
        }


        //Chetan : Commented other sersor since not using
        /*if (sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null) {
            gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
            sensorManager.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_NORMAL);
        } else {
            //Toast.makeText((EngineVitalsActivity)context, "Your device don't support GYROSCOPE", Toast.LENGTH_SHORT).show();
        }

        if (sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY) != null) {
            //Toast.makeText((EngineVitalsActivity)context, "Your device support GRAVITY", Toast.LENGTH_SHORT).show();
            gravity = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
            sensorManager.registerListener(this, gravity, SensorManager.SENSOR_DELAY_NORMAL);
        } else {
            //Toast.makeText((EngineVitalsActivity)context, "Your device don't support TYPE_GRAVITY", Toast.LENGTH_SHORT).show();
        }
        if (sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null) {
            //Toast.makeText((EngineVitalsActivity)context, "Your device support GRAVITY", Toast.LENGTH_SHORT).show();
            magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_NORMAL);
        } else {
            Toast.makeText(this.getApplicationContext(), "Your device don't support Magnetometer", Toast.LENGTH_SHORT).show();
        }*/
        /*Thread thread = new Thread() {
                @Override
                public void run() {
                    subscribe();
                }
            };
            thread.start();*/
            //newSubscribe();
            //subscribe();
    }

    private void subscribeRun(){

        while (true) {
            try {
                Thread.sleep(SUB_SLEEP_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        manageSensonEvents(sensorEvent);
    }
    @Subscribe
    public void tiSensorData(Accelerometer accelerometer){
        Log.d("Sensor service x-",accelerometer.getAx()+" y-"+accelerometer.getAy());
    }

    private void manageSensonEvents(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;
        if (mySensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {

            mGeomagnetic = sensorEvent.values;
            if (mGravity != null && mGeomagnetic != null) {
                float R[] = new float[9];
                float I[] = new float[9];
                boolean success = SensorManager.getRotationMatrix(R, I, mGravity,
                        mGeomagnetic);
                if (success) {
                    float orientation[] = new float[3];
                    SensorManager.getOrientation(R, orientation);
                    magBearing = (int) Math.round(Math.toDegrees(orientation[0]));
                }
            }
        }

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            float ax = sensorEvent.values[0];
            float ay = sensorEvent.values[1];
            float az = sensorEvent.values[2];
            float[] accSensorVals = null;

            //accSensorVals = lowPass(sensorEvent.values.clone(), accSensorVals);

            /** Checking for the speed if vehicle is moving calculate then only */
            if(!preferences.getBoolean(Util.IS_DRIVE_MODE, false)) {
                return;
            }
            loggerTxt.log("Ax:"+ax+", ay:"+ay+", az:"+az+NxtApplication.currLocation);
            // Log.d("Sensor Value", "Ax:"+ax+", ay:"+ay+", az:"+az);
            /** Detecting accident */
            if (ax > maxAccXReading || ay > maxAccYReading || az > maxAccZReading) {

                //showCustomAlert(178);
                //Log.d(SensorManagerService.class.getSimpleName(), " ax:-"+ax+", ay:-"+ay+", az:-"+az);
                float intensity = (ax>ay?ax:ay)>az?(ax>ay?ax:ay):az;

                /** Publishing data for location sharing */
                List<UserContact> soses = userContactRepo.findAll();

                for (UserContact sos : soses) {
                    userContactSettingRepo = new UserContactSettingRepo(getApplicationContext(), UserContactSetting.class);
                    UserContactSetting userContactSetting = userContactSettingRepo.findByUserContactId(sos.getId());
                    if(userContactSetting.getIsSos() && sos.getNumber() != null) {
                        smsAlerts.sendSMSAccidentAlert("", intensity, coords, getApplicationContext());
                        if(Util.isOnline(this)) {
                            new SendAccidentNotificationAsyncTask().execute(sos.getUserContactTopic(), preferences.getString(Util.USERNAME_PREFERENCE, ""));

                        }

                        /** Accident disabled */
                        if(NxtApplication.currLocation != null && false) {
                            MqttMessage message = publishAccident.getAccidentMessage(preferences.getString(Util.USERNAME_PREFERENCE, ""), NxtApplication.currLocation);
                            try {
                                Connection.getClient().publish(NxtApplication.baseTopic+"/"+sos.getUserContactTopic(), message);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            //Log.d(SensorManagerService.class.getSimpleName(), "Harsh Counts :-{harshAccCount:"+harshAccCount+", harshBrakeCount :"+harshBrakeCount+", harshLeftCount:"+harshLeftCount+",harshRightCount : "+harshRightCount+"}");
            //Log.d(SensorManagerService.class.getSimpleName(), "Mild Counts :-{mildAccCount:"+mildAccCount+", mildBrakeCount :"+mildBrakeCount+", mildLeftCount:"+mildLeftCount+",mildRightCount : "+mildRightCount+"}");
            String event = null;

            /** Detecting gravity */
            if(detectGravity(ax, ay, az)) {

                //Log.d(SensorManagerService.class.getSimpleName(), "Detected values:-"+detectedAy+". "+detectedAz+", "+detectedAx);

                /** Detecting Road Condition Events */
                if(detectedAz < harshValues.get("UP")) {
                    event = "HARSH_UP";
                    /*try {
                        Log.d(SensorManagerService.class.getSimpleName(), "Pothole Data published");
                        //onUserNotify("");
                        Connection.getClient().publish(NxtApplication.baseTopic+"/potholes_event/"+preferences.getString(Util.USER_PREFERENCE, ""), publishSensorData.getPotholesEventSensorData(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAz, speed, NxtApplication.currLocation.getSpeed(), preferences.getString(Util.TRIP_ID, "")));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
                } else if(detectedAz > harshValues.get("DOWN")) {
                    event = "HARSH_DOWN";

                    /*try {
                        Log.d(SensorManagerService.class.getSimpleName(), "Pothole Data published");
                        //onUserNotify("");
                        Connection.getClient().publish(NxtApplication.baseTopic+"/potholes_event/"+preferences.getString(Util.USER_PREFERENCE, ""), publishSensorData.getPotholesEventSensorData(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAz, speed, NxtApplication.currLocation.getSpeed(), preferences.getString(Util.TRIP_ID, "")));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
                } else if(detectedAz > mildValues.get("DOWN")) {
                    event = "DOWN";
                } else if(detectedAz < mildValues.get("UP")) {
                    event = "UP";
                }

                if(event != null) {

                    /** Notification for user */
                    //onUserNotify(event);

                    /** RoadCondition pref not set yet */

                    if (Util.isOnline(getApplicationContext())) {
                        if (preferences.getBoolean(Util.IS_UPLOAD_RC, true) && preferences.getBoolean(Util.IS_LOGGED_IN, false)){
                            //publishSensorData.getEventSensorData(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAz, event, speed, updatedLocation.getSpeed(), updatedLocation.getBearing(), magBearing);

                            try {
                                Log.d(SensorManagerService.class.getSimpleName(), "Published:- "+NxtApplication.baseTopic+"/sensor_event_data/"+preferences.getString(Util.USER_PREFERENCE, ""));
                                Connection.getClient().publish(NxtApplication.baseTopic+"/sensor_event_data/"+preferences.getString(Util.USER_PREFERENCE, ""), publishSensorData.getEventSensorData(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAz, event, speed, NxtApplication.currLocation.getSpeed(), NxtApplication.currLocation.getBearing(), magBearing, preferences.getString(Util.TRIP_ID, ""), preferences.getString(Util.SENSOR_ID, "NR")));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        Log.d(SensorManagerService.class.getSimpleName(), "Event Data Logged");

                        if(NxtApplication.currLocation != null) {
                            loggerTxt.log(publishSensorData.getEventSensorDataJSONObject(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAy, event, speed, NxtApplication.currLocation.getSpeed(), NxtApplication.currLocation.getBearing(), magBearing, preferences.getString(Util.TRIP_ID, ""),preferences.getString(Util.SENSOR_ID, "NR")).toString());
                        }
                        sensorDataLoggerService.logAccSb(publishSensorData.getEventSensorDataJSONObject(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAy, event, speed, NxtApplication.currLocation.getSpeed(), NxtApplication.currLocation.getBearing(), magBearing, preferences.getString(Util.TRIP_ID, ""),preferences.getString(Util.SENSOR_ID, "NR")));
                    }

                    event = null;
                    return;
                }

                /** Detecting Driver Behaviour Events */
                if(detectedAy < harshValues.get("LEFT")) {
                    event = "HARSH_LEFT";
                    harshLeftCount++;
                    totalLeftIntensity = totalLeftIntensity + detectedAy;
                } else if(detectedAy > harshValues.get("RIGHT")) {
                    event = "HARSH_RIGHT";
                    harshRightCount++;
                    totalRightIntensity = totalRightIntensity + detectedAy;
                } else if(detectedAy < mildValues.get("LEFT")) {
                    event = "LEFT";
                    mildLeftCount++;
                } else if(detectedAy > mildValues.get("RIGHT")) {
                    event = "RIGHT";
                    mildRightCount++;
                }
                if(event != null) {
                    //onUserNotify(event);

                    /** Driver Behaviour pref not set yet */
                    if (preferences.getBoolean(Util.IS_UPLOAD_DB, true) && preferences.getBoolean(Util.IS_LOGGED_IN, false)){
                        if(Util.isOnline(getApplicationContext())){
                            try {
                                Log.d(SensorManagerService.class.getSimpleName(), "Event Data Published :-"+NxtApplication.baseTopic+"/sensor_event_data/"+preferences.getString(Util.USER_PREFERENCE, ""));
                                Connection.getClient().publish(NxtApplication.baseTopic+"/sensor_event_data/"+preferences.getString(Util.USER_PREFERENCE, ""), publishSensorData.getEventSensorData(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAy, event, speed, NxtApplication.currLocation.getSpeed(), NxtApplication.currLocation.getBearing(), magBearing, preferences.getString(Util.TRIP_ID, ""),preferences.getString(Util.SENSOR_ID, "NR")));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.d(SensorManagerService.class.getSimpleName(), "Event Data Logged");
                            loggerTxt.log(publishSensorData.getEventSensorDataJSONObject(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAy, event, speed, NxtApplication.currLocation.getSpeed(), NxtApplication.currLocation.getBearing(), magBearing, preferences.getString(Util.TRIP_ID, ""),preferences.getString(Util.SENSOR_ID, "NR")).toString());
                            sensorDataLoggerService.logAccSb(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAy, event, speed, NxtApplication.currLocation.getSpeed(), NxtApplication.currLocation.getBearing(), magBearing, preferences.getString(Util.TRIP_ID, ""),preferences.getString(Util.SENSOR_ID, "NR"));
                        }
                    }
                    event = null;
                    return;
                }

                /** Detecting drive behaviour Events */
                if(detectedAx > harshValues.get("ACC")) {
                    event = "HARSH_ACC";
                    harshAccCount++;
                    totalAccIntensity = totalAccIntensity + detectedAy;
                } else if(detectedAx < harshValues.get("BREAKING")) {
                    event = "HARSH_BREAKING";
                    harshBrakeCount++;
                    totalBrakingIntensity = totalBrakingIntensity + detectedAy;
                } else if(detectedAx > mildValues.get("ACC")) {
                    event = "ACC";
                    mildAccCount++;
                } else if(detectedAx < mildValues.get("BREAKING")) {
                    event = "BREAKING";
                    mildBrakeCount++;
                }
                if(event != null) {

                    //sensorDataLoggerService.logAcc(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), coords.getLatitude(), coords.getLongitude(), detectedAz, event, NxtApplication.currLocation.getSpeed(), NxtApplication.currLocation.getBearing(), magBearing);
                    //onUserNotify(event);

                    /** Driver Behaviour pref not set yet */
                    if (preferences.getBoolean(Util.IS_UPLOAD_DB, true) && preferences.getBoolean(Util.IS_LOGGED_IN, false)){

                        if(Util.isOnline(getApplicationContext())) {

                            try {
                                Log.d(SensorManagerService.class.getSimpleName(), "Event Data Published");
                                Connection.getClient().publish(NxtApplication.baseTopic+"/sensor_event_data/"+preferences.getString(Util.USER_PREFERENCE, ""), publishSensorData.getEventSensorData(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAx, event, speed, NxtApplication.currLocation.getSpeed(), NxtApplication.currLocation.getBearing(), magBearing, preferences.getString(Util.TRIP_ID, ""), preferences.getString(Util.SENSOR_ID, "NR") ));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.d(SensorManagerService.class.getSimpleName(), "Event Data Logged");
                            loggerTxt.log(publishSensorData.getEventSensorDataJSONObject(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAy, event, speed, NxtApplication.currLocation.getSpeed(), NxtApplication.currLocation.getBearing(), magBearing, preferences.getString(Util.TRIP_ID, ""),preferences.getString(Util.SENSOR_ID, "NR")).toString());
                            sensorDataLoggerService.logAccSb(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAy, event, speed, NxtApplication.currLocation.getSpeed(), NxtApplication.currLocation.getBearing(), magBearing, preferences.getString(Util.TRIP_ID, ""),preferences.getString(Util.SENSOR_ID, "NR"));
                        }

                        event = null;
                    }
                }
            }

            mGravity = sensorEvent.values;

            /** Use to show the vehicle rotations on screen */
            int angleX = (int) Math.round(Math.toDegrees(Math.acos((float) (ax / Math.sqrt(ax * ax + ay * ay + az * az)))));
            int angleZ = (int) Math.round(Math.toDegrees(Math.acos((float) (az / Math.sqrt(ax * ax + ay * ay + az * az)))));

            /** disabled for the thread optimization */
            // BusProvider.getInstance().post(sensorEvent);
            try {
                Connection.getClient().publish(NxtApplication.baseTopic+"/acc_data/"+preferences.getString(Util.USER_PREFERENCE, ""), publishSensorData.getAccSensorData(ax,ay,az));
            } catch (Exception e) {
                e.printStackTrace();
            }
            BusProvider.getInstance().post(produceAngle(angleX, angleZ));
        }
    }

    /** This method detects gravity acts on which axes */
    private boolean detectGravity(double ax, double ay, double az) {

        if(ay > 9.5) {

            detectedAz = ay;
            detectedAy = ax;
            detectedAx = az;
            return true;
        } else if (ax > 9.5) {

            detectedAz = ax;
            detectedAy = ay;
            detectedAx = az;
            return true;
        } else if (az > 9.5) {

            detectedAz = az;
            detectedAx = ay;
            detectedAy = ax;
            return true;
        } else {
            return false;
        }
    }

    @Produce
    public SensorEvent produceSensorEvent(SensorEvent sensorEvent){
        return sensorEvent;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Produce
    public Angle produceAngle(Integer angleX, Integer angleZ) {
        return new Angle(angleX, null, angleZ);
    }

    public void onUserNotify(String event) {

        NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setContentTitle(event)
                        .setContentText("Event added successfully");

        Intent notificationIntent = new Intent(this, DashboardActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(contentIntent);
        builder.setAutoCancel(true);
        builder.setLights(Color.BLUE, 500, 500);
        builder.setStyle(new NotificationCompat.InboxStyle());

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);

        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(1, builder.build());
    }

    public void findNextObstacle(Location curLocation) {

        Log.d(SensorManagerService.class.getSimpleName(), "Obstacle called"+curLocation);
        if(undulationsLocationArray.isEmpty() && NxtApplication.currLocation != null) {
            if(Util.isOnline(this)) {
               // new FetchRoadConditionAsyncTask().execute();
            }
            return;
        }

        /*for (Location location: undulationsLocationArray) {
            Log.d(SensorManagerService.class.getSimpleName(), " Distances:-"+findDistance(curLocation, location ));
        }*/
        if(curLocation != null) {
            float d = findDistance(curLocation, undulationsLocationArray.get(0));
            double distance = 999999999;

            if(d < distance) {
                //showCustomAlert(178);
                BusProvider.getInstance().post(new Obstacle((int) d, 0));
                showCustomAlert(d);
                if(d < 50 && !undulationsLocationArray.isEmpty()) {
                    undulationsLocationArray.remove(0);
                }
            }
        }
    }

    @Produce
    public Obstacle produceNextObstacle(float obsDistance) {
        Log.d(SensorManagerService.class.getSimpleName(), "Distance published:"+obsDistance);
        return new Obstacle((int) obsDistance, 0);
    }

    private float findDistance(Location src, Location dest) {
        return src.distanceTo(dest);
    }

    public class FetchRoadConditionAsyncTask extends AsyncTask<String,Void,String> {
        @Override
        protected String doInBackground(String... params) {

            /** fetching locations from cloud */
            RoadConditionRestService roadConditionRestService = new RoadConditionRestService();
            try {
                String result = roadConditionRestService.fetchRoadCondition(NxtApplication.currLocation);
                if(result != null){
                    JSONObject roadConditionJson = new JSONObject(result);

                    if(roadConditionJson.has("results")) {
                        JSONArray locationArray = roadConditionJson.getJSONArray("results");
                        for(int counter = 0; counter <locationArray.length(); counter++) {
                            JSONObject locationObject = locationArray.getJSONObject(counter);
                            JSONObject obj = locationObject.getJSONObject("obj");
                            JSONObject loc = obj.getJSONObject("loc");
                            Location location = new Location("");
                            location.setLatitude(loc.getDouble("lat"));
                            location.setLongitude(loc.getDouble("lng"));
                            location.setAccuracy((float) obj.getDouble("acc"));

                            //Log.d(SensorManagerService.class.getSimpleName(), "Calculated Distnace:-"+findDistance(NxtApplication.currLocation, location));
                            boolean isAdd = false;
                            if(NxtApplication.currLocation != null) {
                                for(int index = 0 ; index < undulationsLocationArray.size(); index++) {
                                    if(findDistance(NxtApplication.currLocation,undulationsLocationArray.get(index)) > findDistance(NxtApplication.currLocation, location)) {
                                        undulationsLocationArray.add(index, location);
                                        isAdd = true;
                                        break;
                                    }
                                }
                            } 

                            if(!isAdd) {
                                undulationsLocationArray.add(location);
                            }
                            Log.d(SensorManagerService.class.getSimpleName(), "Location added :-["+location.getLatitude()+","+location.getLongitude()+"]");
                        }
                        Log.d(SensorManagerService.class.getSimpleName(), "Size:-"+undulationsLocationArray.size());
                        findNextObstacle(NxtApplication.currLocation);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            smsAlerts = new SmsAlerts();
            undulationsLocationArray = new ArrayList<>();
            return null;
        }
    }


    public void showCustomAlert(final float distance) {

        // Create layout inflator object to inflate toast.xml file
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        // Call toast.xml file for toast layout
        View toastRoot = inflater.inflate(R.layout.toast, null);
        TextView tvObstacleText =  (TextView) toastRoot.findViewById(R.id.tv_obstacle_text);
        tvObstacleText.setText(" "+distance);

        // Set layout to toast
        nxtObstacleToast.setView(toastRoot);
        nxtObstacleToast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        nxtObstacleToast.setDuration(Toast.LENGTH_SHORT);
        //nxtObstacleToast.show();
        /*try {
            Thread.sleep(1000);
            nxtObstacleToast.cancel();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
    }
    protected float[] lowPass( float[] input, float[] output ) {
        if ( output == null )
            return input;
        for ( int i=0; i<input.length; i++ ) {
            output[i] = output[i] + ALPHA * (input[i] - output[i]);
        } return output;
    }
}
