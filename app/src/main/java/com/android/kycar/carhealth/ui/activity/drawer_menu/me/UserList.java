package com.android.kycar.carhealth.ui.activity.drawer_menu.me;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.beans.People;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.User;
import com.android.kycar.carhealth.repositories.user.UserRepo;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_people.adapter.MyPeopleRecyclerAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class UserList extends Activity {

    private RecyclerView recyclerView;
    private MyPeopleRecyclerAdapter adapter;
    private FloatingActionButton fab;
    private String convoyID;
    private UserRepo userRepo;
    private boolean fromSelectUser = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_user_list);
        recyclerView = (RecyclerView) findViewById(R.id.recyle_view_user);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        if(getIntent().getExtras()!=null) {
            fromSelectUser = getIntent().getBooleanExtra(Util.IS_FROM_SELECT_CAR, false);
            convoyID = getIntent().getStringExtra(Util.CONVOY_ID);
        }
        fab.setOnClickListener(onAddingListener());
        userRepo = new UserRepo(getApplicationContext(), User.class);
    }

    private View.OnClickListener onAddingListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserList.this, ProfileActivity.class));
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAllUsers();
    }

    public void  getAllUsers(){

        List<User> users = userRepo.findAll();
        List<People> peoples = new ArrayList<>();
        for (User user : users){

            peoples.add(new People(user.getId(),
                    user.getFirstName(),
                    user.getPhoneNumber(),
                    user.getProfilePicture(),
                    false,
                    false,
                    false));
        }
        adapter = new MyPeopleRecyclerAdapter(UserList.this, peoples, fromSelectUser);
        recyclerView.setAdapter(adapter);
    }
}
