package com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.entities.trip_transaction.Trip;
import com.android.kycar.carhealth.repositories.trip.TripRepo;
import com.android.kycar.carhealth.repositories.vehicle.VehicleRepo;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides.adapter.TripListAdapter;
import com.android.kycar.carhealth.utils.ConfirmDialog;

import org.json.JSONArray;

import java.util.List;

/**
 * Some code taken from https://github.com/wdkapps/FillUp
 */

public class TripListActivity
        extends AppCompatActivity {

    private List<Trip> trips;
    private TripListAdapter adapter = null;
    /// the currently selected row from the list of records
    private int selectedRow;
    private TripRepo tripRepo;
    private VehicleRepo vehicleRepo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_trips_list);
        ListView lv = (ListView) findViewById( R.id.tripList);
        tripRepo = new TripRepo(getApplicationContext(), Trip.class);
        trips = tripRepo.findAll();
        tripRepo.close();
        adapter = new TripListAdapter(this, trips);
        lv.setAdapter(adapter);
        registerForContextMenu(lv);
        restTripSync();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_trips_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // create the menu
        getMenuInflater().inflate( R.menu.context_trip_list, menu);
        // get index of currently selected row
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        selectedRow = (int) info.id;
        // get record that is currently selected
    }

    public boolean onContextItemSelected(MenuItem item) {

        // get index of currently selected row
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        selectedRow = (int) info.id;
        switch (item.getItemId()) {
            case  R.id.itemDelete:
                showDialog(ConfirmDialog.DIALOG_CONFIRM_DELETE_ID);
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }

    /** Need to work on this for sync with cloud */
    private void restTripSync(){

        List<Trip> trips = tripRepo.findAll();
        JSONArray allTrips = new JSONArray();
        for (Trip trip: trips){
            trip.toString();
        }
    }
}
