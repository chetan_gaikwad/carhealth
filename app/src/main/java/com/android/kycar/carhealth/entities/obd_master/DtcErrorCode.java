package com.android.kycar.carhealth.entities.obd_master;


import androidx.annotation.NonNull;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 8/26/2016.
 */
@RealmClass
public class DtcErrorCode extends RealmObject implements Comparable<DtcErrorCode>{

    @PrimaryKey
    private String id;

    private String dtc;

    private String description;

    private String diagnosis;

    public DtcErrorCode() {
    }

    public DtcErrorCode(String description, String diagnosis, String dtc, String id) {
        this.description = description;
        this.diagnosis = diagnosis;
        this.dtc = dtc;
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getDtc() {
        return dtc;
    }

    public void setDtc(String dtc) {
        this.dtc = dtc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int compareTo(@NonNull DtcErrorCode o) {
        if(this.diagnosis.equals(o.diagnosis))
            return 0;
        else
            return -1;
    }
}
