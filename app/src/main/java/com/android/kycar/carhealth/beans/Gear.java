package com.android.kycar.carhealth.beans;



import com.android.kycar.carhealth.entities.vehicle_master.GearRatio;

import java.util.List;

/**
 * Created by Krishna on 6/6/2016.
 */
public class Gear {

    List<GearRatio> gearRatios;

    public Gear(){

    }
    public Gear(List<GearRatio> gearRatios) {
        this. gearRatios = gearRatios;
    }
    public Integer getGear(Double gr){

        /*for (GearRatio gearRatio : gearRatios) {
            if(gr < gearRatio.getGearRatioMax() && gr > gearRatio.getGearRatioMin()) { return gearRatio.getGear();}
        }
        return 0;*/


        //Default nano GR
        /*if(gr > 141 && gr < 242){
            return 1;
        } else if(gr > 81 && gr < 125){
            return 2;
        } else if(gr > 56 && gr < 67){
            return 3;
        } else if(gr > 21 && gr < 45){
            return 4;
        } else {
            return 0;
        }*/

        //Default Creta GR
        /*if(gr > 110 && gr < 150){
            return 1;
        } else if(gr > 60 && gr < 80){
            return 2;
        } else if(gr > 40 && gr < 55){
            return 3;
        } else if(gr > 30 && gr < 38){
            return 4;
        } else if(gr > 24 && gr < 28){
            return 5;
        } else if(gr > 10 && gr < 23){
            return 6;
        } else {
            return 0;
        }*/

        /** Bolt */
        /*if(gr > 95 && gr < 196){
            return 1;
        } else if(gr > 66 && gr < 93){
            return 2;
        } else if(gr > 44 && gr < 60){
            return 3;
        } else if(gr > 28.5 && gr < 40){
            return 4;
        } else if(gr > 20 && gr < 28){
            return 5;
        } else {
            return 0;
        }*/

        /** Amaze */

        /*if(gr > 105 && gr < 150){
            return 1;
        } else if(gr > 58 && gr < 70){
            return 2;
        } else if(gr > 37 && gr < 42){
            return 3;
        } else if(gr > 27 && gr < 32){
            return 4;
        } else if(gr > 2 && gr < 25){
            return 5;
        } else {
            return 0;
        }
*/
        /** City */


        if(gr > 104 && gr < 250 ){
            return 1;
        } else if(gr > 50  && gr < 80){
            return 2;
        } else if(gr > 32  && gr < 50){
            return 3;
        } else if(gr > 25  && gr < 31){
            return 4;
        } else if(gr > 21 && gr < 25){
            return 5;
        } else if(gr > 10  && gr < 21){
            return 6;
        }else {
            return 0;
        }
    }
}
