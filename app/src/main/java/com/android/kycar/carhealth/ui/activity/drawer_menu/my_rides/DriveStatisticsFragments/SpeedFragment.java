package com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides.DriveStatisticsFragments;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.entities.trip_transaction.TripScore;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides.DriveStatisticsActivity;
import com.android.kycar.carhealth.utils.BusProvider;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class SpeedFragment extends Fragment {

    private PieChart speedVitles;
    private PieChart rpmVitles;
    private boolean isDataAvailable = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        container = (ViewGroup) inflater.inflate(R.layout.speed_fragment_layout, null);
        final ViewGroup cnt = container;

        TripScore tripScore = ((DriveStatisticsActivity)getActivity()).getSensorData();

        speedVitles = (PieChart) container.findViewById(R.id.chart_speed_vitals);
        speedVitles.setUsePercentValues(true);
        speedVitles.setDescription("");
        speedVitles.animateY(2000);
        Drawable d = getResources().getDrawable(R.drawable.temperature);
        speedVitles.setValueTextColor(Color.BLACK);
        speedVitles.setBackgroundColor(Color.parseColor("#DEDADA"));
        speedVitles.setHoleRadius(20);
        speedVitles.setTransparentCircleRadius(35);
        speedVitles.setRotationAngle(0);
        speedVitles.setRotationEnabled(true);
        speedVitles.setValueTextSize(15);

        ArrayList<Entry> entries = new ArrayList<>();
        entries.add(new Entry(tripScore.getSpeedAwesome(), 2));
        entries.add(new Entry(tripScore.getSpeedHigh(), 1));
        entries.add(new Entry(tripScore.getSpeedHarsh(), 0));
        entries.add(new Entry(tripScore.getSpeedOk(), 3));

        /** Sample data */
        /*entries.add(new Entry(40, 3));
        entries.add(new Entry(30, 2));
        entries.add(new Entry(5, 0));
        entries.add(new Entry(15, 1));*/

        ArrayList<String> lables = new ArrayList<>();

        lables.add("Awesome");
        lables.add("High");
        lables.add("Harsh");
        lables.add("Ok");
        addPieData(speedVitles, entries, lables, "");

        rpmVitles  = (PieChart) container.findViewById(R.id.chart_rpm_vitals);
        rpmVitles.setUsePercentValues(true);
        rpmVitles.setDescription("");
        rpmVitles.setValueTextColor(Color.BLACK);
        rpmVitles.setBackgroundColor(Color.parseColor("#DEDADA"));
        rpmVitles.setHoleRadius(20);
        rpmVitles.setTransparentCircleRadius(35);
        rpmVitles.setRotationAngle(0);
        rpmVitles.setRotationEnabled(true);
        rpmVitles.animateY(2000);

        ArrayList<Entry> rpmEntries = new ArrayList<>();

        rpmEntries.add(new Entry(tripScore.getRpmHigh(), 1));
        rpmEntries.add(new Entry(tripScore.getRpmAwesome(), 2));
        rpmEntries.add(new Entry(tripScore.getRpmHarsh(), 0));
        rpmEntries.add(new Entry(tripScore.getRpmOk(), 3));

        if(tripScore.getRpmAwesome() ==0 && tripScore.getRpmOk() == 0 && tripScore.getRpmHigh() == 0 || tripScore.getRpmHarsh() == 0) {
            isDataAvailable = false;
        }

        /** Sample data */
        /*rpmEntries.add(new Entry(40, 1));
        rpmEntries.add(new Entry(25, 2));
        rpmEntries.add(new Entry(5, 0));
        rpmEntries.add(new Entry(30, 3));*/

        ArrayList<String> rpmLables = new ArrayList<>();
        rpmLables.add("Awesome");
        rpmLables.add("High");
        rpmLables.add("Harsh");
        rpmLables.add("Ok");
        addPieData(rpmVitles, rpmEntries, rpmLables, "");



        return container;
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    private void addPieData(PieChart engineVitals, ArrayList<Entry> entries, ArrayList<String> labls, String title) {

        PieDataSet pieDataSet = new PieDataSet(entries, title);
        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());
        pieDataSet.setColors(colors);
        PieData pieData = new PieData(labls, pieDataSet);
        engineVitals.setData(pieData);
    }

    @Override
    public void setUserVisibleHint(boolean visible){
        super.setUserVisibleHint(visible);

        if(!isDataAvailable){
            Toast.makeText(getContext(), "No data logged.", Toast.LENGTH_SHORT).show();
        }

        if (visible && isResumed()){
            speedVitles.animateXY(2000, 2000);
            rpmVitles.animateXY(2000, 2000);
        }
    }
}
