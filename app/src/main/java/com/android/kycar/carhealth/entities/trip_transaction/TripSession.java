package com.android.kycar.carhealth.entities.trip_transaction;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 6/30/2016.
 */
@RealmClass
public class TripSession extends RealmObject {

    @PrimaryKey
    private String id;

    private String tripId;

    private long startTime;

    private long endTime;

    private double startLat,startLong,stopLat,stopLong;

    private int obdDistance;

    private int gpsDistance;

    private int engineRpmMax = 0;

    private int speed = 0;

    private boolean isSync;

    public TripSession() {
    }

    public TripSession(int obdDistance, long endTime, int engineRpmMax, String id, int speed, double startLat, double startLong, long startTime, double stopLat, double stopLong, String tripId) {
        this.obdDistance = obdDistance;
        this.endTime = endTime;
        this.engineRpmMax = engineRpmMax;
        this.id = id;
        this.speed = speed;
        this.startLat = startLat;
        this.startLong = startLong;
        this.startTime = startTime;
        this.stopLat = stopLat;
        this.stopLong = stopLong;
        this.tripId = tripId;
    }

    public TripSession(TripSession tripSession) {
        if(tripSession == null){
            this.id = new Date().getTime()+"";
            return;
        }
        this.obdDistance = tripSession.getObdDistance();
        this.endTime = tripSession.getEndTime();
        this.engineRpmMax = tripSession.getEngineRpmMax();
        this.id = tripSession.getId();
        this.speed = tripSession.getSpeed();
        this.startLat = tripSession.getStartLat();
        this.startLong = tripSession.getStartLong();
        this.startTime = tripSession.getStartTime();
        this.stopLat = tripSession.getStopLat();
        this.stopLong = tripSession.getStopLong();
        this.tripId = tripSession.getTripId();
        this.isSync = tripSession.isSync();
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public int getObdDistance() {
        return obdDistance;
    }

    public void setObdDistance(int obdDistance) {
        this.obdDistance = obdDistance;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public int getEngineRpmMax() {
        return engineRpmMax;
    }

    public void setEngineRpmMax(int engineRpmMax) {
        this.engineRpmMax = engineRpmMax;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public double getStartLat() {
        return startLat;
    }

    public void setStartLat(double startLat) {
        this.startLat = startLat;
    }

    public double getStartLong() {
        return startLong;
    }

    public void setStartLong(double startLong) {
        this.startLong = startLong;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public double getStopLat() {
        return stopLat;
    }

    public void setStopLat(double stopLat) {
        this.stopLat = stopLat;
    }

    public double getStopLong() {
        return stopLong;
    }

    public void setStopLong(double stopLong) {
        this.stopLong = stopLong;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public int getGpsDistance() {
        return gpsDistance;
    }

    public void setGpsDistance(int gpsDistance) {
        this.gpsDistance = gpsDistance;
    }
}
