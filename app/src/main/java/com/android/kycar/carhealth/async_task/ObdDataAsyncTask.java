package com.android.kycar.carhealth.async_task;

import android.os.AsyncTask;


import com.android.kycar.carhealth.services.connection_services.rest_services.ObdDataService;

import org.json.JSONObject;

/**
 * Created by Krishna on 9/29/2016.
 */
public class ObdDataAsyncTask extends AsyncTask<JSONObject, Void, String> {

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(JSONObject... sensorData) {

        try {
            return new ObdDataService().sendRawObdData(sensorData[0]);
        } catch (Exception jsonException) {
            jsonException.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }
}
