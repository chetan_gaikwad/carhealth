package com.android.kycar.carhealth.services.dto.commands.protocol;


import com.android.kycar.carhealth.services.dto.commands.ObdCommand;

/**
 * Reset trouble codes.
 */
public class ResetTroubleCodesCommand extends ObdCommand {

    public ResetTroubleCodesCommand() {
        super("04");
    }

    @Override
    protected void performCalculations() {

    }

    @Override
    public String getFormattedResult() {
        return getResult();
    }

    @Override
    public String getCalculatedResult() {
        return getResult();
    }


    @Override
    public String getName() {
        return getResult();
    }

}
