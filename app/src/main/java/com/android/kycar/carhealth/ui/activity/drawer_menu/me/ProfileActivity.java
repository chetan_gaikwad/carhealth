package com.android.kycar.carhealth.ui.activity.drawer_menu.me;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.ProgessDialogBox;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.User;
import com.android.kycar.carhealth.entities.trip_transaction.Location;
import com.android.kycar.carhealth.repositories.trip.LocationRepo;
import com.android.kycar.carhealth.repositories.user.UserRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.UserRestService;
import com.android.kycar.carhealth.ui.activity.BaseActivity;
import com.android.kycar.carhealth.ui.activity.drawer_menu.sign_in.ActivityLogin;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class ProfileActivity extends BaseActivity {

    private static final int AVATAR_FROM_GALLERY = 1;
    private static int REQUEST_CODE_GALLERY = 101;
    private static int REQUEST_CODE_CAMERA = 102;
    private EditText etName, etEmailId, etMobileNumber, etLicense, etPan, etAadharCard;
    private Button btnAvatar, btnAadharCard, btnPan;
    private ImageView imgProfilePic;
    private Spinner spBloodGroup;
    private ArrayList<String> bloodGroupList;
    private final String[] imageCaptureOptions = {"Gallery", "Camera"};
    private ActionBar mActionBar;
    private UserRepo userRepo;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private ProgessDialogBox dialogBox;
    private EditText etSpeedLimit;

    private EditText etOnGoingMedications;
    private EditText etAccidentalHistory;
    private EditText etAllergies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userRepo = new UserRepo(getApplicationContext(), User.class);

        initUI();

        editor = preferences.edit();
        mActionBar = getSupportActionBar();

        bloodGroupList = new ArrayList<>();
        bloodGroupList.add("A+");
        bloodGroupList.add("A-");
        bloodGroupList.add("B+");
        bloodGroupList.add("B-");
        bloodGroupList.add("O+");
        bloodGroupList.add("O-");
        bloodGroupList.add("AB+");
        bloodGroupList.add("AB-");

        ArrayAdapter<String> bloodGroupAdapter = new ArrayAdapter<String>(this, R.layout.spinner_text, bloodGroupList);
        spBloodGroup.setAdapter(bloodGroupAdapter);

        btnAadharCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //userObject();
            }
        });

        imgProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setTitle("Add avatar");
                builder.setItems(imageCaptureOptions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (imageCaptureOptions[which].equalsIgnoreCase("Gallery")) {
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("image/*");
                            startActivityForResult(Intent.createChooser(intent, "Select Photo"), REQUEST_CODE_GALLERY);
                        } else if (imageCaptureOptions[which].equalsIgnoreCase("Camera")) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, REQUEST_CODE_CAMERA);
                        }
                    }
                });
                builder.show();
            }
        });

        if (preferences.getBoolean(Util.IS_LOGGED_IN, false)) {

            User user = (User) userRepo.findById(preferences.getString(Util.USER_PREFERENCE, ""));
            if (user != null) {
                etName.setText(user.getFirstName());
                etEmailId.setText(user.getEmailId());
                etMobileNumber.setText(user.getPhoneNumber());
                etLicense.setText(user.getLicenceNumber());
                etPan.setText(user.getPan());
                if (user.getProfilePicture() != null && user.getProfilePicture().length() > 0) {
                    imgProfilePic.setImageBitmap(BitmapFactory.decodeFile(user.getProfilePicture()));
                }
                if (user.getBloodGrp() != 0) {
                    spBloodGroup.setSelection(user.getBloodGrp());
                }
                if (user.getOnGoingMedications() != null && !user.getOnGoingMedications().isEmpty()) {
                    etOnGoingMedications.setText(user.getOnGoingMedications());
                }
                if (user.getAccidentalHistory() != null && !user.getAccidentalHistory().isEmpty()) {
                    etAccidentalHistory.setText(user.getAccidentalHistory());
                }
                if (user.getAllergies() != null && !user.getAllergies().isEmpty()) {
                    etAllergies.setText(user.getAllergies());
                }
            } else {
                new GetUserDataFromServer().execute("");
            }
        } else {
            Toast.makeText(ProfileActivity.this, "Please login first", Toast.LENGTH_SHORT).show();
            ProfileActivity.this.finish();
            startActivity(new Intent(ProfileActivity.this, ActivityLogin.class));
        }

        etSpeedLimit = (EditText) findViewById(R.id.sensitivity);
    }

    private void initUI() {

        etName = (EditText) findViewById(R.id.etname);
        etEmailId = (EditText) findViewById(R.id.etemailid);
        etMobileNumber = (EditText) findViewById(R.id.etmobilenumber);
        etLicense = (EditText) findViewById(R.id.etlicensenumber);
        etPan = (EditText) findViewById(R.id.etpan);
        etAadharCard = (EditText) findViewById(R.id.etaadhar);
        spBloodGroup = (Spinner) findViewById(R.id.sp_country);
        imgProfilePic = (ImageView) findViewById(R.id.img_profile_picture);
        btnAadharCard = (Button) findViewById(R.id.btnaadharcard);
        btnPan = (Button) findViewById(R.id.btnpan);
        dialogBox = new ProgessDialogBox(this);
        etAllergies = (EditText) findViewById(R.id.etAllergies);
        etOnGoingMedications = (EditText) findViewById(R.id.etOnGoingMedications);
        etAccidentalHistory = (EditText) findViewById(R.id.etAccidentalHistory);
    }

    public void onSaveProfileInfoClick(View view) {

        /** Saving to realm */
        User user = (User) userRepo.findById(preferences.getString(Util.USER_PREFERENCE, null));
        if (user == null) {
            user = new User();
            user.setId(Util.USER_PREFERENCE);
        }

        User updatedUser = new User();
        updatedUser.setId(user.getId());
        updatedUser.setFirstName(etName.getText().toString().trim());
        updatedUser.setAadharNumber(etAadharCard.getText().toString().trim());
        updatedUser.setBloodGrp(spBloodGroup.getSelectedItemPosition());
        updatedUser.setEmailId(etEmailId.getText().toString().trim());
        updatedUser.setPhoneNumber(etMobileNumber.getText().toString().trim());
        updatedUser.setProfilePicture(preferences.getString(Util.PROFILE_PREF, ""));
        updatedUser.setLicenceNumber(etLicense.getText().toString().trim());

        if (!etAllergies.getText().toString().trim().isEmpty()) {
            updatedUser.setAllergies(etAllergies.getText().toString().trim());
        }
        if (!etAccidentalHistory.getText().toString().trim().isEmpty()) {
            updatedUser.setAccidentalHistory(etAccidentalHistory.getText().toString().trim());
        }
        if (!etOnGoingMedications.getText().toString().trim().isEmpty()) {
            updatedUser.setOnGoingMedications(etOnGoingMedications.getText().toString().trim());
        }

        /** User rest call */
        new UpdateProfileRestCall().execute(updatedUser);

        /** Sending rest call for image update */
        /*Bitmap bmap = imgProfilePic.getDrawingCache();
        String imgString = getStringImage(bmap);
        new UpdateProfilePictureRestCall().execute(imgString);*/
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void onUploadProfilePic(View view) {

        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, AVATAR_FROM_GALLERY);
        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Util.PROFILE_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        Log.d("File Location is ", preferences.getString(Util.PROFILE_PREF, null));
        editor.putString("AVATAR", "Avatar");
        editor.commit();
    }

    private class GetUserDataFromServer extends AsyncTask<String, Void, String> {

        private ProgessDialogBox dialogBox;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogBox = new ProgessDialogBox(ProfileActivity.this);
            dialogBox.showDailog();
        }

        @Override
        protected String doInBackground(String... cred) {
            Log.d("LoginActivity", "Uploading " + cred.length + " readings..");
            try {
                String loginURL = BASE_URL +
                        "/user/find?id=" +
                        URLEncoder.encode(preferences.getString(Util.USER_PREFERENCE, ""), "UTF-8") + "";

                URL obj = new URL(loginURL);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                // optional default is GET
                con.setRequestMethod("GET");

                int responseCode = con.getResponseCode();
                System.out.println("\nSending 'GET' request to URL : " + loginURL);
                System.out.println("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                JSONObject jsonObject = new JSONObject(response.toString());
                String userID = jsonObject.getString("id");
                final String username = jsonObject.getString("name");
                final String email = jsonObject.getString("email");
                final String phoneNo = jsonObject.getString("phone_number");
                if (userID != null && !userID.isEmpty()) {
                    System.out.println(response.toString());
                    System.out.println("\n" + userID.toString());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            etName.setText(username);
                            etEmailId.setText(email);
                            etMobileNumber.setText(phoneNo);

                        }
                    });

                    return "success";
                }
                return "failure";
            } catch (java.io.FileNotFoundException e) {
                return "User not found";
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (dialogBox.isShowing()) {
                dialogBox.dismissDialog();
            }
            if (result != null) {
                if (result.equals("success")) {
                } else if (result.equals("failure")) {
                    Util.showAlert("failed to get data", ProfileActivity.this);
                } else {
                    Util.showAlert(result, ProfileActivity.this);
                }
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = getContentResolver().query(selectedImage,
                filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();

            /*NavigationView mNavigationView = (NavigationView) findViewById(R.id.shitstuff) ;
            View headerLayout = mNavigationView.inflateHeaderView(R.layout.navigation_header);

            ((TextView)headerLayout.findViewById(R.id.header_username)).setText(preferences.getString(Util.USERNAME_PREFERENCE,""));

            //LinearLayout profilePictureLayout = (LinearLayout) headerLayout.findViewById(R.id.profile_header);*/

        imgProfilePic.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        editor.putString(Util.PROFILE_PREF, picturePath);
        editor.commit();
    }

    private class UpdateProfileRestCall extends AsyncTask<User, Void, String> {

        UserRepo userRepo;

        @Override
        protected String doInBackground(User... users) {

            try {
                /** Sending rest call for user profile saving */
                if (Util.isOnline(getApplicationContext())) {
                    String response = new UserRestService().updateUserProfile(users[0]);
                }
                userRepo = new UserRepo(getApplicationContext(), User.class);
                userRepo.saveOrUpdate(users[0]);
                return "User Added Successfully";

            } catch (Exception e) {
                e.printStackTrace();
                return "User not found";
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogBox.showDailog();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (dialogBox.isShowing()) {
                dialogBox.dismissDialog();
            }
            if (result != null) {
                onBackPressed();
                userRepo.close();
            }
        }
    }

    private class UpdateProfilePictureRestCall extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {

            try {
                /** Sending rest call for user profile saving */
                new UserRestService().updateUserProfilePicture(strings[0], preferences.getString(Util.USER_PREFERENCE, null));
                return "Profile Picture Uploaded Successfully";

            } catch (Exception e) {
                e.printStackTrace();
                return "Profile Picture failed to upload";
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogBox.showDailog();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (dialogBox.isShowing()) {
                dialogBox.dismissDialog();
            }
            if (result != null) {
                Toast.makeText(ProfileActivity.this, "Profile Picture Uploaded Successfully", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        }
    }

    public void onClearLocationsClick(View view) {

        Log.d(ProfileActivity.class.getSimpleName(), "saved");
        LocationRepo locationRepo = new LocationRepo(getApplicationContext(), Location.class);
        locationRepo.deleteAll();
        Toast.makeText(getApplicationContext(), "Location cleared", Toast.LENGTH_SHORT).show();
    }
}