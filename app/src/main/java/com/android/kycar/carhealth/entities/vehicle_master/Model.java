package com.android.kycar.carhealth.entities.vehicle_master;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by krishna on 6/12/15.
 */
@RealmClass
public class Model extends RealmObject {

    @PrimaryKey
    private String id;
    
    private String makeId;

    private String name;

    public Model() {
    }

    public Model(String id, String makeId, String name) {
        this.id = id;
        this.makeId = makeId;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMakeId() {
        return makeId;
    }

    public void setMakeId(String makeId) {
        this.makeId = makeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
