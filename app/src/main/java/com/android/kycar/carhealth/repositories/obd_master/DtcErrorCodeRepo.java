package com.android.kycar.carhealth.repositories.obd_master;

import android.content.Context;

import com.android.kycar.carhealth.entities.obd_master.DtcErrorCode;
import com.android.kycar.carhealth.repositories.Repository;


/**
 * Created by Krishna on 8/26/2016.
 */
public class DtcErrorCodeRepo extends Repository {
    public DtcErrorCodeRepo(Context context, Class _aClass) {
        super(context, _aClass);
    }

    public DtcErrorCode findByDtc(String dtc) {
        return realm.where(DtcErrorCode.class).equalTo("dtc", dtc).findFirst();
    }
    public DtcErrorCode findByDiagnostic(String dtc) {
        return realm.where(DtcErrorCode.class).equalTo("diagnosis", dtc).findFirst();
    }
}
