package com.android.kycar.carhealth.services.reciever_services;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by 638 on 1/12/2017.
 */

public class AccelerometerDto implements Parcelable {
    private float x;
    private float y;
    private float z;

    public AccelerometerDto(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    protected AccelerometerDto(Parcel in) {
        x = in.readFloat();
        y = in.readFloat();
        z = in.readFloat();
    }

    public static final Creator<AccelerometerDto> CREATOR = new Creator<AccelerometerDto>() {
        @Override
        public AccelerometerDto createFromParcel(Parcel in) {
            return new AccelerometerDto(in);
        }

        @Override
        public AccelerometerDto[] newArray(int size) {
            return new AccelerometerDto[size];
        }
    };

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public JSONObject toJson() {
        JSONObject rawAccelerometerData = new JSONObject();
        try {
            rawAccelerometerData.put("x", this.getX());
            rawAccelerometerData.put("y", this.getY());
            rawAccelerometerData.put("z", this.getZ());
        } catch (JSONException e){
            e.printStackTrace();
        }
        return rawAccelerometerData;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(x);
        parcel.writeFloat(y);
        parcel.writeFloat(z);
    }
}
