package com.android.kycar.carhealth.services.sensor_services;

import android.content.Context;
import android.util.Log;

import com.android.kycar.carhealth.beans.Coordinates;
import com.android.kycar.carhealth.services.connection_services.mqtt_paho.PublishSensorData;
import com.android.kycar.carhealth.services.utils.Logger;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Krishna on 2/20/2016.
 */
public class SensorDataLoggerService {

    private Logger loggerAcc;

    public SensorDataLoggerService(Context context){

        loggerAcc = new Logger("Logger.csv",context);
        List<String> headers = new ArrayList<String>();
        headers.add("tripId");
        headers.add("timestamp");
        headers.add("loc");
        headers.add("speed");
        headers.add("gpsSpeed");
        headers.add("intensity");
        headers.add("eventType");
        headers.add("gpsBearing");
        headers.add("magBearing");
        headers.add("userId");
        headers.add("sensorId");
        loggerAcc.setHeader(headers);
    }

    public boolean stopSensorDataLoggerService(){

        loggerAcc.finalizeFile();
        Log.d(SensorManagerService.class.getSimpleName(), " File Created ");
        return true;
    }

    public void logAcc(String userId, String vehicleId, Coordinates coordinates, double intensity, String eventType, int speed, float gpsSpeed, float gpsBearing, int magBearing, String tripId, String sensorId){

        List<String > acc = new ArrayList<String>();

        acc.add(tripId);
        acc.add("" + new Date());
        try {
            acc.add(coordinates.toJSON().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        acc.add("" + speed);
        acc.add(vehicleId);
        acc.add("" + gpsSpeed);
        acc.add("" + intensity);
        acc.add(eventType);
        acc.add("" + gpsBearing);
        acc.add("" + magBearing);
        acc.add(userId);
        acc.add(sensorId);
        loggerAcc.logging(acc);
    }

    public void logAccSb(String userId, String vehicleId, Coordinates coordinates, double intensity, String eventType, int speed, float gpsSpeed, float gpsBearing, int magBearing, String tripId, String sensorId){

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(tripId+",");
        stringBuilder.append(new Date()+",");
        try {
            stringBuilder.append(coordinates.toJSON()+",");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        stringBuilder.append(speed + ",");
        stringBuilder.append(vehicleId + ",");
        stringBuilder.append(gpsSpeed + ",");
        stringBuilder.append(intensity + ",");
        stringBuilder.append(eventType + ",");
        stringBuilder.append(gpsBearing + ",");
        stringBuilder.append(magBearing + ",");
        stringBuilder.append(userId + ",");
        stringBuilder.append(sensorId);
        Log.d(SensorManagerService.class.getSimpleName(), "Sb = :- "+stringBuilder);
        loggerAcc.logging(stringBuilder.toString());
    }

    public void logAccSb(JSONObject sensorData) {

        loggerAcc.logging(sensorData.toString());
    }

    public MqttMessage getEventSensorData(String userId, String vehicleId, Coordinates coordinates, double intensity, String eventType, int speed, float gpsSpeed, float gpsBearing, int magBearing, String tripId, String sensorId){

        JSONObject payload = new JSONObject();
        try {
            payload.put("vehicleId", vehicleId);
            payload.put("data", "eventData");
            JSONObject eventData = new JSONObject();
            eventData.put("tripId", tripId );
            eventData.put("timestamp", new Date());
            eventData.put("loc", coordinates.toJSON());
            eventData.put("speed", speed);
            eventData.put("gpsSpeed", gpsSpeed);
            eventData.put("intensity", intensity);
            eventData.put("eventType", eventType);
            eventData.put("gpsBearing", gpsBearing);
            eventData.put("magBearing", magBearing);
            eventData.put("userId", userId);
            eventData.put("sensorName", sensorId);
            payload.put("eventData", eventData);
            Log.d(PublishSensorData.class.getSimpleName(), "Payload:-"+payload.toString());

            MqttMessage message = new MqttMessage(payload.toString().getBytes());
            return message;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
