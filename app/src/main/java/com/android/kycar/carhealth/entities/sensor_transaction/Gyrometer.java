package com.android.kycar.carhealth.entities.sensor_transaction;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by krishna on 8/12/15.
 */
@RealmClass
public class Gyrometer extends RealmObject {

    @PrimaryKey
    private String id;

    private long timestamp;

    private double gx;

    private double gy;

    private double gz;

    public long getTimestamp() {
        return timestamp;
    }

    public Gyrometer() {
    }

    public Gyrometer(String id, double gx, double gy, double gz) {
        this.id = id;
        this.timestamp = new Date().getTime();
        this.gx = gx;
        this.gy = gy;
        this.gz = gz;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public double getGx() {
        return gx;
    }

    public void setGx(double gx) {
        this.gx = gx;
    }

    public double getGy() {
        return gy;
    }

    public void setGy(double gy) {
        this.gy = gy;
    }

    public double getGz() {
        return gz;
    }

    public void setGz(double gz) {
        this.gz = gz;
    }
}
