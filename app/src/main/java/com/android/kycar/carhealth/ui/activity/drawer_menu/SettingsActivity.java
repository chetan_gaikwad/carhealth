package com.android.kycar.carhealth.ui.activity.drawer_menu;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.ConfigActivity;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.repositories.trip.TripRepo;
import com.android.kycar.carhealth.repositories.trip.TripScoreRepo;
import com.android.kycar.carhealth.repositories.trip.TripSessionRepo;
import com.android.kycar.carhealth.ui.activity.BaseActivity;
import com.android.kycar.carhealth.utils.DataCloudSync;
import com.android.kycar.carhealth.utils.ObdCloudSync;
import com.android.kycar.carhealth.utils.SensorEventCloudSync;

import java.util.ArrayList;
import java.util.Set;

import static com.android.kycar.carhealth.config.Util.MEDIATEK_PREFERENCE;


public class SettingsActivity extends BaseActivity implements SensorEventListener {

    private static final int REQUEST_CODE_OVERLAY = 2;
    private String btName = "btname";
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private Toast deviceShakeToast;
    private float maxAx, maxAy, maxAz;
    private TextView txtSensitivityCheck;
    private TextView tvSyncDataStatus;
    private TripRepo tripRepo;
    private TripScoreRepo tripScoreRepo;
    private TripSessionRepo tripSessionRepo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        if(preferences.getString(btName,null)!=null){
            ((TextView)findViewById(R.id.bluetooth_value))
                    .setText(preferences.getString(ConfigActivity.BLUETOOTH_LIST_KEY,"Not selected"));
        }

        if(preferences.getString(MEDIATEK_PREFERENCE,null)!=null){
            ((TextView)findViewById(R.id.mediatek_value))
                    .setText(preferences.getString(MEDIATEK_PREFERENCE,"Not selected"));
        }

        txtSensitivityCheck = (TextView) findViewById(R.id.txt_sensitivity_check);

        if(preferences.contains(Util.SENSOR_CALIBRATION_VALUE_X)) {
            txtSensitivityCheck.setText("Calibrate again");
        } else {
            txtSensitivityCheck.setText("Not calibrated yet");
        }

        tvSyncDataStatus = (TextView) findViewById(R.id.tv_sync_data_status);
        /*if(preferences.contains(Util.IS_UNSYNC_DATA)) {
            tvSyncDataStatus.setText("Sync data now");
        } else {
            tvSyncDataStatus.setText("Data is synced");
        }*/

        if(preferences.getBoolean(Util.CLOUD_UPLOAD,true)){
            ((TextView)findViewById(R.id.cloud_value))
                    .setText("Yes");
        }else{
            ((TextView)findViewById(R.id.cloud_value))
                    .setText("No");
        }

        if(preferences.getBoolean(ConfigActivity.IMPERIAL_UNITS_KEY, false)){
            ((TextView)findViewById(R.id.imperialUnits))
                    .setText("Yes");
        }else{
            ((TextView)findViewById(R.id.imperialUnits))
                    .setText("NO");
        }

        if(preferences.getBoolean(Util.LOCK_WIDGET_ENABLE, false)){
            ((TextView)findViewById(R.id.tv_lock_screen_widget_value))
                    .setText("Lock screen widget activated");
        }else{
            ((TextView)findViewById(R.id.tv_lock_screen_widget_value))
                    .setText("Lock screen widget deactivated");
        }

        maxAx = preferences.getFloat(Util.SENSOR_CALIBRATION_VALUE_X, 20);
        maxAy = preferences.getFloat(Util.SENSOR_CALIBRATION_VALUE_Y, 20);
        maxAz = preferences.getFloat(Util.SENSOR_CALIBRATION_VALUE_Z, 20);

    }

    public void selectDevice(View v){
        final ArrayList deviceStrs = new ArrayList();
        final ArrayList devices = new ArrayList();

        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if(!btAdapter.isEnabled()) {
            Toast.makeText(SettingsActivity.this,
                    "Bluetooth is disabled.",
                    Toast.LENGTH_LONG).show();
        }
        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
        if (pairedDevices.size() > 0)
        {
            for (BluetoothDevice device : pairedDevices)
            {
                deviceStrs.add(device.getName() + "\n" + device.getAddress());
                devices.add(device.getAddress());
            }
        }

        // show list
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(SettingsActivity.this);

        ArrayAdapter adapter = new ArrayAdapter(SettingsActivity.this, android.R.layout.select_dialog_item,
                deviceStrs.toArray(new String[deviceStrs.size()]));

        alertDialog.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                int position = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                String deviceAddress = (String) devices.get(position);
                ((TextView)findViewById(R.id.bluetooth_value)).setText(deviceStrs.get(position).toString());
                editor.putString(ConfigActivity.BLUETOOTH_LIST_KEY, deviceAddress).commit();
                editor.putString(btName, deviceStrs.get(position).toString()).commit();
                Toast.makeText(SettingsActivity.this, "Device selected", Toast.LENGTH_SHORT).show();
            }
        });

        alertDialog.setTitle("Choose Bluetooth device");
        alertDialog.show();
    }

    public void selectMediatekDevice(View v){
        final ArrayList deviceStrs = new ArrayList();
        final ArrayList devices = new ArrayList();

        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if(!btAdapter.isEnabled()) {
            Toast.makeText(SettingsActivity.this,
                    "Bluetooth is disabled.",
                    Toast.LENGTH_LONG).show();
        }
        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
        if (pairedDevices.size() > 0)
        {
            for (BluetoothDevice device : pairedDevices)
            {
                deviceStrs.add(device.getName() + "\n" + device.getAddress());
                devices.add(device.getAddress());
            }
        }

        // show list
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(SettingsActivity.this);

        ArrayAdapter adapter = new ArrayAdapter(SettingsActivity.this, android.R.layout.select_dialog_item,
                deviceStrs.toArray(new String[deviceStrs.size()]));

        alertDialog.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                int position = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                String deviceAddress = (String) devices.get(position);
                ((TextView)findViewById(R.id.mediatek_value)).setText(deviceStrs.get(position).toString());
                editor.putString(MEDIATEK_PREFERENCE, deviceAddress).commit();
                Toast.makeText(SettingsActivity.this, "Device selected", Toast.LENGTH_SHORT).show();
            }
        });

        alertDialog.setTitle("Choose Bluetooth device");
        alertDialog.show();
    }

    public void cloudSending(View view) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        editor.putBoolean(Util.CLOUD_UPLOAD,true).commit();
                        ((TextView)findViewById(R.id.cloud_value))
                                .setText("Yes");
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        editor.putBoolean(Util.CLOUD_UPLOAD,false).commit();
                        ((TextView)findViewById(R.id.cloud_value))
                                .setText("No");
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        builder.setMessage("Send data to cloud?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    public void imperialUnits(View view) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        editor.putBoolean(ConfigActivity.IMPERIAL_UNITS_KEY,true).commit();
                        ((TextView)findViewById(R.id.imperialUnits))
                                .setText("Yes");
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        editor.putBoolean(ConfigActivity.IMPERIAL_UNITS_KEY,false).commit();
                        ((TextView)findViewById(R.id.imperialUnits))
                                .setText("No");
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        builder.setMessage("Use imperial units").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    public void engVitalsUnits(View view) {
       // startActivity(new Intent(this, EngineVitalsActivity.class));
    }

    public void onSensorCalibrationClick(View view) {
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        //accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        //sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);

            Log.d("Range :-", ""+accelerometer.getMaximumRange());
            accelerometer.getMinDelay();

        } else {
            //
        }
        deviceShakeToast = Toast.makeText(getApplicationContext(), "Please shake your device hardly to calibrate sensors values", Toast.LENGTH_LONG);
        deviceShakeToast.show();
    }

    public void onSyncDataClick(View view) {
        Log.d(SettingsActivity.class.getSimpleName(), "On Sync data click");

        if(!Util.isOnline(getApplicationContext())) {
            Toast.makeText(getApplicationContext(),"Please check internet connection", Toast.LENGTH_SHORT).show();
            return;
        }

        boolean result = new DataCloudSync(getApplicationContext()).syncData();
        if(result){
            Toast.makeText(getApplicationContext(),"Data synced with cloud", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(),"Data syncing failed", Toast.LENGTH_SHORT).show();
        }

        boolean sensorEventResult = new SensorEventCloudSync(getApplicationContext()).syncData(preferences.getString(Util.CAR_PREFERENCE, ""));
        if(sensorEventResult) {
            Toast.makeText(getApplicationContext(),"Data synced with cloud", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(),"Data syncing failed", Toast.LENGTH_SHORT).show();
        }

        boolean obdCloudSyncResult = new ObdCloudSync().syncData(preferences.getString(Util.CAR_PREFERENCE, ""));
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            float ax = event.values[0];
            float ay = event.values[1];
            float az = event.values[2];

            Log.d(SettingsActivity.class.getSimpleName(), " Ax:"+ax+", Ay:"+ay+", Az:"+az);

            if(ax< maxAx) {
                maxAx = ax;
            }
            if(ay< maxAy) {
                maxAy = ay;
            }
            if(az< maxAz) {
                maxAz = az;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onDestroy() {

        Log.d(SettingsActivity.class.getSimpleName(), "Max: x:"+maxAx+" y:-"+maxAy+" z:-"+maxAz);
        preferences.edit().putFloat(Util.SENSOR_CALIBRATION_VALUE_X, maxAx);
        preferences.edit().putFloat(Util.SENSOR_CALIBRATION_VALUE_Y, maxAy);
        preferences.edit().putFloat(Util.SENSOR_CALIBRATION_VALUE_Z, maxAz);

        preferences.edit().commit();

        if(sensorManager != null) {
            sensorManager.unregisterListener(this, accelerometer);
        }
        if(deviceShakeToast != null) {
            deviceShakeToast.cancel();
        }
        super.onDestroy();
    }

    public void onLockScreenSettings(View view) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        editor.putBoolean(Util.LOCK_WIDGET_ENABLE,true).commit();
                        ((TextView)findViewById(R.id.tv_lock_screen_widget_value))
                                .setText("Lock screen widget activated");

                        showDrawOverApps();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        editor.putBoolean(Util.LOCK_WIDGET_ENABLE,false).commit();
                        ((TextView)findViewById(R.id.tv_lock_screen_widget_value))
                                .setText("Lock screen widget deactivated");
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        builder.setMessage("Activate lockscreen widget?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }


    public  void showDrawOverApps() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(SettingsActivity.this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, REQUEST_CODE_OVERLAY);
            }
        }
    }
}
