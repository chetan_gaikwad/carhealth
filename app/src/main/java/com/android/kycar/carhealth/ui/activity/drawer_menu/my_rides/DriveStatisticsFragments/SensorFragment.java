package com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides.DriveStatisticsFragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.entities.trip_transaction.TripScore;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides.DriveStatisticsActivity;
import com.android.kycar.carhealth.utils.BusProvider;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;


public class SensorFragment extends Fragment {

    private BarChart pedalBehaviorChart;
    private BarChart steeringBehaviorChart;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        container = (ViewGroup) inflater.inflate(R.layout.sensor_fragment_layout, null);
        final ViewGroup cnt = container;

        TripScore tripScore = ((DriveStatisticsActivity)getActivity()).getSensorData();

        pedalBehaviorChart = (BarChart) container.findViewById(R.id.chart_pedal);

        Log.d(SensorFragment.class.getSimpleName(), "Values :-"+tripScore.getSteeringIntensity());

        ArrayList<BarEntry> pedalBarEntries = new ArrayList<>();
        pedalBarEntries.add(new BarEntry(Math.abs((float) tripScore.getPedalAccFreq()),0));
        pedalBarEntries.add(new BarEntry(Math.abs((float) tripScore.getPedalBreakingFreq()), 1));
        pedalBarEntries.add(new BarEntry(Math.abs((float) tripScore.getPedalAccIntensity()),2));
        pedalBarEntries.add(new BarEntry(Math.abs((float) tripScore.getPedalBreakingIntensity()),3));

        /*pedalBarEntries.add(new BarEntry(Math.abs((float) 25),0));
        pedalBarEntries.add(new BarEntry(Math.abs((float) 35), 1));
        pedalBarEntries.add(new BarEntry(Math.abs((float) 65),2));
        pedalBarEntries.add(new BarEntry(Math.abs((float) 105),3));*/

        //barEntries.add(new BarEntry((float) tripScore.getSteeringFreq(), 5));

        BarDataSet barDataSet = new BarDataSet(pedalBarEntries, null);

        ArrayList<String> behaviours = new ArrayList<>();
        behaviours.add("Acc Frq");
        behaviours.add("Brake Frq");
        behaviours.add("Acc Int");
        behaviours.add("Brake Int");

        BarData barData = new BarData(behaviours, barDataSet);
        pedalBehaviorChart.setDescription("");
        pedalBehaviorChart.animateY(2000);
        pedalBehaviorChart.setData(barData);

        steeringBehaviorChart = (BarChart) container.findViewById(R.id.chart_steering);
        Log.d(SensorFragment.class.getSimpleName(), "Values :-"+tripScore.getSteeringIntensity());

        ArrayList<BarEntry> steeringBarEntries = new ArrayList<>();
        steeringBarEntries.add(new BarEntry(Math.abs((float) tripScore.getSteeringFreq()),0));
        steeringBarEntries.add(new BarEntry(Math.abs((float) tripScore.getSteeringIntensity()),1));

        /*steeringBarEntries.add(new BarEntry(Math.abs((float) 25),0));
        steeringBarEntries.add(new BarEntry(Math.abs((float) 32),1));*/

        BarDataSet steeringBarDataSet = new BarDataSet(steeringBarEntries, null);

        ArrayList<String> steeringTitles = new ArrayList<>();
        steeringTitles.add("Steering Frq");
        steeringTitles.add("Steering Int");

        BarData steeringBarData = new BarData(steeringTitles, steeringBarDataSet);
        steeringBehaviorChart.animateY(2000);
        steeringBehaviorChart.setData(steeringBarData);
        steeringBehaviorChart.setDescription("");

        return container;
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        steeringBehaviorChart.animateY(2000);
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    public void setUserVisibleHint(boolean visible){
        super.setUserVisibleHint(visible);
        if (visible && isResumed()){
            pedalBehaviorChart.animateXY(2000, 2000);
            steeringBehaviorChart.animateXY(2000, 2000);
        }
    }
}
