package com.android.kycar.carhealth.entities.trip_transaction;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 8/3/2016.
 */
@RealmClass
public class TripScore extends RealmObject {

    @PrimaryKey
    private String id;

    private String tripId;

    private String tripSessionId;

    private double pedalAccIntensity;

    private double pedalAccFreq;

    private double pedalBreakingIntensity;

    private double pedalBreakingFreq;

    private double steeringIntensity;

    private double steeringFreq;

    private long speedHarsh, speedHigh, speedOk, speedAwesome;

    private long rpmHarsh, rpmHigh, rpmOk, rpmAwesome;

    private long oilVeryHot, oilHot, oilOptimum, oilCold;

    private long coolantVeryHot, coolantHot, coolantOptimum, coolantCold;

    private boolean isSync;

    public TripScore() {
    }

    public TripScore(JSONObject tripScoreJson) {
        try {
            if (tripScoreJson.has("tripId")) { this.tripId = tripScoreJson.getString("tripId"); }
            if (tripScoreJson.has("id")) { this.id = tripScoreJson.getString("id"); }

            if (tripScoreJson.has("rpmOk")) { this.rpmOk = tripScoreJson.getLong("rpmOk"); }
            if (tripScoreJson.has("rpmHarsh")) { this.rpmHarsh = tripScoreJson.getLong("rpmHarsh"); }
            if (tripScoreJson.has("rpmHigh")) { this.rpmHigh = tripScoreJson.getLong("rpmHigh"); }
            if (tripScoreJson.has("rpmAwesome")) { this.rpmAwesome = tripScoreJson.getLong("rpmAwesome"); }

            if (tripScoreJson.has("oilVeryHot")) { this.oilVeryHot = tripScoreJson.getLong("oilVeryHot"); }
            if (tripScoreJson.has("oilHot")) { this.oilHot = tripScoreJson.getLong("oilHot"); }
            if (tripScoreJson.has("oilOptimum")) { this.oilOptimum = tripScoreJson.getLong("oilOptimum"); }
            if (tripScoreJson.has("oilCold")) { this.oilCold = tripScoreJson.getLong("oilCold"); }

            if (tripScoreJson.has("speedHarsh")) { this.speedHarsh = tripScoreJson.getLong("speedHarsh"); }
            if (tripScoreJson.has("speedHigh")) { this.speedHigh = tripScoreJson.getLong("speedHigh"); }
            if (tripScoreJson.has("speedOk")) { this.speedOk = tripScoreJson.getLong("speedOk"); }
            if (tripScoreJson.has("speedAwesome")) { this.speedAwesome = tripScoreJson.getLong("speedAwesome"); }

            if (tripScoreJson.has("coolantVeryHot")) { this.coolantVeryHot = tripScoreJson.getLong("coolantVeryHot"); }
            if (tripScoreJson.has("coolantHot")) { this.coolantHot = tripScoreJson.getLong("coolantHot"); }
            if (tripScoreJson.has("coolantOptimum")) { this.coolantOptimum = tripScoreJson.getLong("coolantOptimum"); }
            if (tripScoreJson.has("coolantCold")) { this.coolantCold = tripScoreJson.getLong("coolantCold"); }

            if (tripScoreJson.has("tripSessionId")) { this.tripSessionId = tripScoreJson.getString("tripSessionId"); }
            if (tripScoreJson.has("pedalAccIntensity")) { this.pedalAccIntensity = tripScoreJson.getDouble("pedalAccIntensity"); }
            if (tripScoreJson.has("pedalAccFreq")) { this.pedalAccFreq = tripScoreJson.getDouble("pedalAccFreq"); }
            if (tripScoreJson.has("pedalBreakingIntensity")) { this.pedalBreakingIntensity = tripScoreJson.getDouble("pedalBreakingIntensity"); }
            if (tripScoreJson.has("pedalBreakingFreq")) { this.pedalBreakingFreq = tripScoreJson.getDouble("pedalBreakingFreq"); }
            if (tripScoreJson.has("steeringIntensity")) { this.steeringIntensity = tripScoreJson.getDouble("steeringIntensity"); }
            if (tripScoreJson.has("steeringFreq")) { this.steeringFreq = tripScoreJson.getDouble("steeringFreq"); }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public TripScore(String id, double pedalAccFreq, double pedalAccIntensity, double pedalBreakingFreq, double pedalBreakingIntensity, double steeringFreq, double steeringIntensity, String tripId) {
        this.id = id;
        this.pedalAccFreq = pedalAccFreq;
        this.pedalAccIntensity = pedalAccIntensity;
        this.pedalBreakingFreq = pedalBreakingFreq;
        this.pedalBreakingIntensity = pedalBreakingIntensity;
        this.steeringFreq = steeringFreq;
        this.steeringIntensity = steeringIntensity;
        this.tripId = tripId;
    }

    public String getTripSessionId() {
        return tripSessionId;
    }

    public void setTripSessionId(String tripSessionId) {
        this.tripSessionId = tripSessionId;
    }

    public long getCoolantCold() {
        return coolantCold;
    }

    public void setCoolantCold(long coolantCold) {
        this.coolantCold = coolantCold;
    }

    public long getCoolantHot() {
        return coolantHot;
    }

    public void setCoolantHot(long coolantHot) {
        this.coolantHot = coolantHot;
    }

    public long getCoolantOptimum() {
        return coolantOptimum;
    }

    public void setCoolantOptimum(long coolantOptimum) {
        this.coolantOptimum = coolantOptimum;
    }

    public long getCoolantVeryHot() {
        return coolantVeryHot;
    }

    public void setCoolantVeryHot(long coolantVeryHot) {
        this.coolantVeryHot = coolantVeryHot;
    }

    public boolean isSync() {
        return isSync;
    }

    public long getOilCold() {
        return oilCold;
    }

    public void setOilCold(long oilCold) {
        this.oilCold = oilCold;
    }

    public long getOilHot() {
        return oilHot;
    }

    public void setOilHot(long oilHot) {
        this.oilHot = oilHot;
    }

    public long getOilOptimum() {
        return oilOptimum;
    }

    public void setOilOptimum(long oilOptimum) {
        this.oilOptimum = oilOptimum;
    }

    public long getOilVeryHot() {
        return oilVeryHot;
    }

    public void setOilVeryHot(long oilVeryHot) {
        this.oilVeryHot = oilVeryHot;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getRpmAwesome() {
        return rpmAwesome;
    }

    public void setRpmAwesome(long rpmAwesome) {
        this.rpmAwesome = rpmAwesome;
    }

    public long getRpmHarsh() {
        return rpmHarsh;
    }

    public void setRpmHarsh(long rpmHarsh) {
        this.rpmHarsh = rpmHarsh;
    }

    public long getRpmHigh() {
        return rpmHigh;
    }

    public void setRpmHigh(long rpmHigh) {
        this.rpmHigh = rpmHigh;
    }

    public long getRpmOk() {
        return rpmOk;
    }

    public void setRpmOk(long rpmOk) {
        this.rpmOk = rpmOk;
    }

    public long getSpeedAwesome() {
        return speedAwesome;
    }

    public void setSpeedAwesome(long speedAwesome) {
        this.speedAwesome = speedAwesome;
    }

    public long getSpeedHarsh() {
        return speedHarsh;
    }

    public void setSpeedHarsh(long speedHarsh) {
        this.speedHarsh = speedHarsh;
    }

    public long getSpeedHigh() {
        return speedHigh;
    }

    public void setSpeedHigh(long speedHigh) {
        this.speedHigh = speedHigh;
    }

    public long getSpeedOk() {
        return speedOk;
    }

    public void setSpeedOk(long speedOk) {
        this.speedOk = speedOk;
    }

    public double getPedalAccFreq() {
        return pedalAccFreq;
    }

    public void setPedalAccFreq(double pedalAccFreq) {
        this.pedalAccFreq = pedalAccFreq;
    }

    public double getPedalAccIntensity() {
        return pedalAccIntensity;
    }

    public void setPedalAccIntensity(double pedalAccIntensity) {
        this.pedalAccIntensity = pedalAccIntensity;
    }

    public double getPedalBreakingFreq() {
        return pedalBreakingFreq;
    }

    public void setPedalBreakingFreq(double pedalBreakingFreq) {
        this.pedalBreakingFreq = pedalBreakingFreq;
    }

    public double getPedalBreakingIntensity() {
        return pedalBreakingIntensity;
    }

    public void setPedalBreakingIntensity(double pedalBreakingIntensity) {
        this.pedalBreakingIntensity = pedalBreakingIntensity;
    }

    public double getSteeringFreq() {
        return steeringFreq;
    }

    public void setSteeringFreq(double steeringFreq) {
        this.steeringFreq = steeringFreq;
    }

    public double getSteeringIntensity() {
        return steeringIntensity;
    }

    public void setSteeringIntensity(double steeringIntensity) {
        this.steeringIntensity = steeringIntensity;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    /** This is special function to integrate all the tripscore of tripsessions */
    public TripScore(List<TripScore> tripScoreList) {

        for (TripScore tripScore: tripScoreList ) {

            this.pedalAccFreq += tripScore.getPedalBreakingFreq();
            this.pedalAccIntensity += tripScore.getPedalAccIntensity();
            this.pedalBreakingFreq += tripScore.getPedalBreakingFreq();
            this.pedalBreakingIntensity += tripScore.getPedalBreakingIntensity();

            this.steeringFreq += tripScore.getSteeringFreq();
            this.steeringIntensity += tripScore.getSteeringIntensity();

            this.coolantCold += tripScore.getCoolantCold();
            this.coolantHot += tripScore.getCoolantHot();
            this.coolantOptimum += tripScore.getCoolantOptimum();
            this.coolantVeryHot += tripScore.getCoolantVeryHot();

            this.oilCold += tripScore.getOilCold();
            this.oilHot += tripScore.getOilHot();
            this.oilOptimum += tripScore.getOilOptimum();
            this.oilVeryHot += tripScore.getOilVeryHot();

            this.rpmAwesome += tripScore.getRpmAwesome();
            this.rpmHarsh += tripScore.getRpmHarsh();
            this.rpmHigh += tripScore.getRpmHigh();
            this.rpmOk += tripScore.getRpmOk();

            this.speedAwesome += tripScore.getSpeedAwesome();
            this.speedHarsh += tripScore.getSpeedHarsh();
            this.speedHigh += tripScore.getSpeedHigh();
            this.speedOk += tripScore.getSpeedOk();

            Log.d(TripScore.class.getSimpleName(), "counter");
            this.tripId = tripScore.getId();
            this.isSync = tripScore.isSync();
        }
    }

    public TripScore(TripScore tripScore) {
        this.id = tripScore.getId();
        this.tripId = tripScore.getTripId();
        this.tripSessionId = tripScore.getTripSessionId();
        this.isSync = tripScore.isSync();

        this.speedOk = tripScore.getSpeedOk();
        this.speedHigh = tripScore.getSpeedHigh();
        this.speedAwesome = tripScore.getSpeedAwesome();
        this.speedHarsh = tripScore.getSpeedHarsh();

        this.rpmAwesome = tripScore.getRpmAwesome();
        this.rpmOk = tripScore.getRpmOk();
        this.rpmHigh = tripScore.getRpmHigh();
        this.rpmHarsh = tripScore.getRpmHarsh();

        this.oilOptimum = tripScore.getOilOptimum();
        this.oilVeryHot = tripScore.getOilVeryHot();
        this.oilHot = tripScore.getOilHot();
        this.oilCold = tripScore.getOilCold();

        this.coolantOptimum = tripScore.getCoolantOptimum();
        this.coolantVeryHot = tripScore.getOilVeryHot();
        this.coolantHot = tripScore.getCoolantHot();
        this.coolantCold = tripScore.getCoolantCold();

        this.pedalAccIntensity = tripScore.getPedalAccIntensity();
        this.pedalAccFreq = tripScore.getPedalAccFreq();
        this.pedalBreakingIntensity = tripScore.getPedalBreakingIntensity();
        this.pedalBreakingFreq = tripScore.getPedalBreakingFreq();
        this.steeringIntensity = tripScore.getSteeringIntensity();
        this.steeringFreq = tripScore.getSteeringFreq();
    }
}
