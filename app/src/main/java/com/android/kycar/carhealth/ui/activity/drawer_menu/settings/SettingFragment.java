package com.android.kycar.carhealth.ui.activity.drawer_menu.settings;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.preference.ListPreference;
import androidx.preference.PreferenceFragmentCompat;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.Util;

import java.util.ArrayList;
import java.util.Set;


public class SettingFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener{

    private static final int REQUEST_CODE_OVERLAY = 2;
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.prefs);

        conifgureObd();
        conifgureMediatek();

    }

    private void conifgureObd() {
        final BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        ArrayList<CharSequence> pairedDeviceStrings = new ArrayList<>();
        ArrayList<CharSequence> vals = new ArrayList<>();

        ListPreference listPreferenceCategory = (ListPreference) findPreference("bluetooth_list_preference");
        if(!mBtAdapter.isEnabled()) {
            Toast.makeText(getActivity(),
                    "Bluetooth is disabled.",
                    Toast.LENGTH_LONG).show();
        } else {
            if (listPreferenceCategory != null) {
                Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
                if (pairedDevices.size() > 0) {
                    for (BluetoothDevice device : pairedDevices) {
                        pairedDeviceStrings.add(device.getName() + "\n" + device.getAddress());
                        vals.add(device.getAddress());
                    }
                }
                listPreferenceCategory.setEntries(pairedDeviceStrings.toArray(new CharSequence[0]));
                listPreferenceCategory.setEntryValues(vals.toArray(new CharSequence[0]));
            }
        }
    }

    private void conifgureMediatek() {
        final BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        ArrayList<CharSequence> pairedDeviceStrings = new ArrayList<>();
        ArrayList<CharSequence> vals = new ArrayList<>();

        ListPreference listPreferenceCategory = (ListPreference) findPreference(Util.MEDIATEK_PREFERENCE);
        if(!mBtAdapter.isEnabled()) {
            Toast.makeText(getActivity(),
                    "Bluetooth is disabled.",
                    Toast.LENGTH_LONG).show();
        } else {
            if (listPreferenceCategory != null) {
                Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
                if (pairedDevices.size() > 0) {
                    for (BluetoothDevice device : pairedDevices) {
                        pairedDeviceStrings.add(device.getName() + "\n" + device.getAddress());
                        vals.add(device.getAddress());
                    }
                }
                listPreferenceCategory.setEntries(pairedDeviceStrings.toArray(new CharSequence[0]));
                listPreferenceCategory.setEntryValues(vals.toArray(new CharSequence[0]));
            }
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        //unregister the preferenceChange listener
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.i("vfdfv","lock_widget fe");
        if(key.equals(Util.LOCK_WIDGET_ENABLE)){
            if(sharedPreferences.getBoolean(Util.LOCK_WIDGET_ENABLE, false)){
                showDrawOverApps();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //unregister the preference change listener
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    public  void showDrawOverApps() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(getActivity())) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getActivity().getPackageName()));
                startActivityForResult(intent, REQUEST_CODE_OVERLAY);
            }
        }
    }
}
