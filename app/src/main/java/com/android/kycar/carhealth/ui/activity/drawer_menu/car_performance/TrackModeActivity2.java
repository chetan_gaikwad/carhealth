package com.android.kycar.carhealth.ui.activity.drawer_menu.car_performance;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.kycar.carhealth.NxtApplication;
import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.beans.Gear;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.Vehicle;
import com.android.kycar.carhealth.repositories.vehicle.VehicleRepo;
import com.android.kycar.carhealth.services.dto.commands.SpeedCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.RPMCommand;
import com.android.kycar.carhealth.services.dto.enums.AvailableCommandNames;
import com.android.kycar.carhealth.services.dto.utils.RPMTest;
import com.android.kycar.carhealth.services.dto.utils.SpeedTest;
import com.android.kycar.carhealth.services.obd_services.ObdCommandJob;
import com.android.kycar.carhealth.services.obd_services.ObdDataLoggerService;
import com.android.kycar.carhealth.services.obd_services.ObdProgressListener;
import com.android.kycar.carhealth.ui.activity.BaseActivity;
import com.android.kycar.carhealth.utils.BusProvider;
import com.android.kycar.carhealth.utils.FontCache;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TrackModeActivity2 extends BaseActivity implements ObdProgressListener {

    private final String LOG = TrackModeActivity2.class.getSimpleName();
    private TextView textTimer;
    private TextView txtSpeedo;
    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton;
    private ArrayList<SpeedTest> tripSpeedInfo;
    private ArrayList<RPMTest> tripRPMInfo;
    private Map<String, String> commandResult = new HashMap<String, String>();
    private final String KEY_RPM = "rpm";
    private final String KEY_SPEED = "SPEED";
    private Map<Integer, String> dataSheets = new HashMap<>();
    private Map<Integer, String> locationDataSheets = new HashMap<>();
    private Map<Integer, String> deAccelerationDataSheets = new HashMap<>();
    private boolean accelerationFlag = true;
    private StringBuffer result = new StringBuffer();
    private ObdDataLoggerService logger;

    private boolean startTimer = false;
    private boolean flag0 = true;
    private boolean flag60 = true;
    private boolean flag90 = true;
    private boolean flag100 = true;
    private boolean flag120 = true;
    private boolean flag150 = true;
    private boolean deAccelerationFlag = true;

    private long startTime = 0L;
    private Handler customHandler = new Handler();
    private long timeInMilliseconds = 0L;
    private long timeSwapBuff = 0L;
    private long updatedTime = 0L;
    private Gear gear;
    private String lat, longi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logger = new ObdDataLoggerService(this, "s");

        setContentView(R.layout.activity_track_mode2);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        try {
            BusProvider.getInstance().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tripSpeedInfo = new ArrayList<>();
        tripRPMInfo = new ArrayList<>();

        textTimer = (TextView) findViewById(R.id.textTimer);
        txtSpeedo = (TextView) findViewById(R.id.txtSpeedo);
        ((TextView) findViewById(R.id.txtSpeed)).setTypeface(FontCache.get("", this));

        ((TextView) findViewById(R.id.oto60)).setTypeface(FontCache.get("", this));
        ((TextView) findViewById(R.id.oto100)).setTypeface(FontCache.get("", this));
        ((TextView) findViewById(R.id.oto120)).setTypeface(FontCache.get("", this));
        ((TextView) findViewById(R.id.oto150)).setTypeface(FontCache.get("", this));


        txtSpeedo.setTypeface(FontCache.get("", this));
        textTimer.setTypeface(FontCache.get("", this));
        textTimer.setText("00:00:000");

        VehicleRepo vehicleRepo;
        vehicleRepo = new VehicleRepo(this, Vehicle.class);

        Vehicle vehicle = (Vehicle) vehicleRepo.findById(preferences.getString(Util.CAR_PREFERENCE, ""));
        vehicleRepo.close();

        if (vehicle != null) {
            /** By default nano */
            //gear = new Gear(this, "182");
        } else {
            //gear = new Gear(this, "182");
        }

        addListenerOnButton();
    }

    public void startPerformanceTest(View view) {

        textTimer.setText("00:00:000");
        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread, 0);
        //dummyData();
    }

    public void reset(View v) {



        customHandler.removeCallbacks(updateTimerThread);
        if (locationDataSheets.get(60) != null) {
            JSONObject jsonObject = new JSONObject();
            try {
                JSONObject jsonObject60 = new JSONObject();

                jsonObject60.put("time", findDifference(dataSheets.get(0), dataSheets.get(60)) + " sec");
                jsonObject60.put("lat", locationDataSheets.get(60).split("&")[0]);
                jsonObject60.put("longi", locationDataSheets.get(60).split("&")[1]);

                jsonObject.put("0-60", jsonObject60);

                logger.logDataTrack(jsonObject.toString());
                logger.stopSensorDataLoggerService();

                NxtApplication.sendDate(jsonObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        /*flag150 = true;
        flag0 = true;
        flag100 = true;
        flag120 = true;
        flag60 = true;
        flag90 = true;
        timeTaken.setText("");
        result = new StringBuffer();*/
    }

    private void dummyData() {
        tripSpeedInfo = new ArrayList<>();
        for (int i = 0; i < 155; i++) {
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Log.i("performance ", "Time - " + Util.getTimeMicro() + " Speed - " + i);
            if (i >= 150) {
                if (flag150) {
                    flag150 = false;
                    dataSheets.put(150, Util.getTimeMicro());

                    result.append("\n120 to 150 -> " + findDifference(dataSheets.get(0), dataSheets.get(150)) + " sec");

                    TrackModeActivity2.this.runOnUiThread(new Runnable() {
                        public void run() {
                            //timeTaken.setText(result + "\n");
                            ((TextView) findViewById(R.id.oto150value)).setTypeface(FontCache.get("", TrackModeActivity2.this));
                            ((TextView) findViewById(R.id.oto150value)).setText(findDifference(dataSheets.get(0), dataSheets.get(150)) + "");
                        }

                    });
                }
            } else if (i >= 120) {
                if (flag120) {
                    flag120 = false;
                    dataSheets.put(120, Util.getTimeMicro());
                    result.append("\n90 to 120 -> " + findDifference(dataSheets.get(90), dataSheets.get(120)) + " sec");
                    TrackModeActivity2.this.runOnUiThread(new Runnable() {
                        public void run() {
                            //timeTaken.setText(result + "\n");
                            ((TextView) findViewById(R.id.oto120)).setTypeface(FontCache.get("", TrackModeActivity2.this));
                            ((TextView) findViewById(R.id.oto120Value)).setText(findDifference(dataSheets.get(0), dataSheets.get(120)) + "");
                        }

                    });
                }
            } else if (i >= 100) {
                if (flag100) {
                    flag100 = false;
                    dataSheets.put(100, Util.getTimeMicro());
                    result.append("\n0 to 100 -> " + findDifference(dataSheets.get(0), dataSheets.get(100)) + " sec");
                    TrackModeActivity2.this.runOnUiThread(new Runnable() {
                        public void run() {
                            //timeTaken.setText(result + "\n");
                            ((TextView) findViewById(R.id.oto100)).setTypeface(FontCache.get("", TrackModeActivity2.this));
                            ((TextView) findViewById(R.id.oto100value)).setText(findDifference(dataSheets.get(0), dataSheets.get(100)) + "");
                        }

                    });
                }
            } else if (i >= 90) {
                if (flag90) {
                    flag90 = false;
                    dataSheets.put(90, Util.getTimeMicro());
                    result.append("\n60 to 90 -> " + findDifference(dataSheets.get(60), dataSheets.get(90)) + " sec");
                    TrackModeActivity2.this.runOnUiThread(new Runnable() {
                        public void run() {
                            //timeTaken.setText(result + "\n");
                        }

                    });
                }
            } else if (i >= 60) {
                if (flag60) {
                    flag60 = false;
                    dataSheets.put(60, Util.getTimeMicro());
                    result.append("\n0 to 60 -> " + findDifference(dataSheets.get(0), dataSheets.get(60)) + " sec");
                    TrackModeActivity2.this.runOnUiThread(new Runnable() {
                        public void run() {
                            //timeTaken.setText(result + "\n");
                            ((TextView) findViewById(R.id.oto60)).setTypeface(FontCache.get("", TrackModeActivity2.this));
                            ((TextView) findViewById(R.id.oto60Value)).setText(findDifference(dataSheets.get(0), dataSheets.get(60)) + "");
                        }

                    });
                }
            } else if (i > 0) {
                if (flag0) {
                    flag0 = false;
                    dataSheets.put(0, Util.getTimeMicro());
                }
            }
        }
        //performCalculations();
    }

    @Subscribe
    public void stateUpdate(ObdCommandJob job) {
        final String cmdName = job.getCommand().getName();
        String cmdResult = "";
        final String cmdID = LookUpCommand(cmdName);

        if (job.getState().equals(ObdCommandJob.ObdCommandJobState.EXECUTION_ERROR)) {
            cmdResult = job.getCommand().getResult();
            if (cmdResult != null) {
            }
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.NOT_SUPPORTED)) {
            cmdResult = getString(R.string.status_obd_no_support);
        } else {
            cmdResult = job.getCommand().getFormattedResult();
        }

        updateTripStatistic(job, cmdID);
    }

    private void updateTripStatistic(final ObdCommandJob job, final String cmdID) {

        if (cmdID.equals(AvailableCommandNames.SPEED.toString())) {

            final SpeedCommand command = (SpeedCommand) job.getCommand();
            Log.d("Cartest", "Speed " + command.getMetricSpeed());

            TrackModeActivity2.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txtSpeedo.setText(command.getMetricSpeed() + "");
                }
            });


            SpeedTest speedTest = new SpeedTest();
            speedTest.setSpeed(command.getMetricSpeed());
            speedTest.setTimeStamp(Util.getTimeMicro());
            tripSpeedInfo.add(speedTest);

            commandResult.put(KEY_SPEED, (command.getMetricSpeed() * (1000 / 60)) + "");

            Log.i("performance ", "Time - " + Util.getTimeMicro() + " Speed - " + command.getMetricSpeed());
            if (accelerationFlag) {
                if (command.getMetricSpeed() >= 150) {
                    if (flag150) {
                        flag150 = false;
                        dataSheets.put(150, Util.getTimeMicro());
                        result.append("\n120 to 150 >>> " + findDifference(dataSheets.get(120), dataSheets.get(150)) + " sec");

                        TrackModeActivity2.this.runOnUiThread(new Runnable() {
                            public void run() {

                                //timeTaken.setText(result + "\n");
                                ((TextView) findViewById(R.id.oto150value)).setTypeface(FontCache.get("", TrackModeActivity2.this));
                                ((TextView) findViewById(R.id.oto150value)).setText(findDifference(dataSheets.get(0), dataSheets.get(150)) + "");

                            }

                        });
                    }
                } else if (command.getMetricSpeed() >= 120) {
                    if (flag120) {
                        flag120 = false;
                        dataSheets.put(120, Util.getTimeMicro());
                        result.append("\n90 to 120 >>> " + findDifference(dataSheets.get(90), dataSheets.get(120)) + " sec");
                        TrackModeActivity2.this.runOnUiThread(new Runnable() {
                            public void run() {

                                //timeTaken.setText(result + "\n");
                                ((TextView) findViewById(R.id.oto120)).setTypeface(FontCache.get("", TrackModeActivity2.this));
                                ((TextView) findViewById(R.id.oto120Value)).setText(findDifference(dataSheets.get(0), dataSheets.get(120)) + "");

                            }

                        });
                    }
                } else if (command.getMetricSpeed() >= 100) {
                    if (flag100) {
                        flag100 = false;
                        dataSheets.put(100, Util.getTimeMicro());
                        locationDataSheets.put(100, lat+"&"+longi);
                        result.append("\n0 to 100 >>> " + findDifference(dataSheets.get(0), dataSheets.get(100)) + " sec");
                        TrackModeActivity2.this.runOnUiThread(new Runnable() {
                            public void run() {

                                //timeTaken.setText(result + "\n");

                                ((TextView) findViewById(R.id.oto100)).setTypeface(FontCache.get("", TrackModeActivity2.this));
                                ((TextView) findViewById(R.id.oto100value)).setText(findDifference(dataSheets.get(0), dataSheets.get(100)) + "");


                            }

                        });
                    }
                } else if (command.getMetricSpeed() >= 90) {
                    if (flag90) {
                        flag90 = false;
                        dataSheets.put(90, Util.getTimeMicro());
                        locationDataSheets.put(90, lat+"&"+longi);
                        result.append("\n60 to 90 >>> " + findDifference(dataSheets.get(60), dataSheets.get(90)) + " sec");
                        TrackModeActivity2.this.runOnUiThread(new Runnable() {
                            public void run() {

                                //timeTaken.setText(result + "\n");

                            }

                        });
                    }
                } else if (command.getMetricSpeed() >= 60) {

                    if (flag60) {

                        flag60 = false;
                        dataSheets.put(60, Util.getTimeMicro());
                        locationDataSheets.put(60, lat+"&"+longi);
                        result.append("\n0 to 60 >>> " + findDifference(dataSheets.get(0), dataSheets.get(60)) + " sec");
                        Log.i("ff", "\n0 to 60 >>> " + findDifference(dataSheets.get(0), dataSheets.get(60)) + " sec");
                        TrackModeActivity2.this.runOnUiThread(new Runnable() {
                            public void run() {

                                //timeTaken.setText(result + "\n");
                                ((TextView) findViewById(R.id.oto60)).setTypeface(FontCache.get("", TrackModeActivity2.this));
                                ((TextView) findViewById(R.id.oto60Value)).setText(findDifference(dataSheets.get(0), dataSheets.get(60)) + "");

                            }

                        });
                    }
                } else if (command.getMetricSpeed() > 0) {

                    if (!startTimer) {
                        startTimer = true;
                    }
                    if (flag0) {
                        flag0 = false;

                        dataSheets.put(0, Util.getTimeMicro());
                    }
                }
            } else {
                if (command.getMetricSpeed() == 0) {
                    if (deAccelerationFlag) {
                        deAccelerationFlag = false;
                        Log.d(LOG, "Tracking 0");
                        deAccelerationDataSheets.put(0, Util.getTimeMicro());
                        if (deAccelerationDataSheets.get(60) != null) {

                            Log.d(LOG, "Calculating time difference : " + deAccelerationDataSheets.get(60) + " " +
                                    "" + deAccelerationDataSheets.get(0));
                            TrackModeActivity2.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    ((TextView) findViewById(R.id.oto60)).setTypeface(FontCache.get("", TrackModeActivity2.this));
                                    ((TextView) findViewById(R.id.oto60Value)).setText(findDifference(deAccelerationDataSheets.get(60), deAccelerationDataSheets.get(0)) + "");

                                }

                            });
                        }
                    }
                }
                if (command.getMetricSpeed() > 60) {
                    Log.d(LOG, "Tracking 60");
                    deAccelerationDataSheets.put(60, Util.getTimeMicro());
                }
            }
        } else if (cmdID.equals(AvailableCommandNames.ENGINE_RPM.toString())) {
            final RPMCommand command = (RPMCommand) job.getCommand();
            commandResult.put(KEY_RPM, command.getRPM() + "");

            RPMTest rpmTest = new RPMTest();
            Log.d("Cartest", "rpm " + command.getRPM());

            rpmTest.setRpm(command.getRPM());
            rpmTest.setTimeStamp(Util.getTimeMicro());
            tripRPMInfo.add(rpmTest);
            //gearDataSheets.put(RPM_CONST, command.getRPM());
        }

    }

    private double findDifference(String s, String s1) {
        Date date1, date2;

        try {

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss.SSS");
            date1 = simpleDateFormat.parse(s);
            date2 = simpleDateFormat.parse(s1);

            long difference = date2.getTime() - date1.getTime();
            double diffInSec = (double) difference / 1000;
            return diffInSec;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String LookUpCommand(String txt) {
        for (AvailableCommandNames item : AvailableCommandNames.values()) {
            if (item.getValue().equals(txt)) return item.name();
        }
        return txt;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            BusProvider.getInstance().unregister(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Runnable updateTimerThread = new Runnable() {

        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;
            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            secs = secs % 60;
            int milliseconds = (int) (updatedTime % 1000);
            textTimer.setText("" + mins + ":"
                    + String.format("%02d", secs) + ":"
                    + String.format("%03d", milliseconds));
            customHandler.postDelayed(this, 0);
        }
    };

    @Subscribe
    public void locationUpdate(Location location) {

        lat = location.getLatitude() + "";
        longi = location.getLongitude() + "";
    }

    public void addListenerOnButton() {

        radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        radioSexGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.radio_track_accelerate) {
                    ((TextView) findViewById(R.id.oto60)).setTypeface(FontCache.get("", TrackModeActivity2.this));
                    ((TextView) findViewById(R.id.oto60)).setText("0 to 60");

                    ((LinearLayout) findViewById(R.id.ll1)).setVisibility(View.VISIBLE);
                    ((LinearLayout) findViewById(R.id.ll2)).setVisibility(View.VISIBLE);
                    ((TextView) findViewById(R.id.oto120)).setVisibility(View.VISIBLE);
                    ((TextView) findViewById(R.id.oto120Value)).setVisibility(View.VISIBLE);

                    accelerationFlag = true;
                } else {
                    ((TextView) findViewById(R.id.oto60)).setTypeface(FontCache.get("", TrackModeActivity2.this));
                    ((TextView) findViewById(R.id.oto60)).setText("60 to 0");

                    ((LinearLayout) findViewById(R.id.ll1)).setVisibility(View.GONE);
                    ((LinearLayout) findViewById(R.id.ll2)).setVisibility(View.GONE);
                    ((TextView) findViewById(R.id.oto120)).setVisibility(View.GONE);
                    ((TextView) findViewById(R.id.oto120Value)).setVisibility(View.GONE);
                    accelerationFlag = false;
                }
            }
        });

    }
}


