package com.android.kycar.carhealth.entities.vehicle_master;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Krishna on 10/28/2016.
 */
public class EngineVitals extends RealmObject {

    @PrimaryKey
    private String id;

    private String variantId;

    private String userId;

    private int coolantTempMin, coolantTempCold, coolantTempOptimum, coolantTempHot, coolantTempVeryHot, coolantTempWarning;

    private int oilTempMin, oilTempCold, oilTempOptimum, oilTempHot, oilTempVeryHot, oilTempWarning;

    private int speedMin, speedOk, speedAwesome, speedHigh, speedHarsh, seedWarning;

    private int rpmMin, rpmOk, rpmAwesome, rpmHigh,  rpmHarsh, rpmWarning;

    public EngineVitals() {
    }

    public EngineVitals(int coolantTempCold, int coolantTempHot, int coolantTempMin, int coolantTempOptimum, int coolantTempVeryHot, int coolantTempWarning, String id, int oilTempCold, int oilTempHot, int oilTempMin, int oilTempOptimum, int oilTempVeryHot, int oilTempWarning, int rpmAwesome, int rpmHarsh, int rpmHigh, int rpmMin, int rpmOk, int rpmWarning, int seedWarning, int speedAwesome, int speedHarsh, int speedHigh, int speedMin, int speedOk, String userId, String variantId) {
        this.coolantTempCold = coolantTempCold;
        this.coolantTempHot = coolantTempHot;
        this.coolantTempMin = coolantTempMin;
        this.coolantTempOptimum = coolantTempOptimum;
        this.coolantTempVeryHot = coolantTempVeryHot;
        this.coolantTempWarning = coolantTempWarning;
        this.id = id;
        this.oilTempCold = oilTempCold;
        this.oilTempHot = oilTempHot;
        this.oilTempMin = oilTempMin;
        this.oilTempOptimum = oilTempOptimum;
        this.oilTempVeryHot = oilTempVeryHot;
        this.oilTempWarning = oilTempWarning;
        this.rpmAwesome = rpmAwesome;
        this.rpmHarsh = rpmHarsh;
        this.rpmHigh = rpmHigh;
        this.rpmMin = rpmMin;
        this.rpmOk = rpmOk;
        this.rpmWarning = rpmWarning;
        this.seedWarning = seedWarning;
        this.speedAwesome = speedAwesome;
        this.speedHarsh = speedHarsh;
        this.speedHigh = speedHigh;
        this.speedMin = speedMin;
        this.speedOk = speedOk;
        this.userId = userId;
        this.variantId = variantId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getVariantId() {
        return variantId;
    }

    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

    public int getCoolantTempCold() {
        return coolantTempCold;
    }

    public void setCoolantTempCold(int coolantTempCold) {
        this.coolantTempCold = coolantTempCold;
    }

    public int getCoolantTempHot() {
        return coolantTempHot;
    }

    public void setCoolantTempHot(int coolantTempHot) {
        this.coolantTempHot = coolantTempHot;
    }

    public int getCoolantTempMin() {
        return coolantTempMin;
    }

    public void setCoolantTempMin(int coolantTempMin) {
        this.coolantTempMin = coolantTempMin;
    }

    public int getCoolantTempOptimum() {
        return coolantTempOptimum;
    }

    public void setCoolantTempOptimum(int coolantTempOptimum) {
        this.coolantTempOptimum = coolantTempOptimum;
    }

    public int getCoolantTempVeryHot() {
        return coolantTempVeryHot;
    }

    public void setCoolantTempVeryHot(int coolantTempVeryHot) {
        this.coolantTempVeryHot = coolantTempVeryHot;
    }

    public int getCoolantTempWarning() {
        return coolantTempWarning;
    }

    public void setCoolantTempWarning(int coolantTempWarning) {
        this.coolantTempWarning = coolantTempWarning;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getOilTempCold() {
        return oilTempCold;
    }

    public void setOilTempCold(int oilTempCold) {
        this.oilTempCold = oilTempCold;
    }

    public int getOilTempHot() {
        return oilTempHot;
    }

    public void setOilTempHot(int oilTempHot) {
        this.oilTempHot = oilTempHot;
    }

    public int getOilTempMin() {
        return oilTempMin;
    }

    public void setOilTempMin(int oilTempMin) {
        this.oilTempMin = oilTempMin;
    }

    public int getOilTempOptimum() {
        return oilTempOptimum;
    }

    public void setOilTempOptimum(int oilTempOptimum) {
        this.oilTempOptimum = oilTempOptimum;
    }

    public int getOilTempVeryHot() {
        return oilTempVeryHot;
    }

    public void setOilTempVeryHot(int oilTempVeryHot) {
        this.oilTempVeryHot = oilTempVeryHot;
    }

    public int getOilTempWarning() {
        return oilTempWarning;
    }

    public void setOilTempWarning(int oilTempWarning) {
        this.oilTempWarning = oilTempWarning;
    }

    public int getRpmAwesome() {
        return rpmAwesome;
    }

    public void setRpmAwesome(int rpmAwesome) {
        this.rpmAwesome = rpmAwesome;
    }

    public int getRpmHarsh() {
        return rpmHarsh;
    }

    public void setRpmHarsh(int rpmHarsh) {
        this.rpmHarsh = rpmHarsh;
    }

    public int getRpmHigh() {
        return rpmHigh;
    }

    public void setRpmHigh(int rpmHigh) {
        this.rpmHigh = rpmHigh;
    }

    public int getRpmMin() {
        return rpmMin;
    }

    public void setRpmMin(int rpmMin) {
        this.rpmMin = rpmMin;
    }

    public int getRpmOk() {
        return rpmOk;
    }

    public void setRpmOk(int rpmOk) {
        this.rpmOk = rpmOk;
    }

    public int getRpmWarning() {
        return rpmWarning;
    }

    public void setRpmWarning(int rpmWarning) {
        this.rpmWarning = rpmWarning;
    }

    public int getSeedWarning() {
        return seedWarning;
    }

    public void setSeedWarning(int seedWarning) {
        this.seedWarning = seedWarning;
    }

    public int getSpeedAwesome() {
        return speedAwesome;
    }

    public void setSpeedAwesome(int speedAwesome) {
        this.speedAwesome = speedAwesome;
    }

    public int getSpeedHarsh() {
        return speedHarsh;
    }

    public void setSpeedHarsh(int speedHarsh) {
        this.speedHarsh = speedHarsh;
    }

    public int getSpeedHigh() {
        return speedHigh;
    }

    public void setSpeedHigh(int speedHigh) {
        this.speedHigh = speedHigh;
    }

    public int getSpeedMin() {
        return speedMin;
    }

    public void setSpeedMin(int speedMin) {
        this.speedMin = speedMin;
    }

    public int getSpeedOk() {
        return speedOk;
    }

    public void setSpeedOk(int speedOk) {
        this.speedOk = speedOk;
    }
}
