package com.android.kycar.carhealth.entities.vehicle_master;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by krishna on 6/12/15.
 */
@RealmClass
public class Make extends RealmObject {

    @PrimaryKey
    private String id;

    private String name;

    private String origin;

    private String vinBegin;

    public Make() {
    }

    public Make(String id, String name, String origin) {
        this.id = id;
        this.name = name;
        this.origin = origin;
        this.vinBegin = "";
    }

    public Make(String id, String name, String origin, String vinBegin) {
        this.id = id;
        this.name = name;
        this.origin = origin;
        this.vinBegin = vinBegin;
    }

    public String getVinBegin() {
        return vinBegin;
    }

    public void setVinBegin(String vinBegin) {
        this.vinBegin = vinBegin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }
}
