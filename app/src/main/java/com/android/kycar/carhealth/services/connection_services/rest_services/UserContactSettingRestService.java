package com.android.kycar.carhealth.services.connection_services.rest_services;


import com.android.kycar.carhealth.entities.UserContactSetting;

import org.json.JSONObject;

/**
 * Created by Krishna on 6/23/2016.
 */
public class UserContactSettingRestService extends RestService {

    public String createUserContactSetting(UserContactSetting userContactSetting) {

        String url = "/user/user_contact/create";
        JSONObject userContcatSettingJson = new JSONObject();

        try{
            userContcatSettingJson.put("id",userContactSetting.getId());
            userContcatSettingJson.put("isAutoConnect", userContactSetting.getIsAutoConnect());
            userContcatSettingJson.put("isSos", userContactSetting.getIsSos());
            userContcatSettingJson.put("shareLocation", userContactSetting.getShareLocation());
            userContcatSettingJson.put("userId", userContactSetting.getUserContactId());

        }catch (Exception e){
            e.printStackTrace();
        }
        return restServiceCall(url, "POST", userContcatSettingJson);
    }

    public String updateUserContactSetting(UserContactSetting userContactSetting) {

        String url = "/user/user_contact/update";
        JSONObject userContcatSettingJson = new JSONObject();

        try{
            userContcatSettingJson.put("id",userContactSetting.getId());
            userContcatSettingJson.put("isAutoConnect", userContactSetting.getIsAutoConnect());
            userContcatSettingJson.put("isSos", userContactSetting.getIsSos());
            userContcatSettingJson.put("shareLocation", userContactSetting.getShareLocation());
            userContcatSettingJson.put("userId", userContactSetting.getUserContactId());

        }catch (Exception e){
            e.printStackTrace();
        }
        return restServiceCall(url, "PUT", userContcatSettingJson);
    }

    public String deleteUserContactSetting(UserContactSetting userContactSetting) {

        String url = "/user/user_contact/update";
        JSONObject userContcatSettingJson = new JSONObject();

        try{
            userContcatSettingJson.put("id",userContactSetting.getId());
            userContcatSettingJson.put("isAutoConnect", userContactSetting.getIsAutoConnect());
            userContcatSettingJson.put("isSos", userContactSetting.getIsSos());
            userContcatSettingJson.put("shareLocation", userContactSetting.getShareLocation());
            userContcatSettingJson.put("userId", userContactSetting.getUserContactId());

        }catch (Exception e){
            e.printStackTrace();
        }
        return restServiceCall(url, "PUT", userContcatSettingJson);
    }

    public String getUserContactSetting(UserContactSetting userContactSetting) {

        String url = "/user/user_contact/find";

        return restServiceCall(url, "GET");
    }
}
