package com.android.kycar.carhealth.services.dto.commands.protocol;

/**
 * Reset the OBD connection.
 */
public class ObdResetCommand extends ObdProtocolCommand {

    public ObdResetCommand() {
        super("AT Z");
    }


    public ObdResetCommand(ObdResetCommand other) {
        super(other);
    }

    @Override
    public String getFormattedResult() {
        return getResult();
    }

    @Override
    public String getName() {
        return "Reset OBD";
    }

}
