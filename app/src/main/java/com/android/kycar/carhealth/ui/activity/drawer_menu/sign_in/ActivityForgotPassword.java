package com.android.kycar.carhealth.ui.activity.drawer_menu.sign_in;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.async_task.ForgotPasswordAsyncTask;
import com.android.kycar.carhealth.ui.activity.BaseActivity;

import org.json.JSONObject;

/**
 * Created by Krishna on 8/1/2016.
 */
public class ActivityForgotPassword extends BaseActivity {

    private TextView txtEmailOrNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        txtEmailOrNumber = (TextView) findViewById(R.id.txt_email_or_number);
    }

    public void onGetForgotPasswordLinkClick(View v) {
        Log.d(ActivityForgotPassword.class.getSimpleName(), "getForgotpassword click");

        if(txtEmailOrNumber.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please enter email id", Toast.LENGTH_SHORT).show();
            return;
        }

        JSONObject registrationJson = new JSONObject();
        try {
            registrationJson.put("email", txtEmailOrNumber.getText().toString().trim());
            //registrationJson.put("number", phone.getText().toString().trim());
        }catch (Exception e){
            e.printStackTrace();
        }

        new ForgotPasswordAsyncTask().execute(registrationJson);
    }
}
