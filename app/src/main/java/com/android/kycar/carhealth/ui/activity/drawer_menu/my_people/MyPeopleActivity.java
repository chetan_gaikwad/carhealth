package com.android.kycar.carhealth.ui.activity.drawer_menu.my_people;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.beans.ContactData;
import com.android.kycar.carhealth.beans.People;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.UserContact;
import com.android.kycar.carhealth.entities.UserContactSetting;
import com.android.kycar.carhealth.entities.Vehicle;
import com.android.kycar.carhealth.repositories.user.UserContactRepo;
import com.android.kycar.carhealth.repositories.user.UserContactSettingRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.UserContactsRestService;
import com.android.kycar.carhealth.ui.activity.BaseActivity;
import com.android.kycar.carhealth.ui.activity.ContactSelectorActivity;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_cars.AddCarActivity;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_people.adapter.MyPeopleRecyclerAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Krishna on 2/1/2016.
 * This class handle "my people" functionality
 */
public class MyPeopleActivity extends BaseActivity {

    private static final int PERMISSION_USER_CONTACTS = 8;
    private RecyclerView recyclerView;
    private MyPeopleRecyclerAdapter adapter;
    private ArrayList<People> peoples;
    private FloatingActionButton fab;
    private String convoyID;
    private UserContactRepo userContactRepo;
    private UserContactSettingRepo userContactSettingRepo;
    boolean formPeopleSelect = false;
    private final int REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_people);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                //gpsInit();
            } else {
                getUserPermission(this);
            }
        }
        initUI();

        /** Call to sync contacts of user */
        //new SyncContactsRestCall().execute();
    }

    /**
     * initialize ui
     */
    private void initUI() {

        /** Getting users permission */

        userContactSettingRepo = new UserContactSettingRepo(getApplicationContext(), UserContactSetting.class);

        peoples = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recyle_view);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        if(getIntent().getExtras()!=null) {
            formPeopleSelect = getIntent().getBooleanExtra(Util.IS_FROM_SELECT_CAR, false);
            convoyID = getIntent().getStringExtra(Util.CONVOY_ID);
        }
        fab.setOnClickListener(onAddingListener());
        getMyPeople();
    }

    private void getUserPermission(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission(
                Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(
                Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.READ_CONTACTS,
                            Manifest.permission.READ_CONTACTS},
                     PERMISSION_USER_CONTACTS);
        }
    }

    /**
     * Pick contacts from contact chooser
     * @return
     */
    private View.OnClickListener onAddingListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*************** Call contact picker to choose my people **********************/
                Intent contactPicker = new Intent(MyPeopleActivity.this, ContactSelectorActivity.class);
                startActivityForResult(contactPicker, REQUEST_CODE);
            }
        };
    }

    /**
     * handle calback event from contact chooser
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_CODE) {
            if(resultCode == Activity.RESULT_OK) {
                if(data.hasExtra(ContactData.CONTACTS_DATA)) {
                    ArrayList<ContactData> contacts = data.getParcelableArrayListExtra(ContactData.CONTACTS_DATA);
                    if(contacts != null) {
                        Iterator<ContactData> iterContacts = contacts.iterator();
                        Log.i(MyPeopleActivity.class.getSimpleName(),"Iterator size "+contacts.size());
                        while(iterContacts.hasNext()) {

                            ContactData contact = iterContacts.next();

                            /** Checking for duplicate contact */
                            UserContact userContact = userContactRepo.findContactByNumber(contact.phoneNmb);

                            if(userContact != null){
                                Toast.makeText(getApplicationContext(), "Contact already added", Toast.LENGTH_SHORT).show();
                                continue;
                            }
                            UserContact userContact1 = new UserContact(Util.getTimeLong() + "",
                                    contact.displayName,
                                    contact.phoneNmb, contact.token, preferences.getString(Util.USER_PREFERENCE, ""));

                            userContactRepo.saveOrUpdate(userContact1);
                            new UserContactRestCallAsycTask().execute(userContact1);
                            getMyPeople();
                        }
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * get all my people from database and inflate on list-view
     */
    public void  getMyPeople() {

        userContactRepo = new UserContactRepo(getApplicationContext(), UserContact.class);
        userContactSettingRepo = new UserContactSettingRepo(getApplicationContext(), UserContactSetting.class);
        List<UserContact> userContacts = userContactRepo.findAll();
        List<People> peoples = new ArrayList<>();
        for (UserContact userContact : userContacts) {

            if(!userContact.isSync()) {
                new UserContactRestCallAsycTask().execute(new UserContact(userContact));
            }

            UserContactSetting userContactSetting = userContactSettingRepo.findByUserContactId(userContact.getId());

            /** Finding contact token */
            new UserContactTokenRestCallAsycTask().execute(userContact.getId(), userContact.getNumber());

            if(userContactSetting == null) {
                peoples.add(new People(userContact.getId(),userContact.getName(),
                        userContact.getNumber(),
                        null,
                        false,
                        false,
                        false));
            } else {

                if(userContactSetting.getIsAutoConnect()  && userContactSetting.getShareLocation()){
                    peoples.add(new People(userContact.getId(),userContact.getName(),
                            userContact.getNumber(),
                            null,
                            userContactSetting.getIsSos(),
                            userContactSetting.getShareLocation(),
                            userContactSetting.getIsAutoConnect()
                            ));
                } else {
                    peoples.add(new People(userContact.getId(),userContact.getName(),
                            userContact.getNumber(), null, userContactSetting.getIsSos(), false, false
                    ));
                }
            }
            userContactSettingRepo.close();
        }
        userContactRepo.close();

        adapter = new MyPeopleRecyclerAdapter(MyPeopleActivity.this, peoples, formPeopleSelect);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recyclerView.setAdapter(adapter);
            }
        });
    }

    /**
     * Uploading asynchronous task
     */
    private class UserContactRestCallAsycTask extends AsyncTask<UserContact, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(UserContact... userContacts) {

            String response = new UserContactsRestService().createContact(userContacts[0]);
            if(response!=null) {
                try {
                    JSONObject userContactJson = new JSONObject(response);
                    UserContact userContact = new UserContact(userContactJson);
                    /************** Insert contacts selected by user into database ********/
                    userContactRepo = new UserContactRepo(getApplicationContext(), UserContact.class);
                    userContact.setSync(true);
                    userContactRepo.saveOrUpdate(userContact);
                    userContactRepo.close();

                    userContactSettingRepo = new UserContactSettingRepo(getApplicationContext(), UserContactSetting.class);
                    userContactSettingRepo.saveOrUpdate(new UserContactSetting(Util.getTimeLong() + "",
                            userContact.getId(),
                            false,
                            false,
                            false));
                    userContactSettingRepo.close();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                userContactRepo = new UserContactRepo(getApplicationContext(), UserContact.class);
                userContacts[0].setId(new Date().getTime()+"");
                userContacts[0].setSync(false);
                userContactRepo.save(userContacts[0]);
                userContactSettingRepo = new UserContactSettingRepo(getApplicationContext(), UserContactSetting.class);
                userContactSettingRepo.saveOrUpdate(new UserContactSetting(Util.getTimeLong() + "",
                        userContacts[0].getId(),
                        false,
                        false,
                        false));
                getMyPeople();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    private class UserContactTokenRestCallAsycTask extends AsyncTask<String, Void, String> {


        private String userId;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            this.userId = strings[0];
            strings[1] = strings[1].replace("-", "").replace(" ", "").replace("+", "").replace("#", "").replace("*", "").replace(".", "");
            String number = strings[1].substring(strings[1].length()-10);
            Log.d(MyPeopleActivity.class.getSimpleName(), "Number is :"+number);
            return new UserContactsRestService().findUserContactTopic(number);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result!=null) {
                try {
                    JSONObject resultObj = new JSONObject(result);
                    if(resultObj.has("topic")) {
                        userContactRepo = new UserContactRepo(getApplicationContext(), UserContact.class);
                        UserContact userContact = new UserContact((UserContact) userContactRepo.findById(this.userId));
                        userContact.setUserContactTopic(resultObj.getString("topic"));
                        Log.d(MyPeopleActivity.class.getSimpleName(), "User Token found : "+result);
                        userContactRepo.saveOrUpdate(userContact);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Util.showAlert("Failed to find user. Please invite the user", MyPeopleActivity.this);
            }
        }
    }

    /**
     * Uploading asynchronous task
     */
    class SyncContactsRestCall extends AsyncTask<Void, Void, Void> {

        private UserContactRepo userContactRepo;

        @Override
        protected Void doInBackground(Void... params) {

            try {
                userContactRepo = new UserContactRepo(getApplicationContext(), Vehicle.class);
                List<UserContact> unsyncedUserContact = userContactRepo.findAllUnSynced();

                Iterator<UserContact> userContactIterator = unsyncedUserContact.iterator();
                while (userContactIterator.hasNext()) {

                    UserContact userContact = new UserContact(userContactIterator.next());

                    String response = new UserContactsRestService().createContact(userContact);


                    if(response != null) {
                        JSONObject responseObj = new JSONObject(response);

                        /** Removing old record */
                        userContactRepo.deleteById(userContact.getId());

                        /** Updating vehicle id in local db */
                        UserContact userContact1 = new UserContact(responseObj);
                        userContact1.setSync(true);
                        userContactRepo.saveOrUpdate(userContact1);
                        Log.d(AddCarActivity.class.getSimpleName(), "Rest Response:-"+response);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }
}
