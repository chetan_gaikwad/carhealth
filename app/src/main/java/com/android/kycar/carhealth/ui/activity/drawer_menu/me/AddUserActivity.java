package com.android.kycar.carhealth.ui.activity.drawer_menu.me;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.entities.User;
import com.android.kycar.carhealth.repositories.user.UserRepo;

import java.util.Date;


/**
 * Created by Krishna on 2/28/2016.
 */
public class AddUserActivity extends AppCompatActivity {

    private UserRepo userRepo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
    }

    public void onAddUserClick(View view){

        User user = new User();
        user.setId(""+new Date());
        userRepo.saveOrUpdate(user);
    }

}
