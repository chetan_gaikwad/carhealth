package com.android.kycar.carhealth.config;

import android.os.AsyncTask;
import android.os.Build;

public class ASyncUtils {

	public static void startMyTask(AsyncTask asyncTask, Object... params) {
	      if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
	          asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
	      else
	          asyncTask.execute(params);
	  }
	
}
