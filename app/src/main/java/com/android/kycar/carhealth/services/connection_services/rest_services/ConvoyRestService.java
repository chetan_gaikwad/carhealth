package com.android.kycar.carhealth.services.connection_services.rest_services;


import com.android.kycar.carhealth.entities.UserContact;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Krishna on 6/25/2016.
 */
public class ConvoyRestService extends RestService {

    public String create(String userId, String convoyName, List<UserContact> userContacts){

        JSONObject convoyJson = new JSONObject();
        try {
            convoyJson.put("userId", userId);
            convoyJson.put("convoyName", convoyName);

            JSONArray joineeArray = new JSONArray();
            for (UserContact userContact: userContacts) {
                JSONObject userContactJson = new JSONObject();
                userContactJson.put("name", userContact.getName());
                userContactJson.put("number", userContact.getNumber());
                joineeArray.put(userContactJson);
            }

            convoyJson.put("joinee", joineeArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String loginUrl = "/convoy/create";
        return restServiceCall(loginUrl, "POST", convoyJson);
    }

}
