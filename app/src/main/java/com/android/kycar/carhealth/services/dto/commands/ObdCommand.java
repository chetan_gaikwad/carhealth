package com.android.kycar.carhealth.services.dto.commands;

import android.util.Log;

import com.android.kycar.carhealth.NxtApplication;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.services.dto.exceptions.BusInitException;
import com.android.kycar.carhealth.services.dto.exceptions.MisunderstoodCommandException;
import com.android.kycar.carhealth.services.dto.exceptions.NoDataException;
import com.android.kycar.carhealth.services.dto.exceptions.NonNumericResponseException;
import com.android.kycar.carhealth.services.dto.exceptions.ResponseException;
import com.android.kycar.carhealth.services.dto.exceptions.StoppedException;
import com.android.kycar.carhealth.services.dto.exceptions.UnableToConnectException;
import com.android.kycar.carhealth.services.dto.exceptions.UnknownErrorException;
import com.android.kycar.carhealth.services.dto.exceptions.UnsupportedCommandException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Base OBD command.
 */
public abstract class ObdCommand {

    /**
     * Error classes to be tested in order
     */
    private final Class[] ERROR_CLASSES = {
            UnableToConnectException.class,
            BusInitException.class,
            MisunderstoodCommandException.class,
            NoDataException.class,
            StoppedException.class,
            UnknownErrorException.class,
            UnsupportedCommandException.class
    };
    protected ArrayList<Integer> buffer = null;
    protected String cmd = null;
    protected boolean useImperialUnits = false;
    protected String rawData = null;
    protected long responseTimeDelay = 200;
    private long start;
    private long end;

    /**
     * Default ctor to use
     *
     * @param command the command to send
     */
    public ObdCommand(String command) {
        this.cmd = command;
        this.buffer = new ArrayList<Integer>();
    }

    /**
     * Prevent empty instantiation
     */
    private ObdCommand() {
    }

    /**
     * Copy ctor.
     *
     * @param other the ObdCommand to copy.
     */
    public ObdCommand(ObdCommand other) {
        this(other.cmd);
    }

    /**
     * Sends the OBD-II request and deals with the response.
     * <p>
     * This method CAN be overriden in fake commands.
     *
     * @param in  a {@link InputStream} object.
     * @param out a {@link OutputStream} object.
     * @throws IOException            if any.
     * @throws InterruptedException if any.
     */
    public void run(InputStream in, OutputStream out) throws IOException,
            InterruptedException {
        start = System.currentTimeMillis();
        sendCommand(out);
        readResult(in);
        end = System.currentTimeMillis();
    }

    /**
     * Sends the OBD-II request.
     * <p>
     * This method may be overriden in subclasses, such as ObMultiCommand or
     * TroubleCodesCommand.
     *
     * @param out The output stream.
     * @throws IOException            if any.
     * @throws InterruptedException if any.
     */
    protected void sendCommand(OutputStream out) throws IOException,
            InterruptedException {
        // write to OutputStream (i.e.: a BluetoothSocket) with an added
        // Carriage return
        out.write((cmd + "\r").getBytes());
        out.flush();

    /*
     * HACK GOLDEN HAMMER ahead!!
     *
     * Due to the time that some systems may take to respond, let's give it
     * 200ms.
     */
        Thread.sleep(responseTimeDelay);
    }

    /**
     * Resends this command.
     *
     * @param out a {@link OutputStream} object.
     * @throws IOException            if any.
     * @throws InterruptedException if any.
     */
    protected void resendCommand(OutputStream out) throws IOException,
            InterruptedException {
        out.write("\r".getBytes());
        out.flush();
    }

    /**
     * Reads the OBD-II response.
     * <p>
     * This method may be overriden in subclasses, such as ObdMultiCommand.
     *
     * @param in a {@link InputStream} object.
     * @throws IOException if any.
     */
    protected void readResult(InputStream in) throws IOException {
        if(NxtApplication.preferences.getInt(Util.CAR_TYPE,0) == Util.isCrud) {
            readRawDataForAutomatic(in);
        }else {
            readRawData(in);
        }
        checkForErrors();
        fillBuffer();
        performCalculations();
    }

    /**
     * This method exists so that for each command, there must be a method that is
     * called only once to perform calculations.
     */
    protected abstract void performCalculations();

    /**
     *
     */
    protected void fillBuffer() {
        rawData = rawData.replaceAll("\\s", ""); //removes all [ \t\n\x0B\f\r]
        rawData = rawData.replaceAll("(BUS INIT)|(BUSINIT)|(\\.)", "");

        if (!rawData.matches("([0-9A-F])+")) {
            throw new NonNumericResponseException(rawData);
        }

        // read string each two chars
        buffer.clear();
        int begin = 0;
        int end = 2;
        while (end <= rawData.length()) {
            buffer.add(Integer.decode("0x" + rawData.substring(begin, end)));
            begin = end;
            end += 2;
        }
    }

    /**
     * <p>
     * readRawData.</p>
     *
     * @param in a {@link InputStream} object.
     * @throws IOException if any.
     */
    protected void readRawDataForAutomatic(InputStream in) throws IOException {
        byte b = 0;
        StringBuilder res = new StringBuilder();

        // read until '>' arrives OR end of stream reached
        char c;
        while(true)
        {
            b = (byte) in.read();
            if(b == -1) // -1 if the end of the stream is reached
            {
                break;
            }
            c = (char)b;
            if(c == '>') // read until '>' arrives
            {
                break;
            }
            res.append(c);
        }

    /*
     * Imagine the following response 41 0c 00 0d.
     *
     * ELM sends strings!! So, ELM puts spaces between each "byte". And pay
     * attention to the fact that I've put the word byte in quotes, because 41
     * is actually TWO bytes (two chars) in the socket. So, we must do some more
     * processing..
     */
        rawData = res.toString().replaceAll("SEARCHING", "");

        //Util.writeToFile("Logger previous "+rawData);
        Log.i("abbb","Logger previous "+rawData);
    /*
     * Data may have echo or informative cost like "INIT BUS..." or similar.
     * The response ends with two carriage return characters. So we need to take
     * everything from the last carriage return before those two (trimmed above).
     */
        //kills multiline.. rawData = rawData.substring(rawData.lastIndexOf(13) + 1);

        String header = NxtApplication.preferences.getString(Util.DEFAULT_HEADER,"");
        String protocol = NxtApplication.preferences.getString(Util.DEFAULT_PROTOCOL,"");
        rawData = rawData.replace(" ","");
        String[] responses = rawData.split("\r");

        int previousSize = rawData.length();

        //Util.writeToFile("previous data : "+rawData+"   previousSize : "+previousSize);
        //Try for default header
        for(int i=0;i<responses.length;i++){
            String currentResponse = responses[i];
            if(protocol.contains("9141") || protocol.contains("14230")) {
                if (currentResponse.substring(4, 6).contains(header)) {
                    rawData = rawData.replace(header, "");
                    rawData = currentResponse.substring(6);
                    rawData = rawData.substring(0, rawData.length() - 2);
                }
            }else if(protocol.contains("15765")) {

                if (protocol.contains("11/")) {
                    if (currentResponse.substring(0, 3).contains(header)) {
                        rawData = rawData.replace(header, "");
                        rawData = currentResponse.substring(5);
                    }
                }else if(protocol.contains("29/")) {
                    if (currentResponse.substring(6, 8).contains(header)) {
                        rawData = rawData.replace(header, "");
                        rawData = currentResponse.substring(10);
                    }
                }
            }
        }

        //Util.writeToFile("next data : "+rawData+"   next Size : "+previousSize);
        //Default header not found take the recieved header
        if(previousSize == rawData.length()){
            String currentResponse = responses[0];
            if(protocol.contains("9141") || protocol.contains("14230")) {
                rawData = rawData.replace(header, "");
                rawData = currentResponse.substring(6);
                rawData = rawData.substring(0, rawData.length() - 2);
            }else if(protocol.contains("15765")) {
                if (protocol.contains("11/")) {
                    rawData = rawData.replace(header, "");
                    rawData = currentResponse.substring(5);
                }else if(protocol.contains("29/")) {
                    rawData = rawData.replace(header, "");
                    rawData = currentResponse.substring(10);
                }
            }
        }
        //rawData = rawData.substring(6);

        rawData = rawData.replaceAll("\\s", "");//removes all [ \t\n\x0B\f\r]
        String s = rawData;
        Log.i("abbb","Logger after "+rawData);
        //Util.writeToFile("Logger after "+rawData);
    }

    //Here data will come from multiple ecp manipulate it and in end rawdata shud have master header value
    protected void readRawData(InputStream in) throws IOException {
        byte b = 0;
        StringBuilder res = new StringBuilder();

        // read until '>' arrives OR end of stream reached
        char c;
        while(true)
        {
            b = (byte) in.read();
            if(b == -1) // -1 if the end of the stream is reached
            {
                break;
            }
            c = (char)b;
            if(c == '>') // read until '>' arrives
            {
                break;
            }
            res.append(c);
        }

    /*
     * Imagine the following response 41 0c 00 0d.
     *
     * ELM sends strings!! So, ELM puts spaces between each "byte". And pay
     * attention to the fact that I've put the word byte in quotes, because 41
     * is actually TWO bytes (two chars) in the socket. So, we must do some more
     * processing..
     */
        rawData = res.toString().replaceAll("SEARCHING", "");

    /*
     * Data may have echo or informative cost like "INIT BUS..." or similar.
     * The response ends with two carriage return characters. So we need to take
     * everything from the last carriage return before those two (trimmed above).
     */
        //kills multiline.. rawData = rawData.substring(rawData.lastIndexOf(13) + 1);
        rawData = rawData.replaceAll("\\s", "");//removes all [ \t\n\x0B\f\r]
        String s = rawData;
    }
    void checkForErrors() {
        for (Class<? extends ResponseException> errorClass : ERROR_CLASSES) {
            ResponseException messageError;

            try {
                messageError = errorClass.newInstance();
                messageError.setCommand(this.cmd);
            } catch (InstantiationException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }

            if (messageError.isError(rawData)) {
                throw messageError;
            }
        }
    }

    /**
     * @return the raw command response in string representation.
     */
    public String getResult() {
        return rawData;
    }

    /**
     * @return a formatted command response in string representation.
     */
    public abstract String getFormattedResult();

    /**
     * @return the command response in string representation, without formatting.
     */
    public abstract String getCalculatedResult();

    /**
     * @return a list of integers
     */
    protected ArrayList<Integer> getBuffer() {
        return buffer;
    }

    /**
     * @return true if imperial units are used, or false otherwise
     */
    public boolean useImperialUnits() {
        return useImperialUnits;
    }

    /**
     * The unit of the result, as used in {@link #getFormattedResult()}
     *
     * @return a String representing a unit or "", never null
     */
    public String getResultUnit() {
        return "";//no unit by default
    }

    /**
     * Set to 'true' if you want to use imperial units, false otherwise. By
     * default this value is set to 'false'.
     *
     * @param isImperial a boolean.
     */
    public void useImperialUnits(boolean isImperial) {
        this.useImperialUnits = isImperial;
    }

    /**
     * @return the OBD command name.
     */
    public abstract String getName();

    /**
     * Time the command waits before returning from #sendCommand()
     *
     * @return delay in ms
     */
    public long getResponseTimeDelay() {
        return responseTimeDelay;
    }

    /**
     * Time the command waits before returning from #sendCommand()
     *
     * @param responseTimeDelay
     */
    public void setResponseTimeDelay(long responseTimeDelay) {
        this.responseTimeDelay = responseTimeDelay;
    }

    //fixme resultunit
    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public final String getCommandPID() {
        return cmd.substring(3);
    }

}
