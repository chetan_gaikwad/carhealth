package com.android.kycar.carhealth.async_task;

import android.os.AsyncTask;


import com.android.kycar.carhealth.services.connection_services.rest_services.SensorDataService;

import org.json.JSONObject;

/**
 * Created by Krishna on 9/26/2016.
 */
public class SensorEventDataAsyncTask extends AsyncTask<JSONObject, Void, String> {

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(JSONObject... sensorData) {

        try {
            return new SensorDataService().sendRawSensorData(sensorData[0]);
        } catch (Exception jsonException) {
            jsonException.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }
}
