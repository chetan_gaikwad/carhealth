package com.android.kycar.carhealth.ui.activity.drawer_menu.sign_in;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.ProgessDialogBox;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.User;
import com.android.kycar.carhealth.repositories.user.UserRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.FcmRestCallService;
import com.android.kycar.carhealth.services.connection_services.rest_services.UserRestService;
import com.android.kycar.carhealth.ui.activity.BaseActivity;
import com.android.kycar.carhealth.ui.activity.DashboardActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityLogin extends BaseActivity {

    private EditText email;
    private EditText password;
    private ProgessDialogBox dialogBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.pass);
        dialogBox = new ProgessDialogBox(this);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public void login(View v) {

        if (!email.getText().toString().isEmpty() &&
                !password.getText().toString().isEmpty()) {
            new LoginAsyncTask().execute(email.getText().toString(), password.getText().toString(), preferences.getString(Util.USER_FCM_TOKEN, ""));
        } else {
            Toast.makeText(this, "Please fill all the boxes", Toast.LENGTH_SHORT).show();
        }
    }

    public void goToRegister(View v){
        this.finish();
        startActivity(new Intent(ActivityLogin.this, ActivityRegister.class));
    }

    public void onForgotPasswordClick(View v) {
        this.finish();
        startActivity(new Intent(ActivityLogin.this, ActivityForgotPassword.class));
        //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://139.59.253.137:1500/forget/password"));
        //startActivity(browserIntent);
    }

    /** ???? */
    /*@Subscribe
    public void networkCallback(String response){

        try{
        JSONObject jsonObject = new JSONObject(response.toString());
        String userID = jsonObject.getString("id");
        String username = jsonObject.getString("name");
        if(userID!=null && !userID.isEmpty()) {
            System.out.println(response.toString());
            System.out.println("\n" + userID.toString());
            editor.putString(Util.USERNAME_PREFERENCE, username);
            editor.putString(Util.USER_PREFERENCE, userID);
            editor.commit();
        }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }*/

    /**
     * Uploading asynchronous task
     */
    private class LoginAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogBox.showDailog();
        }

        @Override
        protected String doInBackground(String... cred) {

            String response = new UserRestService().login(cred);

            try{
                JSONObject jsonObject = new JSONObject(response);

                if(jsonObject!=null && jsonObject.getBoolean("success")) {
                    JSONObject loginData = new JSONObject(jsonObject.getString("result"));
                    String userId = loginData.getString("id");
                    String username;
                    if(loginData.has("name")) {
                       username = loginData.getString("name");
                    } else {
                        username = "Chetan";
                    }
                    System.out.println(response.toString());
                    System.out.println("\n" + userId.toString());
                    editor.putString(Util.USERNAME_PREFERENCE, username);
                    editor.putString(Util.USER_PREFERENCE, userId);
                    editor.commit();

                    User user = new User(loginData);
                    UserRepo userRepo = new UserRepo(getApplicationContext(), User.class);
                    userRepo.saveOrUpdate(user);
                }
                return response;
            }catch (Exception e){
                e.printStackTrace();
                return response;
            }
        }
        @Override

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(dialogBox.isShowing()){
                dialogBox.dismissDialog();
            }
            if(result!=null) {

                try {
                    JSONObject userLoginObj = new JSONObject(result);

                    if (userLoginObj != null && userLoginObj.getBoolean("success")) {
                        JSONObject loginData = new JSONObject(userLoginObj.getString("result"));
                        JSONObject phoneData = new JSONObject();

                        /*try {
                            if (PermissionChecker.checkSelfPermission(ActivityLogin.this, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                                phoneData.put("hardwareData", getHardwareData());
                                phoneData.put("sensorData", getAllSensorData());
                            }
                            phoneData.put("userId", preferences.getString(Util.USER_PREFERENCE, null));
                            Log.d("Phone data", phoneData.toString());
                            new DeviceRegistrationAsycTask(preferences).execute(phoneData);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/
                        editor.putString(Util.USER_TOPIC, loginData.getString("topic")).commit();
                        editor.putBoolean(Util.IS_LOGGED_IN, true).commit();

                        //ActivityLogin.this.finish();
                        Intent intent =  new Intent(ActivityLogin.this, DashboardActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        intent.putExtra("isLoggedInRecently", "yes");


                        startActivity(intent);
                    } else if(userLoginObj!=null && !userLoginObj.getBoolean("success")){
                        Util.showAlert(userLoginObj.getString("message"), ActivityLogin.this);
                    }
                }catch(JSONException e){
                    e.printStackTrace();
                }
            } else {
                Util.showAlert("Failed to login. Please try again "+result, ActivityLogin.this);
            }
        }
    }

    private class FcmTokenUpdateAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            new FcmRestCallService().registerToken(strings[0], strings[1]);
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {
            super.onPostExecute(result);
            Log.d(ActivityLogin.class.getSimpleName(), "Token sent");
        }
    }

    public JSONObject getHardwareData() throws JSONException {

        Map<String, String> map;
        ArrayList<Map<String, String>> sampleList = new ArrayList<>();

        final String KEY_TITLE = "title";
        final String KEY_DETAILS = "details";

        String serviceName = Context.TELEPHONY_SERVICE;
        TelephonyManager m_telephonyManager = (TelephonyManager) getSystemService(serviceName);
        String IMEI,IMSI;
        IMEI = m_telephonyManager.getDeviceId();
        IMSI = m_telephonyManager.getSubscriberId();

        Log.i("IMEI : ", IMEI);
        Log.i("IMSI : ", IMSI);

        String[] title = {
                "MANUFACTURER",
                "BOARD",
                "DISPLAY",
                "BASE_OS",
                "CODENAME",
                "INCREMENTAL",
                "RELEASE",
                "SECURITY_PATCH",
                "BOOTLOADER",
                "BRAND",
                "DEVICE",
                "IMEI",
                "IMSI",
                "FINGERPRINT",
                "RADIO VERSION",
                "HARDWARE",
                "HOST",
                "ID",
                "MODEL",
                "PRODUCT",
                "SERIAL",
                "TAGS",
                "TYPE",
                "CPU_ABI",
                "CPU_ABI2"};

        String[] detail = {
                Build.MANUFACTURER,
                Build.BOARD,
                Build.DISPLAY,
                Build.VERSION.BASE_OS,
                Build.VERSION.CODENAME,
                Build.VERSION.INCREMENTAL,
                Build.VERSION.RELEASE,
                Build.VERSION.SECURITY_PATCH,
                Build.BOOTLOADER,
                Build.BRAND,
                Build.DEVICE,
                IMEI,
                IMSI,
                Build.FINGERPRINT,
                Build.getRadioVersion(),
                Build.HARDWARE,
                Build.HOST,
                Build.ID,
                Build.MODEL,
                Build.PRODUCT,
                Build.SERIAL,
                Build.TAGS,
                Build.TYPE,
                Build.CPU_ABI,
                Build.CPU_ABI2
        };

        for (int i = 0; i < title.length; i++) {
            map = new HashMap<>();
            map.put(KEY_TITLE, title[i]);
            map.put(KEY_DETAILS, detail[i]);
            sampleList.add(map);
        }

        JSONObject deviceInfo = new JSONObject();
        for (int i = 0; i < title.length; i++) {

            deviceInfo.put(title[i], detail[i]);
        }
        return deviceInfo;
    }

    public JSONArray getAllSensorData() throws JSONException {
        SensorManager smm;
        List<Sensor> sensors;
        smm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensors = smm.getSensorList(Sensor.TYPE_ALL);

        /**
         * ("Vendor : "+mList.get(position).getVendor()+
         "\n Power : "+mList.get(position).getPower()+
         "\n Version : "+mList.get(position).getVersion()+
         "\n Maximum Range : "+mList.get(position).getMaximumRange()+
         "\n Version : "+mList.get(position).getVersion()+
         "\n Resolution : "+mList.get(position).getResolution());
         **/

        JSONArray sensorJsonArray = new JSONArray();
        for (Sensor sensor : sensors) {

            if(sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                preferences.edit().putString(Util.SENSOR_ACC_NAME, sensor.getName()).commit();
            }
            JSONObject sensorObject = new JSONObject();
            sensorObject.put("name", sensor.getName());
            sensorObject.put("minDelay", sensor.getMinDelay());
            sensorObject.put("maxRange", sensor.getMaximumRange());
            sensorObject.put("resolution", sensor.getResolution());
            sensorObject.put("vendor", sensor.getVendor());
            sensorObject.put("version", sensor.getVersion());
            sensorObject.put("power", sensor.getPower());
            sensorObject.put("type", sensor.getType());
            sensorJsonArray.put(sensorObject);
        }
        return sensorJsonArray;
    }

}
