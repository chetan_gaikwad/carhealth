package com.android.kycar.carhealth.services.connection_services.mqtt_paho;

import android.location.Location;
import android.util.Log;


import com.android.kycar.carhealth.beans.Coordinates;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krishna on 5/29/2016.
 */
public class PublishCoods {

    public MqttMessage getCoordData(String userId, String vehicleId, Coordinates coords, String tripId ){

        try {
            JSONObject payload = new JSONObject();
            payload.put("car_id", vehicleId);
            payload.put("data", "coords_data");
            JSONObject coordsData = new JSONObject();
            if(!tripId.isEmpty()){
                coordsData.put("trip_id", tripId);
            }
            coordsData.put("loc", coords.toJSON());
            coordsData.put("usr_id", userId);
            coordsData.put("veh_id", vehicleId);
            payload.put("coords_data", coordsData);

            Log.d("coordsData", payload.toString());
            MqttMessage message = new MqttMessage(payload.toString().getBytes());
            return message;

        } catch (JSONException jsonException){
            jsonException.printStackTrace();
        }
        return null;
    }

    public MqttMessage getCoordData(Location coords, String tripId){

        try {
            JSONObject payload = new JSONObject();
            payload.put("data", "locData");
            payload.put("locData", getJsonCoordData(coords, tripId));

            Log.d("locData", payload.toString());
            MqttMessage message = new MqttMessage(payload.toString().getBytes());
            return message;

        } catch (JSONException jsonException){
            jsonException.printStackTrace();
        }
        return null;
    }

    public JSONObject getJsonCoordData (Location coords, String tripId) {

        try {
            JSONObject loc = new JSONObject();
            loc.put("la", coords.getLatitude());
            loc.put("lo", coords.getLongitude());
            loc.put("ac", coords.getAccuracy());
            loc.put("spd", coords.getSpeed());
            loc.put("gb", coords.getBearing());
            if(tripId != null) {
                loc.put("tripId", tripId);
            }
            return loc;
        } catch (JSONException jsonException){
            jsonException.printStackTrace();
        }
        return null;
    }

    public MqttMessage getLocationShareCoordData(String userName, Location location){

        try {
            JSONObject payload = new JSONObject();
            payload.put("nm", userName);
            payload.put("la", location.getLatitude());
            payload.put("lo", location.getLongitude());
            payload.put("ac", location.getAccuracy());

            Log.d("coordsData", payload.toString());
            MqttMessage message = new MqttMessage(payload.toString().getBytes());
            return message;
        } catch (JSONException jsonException){
            jsonException.printStackTrace();
        }
        return null;
    }

    public MqttMessage publishConvoyCoordsData(String userId, String vehicleId, Coordinates coords, String tripId, String convoyId ) {

        try {
            JSONObject payload = new JSONObject();
            payload.put("data", "convoyData");
            JSONObject convoyData = new JSONObject();
            if(!tripId.isEmpty()){
                convoyData.put("tripId", tripId);
            }
            convoyData.put("loc", coords.toJSON());
            convoyData.put("userId", userId);
            payload.put("convoyData", convoyData);
            return new MqttMessage(payload.toString().getBytes());
        } catch (JSONException jsonException){
            jsonException.printStackTrace();
        }
        return null;
    }
}
