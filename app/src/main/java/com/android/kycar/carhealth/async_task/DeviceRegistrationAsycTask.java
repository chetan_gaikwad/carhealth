package com.android.kycar.carhealth.async_task;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.services.connection_services.rest_services.UserRestService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krishna on 9/18/2016.
 */

public class DeviceRegistrationAsycTask extends AsyncTask<JSONObject, Void, String> {

    private SharedPreferences preferences;

    public DeviceRegistrationAsycTask(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(JSONObject... phoneData) {

        try {
            return new UserRestService().deviceRegistration(phoneData[0]);
        } catch (Exception jsonException) {
            jsonException.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        Log.d(DeviceRegistrationAsycTask.class.getSimpleName(), "Result:-"+result);
        if(result!=null) {
            try {
                JSONObject resultObj = new JSONObject(result);
                //Util.showAlert("Device Registration successfully. ", ActivityRegister.this);
                preferences.edit().putString(Util.SENSOR_ID, resultObj.getString("id")).commit();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            //Util.showAlert("Registration failed. Please try again", ActivityRegister.this);
        }
    }
}