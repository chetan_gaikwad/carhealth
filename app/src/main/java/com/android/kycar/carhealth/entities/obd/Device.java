package com.android.kycar.carhealth.entities.obd;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 1/17/2016.
 */
@RealmClass
public class Device extends RealmObject {

    @PrimaryKey
    private String id;

    private String deviceName;

    private String deviceNickName;

    private String macId;

    private String deviceType;

    public Device() {
    }

    public Device(String id, String deviceName, String deviceNickName, String macId, String deviceType) {
        this.id = id;
        this.deviceName = deviceName;
        this.deviceNickName = deviceNickName;
        this.macId = macId;
        this.deviceType = deviceType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceNickName() {
        return deviceNickName;
    }

    public void setDeviceNickName(String deviceNickName) {
        this.deviceNickName = deviceNickName;
    }

    public String getMacId() {
        return macId;
    }

    public void setMacId(String macId) {
        this.macId = macId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}
