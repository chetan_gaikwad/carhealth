package com.android.kycar.carhealth.ui.manager;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.android.kycar.carhealth.NxtApplication;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class AddressProvider {
    private Context context;

    public AddressProvider(Context context) {
        this.context = context;
    }

    public String getCurrentAddress() {

        Geocoder geoCoder = new Geocoder(context, Locale.getDefault());
        StringBuilder builder = new StringBuilder();
        try {
            List<Address> address = geoCoder.getFromLocation(NxtApplication.currLocation.getLatitude(), NxtApplication.currLocation.getLongitude(), 1);

            if(!address.isEmpty()){
                int maxLines = address.get(0).getMaxAddressLineIndex();
                for (int i=0; i<maxLines; i++) {
                    String addressStr = address.get(0).getAddressLine(i);
                    builder.append(addressStr);
                    builder.append(" ");
                }
            }

            return builder.toString(); //This is the complete address.
        } catch (IOException e) {
            return "Default_add";
        }
        catch (NullPointerException e) {
            return "Default_add";
        }
    }

}
