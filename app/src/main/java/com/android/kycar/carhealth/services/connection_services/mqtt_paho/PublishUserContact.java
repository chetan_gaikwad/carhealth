package com.android.kycar.carhealth.services.connection_services.mqtt_paho;

import android.util.Log;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krishna on 7/9/2016.
 */
public class PublishUserContact {

    public boolean getUserContact(String userToken , String sessionId){

        try {
            JSONObject payload = new JSONObject();
            payload.put("data", "user_contact_token");
            JSONObject userContactToken = new JSONObject();

            userContactToken.put("sessionId", sessionId);
            payload.put("user_contact_token", userContactToken);

            Log.d("token : ", payload.toString());
            MqttMessage message = new MqttMessage(payload.toString().getBytes());

        } catch (JSONException jsonException){
            return false;
        }
        return true;
    }
}
