package com.android.kycar.carhealth.async_task;

import android.os.AsyncTask;


import com.android.kycar.carhealth.services.connection_services.rest_services.ForgotPasswordRestService;

import org.json.JSONObject;

/**
 * Created by Krishna on 10/25/2016.
 */
public class ForgotPasswordAsyncTask extends AsyncTask<JSONObject, Void, String> {
    @Override
    protected String doInBackground(JSONObject... params) {

        new ForgotPasswordRestService().getForgotPasswordLink(params[0]);
        return null;
    }
}
