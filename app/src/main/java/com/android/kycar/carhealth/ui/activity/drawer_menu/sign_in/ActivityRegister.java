package com.android.kycar.carhealth.ui.activity.drawer_menu.sign_in;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.ProgessDialogBox;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.User;
import com.android.kycar.carhealth.repositories.user.UserRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.UserRestService;
import com.android.kycar.carhealth.ui.activity.BaseActivity;
import com.android.kycar.carhealth.ui.activity.DashboardActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ActivityRegister extends BaseActivity {

	private static final String TAG = ActivityRegister.class.getName();
	private EditText name;
	private EditText email;
	private EditText password;
	private EditText reEnterPassword;
	private EditText phone;
	private ProgessDialogBox dialogBox;
	private EditText countryCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_register);
		countryCode = (EditText) findViewById(R.id.country_code);

		Handler handler = new Handler();
		Runnable r = new Runnable() {
			public void run() {
				countryCode.setText(GetCountryZipCode());
			}
		};
		handler.postDelayed(r, 10);

		dialogBox = new ProgessDialogBox(this);
		name = (EditText)findViewById(R.id.name);
		email = (EditText)findViewById(R.id.email);
		password = (EditText)findViewById(R.id.pass);
		reEnterPassword = (EditText)findViewById(R.id.r_pass);
		phone = (EditText)findViewById(R.id.phone);
		if(preferences.getBoolean("registered",false)){

			ActivityRegister.this.finish();
			//startActivity(new Intent(ActivityRegister.this, DrawerActivityDeprecated.class));
		}
	}
	/**
	 * Register mUser
	 * @param v
	 */
	public void register(View v){
		try {
			if (!email.getText().toString().isEmpty() &&
					!password.getText().toString().isEmpty() &&
					!reEnterPassword.getText().toString().isEmpty() &&
					!phone.getText().toString().isEmpty()) {

				Pattern p = Pattern.compile("^(.+)@(.+)$");
				Matcher m = p.matcher(email.getText().toString());
				if(!m.find()) {
					Toast.makeText(getBaseContext(), "Please provide valid e-mail id",Toast.LENGTH_SHORT).show();
					return;
				}
				View view = this.getCurrentFocus();
				if (view != null) {
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
				}
				if(password.getText().toString().equals(
						reEnterPassword.getText().toString())) {

					JSONObject registrationJson = new JSONObject();
					try {
						registrationJson.put("email", email.getText().toString().trim());
						registrationJson.put("number", phone.getText().toString().trim());
						registrationJson.put("password", password.getText().toString().trim());
						registrationJson.put("name", name.getText().toString().trim());
						registrationJson.put("fcmToken", preferences.getString(Util.USER_FCM_TOKEN, ""));
					}catch (Exception e){
						e.printStackTrace();
					}

					new UserRegistrationAsycTask().execute(registrationJson);
				} else {
					Toast.makeText(this, "Password did not matched", Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(this, "Please fill all the boxes", Toast.LENGTH_SHORT).show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String GetCountryZipCode(){
		String CountryID="";
		String CountryZipCode="";

		TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
		//getNetworkCountryIso
		CountryID= manager.getSimCountryIso().toUpperCase();
		String[] rl=this.getResources().getStringArray(R.array.CountryCodes);
		for(int i=0;i<rl.length;i++){
			String[] g=rl[i].split(",");
			if(g[1].trim().equals(CountryID.trim())){
				CountryZipCode=g[0];
				break;
			}
		}
		return CountryZipCode;
	}

	/**
	 * Uploading asynchronous task
	 */
	private class UserRegistrationAsycTask extends AsyncTask<JSONObject, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialogBox.showDailog();
		}

		@Override
		protected String doInBackground(JSONObject... cred) {

			return new UserRestService().registration(cred[0]);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if(dialogBox.isShowing()) {
				dialogBox.dismissDialog();
			}
			if(result!=null) {

				try {
					JSONObject userLoginObj = new JSONObject(result);

					if (userLoginObj != null && userLoginObj.getBoolean("success")) {
						if(userLoginObj.has("exception")) {
							Util.showAlert("Registration failed. "+userLoginObj.getString("exception"), ActivityRegister.this);
							return;
						}

						JSONObject jsonObject = new JSONObject(userLoginObj.getString("result"));
						String userID = jsonObject.getString("id");
						String username = jsonObject.getString("name");
						String userTopic = jsonObject.getString("topic");

						if(userID!=null && !userID.isEmpty()) {
							System.out.println("\n" + userID.toString());
							editor.putString(Util.USERNAME_PREFERENCE, username);
							editor.putString(Util.USER_PREFERENCE, userID);
							editor.putString(Util.USER_TOPIC, userTopic);
							editor.commit();

							User user = new User();
							user.setId(preferences.getString(Util.USER_PREFERENCE, null));
							user.setFirstName(name.getText().toString());
							user.setEmailId(email.getText().toString());
							user.setPhoneNumber(phone.getText().toString());
							UserRepo userRepo = new UserRepo(getApplicationContext(), User.class);
							userRepo.save(user);
						}

						Toast.makeText(ActivityRegister.this, "User registered successfully", Toast.LENGTH_LONG).show();
						editor.putBoolean(Util.IS_LOGGED_IN, true).commit();
						ActivityRegister.this.finish();
						Intent intent =  new Intent(ActivityRegister.this, DashboardActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						startActivity(intent);

					} else if(userLoginObj!=null && !userLoginObj.getBoolean("success")){
						Util.showAlert(userLoginObj.getString("message"), ActivityRegister.this);
					}
				} catch (JSONException e) {
					e.printStackTrace();
					Util.showAlert("Registration failed. Please try again", ActivityRegister.this);
				}
			} else {
				Util.showAlert("Registration failed. Please try again", ActivityRegister.this);
			}
		}
	}

	public void goToLogin(View v){
		this.finish();
		startActivity(new Intent(ActivityRegister.this, ActivityLogin.class));
	}

	@Override
	public void onDestroy(){
		super.onDestroy();
	}
}
