package com.android.kycar.carhealth.exceptions;

/**
 * Created by Krishna on 1/30/2016.
 */
public class NetworkException extends Exception {

    public NetworkException() {
        // TODO Auto-generated constructor stub
    }

    public NetworkException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public NetworkException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public NetworkException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }
}
