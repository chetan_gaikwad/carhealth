package com.android.kycar.carhealth.config;

import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.utils.CustomDialogClass;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by 638 on 11/22/2015.
 */
public class Util {

    public static final String DISTANCE_COVERED = "distance_covered";
    public static final String TEMP_SHARE_USER = "temp_share_location";
    public static final String IS_SHARE_LOC = "is_share_location";
    public static final String USER_TOPIC = "user_topic";
    public static final String USER_FCM_TOKEN = "user_token";
    public static final String PROFILE_PREF = "profile_pic";
    public static final String LOG = "nxtgizmo_log";
    public static final String USER_PREFERENCE = "user_id";
    public static final String USERNAME_PREFERENCE = "usernamep";
    public static final String CARNAME_PREFERENCE = "carnamep";
    public static final String CAR_PREFERENCE = "car_id";
    public static final String CONVOY_NAME = "convoy_name";
    public static final String CONVOY_ID = "convoy_id";
    public static final String IS_CONVOY = "is_convoy";
    public static final String IS_FROM_SELECT_CAR = "is_from_select_car";
    public static final String IS_FROM_CONVOY = "is_from_convoy";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String IS_DRIVE_MODE = "drive_mode";
    public static final String IS_DEFAULT_DATA_LOADED = "is_default_data_loaded";
    public static final String TRIP_ID = "default trip name";
    public static final String TRIP_SESSION_ID = "default trip session name";

    public static final String IS_UPLOAD_RC = "upload_road_condition";
    public static final String IS_UPLOAD_DB = "upload_driver_behaviour";
    public static final String IS_UPLOAD_LOC = "upload_location";
    public static final String IS_UPLOAD_OBD = "upload_obd_data";
    public static final String VIN_PREFERENCE = "vin";
    public static final String GEAR_PREF = "gear_pref";
    public static final String CAR_TYPE = "Car_Type";
    public static final String DEFAULT_HEADER = "default_ecu_header";
    public static final String DEFAULT_PROTOCOL = "default_protocol";
    public static final String DEFAULT_SESSION_ID = "defaultsessonid";

    public static final int isNormal = 1;
    public static final int isCan = 2;
    public static final int isCrud = 3;
    public static final String CLOUD_UPLOAD = "cloud_upload";
    public static final String LOCK_WIDGET_ENABLE = "lock_widget";
    public static final String SENSOR_CALIBRATION_VALUE_X = "sensor_calibrated_value_x";
    public static final String SENSOR_CALIBRATION_VALUE_Y = "sensor_calibrated_value_y";
    public static final String SENSOR_CALIBRATION_VALUE_Z = "sensor_calibrated_value_z";
    public static final String SENSOR_ACC_NAME = "acceleration_sensor_name";
    public static final String SENSOR_ID = "sensor_id";
    public static final String MEDIATEK_PREFERENCE = "mediatek_mac";
    private static boolean result;

    public static String getTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

    public static String getTimeDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

    public static long getTimeLong() {
        Calendar c = Calendar.getInstance();
        return c.getTime().getTime();
    }

    public static void showAlert(String msg, Context ctx){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ctx);

        // set title
        alertDialogBuilder.setTitle("Car Health");

        // set dialog message
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
        ;

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public static String getTimeMicro() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

    public static void writeToFile(Context context, String data) {
        try {
            File myFile = new File("/sdcard/config.txt");
            if(!myFile.exists()) {
                myFile.createNewFile();
            }
            FileOutputStream fOut = new FileOutputStream(myFile,true);
            OutputStreamWriter myOutWriter =
                    new OutputStreamWriter(fOut);
            myOutWriter.append("\n"+data);
            myOutWriter.close();
            fOut.close();
            Log.d(LOG,"Done writing SD 'mysdfile.txt'");
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }
    public static void writeToFile(String data) {
        try {
            File myFile = new File("/sdcard/Automatic_fix.txt");
            if(!myFile.exists()) {
                myFile.createNewFile();
            }
            FileOutputStream fOut = new FileOutputStream(myFile,true);
            OutputStreamWriter myOutWriter =
                    new OutputStreamWriter(fOut);
            myOutWriter.append("\n"+data);
            myOutWriter.close();
            fOut.close();
            Log.d(LOG,"Done writing SD 'mysdfile.txt'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());
    }

    public static void showDismissalDialog(Context activity){

        final CustomDialogClass cdd=new CustomDialogClass(activity,"speed limit alert", R.drawable.speed_limit_alert);
        cdd.show();

        // Hide after some seconds
        final Handler handler  = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (cdd.isShowing()){
                    cdd.dismiss();
                }
            }
        };
        handler.postDelayed(runnable, 500);
        /*
        NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(activity)
                        .setContentTitle("")
                        .setContentText("Event added successfully");

        Intent notificationIntent = new Intent(activity, DashboardActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(activity, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(contentIntent);
        builder.setAutoCancel(true);
        builder.setLights(Color.BLUE, 500, 500);
        builder.setStyle(new NotificationCompat.InboxStyle());

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);

        // Add as notification
        NotificationManager manager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(1, builder.build());*/
    }

    public static float findDistance(Location src, Location dest) {
        return (dest.distanceTo(src)/1000);
    }
}
