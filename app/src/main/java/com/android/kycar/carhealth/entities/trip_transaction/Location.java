package com.android.kycar.carhealth.entities.trip_transaction;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 1/16/2016.
 */
@RealmClass
public class Location extends RealmObject {

    @PrimaryKey
    private String id;

    private double latitude;

    private double longitude;

    private String event;

    private float bearing;

    public Location() {
    }

    public Location(String id, double latitude, double longitude, String event, float bearing) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.event = event;
        this.bearing = bearing;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public float getBearing() {
        return bearing;
    }

    public void setBearing(float bearing) {
        this.bearing = bearing;
    }
}
