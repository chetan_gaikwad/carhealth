package com.android.kycar.carhealth.ui.activity.drawer_menu.my_cars;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.vehicle_master.GearRatio;
import com.android.kycar.carhealth.repositories.vehicle.GearRatioRepo;
import com.android.kycar.carhealth.services.dto.commands.SpeedCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.RPMCommand;
import com.android.kycar.carhealth.services.dto.enums.AvailableCommandNames;
import com.android.kycar.carhealth.services.dto.utils.RPMTest;
import com.android.kycar.carhealth.services.dto.utils.SpeedTest;
import com.android.kycar.carhealth.services.obd_services.ObdCommandJob;
import com.android.kycar.carhealth.utils.BusProvider;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateGearRatioActivity extends AppCompatActivity {

    TextView tvMinGearRatio;
    TextView tvMaxGearRatio;

    protected SharedPreferences preferences;
    protected SharedPreferences.Editor editor;

    float maxGearRatioValue;
    float minGearRatioValue;

    EditText etMinRatio;

    EditText etMaxRatio;

    Spinner spGearNumber;

    private String variantId;

    private Map<String, String> commandResult = new HashMap<String, String>();
    private final String KEY_RPM = "rpm";
    private final String KEY_SPEED = "SPEED";

    private GearRatioRepo gearRatioRepo;

    List<Double> list;

    private File file;
    private FileOutputStream fileOutputStream;
    private PrintWriter pw;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate_gear_ratio);

        preferences = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        editor = preferences.edit();

        try {
            BusProvider.getInstance().register(this);
        }catch (Exception e){
            e.printStackTrace();
        }
        etMaxRatio = (EditText) findViewById(R.id.etMaxRatio);
        etMinRatio = (EditText) findViewById(R.id.etMinRatio);
        spGearNumber = (Spinner) findViewById(R.id.spGearNumber);

        tvMaxGearRatio = (TextView) findViewById(R.id.tvMaxGearRatio);
        tvMinGearRatio = (TextView) findViewById(R.id.tvMinGearRatio);

        maxGearRatioValue = -1;
        minGearRatioValue = -1;

        list = new ArrayList<Double>();

        spGearNumber.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                tvMaxGearRatio.setText(""+-1);
                tvMinGearRatio.setText(""+-1);
                minGearRatioValue = -1;
                maxGearRatioValue = -1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        variantId = getIntent().getStringExtra("variantId");

        ArrayList<String> gears = new ArrayList<>();
        gears.add("-1");
        gears.add("0");
        gears.add("1");
        gears.add("2");
        gears.add("3");
        gears.add("4");
        gears.add("5");
        gears.add("6");
        Spinner spinner = (Spinner) findViewById(R.id.spGearNumber);

        setSpinnerAdapter(spinner, gears);
        gearRatioRepo = new GearRatioRepo(getApplicationContext(), GearRatio.class);

        File root = Environment.getExternalStorageDirectory();
        File dir = new File(root.getAbsolutePath() + "/download");
        dir.mkdirs();
        file = new File(dir, "logs.txt");
        try {
            fileOutputStream = new FileOutputStream(file);
            pw = new PrintWriter(fileOutputStream);

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setSpinnerAdapter(Spinner spinner, ArrayList<String> list){

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(CalculateGearRatioActivity.this, android.R.layout.simple_spinner_dropdown_item, list);
        spinner.setAdapter(adapter);
    }

    public void onSaveClick(View view){

        Log.d("In method", "onSaveClick variant id"+variantId);
        if(variantId == null){
            Toast.makeText(this, "Please add Car for calculation", Toast.LENGTH_LONG).show();
            return;
        }
        //String variantId = realm.where(Vehicle.class).equalTo("id", vehicleId).findFirst().getVerientId();
        Integer gear = Integer.parseInt(spGearNumber.getSelectedItem().toString());
        GearRatio gearRatio = gearRatioRepo.findByGearNumAndVariantId(gear, variantId);
        GearRatio newGearRatio = new GearRatio();

        if(gearRatio != null){
            newGearRatio.setId(gearRatio.getId());
        } else {
            newGearRatio.setId(new Date().toString());
        }

        newGearRatio.setGear(gear);
        newGearRatio.setVariantId(variantId);
        newGearRatio.setGearRatioMax(Double.parseDouble(etMaxRatio.getText().toString()));
        newGearRatio.setGearRatioMin(Double.parseDouble(etMinRatio.getText().toString()));

        Log.d("Data", "" + etMaxRatio.getText());
        if(etMaxRatio.getText().equals("") || etMinRatio.getText().equals("")){
            return;
        }
        gearRatioRepo.saveOrUpdate(newGearRatio);


        /** fetching all data from the db*/
        Log.d("Variant Id", "" + variantId);
        List<GearRatio> gearRatios = gearRatioRepo.findAll();
        for (GearRatio gearRatio1 : gearRatios){
            Log.d("Gear ratio", ""+gearRatio1.getVariantId()+ ", Gear No : " + gearRatio1.getGear() +", Max : "+ gearRatio1.getGearRatioMax() +", Min : "+ gearRatio1.getGearRatioMin());
        }
    }

    @Subscribe
    public void stateUpdate(ObdCommandJob job) {
        final String cmdName = job.getCommand().getName();
        String cmdResult = "";
        final String cmdID = LookUpCommand(cmdName);

        if (job.getState().equals(ObdCommandJob.ObdCommandJobState.EXECUTION_ERROR)) {
            cmdResult = job.getCommand().getResult();
            if (cmdResult != null) {
            }
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.NOT_SUPPORTED)) {
            cmdResult = getString(R.string.status_obd_no_support);
        } else {
            cmdResult = job.getCommand().getFormattedResult();
        }

        updateTripStatistic(job, cmdID);
    }

    private void updateTripStatistic(final ObdCommandJob job, final String cmdID) {

        if (cmdID.equals(AvailableCommandNames.SPEED.toString())) {

            final SpeedCommand command = (SpeedCommand) job.getCommand();
            Log.d("Cartest", "Speed " + command.getMetricSpeed());

            CalculateGearRatioActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //speed.setText(command.getMetricSpeed() + "");
                }
            });

            SpeedTest speedTest = new SpeedTest();
            speedTest.setSpeed(command.getMetricSpeed());
            speedTest.setTimeStamp(Util.getTime());

            commandResult.put(KEY_SPEED, (command.getMetricSpeed()) + "");

        } else if (cmdID.equals(AvailableCommandNames.ENGINE_RPM.toString())) {
            final RPMCommand command = (RPMCommand) job.getCommand();
            commandResult.put(KEY_RPM, command.getRPM() + "");
            CalculateGearRatioActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //rpm.setText(command.getRPM() + "");
                }
            });

            RPMTest rpmTest = new RPMTest();
            Log.d("Cartest","rpm "+command.getRPM());
            rpmTest.setRpm(command.getRPM());
            rpmTest.setTimeStamp(Util.getTime());
        }

        if(commandResult.get(KEY_RPM)!=null &&
                commandResult.get(KEY_SPEED)!=null &&
                (Integer.parseInt(commandResult.get(KEY_SPEED)))>0 ){
            final float gr = (float)(Integer.parseInt(commandResult.get(KEY_RPM))) /
                    (Integer.parseInt(commandResult.get(KEY_SPEED)));
            Log.i("cartest obd", "(RPM/SPEED) - " + gr);
            CalculateGearRatioActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    displayMinMaxGearRatio(gr);
                }
            });
        }
    }

    private void displayMinMaxGearRatio(float gearRatio){

        if(maxGearRatioValue == -1 || maxGearRatioValue < gearRatio){
            maxGearRatioValue = gearRatio;
            tvMaxGearRatio.setText(""+gearRatio);
        } else if(minGearRatioValue == -1 || minGearRatioValue > gearRatio){
            minGearRatioValue = gearRatio;
            tvMinGearRatio.setText(""+gearRatio);
        }

        checkExternalMedia();
        list.add(new Double(gearRatio));
        writeToSDFile(gearRatio);
        //readRaw();

    }

    public static String LookUpCommand(String txt) {
        for (AvailableCommandNames item : AvailableCommandNames.values()) {
            if (item.getValue().equals(txt)) return item.name();
        }
        return txt;
    }

    private void checkExternalMedia(){
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // Can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // Can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Can't read or write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
        /*tv.append("\n\nExternal Media: readable="
                +mExternalStorageAvailable+" writable="+mExternalStorageWriteable);*/
    }

    /** Method to write ascii text characters to file on SD card. Note that you must add a
     WRITE_EXTERNAL_STORAGE permission to the manifest file or this method will throw
     a FileNotFound Exception because you won't have write permission. */

    private void writeToSDFile(float gearRatio){

       // Find the root of the external storage.
        // See http://developer.android.com/guide/topics/data/data-  storage.html#filesExternal

        try {

            pw.println(new Date()+","+gearRatio);
            pw.println();
            pw.flush();
        }  catch (Exception e){
            e.printStackTrace();
        }
        //tv.append("\n\nFile written to "+file);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        /*long totalEntries = list.size();
        Log.d("totalEntries ->", ""+totalEntries);
        Map<Double, Integer> occurenceMap  = new HashMap<Double, Integer>();
        for (Double entry : list){
            Integer freq = (Integer)occurenceMap.get(entry);
            occurenceMap.put(entry, (freq == null) ? 1 : freq + 1);
        }

        Log.d("Size occurenceMap", "" + occurenceMap.size());

        ArrayList<Double> filteredList = new ArrayList<Double>();
        for (Double key : occurenceMap.keySet()) {
            //System.out.println("Val => " + key + " freq =>"+(Integer)occurenceMap.get(key));
            if((Integer)occurenceMap.get(key) > totalEntries+0.1){
                filteredList.add((Double)key);
            }
        }

        ArrayList<ArrayList<Double>> gearRanges = new ArrayList<ArrayList<Double>>();
        ArrayList<Double> gearNumber = new ArrayList<Double>();

        for (int i = 0; i < filteredList.size()-1; i++) {

            Log.d("Filted list ", ""+filteredList.get(i));
            if((filteredList.get(i+1) - filteredList.get(i) ) < 5){
                gearNumber.add(filteredList.get(i));
                gearNumber.add(filteredList.get(i+1));
            } else {
                gearRanges.add(gearNumber);
                gearNumber = new ArrayList<Double>();
            }
        }
        gearRanges.add(gearNumber);
        Log.d("Number of gears ===>" , ""+ gearRanges.size());

        for(int index = 0 ; index< gearRanges.size(); index ++){
            ArrayList<Double> gearRange = gearRanges.get(index);
            for (int index2 = 0 ; index < gearRange.size(); index2 ++){
                GearRatio gearRatio = gearRatioRepo.findByGearNumAndVariantId(index + 1, variantId);
                if(gearRatio == null){
                    gearRatio = new GearRatio( new Date().toString() , index+1,  variantId,  gearRange.get(0),  gearRange.get(gearRange.size() - 1));
                } else {
                    gearRatio.setGear(index + 1);
                    gearRatio.setVariantId(variantId);
                    gearRatio.setGearRatioMax(gearRange.get(gearRange.size() - 1));
                    gearRatio.setGearRatioMin(gearRange.get(0));
                }
                gearRatioRepo.saveOrUpdate(gearRatio);
            }
        }

        for (ArrayList<Double> gearRange : gearRanges){

            for (Double d : gearRange){
                Log.d("values", ""+ d);
            }
            //Log.d("Max", ""+ gearRange.get(gearRange.size() - 1));
            //Log.d("Min", "" + gearRange.get(0));
        }
        *//** Finding saved data *//*
        List<GearRatio> gearRatios = gearRatioRepo.findAll();
        for (GearRatio gearRatio : gearRatios){
            Log.d("Gear Ration ==>", gearRatio.getVariantId()+", Gear-"+gearRatio.getGear() +", Min-"+gearRatio.getGearRatioMin()+", Max-"+gearRatio.getGearRatioMax());
        }

        pw.close();
        try {
            fileOutputStream.close();
        } catch (Exception e){
            e.printStackTrace();
        }*/
    }
}
