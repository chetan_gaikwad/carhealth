package com.android.kycar.carhealth.ui.fragments;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.kycar.carhealth.NxtApplication;
import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.beans.MapMarkerBean;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.trip_transaction.Trip;
import com.android.kycar.carhealth.repositories.trip.LocationRepo;
import com.android.kycar.carhealth.repositories.trip.TripRepo;
import com.android.kycar.carhealth.services.connection_services.mqtt_paho.PublishCoods;
import com.android.kycar.carhealth.services.connection_services.mqtt_paho_android_service.Connection;
import com.android.kycar.carhealth.services.utils.Logger;
import com.android.kycar.carhealth.services.utils.LoggerTxt;
import com.android.kycar.carhealth.ui.activity.DashboardActivity;
import com.android.kycar.carhealth.utils.BusProvider;
import com.android.kycar.carhealth.utils.CustomDialogPojo;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.otto.Subscribe;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chetan on 7/29/2015.
 */
public class MapFragment extends Fragment implements LocationListener, OnMapReadyCallback {

    private LocationRepo locationRepo;
    private List<Location> locations;
    private LoggerTxt tripCoords;
    private Logger loggerCoords;
    private GoogleMap mMap;
    private PublishCoods publishCoods;
    private Location mLastLocation = null;
    private MapView mMapView;
    private TextView mLatLong;
    private TextView dirs;
    private TextView speed;
    private SharedPreferences preferences;
    private SensorManager sensorManager;
    private Sensor orientSensor = null;
    private ArrayList<Marker> markers = new ArrayList<>();
    private Marker defaultmarker;
    private Location previousLocation;

    SensorEventListener orientListener = new SensorEventListener() {

        public void onSensorChanged(SensorEvent event) {
            float x = event.values[0];
            String dir = "";
            if (x >= 337.5 || x < 22.5) {
                dir = "N";
            } else if (x >= 22.5 && x < 67.5) {
                dir = "NE";
            } else if (x >= 67.5 && x < 112.5) {
                dir = "E";
            } else if (x >= 112.5 && x < 157.5) {
                dir = "SE";
            } else if (x >= 157.5 && x < 202.5) {
                dir = "S";
            } else if (x >= 202.5 && x < 247.5) {
                dir = "SW";
            } else if (x >= 247.5 && x < 292.5) {
                dir = "W";
            } else if (x >= 292.5 && x < 337.5) {
                dir = "NW";
            }
            dirs.setText("Direction : "+dir+" ");
        }
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        container = (ViewGroup) inflater.inflate(R.layout.map_layout, null);
        preferences = PreferenceManager.getDefaultSharedPreferences(container.getContext());
        publishCoods = new PublishCoods();
        loggerCoords = new Logger("coords.csv");
        List<String> headers = new ArrayList<>();
        headers.add("timestamp");
        headers.add("lat");
        headers.add("long");
//        loggerCoords.setHeader(headers);

        mMapView = (MapView) container.findViewById(R.id.mapView);

        dirs = (TextView)container.findViewById(R.id.dir);
        speed = (TextView)container.findViewById(R.id.speed);
        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        // get Orientation sensor
        List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ORIENTATION);
        if (sensors.size() > 0)
            orientSensor = sensors.get(0);

        mLatLong = (TextView)container.findViewById(R.id.lat_long);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();// needed to get the map to display immediately
        sensorManager.registerListener(orientListener, orientSensor,
                SensorManager.SENSOR_DELAY_UI);
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(getActivity()
                    , Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initUI();
                    }
                });
            }
        }else{
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    initUI();
                }
            });
        }

        if(getArguments() != null) {
            String title = getArguments().getString("title");
            Log.d(MapFragment.class.getSimpleName(), "Got data");
        }

        /** Fetch location  */

        locationRepo = new LocationRepo(getContext(), Location.class);
//        locations = locationRepo.findAll();

        return container;
    }

    private void initUI() {
        mMapView.getMapAsync(this);
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onProviderDisabled(String provider) {
    }

    public void onLocationChanged(Location location) {

        //updateConvoy();

        NxtApplication.currLocation = location;


        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mLatLong.setText("Latitude : " + String.format("%.6f", location.getLatitude()) +
                "  Altitude : " + location.getAltitude() +
                "\nLongitude : " + String.format("%.6f", location.getLongitude())+"" +
                "  Accuracy : "+location.getAccuracy());

        speed.setText(" Speed : "+location.getSpeed());
        mLastLocation = location;

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        /** Publishing data for location sharing */

        if(!NxtApplication.shareLocationUserTopics.isEmpty()) {
            MqttMessage message = publishCoods.getLocationShareCoordData(preferences.getString(Util.USERNAME_PREFERENCE, ""), location);
            for ( String topic : NxtApplication.shareLocationUserTopics ) {
                try {
                    Connection.getClient().publish(NxtApplication.baseTopic+"/"+topic, message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        /** Checking distance */

        if(previousLocation == null) {
            previousLocation = location;
        }

        if(location.distanceTo(previousLocation) > 250) {

            if( preferences.getBoolean(Util.IS_UPLOAD_LOC, true) && preferences.getBoolean(Util.IS_LOGGED_IN, false) && preferences.contains(Util.TRIP_ID)) {

                if(tripCoords == null) {
                    TripRepo tripRepo = new TripRepo(getContext(), Trip.class);
                    Trip trip = (Trip) tripRepo.findById(preferences.getString(Util.TRIP_ID, ""));
                    tripCoords = new LoggerTxt("trip", trip.getTripName());
                }
                JSONObject loc = new JSONObject();
                try {
                    loc.put("la", location.getLatitude());
                    loc.put("lo", location.getLongitude());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                tripCoords.log(loc.toString());
                Log.d(MapFragment.class.getSimpleName(), "Trip data sent distance:-"+location.distanceTo(previousLocation));


                /*if(Util.isOnline(getContext())) {
                    MqttMessage message = publishCoods.getCoordData(location, preferences.getString(Util.TRIP_ID, null));
                    try {
                        Connection.getClient().publish(NxtApplication.baseTopic+"/coords_data/"+preferences.getString(Util.USER_PREFERENCE, ""), message);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    *//** Local file system logging *//*
                    //loggerCoords.logging(publishCoods.getCoordData(location).toString());
                }*/
            }
            previousLocation = location;
        }

        /*for (com.nxt.autoz.entities.trip_transaction.Location loc: locations) {
            float dis = findDistance(loc, NxtApplication.currLocation);
            if(dis < 50) {
                showEvent(new CustomDialogPojo(10), "Bad road ahead "+dis+" meter");
            }
        }*/
    }

    @Subscribe
    public void showEvent(CustomDialogPojo event) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Util.showDismissalDialog(getActivity());
            }
        });
    }


    private void setUpMapIfNeeded() throws SecurityException {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            ((SupportMapFragment) DashboardActivity.mFragmentManager
                    .findFragmentById(R.id.map))
                    .getMapAsync(this);

            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                mMap.setTrafficEnabled(true);
            }
        }

        // mMap = supportMapFragment.getMap();
        mMap.setMyLocationEnabled(true);
        NxtApplication.currLocation = mMap.getMyLocation();

        LocationManager locationManager = (LocationManager) getActivity()
                .getSystemService(getActivity().LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        if (ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            String bestProvider = locationManager.getBestProvider(criteria, true);

            NxtApplication.currLocation = locationManager.getLastKnownLocation(bestProvider);
            /*if (location != null && NxtApplication.mqttAndroidClient!=  null) {
                onLocationChanged(location);
            }*/

            /** Setting GPS sensitivity */
            locationManager.requestLocationUpdates(bestProvider, 50, 10, MapFragment.this);
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    /*private void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }*/

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
        mMapView.onPause();
    }

    @Subscribe
    public void a2DpControl(String event) {
        Log.i("dsk", "Got a2dp event in map");
        ((DashboardActivity)getActivity()).onAudInput(event,1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        loggerCoords.finalizeFile();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Subscribe
    public void getUserContactLocationChanged(MapMarkerBean mapMarkerBean) {

        //Toast.makeText(this.getContext(), "Message arrived "+mapMarkerBean.getLatitude()+" "+mapMarkerBean.getLongitude(), Toast.LENGTH_SHORT).show();

        if(mapMarkerBean.isSessionEnd()) {
            for (Marker marker : markers) {
                if(marker.getTitle().equals(mapMarkerBean.getMarkerTile())) {
                    marker.setVisible(false);
                    markers.remove(marker);
                }
            }
            return;
        } if(mapMarkerBean.isSessionStart()) {
            userNotification(mapMarkerBean);

            for (Marker marker : markers) {
                if(marker.getTitle().equals(mapMarkerBean.getMarkerTile())) {
                    marker.setPosition(new LatLng(mapMarkerBean.getLatitude(), mapMarkerBean.getLongitude()));
                    return;
                }
            }
            Marker newMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(mapMarkerBean.getLatitude(), mapMarkerBean.getLongitude())).title(mapMarkerBean.getMarkerTile()));
            markers.add(newMarker);

        } if(mapMarkerBean.isAccidentMarker()) {
            userNotification(mapMarkerBean);
            Marker newMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(mapMarkerBean.getLatitude(), mapMarkerBean.getLongitude())).title(mapMarkerBean.getMarkerTile()));
            markers.add(newMarker);
        } else {
            for (Marker marker : markers) {
                if(marker.getTitle().equals(mapMarkerBean.getMarkerTile())) {
                    marker.setPosition(new LatLng(mapMarkerBean.getLatitude(), mapMarkerBean.getLongitude()));
                    return;
                }
            }

            Marker newMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(mapMarkerBean.getLatitude(), mapMarkerBean.getLongitude())).title(mapMarkerBean.getMarkerTile()));
            markers.add(newMarker);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(MapFragment.class.getSimpleName(), "Map is ready to use");
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        LocationManager locationManager = (LocationManager) getActivity()
                .getSystemService(getActivity().LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        if (ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            String bestProvider = locationManager.getBestProvider(criteria, true);
            Location location = locationManager.getLastKnownLocation(bestProvider);
            if (location != null) {
                onLocationChanged(location);
                NxtApplication.currLocation = location;
            }
            locationManager.requestLocationUpdates(bestProvider, 0, 0, MapFragment.this);
        }
        setUpMapIfNeeded();
    }

    private void userNotification(MapMarkerBean mapMarkerBean) {

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext());
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        if(mapMarkerBean.isAccidentMarker()) {
            mBuilder.setContentTitle(mapMarkerBean.getMarkerTile());
        } else {
            mBuilder.setContentTitle(mapMarkerBean.getMarkerTile()+" wants to travels connected");
        }
        mBuilder.setContentText("Click to view on your map");

        Intent myIntent = new Intent(getContext(), MapFragment.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                getContext(),
                0,
                myIntent,
                Intent.FILL_IN_ACTION);

        mBuilder.setContentIntent(pendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        // notificationID allows you to update the notification later on.
        mNotificationManager.notify(0, mBuilder.build());
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);

        if(getActivity()!= null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("Markers ", markers.size()+"");
                    for (Marker marker : markers) {
                        marker.setPosition(marker.getPosition());
                    }

                    try {
                        if(((DashboardActivity)getActivity()) != null) {
                            Location location =  ((DashboardActivity)getActivity()).showLocation();
                            if(location != null) {

                                Log.d(MapFragment.class.getSimpleName(), "Got location marker lat:-"+location.getLatitude()+" long:-"+location.getLongitude());
                                defaultmarker = mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title(location.getProvider()));
                            } else {
                                if(defaultmarker != null){
                                    defaultmarker.remove();
                                }
                            }
                        } else {
                            defaultmarker.remove();
                        }
                        Log.d(MapFragment.class.getSimpleName(), "Map Visible ");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
