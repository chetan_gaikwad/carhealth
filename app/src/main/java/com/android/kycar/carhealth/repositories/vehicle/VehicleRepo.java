package com.android.kycar.carhealth.repositories.vehicle;

import android.content.Context;


import com.android.kycar.carhealth.entities.Vehicle;
import com.android.kycar.carhealth.repositories.Repository;

import java.util.List;

import io.realm.exceptions.RealmException;

/**
 * Created by Krishna on 2/1/2016.
 */
public class VehicleRepo extends Repository {

    public VehicleRepo(Context context, Class _aClass) {
        super(context, _aClass);
    }

    public List<Vehicle> findAllActive() throws RealmException {

        List<Vehicle> vehicles = realm.where(Vehicle.class).equalTo("isActive", true).findAll();
        return vehicles;
    }

    public List<Vehicle> findAllUnsync() throws RealmException {

        List<Vehicle> vehicles = realm.where(Vehicle.class).equalTo("isSync", false).findAll();
        return vehicles;
    }

    public List<Vehicle> findAllByUserId(String userId) throws RealmException {

        List<Vehicle> vehicles = realm.where(Vehicle.class).equalTo("isActive", true).equalTo("userId", userId).findAll();
        return vehicles;
    }

    public Vehicle findByVin(String vin) throws RealmException {
        return realm.where(Vehicle.class).equalTo("vin", vin).findFirst();
    }
}
