package com.android.kycar.carhealth.task;


import com.android.kycar.carhealth.utils.CarVinStates;

public interface OnTaskCompleted{
    void onTaskCompleted(Integer carType, CarVinStates carVinStates, String vin);
}