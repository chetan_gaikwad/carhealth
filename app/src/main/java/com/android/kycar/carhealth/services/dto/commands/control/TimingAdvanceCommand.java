package com.android.kycar.carhealth.services.dto.commands.control;


import com.android.kycar.carhealth.services.dto.commands.PercentageObdCommand;
import com.android.kycar.carhealth.services.dto.enums.AvailableCommandNames;

/**
 * Timing Advance
 */
public class TimingAdvanceCommand extends PercentageObdCommand {

    public TimingAdvanceCommand() {
        super("01 0E");
    }

    /**
     * @param other a {@link TimingAdvanceCommand} object.
     */
    public TimingAdvanceCommand(TimingAdvanceCommand other) {
        super(other);
    }

    @Override
    public String getName() {
        return AvailableCommandNames.TIMING_ADVANCE.getValue();
    }

}
