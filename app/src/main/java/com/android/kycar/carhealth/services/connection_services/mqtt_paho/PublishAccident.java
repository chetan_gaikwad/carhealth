package com.android.kycar.carhealth.services.connection_services.mqtt_paho;

import android.location.Location;
import android.util.Log;


import com.android.kycar.carhealth.beans.Coordinates;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krishna on 5/3/2016.
 */
public class PublishAccident {

    public MqttMessage getAccidentMessage(String userId, String vehicleId, Coordinates coords, float intensity, String tripId, int speed, String event, float gpsSpeed, float gpsBearing, int magBearing){

        try {
            JSONObject payload = new JSONObject();
            payload.put("car_id", vehicleId);
            payload.put("data", "accident_data");
            JSONObject accidentData = new JSONObject();
            if(!tripId.isEmpty()){
                accidentData.put("trip_id", tripId);
            }
            accidentData.put("user_id", userId);
            accidentData.put("intensity", intensity);
            accidentData.put("loc", coords.toJSON());
            accidentData.put("speed", speed);
            accidentData.put("event", event);
            accidentData.put("gps_speed", gpsSpeed);
            accidentData.put("gps_bearing", gpsBearing);
            accidentData.put("mag_bearing", magBearing);
            payload.put("accident_data", accidentData);

            Log.d(PublishAccident.class.getSimpleName(), payload.toString());

            MqttMessage message = new MqttMessage(payload.toString().getBytes());
            return message;
        } catch (JSONException jsonException){
            jsonException.printStackTrace();
        }
        return null;
    }


    public MqttMessage getAccidentMessage(String userName, Location location){

        try {
            JSONObject payload = new JSONObject();
            payload.put("nm", userName+ " Crashed");
            payload.put("acc", true);
            payload.put("la", location.getLatitude());
            payload.put("lo", location.getLongitude());
            payload.put("ac", location.getAccuracy());

            Log.d("coordsData", payload.toString());
            MqttMessage message = new MqttMessage(payload.toString().getBytes());
            return message;
        } catch (JSONException jsonException){
            jsonException.printStackTrace();
        }
        return null;
    }

    public MqttMessage getAccidentMessage(String userName, double lat, double lng, double acc){

        try {
            JSONObject payload = new JSONObject();
            payload.put("nm", userName+ " Crashed");
            payload.put("acc", true);
            payload.put("la", lat);
            payload.put("lo", lng);
            payload.put("ac", acc);

            Log.d("coordsData", payload.toString());
            MqttMessage message = new MqttMessage(payload.toString().getBytes());
            return message;
        } catch (JSONException jsonException){
            jsonException.printStackTrace();
        }
        return null;
    }

}
