package com.android.kycar.carhealth.services.connection_services.mqtt_paho;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.Nullable;

import com.android.kycar.carhealth.config.Util;


/**
 * Created by Krishna on 7/9/2016.
 */
public class SubscribeService extends Service {

    private SubscribeUserChannel subscribeUserChannel;
    private String topic;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @Override
    public void onCreate() {

        preferences = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        editor = preferences.edit();
        topic = preferences.getString(Util.USER_TOPIC, null);

        Log.d(SubscribeService.class.getSimpleName(), "Token Service started "+topic);
        this.subscribeUserChannel = new SubscribeUserChannel(topic, getApplicationContext());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
