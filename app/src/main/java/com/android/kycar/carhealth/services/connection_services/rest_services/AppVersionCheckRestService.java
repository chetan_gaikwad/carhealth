package com.android.kycar.carhealth.services.connection_services.rest_services;

import org.json.JSONObject;

/**
 * Created by Krishna on 10/25/2016.
 */
public class AppVersionCheckRestService extends RestService {

    public String checkAppVersion(JSONObject jsonObject) {

        String url = "/version/checkVersionIsLatest";
        return restServiceCall(url, "POST", jsonObject);
    }
}
