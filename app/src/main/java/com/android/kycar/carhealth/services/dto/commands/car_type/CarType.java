package com.android.kycar.carhealth.services.dto.commands.car_type;

/**
 * Created by 638 on 5/29/2016.
 */
public class CarType {
    private int carType;

    public int getCarType() {
        return carType;
    }

    public void setCarType(int carType) {
        this.carType = carType;
    }
}
