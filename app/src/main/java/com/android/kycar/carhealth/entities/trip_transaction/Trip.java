package com.android.kycar.carhealth.entities.trip_transaction;

import android.location.Location;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by krishna on 10/12/15.
 */
@RealmClass
public class Trip extends RealmObject {

    @PrimaryKey
    private String id;

    private String tripName;

    private long startTime;

    private long endTime;

    private String vehicleId;

    private String userId;

    private int status;

    private String startLocation;

    private double startLat,startLong,stopLat,stopLong;

    private int obdDistance;

    private int gpsDistance;

    private int engineRpmMax = 0;

    private int speed = 0;

    private String carID;

    private String stopLocation;

    private boolean isConvoy;

    private boolean isSync;

    private boolean isRouteDataAvailable;

    public Trip() {
    }

    public Trip(JSONObject tripJson) {

        try {
            if (tripJson.has("tripId")) { this.id = tripJson.getString("tripId"); }
            if (tripJson.has("startTime")) { this.startTime = tripJson.getLong("startTime"); }
            if (tripJson.has("distance")) { this.obdDistance = tripJson.getInt("distance"); }
            if (tripJson.has("stopLocation")) { this.stopLocation = tripJson.getString("stopLocation"); }
            if (tripJson.has("userId")) { this.userId = tripJson.getString("userId"); }
            if (tripJson.has("tripName")) { this.tripName = tripJson.getString("tripName"); }
            if (tripJson.has("startLocation")) { this.startLocation = tripJson.getString("startLocation"); }
            if (tripJson.has("endTime")) { this.endTime = tripJson.getLong("endTime"); }
            this.status = 0;

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public Trip(Trip trip) {

        if(trip == null) {
            return;
        }

        this.id = trip.getId();
        this.tripName = trip.getTripName();
        this.startTime = trip.getStartTime();
        this.endTime = trip.getEndTime();
        this.status = trip.getStatus();
        this.obdDistance = trip.getObdDistance();
        this.carID = trip.getCarID();
        this.vehicleId = trip.getVehicleId();
        this.engineRpmMax = trip.getEngineRpmMax();
        this.speed = trip.getSpeed();
        this.startLat = trip.getStartLat();
        this.stopLat = trip.getStopLat();
        this.startLong = trip.getStartLong();
        this.stopLong = trip.getStopLong();
        this.isConvoy = trip.isConvoy();
        this.userId = trip.getUserId();
        this.startLocation = trip.getStartLocation();
        this.stopLocation = trip.getStopLocation();
        this.isSync = trip.isSync();
        this.isRouteDataAvailable = trip.isRouteDataAvailable();
    }

    public Trip(String id, String tripName, long startTime, long endTime, String vehicleId, String userId, int status, Location startLocation, Location stopLocation, Boolean isConvoy, int obdDistance) {
        this.id = id;
        this.tripName = tripName;
        this.startTime = startTime;
        this.endTime = endTime;
        this.vehicleId = vehicleId;
        this.userId = userId;
        this.status = status;
        this.isConvoy = isConvoy;
        this.obdDistance = obdDistance;
        this.isRouteDataAvailable = false;
    }

    public boolean isRouteDataAvailable() {
        return isRouteDataAvailable;
    }

    public void setRouteDataAvailable(boolean routeDataAvailable) {
        isRouteDataAvailable = routeDataAvailable;
    }

    public boolean isConvoy() {
        return isConvoy;
    }

    public void setConvoy(boolean convoy) {
        isConvoy = convoy;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getStopLocation() {
        return stopLocation;
    }

    public void setStopLocation(String stopLocation) {
        this.stopLocation = stopLocation;
    }

    public int getEngineRpmMax() {
        return engineRpmMax;
    }

    public void setEngineRpmMax(int engineRpmMax) {
        this.engineRpmMax = engineRpmMax;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getId() {
        return id;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getObdDistance() {
        return obdDistance;
    }

    public void setObdDistance(int obdDistance) {
        this.obdDistance = obdDistance;
    }

    public String getCarID() {
        return carID;
    }

    public void setCarID(String carID) {
        this.carID = carID;
    }

    public double getStartLat() {
        return startLat;
    }

    public void setStartLat(double startLat) {
        this.startLat = startLat;
    }

    public double getStartLong() {
        return startLong;
    }

    public void setStartLong(double startLong) {
        this.startLong = startLong;
    }

    public double getStopLat() {
        return stopLat;
    }

    public void setStopLat(double stopLat) {
        this.stopLat = stopLat;
    }

    public double getStopLong() {
        return stopLong;
    }

    public void setStopLong(double stopLong) {
        this.stopLong = stopLong;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public int getGpsDistance() {
        return gpsDistance;
    }

    public void setGpsDistance(int gpsDistance) {
        this.gpsDistance = gpsDistance;
    }

}
