package com.android.kycar.carhealth.async_task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.android.kycar.carhealth.entities.vehicle_master.GearRatio;
import com.android.kycar.carhealth.repositories.vehicle.GearRatioRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.FetchVehicleGearRatioRestCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Krishna on 2/22/2017.
 */

public class FetchVehicleGearRatioAsyncTask extends AsyncTask<String, Void, String> {

    private Context context;

    public FetchVehicleGearRatioAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... variantId) {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("variantId", variantId[0]);
            return new FetchVehicleGearRatioRestCall().getGearRatio(jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
            return "failed";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if(result!=null) {
            if(result!=null) {
                try {
                    JSONObject resObj = new JSONObject(result);
                    JSONArray gearRatios = resObj.getJSONArray("data");

                    GearRatioRepo gearRatioRepo = new GearRatioRepo(context, GearRatio.class);

                    for(int counter = 0; counter<gearRatios.length(); counter++) {

                        JSONObject gearRatio = (JSONObject) gearRatios.get(counter);
                        GearRatio gearRatioObj = new GearRatio(gearRatio);
                        gearRatioRepo.saveOrUpdate(gearRatioObj);
                    }

                    List<GearRatio> gearRatiosList = gearRatioRepo.findAll();
                    Log.d(FetchVehicleGearRatioAsyncTask.class.getSimpleName(), "Gear ratio size:-"+gearRatiosList.size());
                    for (GearRatio gearRatio :
                            gearRatiosList) {

                        Log.d(FetchVehicleGearRatioAsyncTask.class.getSimpleName(), "Gear ratio "+gearRatio.getGear());
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
