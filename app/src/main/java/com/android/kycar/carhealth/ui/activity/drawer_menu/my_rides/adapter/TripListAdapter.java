package com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.entities.Vehicle;
import com.android.kycar.carhealth.entities.trip_transaction.Trip;
import com.android.kycar.carhealth.repositories.trip.TripRepo;
import com.android.kycar.carhealth.repositories.vehicle.VehicleRepo;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides.DriveStatisticsActivity;
import com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides.ShowRouteActivity;

import java.util.Date;
import java.util.List;

public class TripListAdapter extends ArrayAdapter<Trip> {

    private final Activity activity;
    private final List<Trip> trips;
    private VehicleRepo vehicleRepo;
    private TripRepo tripRepo;
    private SharedPreferences preferences;
    private int lastPosition = -1;

    /**
     * DESCRIPTION:
     * Constructs an instance of TripListAdapter.
     *
     * @param activity - the Android Activity instance that owns the ListView.
     * @param trips  - the List of TripRecord instances for display in the ListView.
     */
    public TripListAdapter(Activity activity, List<Trip> trips) {
        super(activity, R.layout.row_trip_list, trips);
        this.activity = activity;
        this.trips = trips;
        vehicleRepo = new VehicleRepo(getContext(), Vehicle.class);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
    }

    /**
     * DESCRIPTION:
     * Constructs and populates a View for display of the TripRecord at the index
     * of the List specified by the position parameter.
     *
     * @see ArrayAdapter#getView(int, View, ViewGroup)
     */
    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        // create a view for the row if it doesn't already exist
        if (view == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            view = inflater.inflate(R.layout.row_trip_list, null);
        }

        // get widgets from the view
        TextView tvTripName = (TextView) view.findViewById(R.id.tv_trip_name);
        TextView tvStartTime = (TextView) view.findViewById(R.id.tv_start_time);
        TextView tvTripEndTime = (TextView) view.findViewById(R.id.tv_end_time);
        TextView tvTripDuration = (TextView) view.findViewById(R.id.tv_trip_duration);
        TextView tvTripDistance = (TextView) view.findViewById(R.id.tv_trip_distance);
        TextView tvCarName = (TextView) view.findViewById(R.id.tv_car_name);
        ImageView imgDelete = (ImageView) view.findViewById(R.id.image_delete);
        ImageView imgTripStats = (ImageView) view.findViewById(R.id.img_stats);
        ImageView imgCarPicture = (ImageView) view.findViewById(R.id.img_car_picture);
        ImageView imgTripRoute = (ImageView) view.findViewById(R.id.img_show_route);
        ImageView imgTripOnline = (ImageView) view.findViewById(R.id.img_trip_online);

        imgTripRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Intent intent = new Intent(getContext(), DriverBehaviourStats.class);
                Intent intent = new Intent(getContext(), ShowRouteActivity.class);
                intent.putExtra("tripId", trips.get(position).getId());
                activity.startActivity(intent);
            }
        });

        imgTripStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Intent intent = new Intent(getContext(), DriverBehaviourStats.class);
                Intent intent = new Intent(getContext(), DriveStatisticsActivity.class);
                intent.putExtra("tripId", trips.get(position).getId());
                activity.startActivity(intent);
            }
        });

        //Trip delete handle
        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(v.getContext())
                        .setTitle("Confirm")
                        .setMessage("Do you want delete this trip?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                tripRepo = new TripRepo(getContext(), Trip.class);
                                tripRepo.deleteById(trips.get(position).getId());

                                /*if (preferences.getString(Util.TRIP_ID, "").equals(trips.get(position).getId())) {
                                    preferences.edit().remove(Util.TRIP_ID).commit();
                                    preferences.edit().remove(Util.TRIP_SESSION_ID).commit();
                                }*/
                                tripRepo.close();
                                notifyDataSetChanged();
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });
        // populate row widgets from record data
        Trip trip = trips.get(position);
        tvTripName.setText(trip.getTripName());
        if(trip.getStatus() == 1) {
            imgTripOnline.setVisibility(View.VISIBLE);
        }

        if(trip.getStartTime() != 0 ){
            tvStartTime.setText(new Date(trip.getStartTime()).toString());
        }
        if(trip.getStatus() == 3 && trip.getEndTime() != 0) {
            tvTripEndTime.setText(new Date(trip.getEndTime()).toString());

            /** Calculating duration */
            long diff = (new Date(trip.getEndTime()).getTime()-new Date(trip.getStartTime()).getTime());

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            System.out.print(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");

            Log.d("Date difference:-", " Days:" + diffDays + " Hours:"+diffHours + " Minutes:" + diffMinutes + " Second:" + diffSeconds);
            StringBuilder dateDiff = new StringBuilder();

            if(diffDays > 0) {
                dateDiff.append(String.format("%02d", diffDays)+":");
            }
            if(diffHours > 0 ) {
                dateDiff.append(String.format("%02d", diffHours)+":");
            } else {
                dateDiff.append("00:");
            }
            if(diffMinutes > 0 ) {
                dateDiff.append(String.format("%02d", diffMinutes)+":");
            } else {
                dateDiff.append("00:");
            }
            if(diffSeconds > 0 ) {
                dateDiff.append(String.format("%02d", diffSeconds));
            } else {
                dateDiff.append("00");
            }
            tvTripDuration.setText(dateDiff.toString());
        }

        if(trip.getVehicleId() != null) {
            Vehicle vehicle  = (Vehicle) vehicleRepo.findById(trip.getVehicleId());
            if(vehicle != null) {
                tvCarName.setText((vehicle.getNickName() == null)? vehicle.getNickName():vehicle.getName());
                if(vehicle.getVehicleImage() != null && vehicle.getVehicleImage().length() > 0){
                    imgCarPicture.setImageBitmap(BitmapFactory.decodeFile(vehicle.getVehicleImage()));
                }
            }
        }

        String engineRuntime = null; //record.getEngineRuntime();
        if (engineRuntime == null)
            engineRuntime = "None";
        /*rowEngine.setText("Engine Runtime: " + engineRuntime + "\tMax RPM: " + rpmMax);
        rowOther.setText("Max speed: " + String.valueOf(record.getSpeed()));
        */
        if(trip.getObdDistance() > 0) {
            tvTripDistance.setText("Distance : " + trip.getObdDistance());
        }

        setAnimation(view, position);

        return view;
    }

    private String calcDiffTime(Date start, Date end) {
        long diff = end.getTime() - start.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        StringBuffer res = new StringBuffer();

        if (diffDays > 0)
            res.append(diffDays + "d");
        if (diffHours > 0) {
            if (res.length() > 0) {
                res.append(" ");
            }
            res.append(diffHours + "h");
        }
        if (diffMinutes > 0) {
            if (res.length() > 0) {
                res.append(" ");
            }
            res.append(diffMinutes + "m");
        }
        if (diffSeconds > 0) {
            if (res.length() > 0) {
                res.append(" ");
            }
            res.append(diffSeconds + "s");
        }
        return res.toString();
    }

    /**
     * DESCRIPTION:
     * Called by parent when the underlying data set changes.
     *
     * @see ArrayAdapter#notifyDataSetChanged()
     */
    @Override
    public void notifyDataSetChanged() {

        // configuration may have changed - get current settings
        //todo
        //getSettings();
        super.notifyDataSetChanged();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {

            Animation animation = AnimationUtils.loadAnimation(activity, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
