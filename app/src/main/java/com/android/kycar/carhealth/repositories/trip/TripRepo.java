package com.android.kycar.carhealth.repositories.trip;

import android.content.Context;
import android.util.Log;


import com.android.kycar.carhealth.entities.trip_transaction.Trip;
import com.android.kycar.carhealth.entities.trip_transaction.TripScore;
import com.android.kycar.carhealth.entities.trip_transaction.TripSession;
import com.android.kycar.carhealth.repositories.Repository;

import java.util.List;

import io.realm.RealmObject;
import io.realm.exceptions.RealmException;

/**
 * Created by Krishna on 2/1/2016.
 */
public class TripRepo extends Repository {

    private TripSessionRepo tripSessionRepo;
    private TripScoreRepo tripScoreRepo;
    private Context context;

    public TripRepo(Context context, Class _aClass) {
        super(context, _aClass);
        this.context = context;
    }

    public Trip save(Trip trip) {

        if(findAll().size() > 20) {
            deleteOldRecord();
        }
        realm.beginTransaction();
        trip = (Trip) realm.copyToRealm((RealmObject) trip);
        realm.commitTransaction();
        return trip;
    }

    private boolean deleteOldRecord() {

        List<Trip> trips = realm.where(Trip.class).findAll();
        deleteById(trips.get(0).getId());
        tripSessionRepo = new TripSessionRepo(context, TripSession.class);
        tripSessionRepo.deleteByTripId(trips.get(0).getId());
        tripScoreRepo = new TripScoreRepo(context, TripScore.class);
        tripScoreRepo.deleteByTripId(trips.get(0).getId());
        return true;
    }

    public boolean deleteById(String tripId) throws RealmException {


        Log.d(TripRepo.class.getSimpleName(), "Trip Id:-"+tripId);
        tripSessionRepo = new TripSessionRepo(context, TripSession.class);
        tripSessionRepo.deleteByTripId(tripId);
        tripSessionRepo.close();

        tripScoreRepo = new TripScoreRepo(context, TripScore.class);
        tripScoreRepo.deleteByTripId(tripId);
        tripScoreRepo.close();
        super.deleteById(tripId);
        return true;
    }

    public List<Trip> findAllUnSynced() {
        return realm.where(Trip.class).equalTo("isSync", false).findAll();
    }

    public List<Trip> findAllByUserId(String userId) throws RealmException {

        List<Trip> trips = realm.where(Trip.class).equalTo("isActive", true).equalTo("userId", userId).findAll();
        return trips;
    }
}
