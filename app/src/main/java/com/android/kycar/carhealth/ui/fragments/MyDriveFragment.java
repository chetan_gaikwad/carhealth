package com.android.kycar.carhealth.ui.fragments;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Matrix;
import android.hardware.SensorEvent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.beans.Angle;
import com.android.kycar.carhealth.services.connection_services.mqtt_paho.PublishSensorData;
import com.android.kycar.carhealth.services.sensor_services.SensorManagerService;
import com.android.kycar.carhealth.services.utils.LoggerTxt;
import com.android.kycar.carhealth.ui.activity.SecurityController;
import com.android.kycar.carhealth.utils.BusProvider;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Krishna on 1/30/2016.
 */
public class MyDriveFragment extends Fragment {

    private double detectedAy, detectedAz, detectedAx;
    private LoggerTxt loggerTxt;
    private HashMap<String, Double> harshValues;
    private HashMap<String, Double> mildValues;

    private TextView tvMaxInclineXPosTextView;
    private TextView tvMaxInclineXNegTextView;
    private TextView tvMaxInclineYPosTextView;
    private TextView tvMaxInclineYNegTextView;
    private TextView tvTopInclineYPos;
    private TextView tvTopInclineYNeg;
    private TextView tvTopInclineXPos;
    private TextView tvTopInclineXNeg;

    private int topInclineYPos;
    private int topInclineYNeg;
    private int topInclineXPos;
    private int topInclineXNeg;

    private Button butRefreshUI;

    private TextView tvAx;
    private TextView tvAy;
    private TextView tvAz;

    private int topXpos;
    private int topXneg;
    private int topYpos;
    private int topYneg;

    private ImageView imgCarSideView;
    private ImageView imgCarFrontView;
    private Matrix defaultMatrixImgCarSideView;
    private Matrix defaultMatrixImgCarFrontView;

    private FloatingActionButton fab;

    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    protected SharedPreferences preferences;
    private static final String TAG = SecurityController.class.getName();
    private BluetoothDevice dev = null;
    private BluetoothSocket sock = null;
    private BluetoothSocket sockFallback = null;
    private TextView txtSensorValues;
    private TextView txtSensorMaxValues;

    private OutputStream mmOutputStream;
    private InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    private int readBufferPosition;
    private int counter;
    volatile boolean stopWorker;
    private PublishSensorData publishSensorData;
    private Button butBtConnect;
    private Button butTiBtConnect;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        container = (ViewGroup) inflater.inflate(R.layout.my_drive_layout, null);

        fab = (FloatingActionButton) container.findViewById(R.id.fab);
        //fab.setOnClickListener(onAddingListener());

        butRefreshUI = (Button) container.findViewById(R.id.butRefreshUI);

        butRefreshUI.setOnClickListener(onClickListener);

        imgCarSideView = (ImageView) container.findViewById(R.id.imgCarSideView);
        imgCarFrontView = (ImageView) container.findViewById(R.id.imgCarFrontView);

        defaultMatrixImgCarSideView = imgCarSideView.getImageMatrix();
        defaultMatrixImgCarFrontView = imgCarFrontView.getImageMatrix();

        tvTopInclineXNeg = (TextView) container.findViewById(R.id.tvTopInclineXNeg);
        tvTopInclineXPos = (TextView) container.findViewById(R.id.tvTopInclineXPos);
        tvTopInclineYNeg = (TextView) container.findViewById(R.id.tvTopInclineYNeg);
        tvTopInclineYPos = (TextView) container.findViewById(R.id.tvTopInclineYPos);
        topInclineXNeg = 0;
        topInclineXPos = 0;
        topInclineYNeg = 0;
        topInclineYPos = 0;

        tvMaxInclineXPosTextView = (TextView) container.findViewById(R.id.tvMaxInclineXPos);
        tvMaxInclineXNegTextView = (TextView) container.findViewById(R.id.tvMaxInclineXNeg);
        tvMaxInclineYPosTextView = (TextView) container.findViewById(R.id.tvMaxInclineYPos);
        tvMaxInclineYNegTextView = (TextView) container.findViewById(R.id.tvMaxInclineYNeg);

        topXpos = 0;
        topXneg = 0;
        topYneg = 0;
        topYpos = 0;

        mildValues = new HashMap<String, Double>();
        mildValues.put("ACC", 4.5);
        mildValues.put("BREAKING", -4.5);
        mildValues.put("LEFT", -4.5);
        mildValues.put("RIGHT", 4.5);
        mildValues.put("UP", -0.15);
        mildValues.put("DOWN",11.2 );

        harshValues= new HashMap<String, Double>();
        harshValues.put("ACC", 6.0);
        harshValues.put("BREAKING", -6.0);
        harshValues.put("LEFT", -6.0);
        harshValues.put("RIGHT", 6.0);
        harshValues.put("UP", -5.0);
        harshValues.put("DOWN", 18.0);

        /*final Handler mHandler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                updateMyDriveUI(DashboardActivity.angle);
                setAccData(DashboardActivity.sensorEvent);
                // do your stuff here, called every second
                mHandler.postDelayed(this, 10);
            }
        };
        mHandler.post(runnable);*/
        butBtConnect = (Button) container.findViewById(R.id.butBtConnect);

        butBtConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBtConnectClick(v);
            }
        });

        butTiBtConnect = (Button) container.findViewById(R.id.butBtTiConnect);
        butTiBtConnect.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onTiBtConnectClick(v);
            }
        });

        loggerTxt = new LoggerTxt("rc");

        return container;
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.butRefreshUI){
                resetUI(v);
            }}
    } ;

    public void resetUI(View view){

        topInclineXNeg = 0;
        topInclineXPos = 0;
        topInclineYNeg = 0;
        topInclineYPos = 0;
    }

    @Subscribe
    public void updateMyrDriveUI(Angle angles){

        updateMyDriveUI(angles);
    }

    public void setAccData(SensorEvent sensorEvent){

        if(sensorEvent == null){
            return;
        }

        float ax = sensorEvent.values[0];
        float ay = sensorEvent.values[1];
        float az = sensorEvent.values[2];

        tvAx.setText(""+ax);
        tvAx.setText(""+ay);
        tvAz.setText(""+az);
    }

    public void updateMyDriveUI(Angle angle){

        if(angle!=null) {
            if (angle.getAngleX() <= 90) {
                if(topInclineXPos < (90 - angle.getAngleX())){
                    topInclineXPos = (90 - angle.getAngleX());
                    tvTopInclineXPos.setText("" + (90 - angle.getAngleX()));
                }
                tvMaxInclineXPosTextView.setText("" + (90 - angle.getAngleX()));
            } else {
                if(topInclineXNeg < (angle.getAngleX() - 90)){
                    topInclineXNeg = (angle.getAngleX() - 90);
                    tvTopInclineXNeg.setText("" + (angle.getAngleX() - 90));
                }
                tvMaxInclineXNegTextView.setText("" + (angle.getAngleX() - 90));
            }

            if (angle.getAngleZ() <= 90) {
                if(topInclineYPos < (90 - angle.getAngleZ())){
                    topInclineYPos = (90 - angle.getAngleZ());
                    tvTopInclineYPos.setText("" + (90 - angle.getAngleZ()));
                }
                tvMaxInclineYPosTextView.setText("" + (90 - angle.getAngleZ()));
            } else {
                if(topInclineYNeg < (angle.getAngleZ() - 90)){
                    topInclineYNeg = (angle.getAngleZ() - 90);
                    tvTopInclineYNeg.setText("" + (angle.getAngleZ() - 90));
                }
                tvMaxInclineYNegTextView.setText("" + (angle.getAngleZ() - 90));
            }

            Matrix matrix = new Matrix();
            imgCarSideView.setScaleType(ImageView.ScaleType.MATRIX);
            matrix.postRotate((float) 90 - angle.getAngleZ(), imgCarSideView.getDrawable().getBounds().width() / 2, imgCarSideView.getDrawable().getBounds().height() / 2);
            imgCarSideView.setImageMatrix(matrix);

            Matrix matrix2 = new Matrix();
            imgCarFrontView.setScaleType(ImageView.ScaleType.MATRIX);
            matrix2.postRotate((float) 90 - angle.getAngleX(), imgCarFrontView.getDrawable().getBounds().width() / 2, imgCarFrontView.getDrawable().getBounds().height() / 2);
            imgCarFrontView.setImageMatrix(matrix2);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause(){
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    public void onTiBtConnectClick(View v) {
        Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage("com.example.ti.ble.sensortag");
        if (launchIntent != null) {
            startActivity(launchIntent);//null pointer check in case package name was not found
        }
    }

    public void onBtConnectClick(View v){

        Log.d(MyDriveFragment.class.getSimpleName(), "Starting service..");
        // get the remote Bluetooth device
        // final String remoteDevice = prefs.getString(ConfigActivity.BLUETOOTH_LIST_KEY, null);
        final String remoteDevice = "2C:F7:F1:81:3A:42";
        /** Need to code for device selection */

        if (remoteDevice == null || "".equals(remoteDevice)) {
            Toast.makeText(getContext(), getString(R.string.text_bluetooth_nodevice), Toast.LENGTH_LONG).show();

            // log error
            Log.e(MyDriveFragment.class.getSimpleName(), "No Bluetooth device has been selected.");
        } else {

            final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
            dev = btAdapter.getRemoteDevice(remoteDevice);
            Log.d(MyDriveFragment.class.getSimpleName(), "Stopping Bluetooth discovery.");
            btAdapter.cancelDiscovery();

            try {
                startObdConnection();
            } catch (Exception e) {
                Log.e(
                        MyDriveFragment.class.getSimpleName(),
                        "There was an error while establishing connection. -> "
                                + e.getMessage()
                );
            }
        }
    }

    private void startObdConnection() throws IOException {
        Log.d(MyDriveFragment.class.getSimpleName(), "Starting OBD connection..");
        try {
            // Instantiate a BluetoothSocket for the remote device and connect it.
            sock = dev.createInsecureRfcommSocketToServiceRecord(MY_UUID);
            sock.connect();

            Toast.makeText(getContext(),"Connected",Toast.LENGTH_LONG).show();
        } catch (Exception e1) {
            Log.e(MyDriveFragment.class.getSimpleName(), "There was an error while establishing Bluetooth connection. Falling back..", e1);
            Class<?> clazz = sock.getRemoteDevice().getClass();
            Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};
            try {

                /************Fallback method 1*********************/
                Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                Object[] params = new Object[]{Integer.valueOf(1)};
                sockFallback = (BluetoothSocket) m.invoke(sock.getRemoteDevice(), params);
                sockFallback.connect();
                sock = sockFallback;
                Log.e("", "Connected");
            } catch (Exception e2) {
                Log.e(MyDriveFragment.class.getSimpleName(), "Couldn't fallback while establishing Bluetooth connection. Stopping app..", e2);
                throw new IOException();
            }
        }
        mmOutputStream = sock.getOutputStream();
        mmInputStream = sock.getInputStream();
        beginListenForData();
    }

    void beginListenForData() {
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character

        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[50000];
        workerThread = new Thread(new Runnable()
        {
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopWorker)
                {
                    try
                    {
                        int bytesAvailable = mmInputStream.available();
                        if(bytesAvailable > 0)
                        {
                            byte[] packetBytes = new byte[bytesAvailable];
                            mmInputStream.read(packetBytes);

                            for(int i=0;i<bytesAvailable;i++){
                                byte b = packetBytes[i];
                                if(b == delimiter)
                                {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;

                                    handler.post(new Runnable()
                                    {
                                        public void run()
                                        {

                                            /** Actual sensor data*/
                                            Log.d(MyDriveFragment.class.getSimpleName(), "data is ==>"+data);

                                            try {
                                                JSONObject jsonData = new JSONObject(data);

                                                JSONObject acJsonData = jsonData.getJSONObject("a");
                                                double ax = acJsonData.getDouble("x");
                                                double ay = acJsonData.getDouble("y");
                                                double az = acJsonData.getDouble("z");

                                                logSensorData(ax, ay, az, data);

                                                int angleX = (int) Math.round(Math.toDegrees(Math.acos((float) (ax / Math.sqrt(ax * ax + ay * ay + az * az)))));
                                                int angleZ = (int) Math.round(Math.toDegrees(Math.acos((float) (az / Math.sqrt(ax * ax + ay * ay + az * az)))));

                                                /** disabled for the thread optimization */
                                                // BusProvider.getInstance().post(sensorEvent);
                                                BusProvider.getInstance().post(produceAngle(angleX, angleZ));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            /*JSONObject jsonObject = createJsonStructure(data);
                                            if(jsonObject != null) {
                                                txtSensorValues.setText(jsonObject.toString());
                                                if (preferences.getBoolean(Util.IS_UPLOAD_RC, true) && preferences.getBoolean(Util.IS_LOGGED_IN, false)){
                                                    //publishSensorData.getEventSensorData(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAy, event, speed, updatedLocation.getSpeed(), updatedLocation.getBearing(), magBearing);

                                                    *//**Need to check threshold*//*
                                                    if(false){
                                                        try {
                                                            Log.d(SecurityController.class.getSimpleName(), "Event Data published");
                                                            Connection.getClient().publish(NxtApplication.baseTopic+"/sensor_event_data/"+preferences.getString(Util.USER_PREFERENCE, ""), publishSensorData.getEventSensorData(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), preferences.getString(Util.CAR_PREFERENCE, ""), new Coordinates(), 0.0, "", 0, NxtApplication.currLocation.getSpeed(), NxtApplication.currLocation.getBearing(), 0, preferences.getString(Util.TRIP_ID, ""), preferences.getString(Util.SENSOR_ACC_NAME, "")));
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }
                                            }*/
                                        }
                                    });
                                }
                                else
                                {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                        Log.d(MyDriveFragment.class.getSimpleName(), "readBuffer:-"+readBufferPosition);
                    }
                    catch (IOException ex)
                    {
                        stopWorker = true;
                    }
                }
            }
        });
        workerThread.start();
    }

    public Angle produceAngle(Integer angleX, Integer angleZ) {
        return new Angle(angleX, null, angleZ);
    }

    @Override
    public void onDestroy() {
        try {
            if (sock != null) {
                sock.close();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        loggerTxt.finalize();
        super.onDestroy();
    }


    private void logSensorData(double ax, double ay, double az, String data){

        String event = null;

        /** Detecting gravity */
        if(detectGravity(ax, ay, az)) {

            /** Detecting Road Condition Events */
            if(detectedAz < harshValues.get("UP")) {
                event = "HARSH_UP";
            } else if(detectedAz > harshValues.get("DOWN")) {
                event = "HARSH_DOWN";
            } else if(detectedAz > mildValues.get("DOWN")) {
                event = "DOWN";
            } else if(detectedAz < mildValues.get("UP")) {
                event = "UP";
            }

            if(event != null) {

                //loggerTxt.log(publishSensorData.getEventSensorDataJSONObject(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAy, event, speed, NxtApplication.currLocation.getSpeed(), NxtApplication.currLocation.getBearing(), magBearing, preferences.getString(Util.TRIP_ID, ""),preferences.getString(Util.SENSOR_ID, "NR")).toString());

                loggerTxt.log(data);
                event = null;
                return;
            }

            /** Detecting Driver Behaviour Events */
            if(detectedAy < harshValues.get("LEFT")) {
                event = "HARSH_LEFT";
             } else if(detectedAy > harshValues.get("RIGHT")) {
                event = "HARSH_RIGHT";
             } else if(detectedAy < mildValues.get("LEFT")) {
                event = "LEFT";
             } else if(detectedAy > mildValues.get("RIGHT")) {
                event = "RIGHT";
             }

            if(event != null) {

                Log.d(SensorManagerService.class.getSimpleName(), "Event Data Logged");
                //loggerTxt.log(publishSensorData.getEventSensorDataJSONObject(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAy, event, speed, NxtApplication.currLocation.getSpeed(), NxtApplication.currLocation.getBearing(), magBearing, preferences.getString(Util.TRIP_ID, ""),preferences.getString(Util.SENSOR_ID, "NR")).toString());
                loggerTxt.log(data);
                event = null;
                return;
            }

            /** Detecting drive behaviour Events */
            if(detectedAx > harshValues.get("ACC")) {
                event = "HARSH_ACC";
            } else if(detectedAx < harshValues.get("BREAKING")) {
                event = "HARSH_BREAKING";
            } else if(detectedAx > mildValues.get("ACC")) {
                event = "ACC";
            } else if(detectedAx < mildValues.get("BREAKING")) {
                event = "BREAKING";
            }
            if(event != null) {

                Log.d(SensorManagerService.class.getSimpleName(), "Event Data Logged");
                //loggerTxt.log(publishSensorData.getEventSensorDataJSONObject(preferences.getString(Util.USER_PREFERENCE, "UNDEFINE"), vehicleId, coords, detectedAy, event, speed, NxtApplication.currLocation.getSpeed(), NxtApplication.currLocation.getBearing(), magBearing, preferences.getString(Util.TRIP_ID, ""),preferences.getString(Util.SENSOR_ID, "NR")).toString());
                loggerTxt.log(data);
                event = null;
                }
            }
        }

    /** This method detects gravity acts on which axes */
    private boolean detectGravity(double ax, double ay, double az) {

        if(ay > 9.5) {

            detectedAz = ay;
            detectedAy = ax;
            detectedAx = az;
            return true;
        } else if (ax > 9.5) {

            detectedAz = ax;
            detectedAy = ay;
            detectedAx = az;
            return true;
        } else if (az > 9.5) {

            detectedAz = az;
            detectedAx = ay;
            detectedAy = ax;
            return true;
        } else {
            return false;
        }
    }
}
