package com.android.kycar.carhealth.services.dto.commands.protocol;


import com.android.kycar.carhealth.services.dto.enums.ObdProtocols;

/**
 * Select the protocol to use.
 */
public class SelectProtocolCommand extends ObdProtocolCommand {

    private final ObdProtocols protocol;

    public SelectProtocolCommand(final ObdProtocols protocol) {
        super("AT SP " + protocol.getValue());
        this.protocol = protocol;
    }

    @Override
    public String getFormattedResult() {
        return getResult();
    }

    @Override
    public String getName() {
        return "Select Protocol " + protocol.name();
    }

}
