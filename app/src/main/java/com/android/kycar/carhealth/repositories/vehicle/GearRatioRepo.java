package com.android.kycar.carhealth.repositories.vehicle;

import android.content.Context;


import com.android.kycar.carhealth.entities.vehicle_master.GearRatio;
import com.android.kycar.carhealth.repositories.Repository;

import java.util.List;

import io.realm.exceptions.RealmException;

/**
 * Created by Krishna on 2/2/2016.
 */
public class GearRatioRepo extends Repository {
    public GearRatioRepo(Context context, Class _aClass) {
        super(context, _aClass);
    }

    public GearRatio findByGearNumAndVariantId(Integer gear, String variantId) throws RealmException {
        return realm.where(GearRatio.class).equalTo("variantId", variantId).equalTo("gear", gear).findFirst();
    }

    public GearRatio findGearByRatioAndVariantId(Double gearRatio, String variantId ) throws RealmException {
        return realm.where(GearRatio.class).equalTo("variantId", variantId).greaterThanOrEqualTo("gearRatioMax", gearRatio).lessThanOrEqualTo("gearRatioMin", gearRatio).findFirst();
    }

    public List<GearRatio> findGearRatiosByVariantId(String variantId) throws RealmException {
        return  realm.where(GearRatio.class).equalTo("variantId", variantId).findAll();
    }
}
