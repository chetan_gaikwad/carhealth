package com.android.kycar.carhealth.services.sensor_services;

import android.content.Context;

import com.android.kycar.carhealth.services.utils.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Krishna on 4/23/2016.
 */
public class TripDataLoggerService {

    private Logger logger;

    private boolean isStartTrip;
    private double lat, log;
    private Date startTime;
    private int obdDistanceCovered;

    public TripDataLoggerService(Context context){

        logger = new Logger("Trip.csv",context);
        List<String> headers = new ArrayList<String>();
        headers.add("status");
        headers.add("timestamp");
        headers.add("km_count");
        headers.add("latitude");
        headers.add("longitude");
        logger.setHeader(headers);
    }

    public boolean stopTripService( double lat, double lng, int obdKmCount){

        List<String > trip = new ArrayList<String>();

        trip.add("STOPPED");
        trip.add(new Date().toString());
        trip.add("" + lat);
        trip.add("" + lng);
        trip.add("" + obdKmCount);

        logger.logging(trip);

        List<String > endTrip = new ArrayList<String>();

        endTrip.add("COMPLETED");
        //Log.d("Distance----", calcDiffTime(new Date(), startTime)+"");
        //endTrip.add(calcDiffTime(new Date(), startTime)+"");
        endTrip.add("" );
        endTrip.add("" );
        endTrip.add("" +(obdKmCount - obdDistanceCovered));

        logger.logging(endTrip);

        logger.finalizeFile();
        return true;
    }

    public void logStartTrip( double lat, double lng, int obdKmCount){

        startTime = new Date();
        obdDistanceCovered = obdKmCount;
        List<String > trip = new ArrayList<String>();

        trip.add("STARTED");
        trip.add(new Date().toString());
        trip.add("" + lat);
        trip.add("" + lng);
        trip.add("" + obdKmCount);

        logger.logging(trip);
    }

    private String calcDiffTime(Date start, Date end) {
        long diff = end.getTime() - start.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        StringBuffer res = new StringBuffer();

        if (diffDays > 0)
            res.append(diffDays + "d");

        if (diffHours > 0) {
            if (res.length() > 0) {
                res.append(" ");
            }
            res.append(diffHours + "h");
        }

        if (diffMinutes > 0) {
            if (res.length() > 0) {
                res.append(" ");
            }
            res.append(diffMinutes + "m");
        }

        if (diffSeconds > 0) {
            if (res.length() > 0) {
                res.append(" ");
            }

            res.append(diffSeconds + "s");
        }
        return res.toString();
    }

}
