package com.android.kycar.carhealth.services.dto.utils;

/**
 * Created by 638 on 12/6/2015.
 */
public class CarTest {
    private String speed;
    private String rpm;
    private String timeStamp;

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getRpm() {
        return rpm;
    }

    public void setRpm(String rpm) {
        this.rpm = rpm;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "CarTest{" +
                "speed='" + speed + '\'' +
                ", rpm='" + rpm + '\'' +
                ", timeStamp='" + timeStamp + '\'' +
                '}';
    }
}
