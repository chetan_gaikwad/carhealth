package com.android.kycar.carhealth.exceptions;

/**
 * Created by Krishna on 1/30/2016.
 */
public class ConnectionException extends Exception {

    public ConnectionException() {
        // TODO Auto-generated constructor stub
    }

    public ConnectionException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public ConnectionException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public ConnectionException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }
}
