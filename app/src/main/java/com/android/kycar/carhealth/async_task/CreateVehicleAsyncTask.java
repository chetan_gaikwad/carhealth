package com.android.kycar.carhealth.async_task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.android.kycar.carhealth.entities.Vehicle;
import com.android.kycar.carhealth.repositories.vehicle.VehicleRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.VehicleRestService;

import org.json.JSONObject;

import java.util.Date;

/**
 * Uploading asynchronous task
 */
public class CreateVehicleAsyncTask extends AsyncTask<Vehicle, Void, String> {

    private Context context;
    private VehicleRepo vehicleRepo;
    public CreateVehicleAsyncTask(Context context) {
        this.context = context;
    }


    @Override
    protected String doInBackground(Vehicle... vehicles) {

        vehicleRepo = new VehicleRepo(context, Vehicle.class);
        try {
            String response = new VehicleRestService().createVehicle(vehicles[0]);
            if(response != null) {
                JSONObject responseObj = new JSONObject(response);
                vehicles[0].setId(responseObj.getString("id"));
                vehicles[0].setSync(true);
                vehicleRepo.saveOrUpdate(vehicles[0]);
            } else {
                vehicles[0].setId(new Date().getTime()+"");
                vehicles[0].setSync(false);
                vehicleRepo.saveOrUpdate(vehicles[0]);
            }
            vehicleRepo.close();
            Log.d("asdacc", "Rest Response:-"+response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //dialogBox.showDailog();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        /*if(dialogBox.isShowing()){
               dialogBox.dismissDialog();
           }*/
            //Toast.makeText(AddCarActivity.this,"Car added successfully",Toast.LENGTH_SHORT).show();
            //onBackPressed();
    }
}