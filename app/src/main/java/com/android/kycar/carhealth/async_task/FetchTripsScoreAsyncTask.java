package com.android.kycar.carhealth.async_task;

import android.content.Context;
import android.os.AsyncTask;


import com.android.kycar.carhealth.entities.trip_transaction.TripScore;
import com.android.kycar.carhealth.repositories.trip.TripScoreRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.TripRestService;

import org.json.JSONException;
import org.json.JSONObject;

public class FetchTripsScoreAsyncTask extends AsyncTask<String, Void, String> {

    private Context context;

    public FetchTripsScoreAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
            super.onPreExecute();
        }

    @Override
    protected String doInBackground(String... strings) {

        return new TripRestService().getTripScoreByTripId(strings[0]);
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        /** Trips to realm */

        try {
            JSONObject resObj = new JSONObject(result);

            TripScore tripScore = new TripScore(resObj);
            TripScoreRepo tripScoreRepo = new TripScoreRepo(context, TripScore.class);
            tripScoreRepo.saveOrUpdate(tripScore);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }