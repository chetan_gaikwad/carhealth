package com.android.kycar.carhealth.services.connection_services.rest_services;

import org.json.JSONObject;

/**
 * Created by Krishna on 9/29/2016.
 */
public class ObdDataService extends RestService {

    public String sendRawObdData(JSONObject data) {

        String rawObdData = "/obd_data/obd_raw_data";
        return restServiceCall(rawObdData, "POST", data);
    }
}
