package com.android.kycar.carhealth.services.connection_services.mqtt_paho;

import android.util.Log;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Krishna on 5/3/2016.
 */
public class PublishObdData {

    public MqttMessage getObdPacket(Map<String,String> packet, JSONObject obdValue){

        try {
            JSONObject payload = new JSONObject();
            payload.put("data", "obdPacket");
            payload.put("obdPacket", getObdPacketJson(packet, obdValue));
            MqttMessage message = new MqttMessage(payload.toString().getBytes());
            return message;
        } catch (JSONException e) {
            Log.i(PublishObdData.class.getSimpleName(),e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject getObdPacketJson(Map<String,String> packet, JSONObject obdValue) {
        JSONObject obdPacket = new JSONObject(packet);
        try {
            if(obdValue.has("loc")) {
                obdPacket.put("loc", obdValue.getJSONObject("loc"));
            }
            if(obdValue.has("gear")){
                obdPacket.put("gear", obdValue.getInt("gear"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obdPacket;
    }
}
