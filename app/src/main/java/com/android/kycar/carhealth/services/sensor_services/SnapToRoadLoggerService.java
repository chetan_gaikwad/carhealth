package com.android.kycar.carhealth.services.sensor_services;

import android.content.Context;
import android.util.Log;

import com.android.kycar.carhealth.beans.Coordinates;
import com.android.kycar.carhealth.services.utils.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Krishna on 2/21/2016.
 */
public class SnapToRoadLoggerService {

    Logger snapToRoadLogger;

    public SnapToRoadLoggerService(Context context){

        snapToRoadLogger = new Logger(new Date()+"Snaptoroad.csv",context);
        List<String> locHeaders = new ArrayList<String>();
        locHeaders.add("timestamp");
        locHeaders.add("speed");
        locHeaders.add("lat");
        locHeaders.add("long");
        locHeaders.add("dirction");
        locHeaders.add("userId");
        locHeaders.add("device");
        locHeaders.add("vehicleId");
        locHeaders.add("macId");
        snapToRoadLogger.setHeader(locHeaders);
    }

    public boolean stopSensorDataLoggerService(){

        snapToRoadLogger.finalizeFile();
        return true;
    }

    public void logLocation(String direction, Integer speed, Coordinates coords){

        Log.d("Location logged", "" + coords.toString());
        List<String > loc = new ArrayList<String>();
        loc.add(new Date().toString());
        loc.add("" + speed);
        loc.add("" + coords.getLatitude());
        loc.add("" + coords.getLongitude());
        loc.add(direction);
        loc.add("USER1");
        loc.add("DEVICE1");
        loc.add("VEHICLE_ID");
        loc.add("MAC_ID");
        snapToRoadLogger.logging(loc);
    }
}
