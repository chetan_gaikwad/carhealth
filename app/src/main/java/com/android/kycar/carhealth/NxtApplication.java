package com.android.kycar.carhealth;

import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import androidx.core.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.android.kycar.carhealth.beans.Gear;
import com.android.kycar.carhealth.config.ConfigActivity;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.UserContact;
import com.android.kycar.carhealth.entities.UserContactSetting;
import com.android.kycar.carhealth.entities.Vehicle;
import com.android.kycar.carhealth.entities.trip_transaction.Trip;
import com.android.kycar.carhealth.entities.vehicle_master.GearRatio;
import com.android.kycar.carhealth.repositories.trip.TripRepo;
import com.android.kycar.carhealth.repositories.user.UserContactRepo;
import com.android.kycar.carhealth.repositories.user.UserContactSettingRepo;
import com.android.kycar.carhealth.repositories.vehicle.GearRatioRepo;
import com.android.kycar.carhealth.repositories.vehicle.VehicleRepo;
import com.android.kycar.carhealth.services.connection_services.mqtt_paho.PublishObdData;
import com.android.kycar.carhealth.services.connection_services.mqtt_paho_android_service.Connection;
import com.android.kycar.carhealth.services.dto.commands.SpeedCommand;
import com.android.kycar.carhealth.services.dto.commands.control.DistanceSinceCCCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.OilTempCommand;
import com.android.kycar.carhealth.services.dto.commands.engine.RPMCommand;
import com.android.kycar.carhealth.services.dto.commands.temperature.EngineCoolantTemperatureCommand;
import com.android.kycar.carhealth.services.dto.enums.AvailableCommandNames;
import com.android.kycar.carhealth.services.obd_services.ObdCommandJob;
import com.android.kycar.carhealth.services.obd_services.ObdDataLoggerService;
import com.android.kycar.carhealth.ui.activity.DashboardActivity;
import com.android.kycar.carhealth.utils.BusProvider;
import com.android.kycar.carhealth.utils.CustomDialogPojo;
import com.android.kycar.carhealth.utils.NotificationView;
import com.android.kycar.carhealth.utils.PreferenceUtils;
import com.squareup.otto.Subscribe;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * Created by Krishna on 2/27/2016.
 */
public class NxtApplication extends Application implements TextToSpeech.OnInitListener {


    private static final long ALERT_SCHEDULER_TIME = 1000 * 60 * 5;
    public TextToSpeech tts;
    public static final String baseTopic = "nxtgizmo";
    public static List<String> shareLocationUserTopics = new ArrayList<>();
    private SharedPreferences.Editor editor;
    public static Location currLocation;
    private Map<String, String> dataSheets = new HashMap<>();
    private Handler coolantHandler;
    private Handler oilHandler;
    private JSONObject obdValues = new JSONObject();

    public static SharedPreferences preferences;
    private PublishObdData publishObdData;
    private ObdDataLoggerService logger;

    static public double speedHarsh, speedHigh, speedOk, speedAwesome;
    static public double rpmHarsh, rpmHigh, rpmOk, rpmAwesome;
    static public double oilVeryHot, oilHot, oilOptimum, oilCool;
    static public double coolantVeryHot, coolantHot, coolantOptimum, coolantCool;

    private Gear gear;
    private String variantId;
    private VehicleRepo vehicleRepo;
    private int speed;
    private int rpm;
    private boolean showEngineOilAlert = true;
    private boolean showCoolantAlert = true;
    public static StringBuilder batteryVoltage = new StringBuilder();
    private int tripStartDistance;
    static String topic = "smarton";



    public static void updateShareLocationUsers(Context context) {

        UserContactRepo userContactRepo = new UserContactRepo(context, UserContact.class);
        List<UserContact> userContacts = userContactRepo.findAll();
        shareLocationUserTopics.clear();
        if (!userContacts.isEmpty()) {
            for (UserContact userContact : userContacts) {
                UserContactSettingRepo userContactSettingRepo = new UserContactSettingRepo(context, UserContactSetting.class);
                UserContactSetting userContactSetting = userContactSettingRepo.findByUserContactId(userContact.getId());
                if (userContactSetting != null) {
                    if (userContactSetting.getShareLocation() && userContactSetting.getShareLocation()) {
                        shareLocationUserTopics.add(userContact.getUserContactTopic());
                    }
                }
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();


        coolantHandler = new Handler();
        oilHandler = new Handler();
        preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        editor = preferences.edit();
        editor.putBoolean(Util.IS_DRIVE_MODE, false).commit();
        BusProvider.getInstance().register(NxtApplication.this);
        publishObdData = new PublishObdData();

        if (preferences.getBoolean(Util.IS_LOGGED_IN, false)) {
            vehicleRepo = new VehicleRepo(getApplicationContext(), Vehicle.class);
            Vehicle vehicle = (Vehicle) vehicleRepo.findById(preferences.getString(Util.CAR_PREFERENCE, ""));
            if (vehicle != null) {
                variantId = vehicle.getVariantId();
                if (vehicle != null) {
                    GearRatioRepo gearRatioRepo = new GearRatioRepo(getApplicationContext(), GearRatio.class);
                    List<GearRatio> gearRatios = gearRatioRepo.findGearRatiosByVariantId(variantId);
                    gear = new Gear(gearRatios);
                }
            }
        }
        tts = new TextToSpeech(getApplicationContext(), this);
        tts.setLanguage(Locale.US);

        //customNotification(1,"gvsga","sdj","wq");
    }

    public void startLoggingObdData() {
        if (preferences.getBoolean(PreferenceUtils.PREFERENCE_ENABLE_OFFLINE_LOGGING, false)) {
            logger = new ObdDataLoggerService(this);
        }

    }

    public void stopLoggingObdData() {
        dataSheets.clear();
        batteryVoltage = new StringBuilder();
        if (preferences.getBoolean(PreferenceUtils.PREFERENCE_ENABLE_OFFLINE_LOGGING, false))
            logger.stopSensorDataLoggerService();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS problem", "This Language is not supported");
            } else {
                Log.e("TTS success", "Text-to-speech stated successfully");
            }
        } else {
            Log.e("TTS", "Initialization Failed!");
        }
    }

    public int getDistance() {

        return dataSheets.get("distance_traveled_after_codes_cleared") == null ? -1 : Integer.parseInt(dataSheets.get("distance_traveled_after_codes_cleared").replace("km", "").replace("m", ""));
    }

    public static String LookUpCommand(String txt) {
        for (AvailableCommandNames item : AvailableCommandNames.values()) {
            if (item.getValue().equals(txt)) return item.name();
        }
        return txt;
    }

    /**
     * Get current trip start distance km
     *
     * @param tripId
     * @return
     */
    private int getTripStartDistance(String tripId) {
        TripRepo tripRepo = new TripRepo(getApplicationContext(), Trip.class);
        Trip trip = (Trip) tripRepo.findById(tripId);
        return trip.getObdDistance();
    }

    /**
     * Get current trip start distance km
     *
     * @param tripId
     * @return
     */
    private long getTripStartTime(String tripId) {
        TripRepo tripRepo = new TripRepo(getApplicationContext(), Trip.class);
        Trip trip = (Trip) tripRepo.findById(tripId);
        return trip.getStartTime();
    }

    @Subscribe
    public void stateUpdate(ObdCommandJob job) {
        final String cmdName = job.getCommand().getName();
        String cmdResult = "";
        final String cmdID = LookUpCommand(cmdName);

        if (job.getState().equals(ObdCommandJob.ObdCommandJobState.EXECUTION_ERROR)) {
            cmdResult = job.getCommand().getResult();
            if (cmdResult != null) {
            }
        } else if (job.getState().equals(ObdCommandJob.ObdCommandJobState.NOT_SUPPORTED)) {
            cmdResult = getString(R.string.status_obd_no_support);
        } else {
            cmdResult = job.getCommand().getFormattedResult();
        }
        if (!cmdID.equals("Select Protocol AUTO")) {

            dataSheets.put(cmdID.toLowerCase(), cmdResult);
            if (cmdID.equalsIgnoreCase("DISTANCE_TRAVELED_AFTER_CODES_CLEARED")) {
                final DistanceSinceCCCommand command = (DistanceSinceCCCommand) job.getCommand();
                String str = cmdResult.replace("km", "");
                preferences.edit().putInt(Util.DISTANCE_COVERED, Integer.parseInt(dataSheets.get("distance_traveled_after_codes_cleared").replace("km", "").replace("m", ""))).commit();
                if (preferences.contains(Util.TRIP_ID)) {
                    tripStartDistance = getTripStartDistance(preferences.getString(Util.TRIP_ID, ""));
                    customNotification(1,
                            "Current trip statistics",
                            "Distance covered : " + (command.getKm() - tripStartDistance),
                            "Fares : " + calculateFares((command.getKm() - tripStartDistance)));
                }
            }

            //Log.d("hbvhb"," "+job.getCommand().getName() + "==> "+cmdResult);
        }

        sendDataToMqtt();

        /** Speed alert */
        if (cmdID.equals(AvailableCommandNames.SPEED.toString())) {
            final SpeedCommand command = (SpeedCommand) job.getCommand();

            if (command.getMetricSpeed() >=
                    Integer.parseInt(preferences.getString(ConfigActivity.SPEED_km_pref, "60"))) {
                showEvent(new CustomDialogPojo(10), "Over-speed Alert", "Speed above limit");
            }
            dataSheets.put("speed_zone", getSpeedZone(command.getMetricSpeed()) + "");
        }

        /** Coolant temp alert */
        if (cmdID.equals(AvailableCommandNames.ENGINE_COOLANT_TEMP.toString())) {
            final EngineCoolantTemperatureCommand command = (EngineCoolantTemperatureCommand) job.getCommand();
            if (command.getTemperature() >=
                    Integer.parseInt(preferences.getString(ConfigActivity.COOLANT_TEMP_pref, "105"))) {
                if (showCoolantAlert) {
                    showEvent(new CustomDialogPojo(10), "Over-heat Alert", "Coolant temperature above limit ");
                    showCoolantAlert = false;
                    coolantHandler.postDelayed(coolantAlertScheduler, ALERT_SCHEDULER_TIME);
                }

            }
            dataSheets.put("coolant_zone", getCoolantTempZone((int) command.getTemperature()) + "");
        }

        /** Oil temp alert */
        if (cmdID.equals(AvailableCommandNames.ENGINE_OIL_TEMP.toString())) {
            final OilTempCommand command = (OilTempCommand) job.getCommand();

            if (command.getOilTemp() >=
                    Integer.parseInt(preferences.getString(ConfigActivity.OIL_TEMP_pref, "105"))) {
                if (showEngineOilAlert) {
                    showEvent(new CustomDialogPojo(10), "Over-heat Alert", "Oil temperature above limit ");
                    showEngineOilAlert = false;
                    oilHandler.postDelayed(oilAlertScheduler, ALERT_SCHEDULER_TIME);
                }
            }
            dataSheets.put("oil_zone", getOilTempZone(command.getOilTemp()) + "");
        }

        if (cmdID.equals(AvailableCommandNames.ENGINE_RPM.toString())) {
            final RPMCommand command = (RPMCommand) job.getCommand();
            dataSheets.put("rpm_zone", getRpmZone(command.getRPM()) + "");
        }

        /** Calculate stats */
//        calculateStats();
        updateTripStatistic(job, cmdID);
    }

    private double calculateFares(int distanceTravelled) {
        long timeDuration = calculateDuration();
        double fares = (timeDuration * 0.5) + (distanceTravelled * 5);
        return fares;
    }

    private long calculateDuration() {
        /** Calculating duration */
        long diff = (new Date().getTime() - getTripStartTime(preferences.getString(Util.TRIP_ID, "")));

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        System.out.print(diffDays + " days, ");
        System.out.print(diffHours + " hours, ");
        System.out.print(diffMinutes + " minutes, ");
        System.out.print(diffSeconds + " seconds.");

        return diffMinutes;
    }

    private void updateTripStatistic(final ObdCommandJob job, final String cmdID) {

        if (cmdID.equals(AvailableCommandNames.SPEED.toString())) {
            final SpeedCommand command = (SpeedCommand) job.getCommand();
            speed = command.getMetricSpeed();

            //BusProvider.getInstance().post(new CustomDialogPojo());
        } else if (cmdID.equals(AvailableCommandNames.ENGINE_RPM.toString())) {
            final RPMCommand command = (RPMCommand) job.getCommand();
            rpm = command.getRPM();
        }
    }

    private final Runnable coolantAlertScheduler = new Runnable() {
        public void run() {
            Log.d("hgh", "coolantAlertScheduler true");
            showCoolantAlert = true;
        }
    };
    private final Runnable oilAlertScheduler = new Runnable() {
        public void run() {
            Log.d("hgh", "oilAlertScheduler true");
            showEngineOilAlert = true;
        }
    };

    public void showDialog(Context context, String title, String desc, int iconResource) {
     /*   final MaterialStyledDialog.Builder dialogHeader_3 = new MaterialStyledDialog.Builder(context)
                .setHeaderDrawable(R.color.black)
                .setIcon(iconResource)
                .withDialogAnimation(true)
                .setTitle(title)
                .setDescription(desc)
                //.setPositiveText("GitHub")
                .setNegativeText("Hide")
                .autoDismiss(true);*/
        //dialogHeader_3.show();
    }

    /**
     * Calculating the trip statistic for the vehicle by Trip
     */
    private void calculateStats() {

        if (dataSheets.containsKey("engine_rpm") && !dataSheets.get("engine_rpm").equals("null")
                ) {
            int rpm = Integer.parseInt(dataSheets.get("engine_rpm"));

            if (rpm < 2000) {
                rpmOk = rpmOk + 0.1;
            } else if (rpm < 3300) {
                rpmAwesome = rpmAwesome + 0.1;
            } else if (rpm < 3700) {
                rpmHigh = rpmHigh + 0.1;
            } else if (rpm > 3700) {
                rpmHarsh = rpmHarsh + 0.1;
            }

            /*if (rpm < engineVitals.getRpmOk()) {
                rpmOk = rpmOk + 0.1;
            } else if (rpm < engineVitals.getRpmAwesome()) {
                rpmAwesome = rpmAwesome + 0.1;
            } else if (rpm < engineVitals.getRpmHigh()) {
                rpmHigh = rpmHigh + 0.1;
            } else if(rpm > engineVitals.getRpmHigh()) {
                rpmHarsh = rpmHarsh + 0.1;
            }*/
        }

        if (dataSheets.containsKey("speed") && !dataSheets.get("speed").equals("null")) {
            int speed = Integer.parseInt(dataSheets.get("speed"));
            if (speed < 30) {
                speedOk = speedOk + 0.1;
            } else if (speed < 50) {
                speedAwesome = speedAwesome + 0.1;
            } else if (speed < 70) {
                speedHigh = speedHigh + 0.1;
            } else if (speed > 70) {
                speedHarsh = speedHarsh + 0.1;
            }

            /*if ( speed < engineVitals.getSpeedOk()) {
                speedOk = speedOk + 0.1;
            } else if(speed < engineVitals.getSpeedAwesome()) {
                speedAwesome = speedAwesome + 0.1;
            } else if(speed < engineVitals.getSpeedHigh()) {
                speedHigh = speedHigh + 0.1;
            } else if(speed > engineVitals.getSpeedHigh()) {
                speedHarsh = speedHarsh + 0.1;
            }*/
        }

        if (dataSheets.containsKey("speed") && !dataSheets.get("speed").equals("null") && dataSheets.containsKey("engine_rpm") && !dataSheets.get("engine_rpm").equals("null")) {
            try {
                if (speed > 0 && rpm > 0) {

                    if (gear != null) {
                        obdValues.put("rpm/speed", (rpm / speed) + "");
                        obdValues.put("gear", gear.getGear((double) (rpm / speed)));
                        dataSheets.put("gear", gear.getGear((double) (rpm / speed)) + "");
                        dataSheets.put("rpm/speed", (rpm / speed) + "");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (dataSheets.containsKey("engine_oil_temp") && !dataSheets.get("engine_oil_temp").equals("null")) {
            int engOilTemp = Integer.parseInt(dataSheets.get("engine_oil_temp").replace("C", ""));
            if (engOilTemp < 90) {
                oilCool = oilCool + 0.1;
            } else if (engOilTemp < 110) {
                oilOptimum = oilOptimum + 0.1;
            } else if (engOilTemp < 118) {
                oilHot = oilHot + 0.1;
            } else {
                oilVeryHot = oilVeryHot + 0.1;
            }

            /*if (engOilTemp < engineVitals.getOilTempCold()) {
                oilCool = oilCool + 0.1;
            } else if(engOilTemp < engineVitals.getOilTempOptimum()) {
                oilOptimum = oilOptimum + 0.1;
            } else if (engOilTemp < engineVitals.getOilTempHot()) {
                oilHot = oilHot + 0.1;
            } else {
                oilVeryHot = oilVeryHot + 0.1;
            }*/
        }

        if (dataSheets.containsKey("engine_coolant_temp") && !dataSheets.get("engine_coolant_temp").equals("null")) {
            int coolantTemp = Integer.parseInt(dataSheets.get("engine_coolant_temp").replace("C", ""));

            if (coolantTemp < 90) {
                coolantCool = coolantCool + 0.1;
            } else if (coolantTemp < 110) {
                coolantOptimum = coolantOptimum + 0.1;
            } else if (coolantTemp < 118) {
                coolantHot = coolantHot + 0.1;
            } else {
                coolantVeryHot = coolantVeryHot + 0.1;
            }

            /*if (coolantTemp < engineVitals.getCoolantTempCold()) {
                coolantCool = coolantCool + 0.1;
            } else if(coolantTemp < engineVitals.getCoolantTempOptimum()) {
                coolantOptimum = coolantOptimum + 0.1;
            } else if (coolantTemp < engineVitals.getCoolantTempHot()) {
                coolantHot = coolantHot + 0.1;
            } else {
                coolantVeryHot = coolantVeryHot + 0.1;
            }*/
        }
    }

    @Subscribe
    public void locationUpdate(Location location) {

        NxtApplication.currLocation = location;
        try {
            JSONObject locObject = new JSONObject();
            dataSheets.put("lat", location.getLatitude() + "");
            dataSheets.put("lng", location.getLongitude() + "");
            dataSheets.put("spd", location.getSpeed() + "");
            dataSheets.put("alt", location.getAltitude() + "");
            dataSheets.put("acc", location.getAccuracy() + "");
            obdValues.put("loc", locObject);
            //dataSheets.put("location", locObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void sendDataToMqtt() {

        if (preferences != null) {
            dataSheets.put("userId", preferences.getString(Util.USER_PREFERENCE, "Not logged-In"));
            dataSheets.put("vehicleId", preferences.getString(Util.CAR_PREFERENCE, "Car NA"));
            dataSheets.put("macId", preferences.getString(ConfigActivity.BLUETOOTH_LIST_KEY, "NA"));
            dataSheets.put("vin", preferences.getString(ConfigActivity.VEHICLE_ID_KEY, "NA"));

            if (preferences.getString(Util.TRIP_ID, null) != null) {
                dataSheets.put("tripId", preferences.getString(Util.TRIP_ID, null));
            }
            dataSheets.put("tripId", preferences.getString(Util.TRIP_ID, "NA"));
        }
        dataSheets.put("timestamp", Util.getTime());
        dataSheets.put("battery", batteryVoltage.toString());
        //if (preferences.getBoolean(Util.IS_LOGGED_IN, false) && preferences.getBoolean(Util.IS_UPLOAD_OBD, true)) {
            Log.d(NxtApplication.class.getSimpleName(), "Obd Data sending");
            if(Util.isOnline(getApplicationContext())) {
                try {
                    MqttMessage message = publishObdData.getObdPacket(dataSheets, obdValues);
                    Connection.getClient().publish(topic, message);
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        //}

        //Log OBD data and Location Data offline in txt file
        if (preferences.getBoolean(PreferenceUtils.PREFERENCE_ENABLE_OFFLINE_LOGGING, false)) {
            logger.logData(dataSheets);
        }
    }

    public static void sendDate(){
        try {
            JSONObject payload = new JSONObject();
            payload.put("data", "obdPacket");
            payload.put("obdPacket", "kfmdkmf");
            MqttMessage message = new MqttMessage(payload.toString().getBytes());

            Connection.getClient().publish(topic, message);
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendDate(JSONObject jsonObject){
        try {
            MqttMessage message = new MqttMessage(jsonObject.toString().getBytes());

            Connection.getClient().publish(topic, message);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public SharedPreferences getPreference() {
        return preferences;
    }

    public void onUserNotify(String name) {

        NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setContentTitle("View " + name + " on map")
                        .setContentText(name + " Just shared location with you.");

        Intent notificationIntent = new Intent(this, DashboardActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(contentIntent);
        builder.setAutoCancel(true);
        builder.setLights(Color.BLUE, 500, 500);
        builder.setStyle(new NotificationCompat.InboxStyle());
        builder.setSmallIcon(R.mipmap.ic_launcher);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);

        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(1, builder.build());
    }

    public void showEvent(CustomDialogPojo event, String title, String msg) {
        if (!tts.isSpeaking()) {

            if (Build.VERSION.RELEASE.startsWith("5")) {
                //tts.speak(msg, TextToSpeech.QUEUE_FLUSH, null, null);
                tts.speak(msg, TextToSpeech.QUEUE_FLUSH, null);
            } else {
                tts.speak(msg, TextToSpeech.QUEUE_FLUSH, null);
            }
        }
        showDialog(getApplicationContext(), title, msg, R.drawable.alert_icon);
    }

    public void textToSpeech(String msg) {
        if (!tts.isSpeaking()) {

            if (Build.VERSION.RELEASE.startsWith("5")) {
                //tts.speak(msg, TextToSpeech.QUEUE_FLUSH, null, null);
                tts.speak(msg, TextToSpeech.QUEUE_FLUSH, null);
            } else {
                tts.speak(msg, TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }


    public void customNotification(int notificationId, String title, String data, String data3) {
        Log.i("customNotification", "Screen went OFF. Showing custom notification");
        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews remoteViews = new RemoteViews(getApplicationContext().getPackageName(),
                R.layout.noti_dialog);


        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(getApplicationContext(), NotificationView.class);
        // Send data to NotificationView Class
        intent.putExtra("title", title);
        intent.putExtra("text", data);
        intent.putExtra("text2", data3);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        androidx.core.app.NotificationCompat.Builder builder
                = new androidx.core.app.NotificationCompat.Builder(getApplicationContext())
                // Set Icon
                .setSmallIcon(R.drawable.ic_launcher)
                // Set Ticker Message
                .setTicker("Sticker")
                // Dismiss Notification
                .setAutoCancel(true)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Set RemoteViews into Notification
                .setContent(remoteViews)
                .setOngoing(true);

        // Locate and set the Image into customnotificationtext.xml ImageViews
        remoteViews.setImageViewResource(R.id.image, R.drawable.ic_launcher);

        // Locate and set the Text into customnotificationtext.xml TextViews
        remoteViews.setTextViewText(R.id.title, title);
        remoteViews.setTextViewText(R.id.text, data);
        remoteViews.setTextViewText(R.id.text2, data3);

        // Create Notification Manager
        NotificationManager notificationmanager =
                (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(notificationId, builder.build());
    }


    private int getRpmZone(int rpm) {
        int zone = 0;
        if (rpm > 0 && rpm <= 1800) {
            zone = 1;
        } else if (rpm > 1800 && rpm <= 2500) {
            zone = 2;
        } else if (rpm > 2500 && rpm <= 3200) {
            zone = 3;
        } else if (rpm > 3200) {
            zone = 4;
        }
        return zone;
    }

    private int getSpeedZone(int speed) {
        int zone = 0;
        if (speed > 0 && speed <= 40) {
            zone = 1;
        } else if (speed > 40 && speed <= 80) {
            zone = 2;
        } else if (speed > 80 && speed <= 100) {
            zone = 3;
        } else if (speed > 100) {
            zone = 4;
        }
        return zone;
    }

    private int getOilTempZone(int oilTemp) {
        int zone = 0;
        if (oilTemp <= 90) {
            zone = 1;
        } else if (oilTemp > 90 && oilTemp <= 112) {
            zone = 2;
        } else if (oilTemp > 112 && oilTemp <= 118) {
            zone = 3;
        } else if (oilTemp > 118) {
            zone = 4;
        }
        return zone;
    }

    private int getCoolantTempZone(int coolantTemp) {
        int zone = 0;
        if (coolantTemp <= 90) {
            zone = 1;
        } else if (coolantTemp > 90 && coolantTemp <= 112) {
            zone = 2;
        } else if (coolantTemp > 112 && coolantTemp <= 118) {
            zone = 3;
        } else if (coolantTemp > 118) {
            zone = 4;
        }
        return zone;
    }
}