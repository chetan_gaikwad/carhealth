package com.android.kycar.carhealth.ui.activity.drawer_menu.my_rides;

import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.entities.trip_transaction.Trip;
import com.android.kycar.carhealth.repositories.trip.TripRepo;
import com.android.kycar.carhealth.services.utils.Logger;
import com.android.kycar.carhealth.services.utils.LoggerTxt;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Krishna on 10/17/2016.
 */
public class ShowRouteActivity extends AppCompatActivity implements OnMapReadyCallback {

    private Logger tripCoords;
    private MapView mMapView;
    private GoogleMap mMap;
    private String tripId;
    private Trip trip;
    private TripRepo tripRepo;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_route);
        mMapView = (MapView) findViewById(R.id.trip_route_map);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();// needed to get the map to display immediately
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            tripId = extras.getString("tripId");
            tripRepo = new TripRepo(getApplicationContext(), Trip.class);
            trip = (Trip) tripRepo.findById(tripId);
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;

                if(trip.isRouteDataAvailable()) {

                } else {
                    /** Calling rest call to get multiple points */
                    new FetchTripRouteLocationsAsyncTask().execute(tripId);
                }

                /** Showing start point and end point */
                Marker startMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(trip.getStartLat(), trip.getStartLong())).title("Start Point"));
                Marker stopMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(trip.getStopLat(), trip.getStopLong())).title("Stop Point"));
                LatLng latLng = new LatLng(trip.getStartLat(), trip.getStartLong());
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

            }
        });

        /** Fetching csv file and getting route form direction API */
        tripCoords = new Logger(trip.getTripName()+".csv");
        if(tripCoords == null) {

            //Log.d(ShowRouteActivity.class.getSimpleName(), "Size ==>"+tripCoords.)
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.d(ShowRouteActivity.class.getSimpleName(), "Map is ready to use .....");


        // Setting onclick event listener for the map
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {

            }
        });

        //mMap.setMyLocationEnabled(true);
        /*LocationManager locationManager = (LocationManager) getActivity()
                .getSystemService(getActivity().LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            String bestProvider = locationManager.getBestProvider(criteria, true);
            Location location = locationManager.getLastKnownLocation(bestProvider);
            if (location != null) {
                onLocationChanged(location);
            }
            locationManager.requestLocationUpdates(bestProvider, 0, 0, MapFragment.this);
        }
        setUpMapIfNeeded();*/

    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        /*client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ShowRoute Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.nxt.autoz.ui.activity.drawer_menu.my_rides/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);*/
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        /*Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ShowRoute Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.nxt.autoz.ui.activity.drawer_menu.my_rides/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();*/
    }

    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<List<String>, Void, List<String>> {

        /*@Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }*/

        @Override
        protected List<String> doInBackground(List<String>... urlList) {
            // For storing data from web service
            List<String> result = new ArrayList<>();

            for(List<String> urls : urlList) {
                try {
                    // Fetching the data from web service
                    String data = downloadUrl(urls.get(0));
                    result.add(data);
                    Log.d("Background Task data", data.toString());
                } catch (Exception e) {
                    Log.d("Background Task", e.toString());
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(List<String> resultList) {
            super.onPostExecute(resultList);

            PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
            for(String result: resultList) {
                try {
                    JSONObject resObj = new JSONObject(result);
                    JSONArray snappedPoints = resObj.getJSONArray("snappedPoints");
                    for (int z = 0; z < snappedPoints.length(); z++) {
                        JSONObject snappedPoint = (JSONObject) snappedPoints.get(z);
                        JSONObject location = snappedPoint.getJSONObject("location");
                        LatLng point = new LatLng(location.getDouble("latitude"), location.getDouble("longitude"));
                        options.add(point);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Polyline line = mMap.addPolyline(options);
            }

            //ParserTask parserTask = new ParserTask();
            // Invokes the thread for parsing the JSON data
            //parserTask.execute(result);
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.RED);

                Log.d("onPostExecute", "onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                mMap.addPolyline(lineOptions);
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }

    public class DataParser {

        /**
         * Receives a JSONObject and returns a list of lists containing latitude and longitude
         */
        public List<List<HashMap<String, String>>> parse(JSONObject jObject) {

            List<List<HashMap<String, String>>> routes = new ArrayList<>();
            JSONArray jRoutes;
            JSONArray jLegs;
            JSONArray jSteps;

            try {

                jRoutes = jObject.getJSONArray("routes");

                /** Traversing all routes */
                for (int i = 0; i < jRoutes.length(); i++) {
                    jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                    List path = new ArrayList<>();

                    /** Traversing all legs */
                    for (int j = 0; j < jLegs.length(); j++) {
                        jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                        /** Traversing all steps */
                        for (int k = 0; k < jSteps.length(); k++) {
                            String polyline = "";
                            polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                            List<LatLng> list = decodePoly(polyline);

                            /** Traversing all points */
                            for (int l = 0; l < list.size(); l++) {
                                HashMap<String, String> hm = new HashMap<>();
                                hm.put("lat", Double.toString((list.get(l)).latitude));
                                hm.put("lng", Double.toString((list.get(l)).longitude));
                                path.add(hm);
                            }
                        }
                        routes.add(path);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
            }


            return routes;
        }


        /**
         * Method to decode polyline points
         * Courtesy : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
         */
        private List<LatLng> decodePoly(String encoded) {

            List<LatLng> poly = new ArrayList<>();
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;

            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng p = new LatLng((((double) lat / 1E5)),
                        (((double) lng / 1E5)));
                poly.add(p);
            }

            return poly;
        }
    }

    public class FetchTripRouteLocationsAsyncTask extends AsyncTask<String, Void, JSONArray> {

        @Override
        protected JSONArray doInBackground(String... params) {

            JSONArray data = null;
            try {
                /** Saving all trip score */

                File tripCoords = new File(trip.getTripName()+"txt");
                BufferedReader br = null;
                try  {
                    br = new BufferedReader(new FileReader(tripCoords));

                    String line = "";
                    StringBuilder fileContent = new StringBuilder();
                    fileContent.append("[");

                    while ((line = br.readLine()) != null) {
                        fileContent.append(line);
                    }
                    fileContent.deleteCharAt(fileContent.length()-1);
                    fileContent.append("]");
                    Log.d(ShowRouteActivity.class.getSimpleName(), ""+fileContent.length());

                    /** Sending data to server */
                    try {
                        data = new JSONArray(fileContent.toString());
                        Log.d(ShowRouteActivity.class.getSimpleName(), "Data:-"+data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (br != null) {
                        try {
                            br.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return data;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONArray resObject) {
            super.onPostExecute(resObject);
            Log.d(ShowRouteActivity.class.getSimpleName(), "Result:- " + resObject);

            if (resObject != null) {
                //Toast.makeText(this, "Trip created successfully", Toast.LENGTH_SHORT).show();
                try {

                    JSONObject startLocation = (JSONObject) resObject.get(0);
                    JSONObject stopLocation = (JSONObject) resObject.get(resObject.length()-1);

                    Location location = new Location("");
                    location.setLatitude(startLocation.getDouble("la"));
                    location.setLongitude(startLocation.getDouble("lo"));
                    Location location2 = new Location("");
                    location2.setLatitude(stopLocation.getDouble("la"));
                    location2.setLongitude(stopLocation.getDouble("lo"));

                    LatLng origin = new LatLng(location.getLatitude(), location.getLongitude());
                    LatLng dest = new LatLng(location2.getLatitude(), location2.getLongitude());

                    /** Counter for 90-90 points */

                    List<StringBuilder> multiplePointsList = new ArrayList<>();

                    for(int counter = 0; counter< (resObject.length()-1); counter= counter+1) {
                        JSONObject point = resObject.getJSONObject(counter);
                        if(multiplePointsList.size() <= counter/90) {
                            multiplePointsList.add(new StringBuilder(""));
                        }
                        multiplePointsList.get(counter/90).append("|"+point.getDouble("la")+","+point.getDouble("lo"));
                    }

                    /*String url2 = "https://maps.googleapis.com/maps/api/directions/json?origin="+location.getLatitude()+","+ location.getLongitude()+
                            "&destination="+location2.getLatitude()+","+location2.getLongitude() +"&" +
                            "waypoints=optimize:true"+multiplePoints+"&sensor=false";*/

                    List<String> snapToRoadList = new ArrayList<>();
                    for(StringBuilder multiplePoints : multiplePointsList) {
                        String snapToRoad = "https://roads.googleapis.com/v1/snapToRoads?" +
                                "path=" +multiplePoints+
                                "&interpolate=true" +
                                "&key=AIzaSyALb4C0-kGIjZO8hQOxX08SrHXHff6fDGM";
                        Log.d("onMapClick", snapToRoad.toString());
                        snapToRoadList.add(snapToRoad);
                    }

                    /** Saving data to file */
                    LoggerTxt snapToRoadTxt = new LoggerTxt("snapToRoad"+trip.getTripName()+".txt");
                    snapToRoadTxt.log(snapToRoadList.toString());
                    snapToRoadTxt.close();

                    Trip trip = new Trip((Trip) tripRepo.findById(tripId));
                    trip.setRouteDataAvailable(true);

                    drawRoute(snapToRoadList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void drawRoute(List<String> snapToRoadList) throws JSONException {

        FetchUrl fetchUrl = new FetchUrl();
        // Start downloading json data from Google Directions API
        fetchUrl.execute(snapToRoadList);
        JSONObject startLocation = new JSONObject(snapToRoadList.get(0).toString());

        Location location = new Location("");
        location.setLatitude(startLocation.getDouble("la"));
        location.setLongitude(startLocation.getDouble("lo"));
        Location location2 = new Location("");

        LatLng origin = new LatLng(location.getLatitude(), location.getLongitude());

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
    }
}
