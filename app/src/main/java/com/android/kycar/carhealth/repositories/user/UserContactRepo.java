package com.android.kycar.carhealth.repositories.user;

import android.content.Context;


import com.android.kycar.carhealth.entities.UserContact;
import com.android.kycar.carhealth.repositories.Repository;

import java.util.List;

/**
 * Created by Krishna on 2/1/2016.
 */
public class UserContactRepo extends Repository {
    public UserContactRepo(Context context, Class _aClass) {
        super(context, _aClass);
    }

    public UserContact findContactByNumber(String number) {
        return realm.where(UserContact.class).equalTo("number", number).findFirst();
    }

    public List<UserContact> findAllUnSynced() {
        return realm.where(UserContact.class).equalTo("isSync", false).findAll();
    }
}
