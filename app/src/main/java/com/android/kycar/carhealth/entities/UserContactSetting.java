package com.android.kycar.carhealth.entities;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Krishna on 1/12/2016.
 */
@RealmClass
public class UserContactSetting extends RealmObject {


    @PrimaryKey
    private String id;

    private String userContactId;

    private boolean isSos;

    private boolean shareLocation;

    private boolean isAutoConnect;

    public boolean getIsAutoConnect() {
        return isAutoConnect;
    }

    public void setIsAutoConnect(boolean isAutoConnect) {
        this.isAutoConnect = isAutoConnect;
    }

    public UserContactSetting() {
    }

    public UserContactSetting(String id, String userContactId, boolean isSos, boolean shareLocation, boolean isAutoConnect) {
        this.id = id;
        this.userContactId = userContactId;
        this.isSos = isSos;
        this.shareLocation = shareLocation;
        this.isAutoConnect = isAutoConnect;
    }

    public UserContactSetting (UserContactSetting userContactSetting) {
        if(userContactSetting != null) {
            this.id = userContactSetting.getId();
            this.userContactId = userContactSetting.getUserContactId();
            this.isAutoConnect = userContactSetting.getIsAutoConnect();
            this.isSos = userContactSetting.getIsSos();
            this.shareLocation = userContactSetting.getShareLocation();
        } else {
            userContactSetting.id = new Date().getTime()+"";
            this.isSos = false;
            this.isAutoConnect = false;
            this.shareLocation = false;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserContactId() {
        return userContactId;
    }

    public void setUserContactId(String userContactId) {
        this.userContactId = userContactId;
    }

    public boolean getIsSos() {
        return isSos;
    }

    public void setIsSos(boolean isSos) {
        this.isSos = isSos;
    }

    public boolean getShareLocation() {
        return shareLocation;
    }

    public void setShareLocation(boolean shareLocation) {
        this.shareLocation = shareLocation;
    }
}
