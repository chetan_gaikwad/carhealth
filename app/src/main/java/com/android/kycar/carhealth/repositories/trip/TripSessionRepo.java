package com.android.kycar.carhealth.repositories.trip;

import android.content.Context;
import android.util.Log;


import com.android.kycar.carhealth.entities.trip_transaction.TripSession;
import com.android.kycar.carhealth.repositories.Repository;

import java.util.List;
/**
 * Created by Krishna on 8/3/2016.
 */
public class TripSessionRepo extends Repository {
    public TripSessionRepo(Context context, Class _aClass) {
        super(context, _aClass);
    }


    public boolean deleteByTripId (String tripId) {

        List<TripSession> tripSessions = realm.where(aClass).equalTo("tripId", tripId).findAll();
        Log.d(TripScoreRepo.class.getSimpleName(), "Size :- "+tripSessions.size());
        for (int counter = 0; counter < tripSessions.size(); counter++) {
            deleteById(tripSessions.get(counter).getId());
        }
        return true;
    }

    public List<TripSession> findAllUnSynced() {
        return realm.where(TripSession.class).equalTo("isSync", false).findAll();
    }
}
