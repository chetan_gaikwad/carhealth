package com.android.kycar.carhealth.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.kycar.carhealth.config.ProgessDialogBox;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Krishna on 4/25/2016.
 */
public class CreateTripId extends AsyncTask<String, Void, String> {


    private ProgessDialogBox dialogBox;
    private Context context;
    public CreateTripId(Context context) {

        this.context = context;
        Log.d("Create Trip", "Trip id created");
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialogBox = new ProgessDialogBox(context);
        dialogBox.showDailog();
    }

    @Override
    protected String doInBackground(String... cred1) {
        try{

            /** Rest URL for the creating trip */
            String url = "http://139.59.253.137:1500/statistics/find";

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("POST");
            con.setRequestProperty("userId", cred1[0]);
            con.setRequestProperty("deviceId", cred1[1]);

            Log.i("Params", "url is " + cred1[0]);
            Log.i("Params", "url is " + cred1[1]);

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            Log.d("Data fetched--->", ""+response);
            return response.toString();
        }catch (java.io.FileNotFoundException e){
            return "User not found";
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(dialogBox.isShowing()){
            dialogBox.dismissDialog();
        }
        if (result!=null)
            BusProvider.getInstance().post(result);
    }
}
