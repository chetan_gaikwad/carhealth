package com.android.kycar.carhealth.beans;

/**
 * Created by Krishna on 7/17/2016.
 */
public class MapMarkerBean {

    private double latitude;
    private double longitude;
    private String markerTile;
    private boolean isSessionEnd;
    private boolean isSessionStart;
    private boolean isAccidentMarker;
    public MapMarkerBean() {
    }

    public MapMarkerBean(boolean isAccidentMarker, boolean isSessionEnd, boolean isSessionStart, double latitude, double longitude, String markerTile) {
        this.isAccidentMarker = false;
        this.isSessionEnd = false;
        this.isSessionStart = isSessionStart;
        this.latitude = latitude;
        this.longitude = longitude;
        this.markerTile = markerTile;
    }

    public boolean isAccidentMarker() {
        return isAccidentMarker;
    }

    public void setAccidentMarker(boolean accidentMarker) {
        isAccidentMarker = accidentMarker;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getMarkerTile() {
        return markerTile;
    }

    public void setMarkerTile(String markerTile) {
        this.markerTile = markerTile;
    }

    public boolean isSessionEnd() {
        return isSessionEnd;
    }

    public void setSessionEnd(boolean sessionEnd) {
        isSessionEnd = sessionEnd;
    }

    public boolean isSessionStart() {
        return isSessionStart;
    }

    public void setSessionStart(boolean sessionStart) {
        isSessionStart = sessionStart;
    }
}
