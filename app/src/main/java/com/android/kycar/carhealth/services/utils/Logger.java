package com.android.kycar.carhealth.services.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import androidx.core.content.ContextCompat;
import android.util.Log;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Krishna on 2/6/2016.
 */
public class Logger {

    private File file;
    private FileOutputStream fileOutputStream;
    private PrintWriter pw;
    private String folderPath = "/car_health/logger";
    private Context context;
    private String OBD = "/obd";

    //This is always ne using for real CSV logData
    public Logger(String fileName, Context context) {
        this.context = context;
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());

        try {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                File root = android.os.Environment.getExternalStorageDirectory();

                //File dir = new File(root.getAbsolutePath() + folderPath + calendar.get(Calendar.YEAR) + calendar.get(Calendar.MONTH) + calendar.get(Calendar.DAY_OF_MONTH));

                File dir = new File(root.getAbsolutePath() + folderPath);
                dir.mkdirs();
                file = new File(dir, fileName);

                try {
                    fileOutputStream = new FileOutputStream(file);
                    pw = new PrintWriter(fileOutputStream);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //This is mainly used for json like csv data
    public Logger(String fileName) {
        this.context = context;
        try {
            File root = android.os.Environment.getExternalStorageDirectory();
            File dir = new File(root.getAbsolutePath() + folderPath + OBD);
            dir.mkdirs();
            file = new File(dir, fileName);

            Log.d(Logger.class.getSimpleName(), "Size==>" + file.length());
            try {
                fileOutputStream = new FileOutputStream(file);
                pw = new PrintWriter(fileOutputStream);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Logger(String fileName, String tripName) {

        File root = android.os.Environment.getExternalStorageDirectory();
        File dir = new File(root.getAbsolutePath() + folderPath + "/obd");
        dir.mkdirs();
        file = new File(dir, fileName);

        Log.d(Logger.class.getSimpleName(), "Size==>" + file.length());
        try {
            fileOutputStream = new FileOutputStream(file);
            pw = new PrintWriter(fileOutputStream);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setHeader(List<String> headers) {
        try {


            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                try {
                    for (String str : headers) {
                        pw.print(str + ",");
                    }
                    pw.println();
                    pw.flush();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void logging(List<String> logs) {
        try {

            Log.d("cdd", "Logging " + logs);

            try {
                for (String str : logs) {
                    pw.print(str + ",");
                }
                pw.println();
                pw.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loggingString(String logs) {
        try {

            Log.d("cdd", "Logging " + logs);

            try {
                pw.print(logs);
                pw.println();
                pw.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void logging(JSONObject logs) {

        try {
            pw.println();
            pw.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void logging(String str) {
        try {

            pw.print(str + ",");
            pw.println();
            pw.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean finalizeFile() {
        try {

            if (file.length() == 0) {
                file.delete();
            }
            pw.close();
            try {
                fileOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

}
