package com.android.kycar.carhealth.services.connection_services.rest_services;


import com.android.kycar.carhealth.entities.Vehicle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krishna on 5/31/2016.
 */
public class VehicleRestService extends RestService{

    public String createVehicle(Vehicle vehicle){

        String url = "/vehicle/create";
        JSONObject vehicleJson = new JSONObject();

        try{
            vehicleJson.put("name", vehicle.getName());
            vehicleJson.put("nickName", vehicle.getNickName());
            vehicleJson.put("year", vehicle.getYear());
            vehicleJson.put("userId",vehicle.getUserId());
            vehicleJson.put("variantId", vehicle.getVariantId());
            vehicleJson.put("regNumber", vehicle.getRegNumber());
            vehicleJson.put("vin", vehicle.getVin());
            vehicleJson.put("encodedVin",vehicle.getEncodedVin());
            vehicleJson.put("puc", vehicle.getPuc());
            vehicleJson.put("insurance", vehicle.getInsurance());
            vehicleJson.put("rc", vehicle.getRc());
            vehicleJson.put("isActive",vehicle.isActive());
            vehicleJson.put("vehicleImage", vehicle.getVehicleImage());

        }catch (Exception e){
            e.printStackTrace();
        }
        return restServiceCall(url, "POST", vehicleJson);
    }

    public String updateVehicle(Vehicle vehicle){

        String url = "/vehicle/update";
        JSONObject vehicleJson = new JSONObject();

        try{
            vehicleJson.put("id",vehicle.getId());
            vehicleJson.put("name", vehicle.getName());
            vehicleJson.put("nickName", vehicle.getNickName());
            vehicleJson.put("year", vehicle.getYear());
            vehicleJson.put("userId",vehicle.getUserId());
            vehicleJson.put("variantId", vehicle.getVariantId());
            vehicleJson.put("regNumber", vehicle.getRegNumber());
            vehicleJson.put("vin", vehicle.getVin());
            vehicleJson.put("encodedVin",vehicle.getEncodedVin());
            vehicleJson.put("puc", vehicle.getPuc());
            vehicleJson.put("insurance", vehicle.getInsurance());
            vehicleJson.put("rc", vehicle.getRc());
            vehicleJson.put("isActive",vehicle.isActive());
            vehicleJson.put("vehicleImage", vehicle.getVehicleImage());

        }catch (Exception e){
            e.printStackTrace();
        }
        return restServiceCall(url, "PUT", vehicleJson);
    }

    public String deleteVehicle(String id){

        String url = "/vehicle/delete?id="+id;
        return restServiceCall(url, "DELETE");
    }

    public List<Vehicle> fetchAllVehicle(){

        String response = restServiceCall("/vehicle/find", "GET");
        try{
            JSONArray responseArray = new JSONArray(response.toString());
            List<Vehicle> vehicles = new ArrayList<>();

            for(int i=0 ; i<responseArray.length(); i++) {
                JSONObject responseObj = (JSONObject) responseArray.get(i);
                vehicles.add(getVehicleObject(responseObj));
            }
            return vehicles;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private Vehicle getVehicleObject(JSONObject vehicleObj){

        Vehicle vehicle = new Vehicle();
        try {
            vehicle.setId(vehicleObj.getString("id"));
            vehicle.setEncodedVin(vehicleObj.getString("encodedVin"));
            //vehicle.setInsurance(vehicleObj.get("insurance"));
            vehicle.setActive(vehicleObj.getBoolean("isActive"));
            vehicle.setName(vehicleObj.getString("name"));
            vehicle.setNickName(vehicleObj.getString("nickName"));
            /*vehicle.setPuc(vehicleObj.get("puc"));
            vehicle.setRc(vehicleObj.get("rc"));*/
            vehicle.setRegNumber(vehicleObj.getString("regNumber"));
            vehicle.setUserId(vehicleObj.getString("userId"));
            vehicle.setVariantId(vehicleObj.getString("variantId"));
            vehicle.setVin(vehicleObj.getString("vin"));

            return vehicle;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
