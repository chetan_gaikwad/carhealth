

package com.android.kycar.carhealth.async_task;


import android.os.AsyncTask;

import com.android.kycar.carhealth.NxtApplication;
import com.android.kycar.carhealth.services.connection_services.rest_services.FcmRestCallService;


public class SendAccidentNotificationAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... tokens) {

            try{
                return new FcmRestCallService().sendAccidentNotification(tokens[0], tokens[1], NxtApplication.currLocation);
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            if(result!=null) {
                //  Toast.makeText(DashboardActivity.this, "Trip created successfully", Toast.LENGTH_SHORT).show();
            } else  {
                //  Toast.makeText(DashboardActivity.this, "Trip creation failed", Toast.LENGTH_SHORT).show();
            }
        }
    }