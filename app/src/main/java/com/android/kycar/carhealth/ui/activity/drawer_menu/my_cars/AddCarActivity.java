package com.android.kycar.carhealth.ui.activity.drawer_menu.my_cars;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.async_task.FetchVehicleGearRatioAsyncTask;
import com.android.kycar.carhealth.config.ProgessDialogBox;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.entities.Vehicle;
import com.android.kycar.carhealth.entities.vehicle_master.FuelType;
import com.android.kycar.carhealth.entities.vehicle_master.Make;
import com.android.kycar.carhealth.entities.vehicle_master.Model;
import com.android.kycar.carhealth.entities.vehicle_master.Variant;
import com.android.kycar.carhealth.repositories.vehicle.FuelTypeRepo;
import com.android.kycar.carhealth.repositories.vehicle.MakeRepo;
import com.android.kycar.carhealth.repositories.vehicle.ModelRepo;
import com.android.kycar.carhealth.repositories.vehicle.VariantRepo;
import com.android.kycar.carhealth.repositories.vehicle.VehicleRepo;
import com.android.kycar.carhealth.services.connection_services.rest_services.VehicleRestService;
import com.android.kycar.carhealth.task.CalculateVinTask;
import com.android.kycar.carhealth.task.OnTaskCompletedString;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddCarActivity extends AppCompatActivity implements OnTaskCompletedString {

    private AutoCompleteTextView spMake;
    private AutoCompleteTextView spModel;
    private Spinner spFuel;
    private Spinner spVariant;
    private Spinner spYear;
    private EditText etCarNickName;
    private MakeRepo makeRepo;
    private ModelRepo modelRepo;
    private VariantRepo variantRepo;
    private VehicleRepo vehicleRepo;
    private FuelTypeRepo fuelTypeRepo;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private ProgessDialogBox dialogBox;
    private TextView tvGetVinFromCar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car);
        makeRepo = new MakeRepo(getApplicationContext(), Make.class);
        modelRepo = new ModelRepo(getApplicationContext(), Model.class);
        variantRepo = new VariantRepo(getApplicationContext(), Variant.class);
        vehicleRepo = new VehicleRepo(getApplicationContext(), Vehicle.class);
        fuelTypeRepo = new FuelTypeRepo(getApplicationContext(), FuelType.class);
        etCarNickName = (EditText) findViewById(R.id.etcarnicknameaddcar);
        spMake = (AutoCompleteTextView) findViewById(R.id.spmake);
        tvGetVinFromCar = (TextView) findViewById(R.id.tv_get_vin_from_car);
        dialogBox = new ProgessDialogBox(this);

        spMake.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> p, View v, int pos, long id) {
                onMakeChange(p.getItemAtPosition(pos).toString());
                ((TextView) p.getChildAt(0)).setTextColor(Color.WHITE);
            }
        });

        spModel = (AutoCompleteTextView) findViewById(R.id.spmodel);

        spModel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> p, View v, int pos, long id) {
                onModelSelected(p.getItemAtPosition(pos).toString());
                ((TextView) p.getChildAt(0)).setTextColor(Color.WHITE);
            }
        });

        /*spModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onModelSelected(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/

        spFuel = (Spinner) findViewById(R.id.spfuel);
        spVariant = (Spinner) findViewById(R.id.spvariant);
        spYear = (Spinner) findViewById(R.id.spyear);

        ArrayList<String> makeList = new ArrayList<>();
        List<Make> makes = (List<Make>) makeRepo.findAll();
        makeRepo.close();
        for(Make make : makes){
            makeList.add(make.getName());
        }

        setSpinnerAdapter(spMake, makeList);

        List<FuelType> fuelTypes = fuelTypeRepo.findAll();
        ArrayList<String> fuelList = new ArrayList<>();
        for (FuelType fuelType : fuelTypes){
            fuelList.add(fuelType.getFuelType());
        }
        setSpinnerAdapter(spFuel, fuelList);

        ArrayList<String> yearList = new ArrayList<>();
        yearList.add("2016");
        yearList.add("2015");
        yearList.add("2014");
        yearList.add("2013");
        yearList.add("2012");
        yearList.add("2011");
        yearList.add("2010");
        yearList.add("2009");
        yearList.add("2008");
        yearList.add("2007");
        yearList.add("2006");
        yearList.add("2005");
        setSpinnerAdapter(spYear, yearList);
        preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        editor = preferences.edit();
    }

    public void onAddCarClick(View view){

        Vehicle vehicle = new Vehicle();
        vehicle.setName(spModel.getText().toString().trim());
        preferences.edit().putString(Util.CARNAME_PREFERENCE, etCarNickName.getText().toString().trim());

        Variant variant;
        if(spVariant.getSelectedItem() == null){
            vehicle.setVariantId(new Date().toString().trim());
        } else {
            variant = variantRepo.findByName(spVariant.getSelectedItem().toString().trim());
            if(variant == null){
                vehicle.setVariantId(new Date().toString().trim());
            } else {
                vehicle.setVariantId(variant.getId());
            }
        }
        vehicle.setYear(spYear.getSelectedItem().toString().trim());
        vehicle.setActive(true);
        vehicle.setVin(tvGetVinFromCar.getText()+"");
        vehicle.setNickName(etCarNickName.getText().toString().trim());
        vehicle.setUserId(preferences.getString(Util.USER_PREFERENCE, ""));

        /** Rest call for vehicle */
        if(Util.isOnline(getApplicationContext())) {
            new AddVehicleRestCall().execute(vehicle);
            /** Fetching engine vitals data from server */
            new FetchVehicleGearRatioAsyncTask(getApplicationContext()).execute(vehicle.getVariantId());

        }
    }

    public void onMakeChange(String selectedMake){

        String makeId = makeRepo.findByName(selectedMake).getId();
        List<Model> models = modelRepo.findByMakeId(makeId);
        modelRepo.close();
        ArrayList<String> modelString  = new ArrayList<String>();
        for (Model model : models) {
            modelString.add(model.getName());
        }
        setSpinnerAdapter(spModel, modelString);
    }

    public void onModelSelected(String selectedModel){

        String modelId = modelRepo.findByName(selectedModel).getId();
        List<Variant> variants = variantRepo.findByModelId(modelId);
        variantRepo.close();
        ArrayList<String> varinetString = new ArrayList<String>();
        for (Variant variant : variants){
            varinetString.add(variant.getName());
        }
        setSpinnerAdapter(spVariant, varinetString);
    }

    private void setSpinnerAdapter(Spinner spinner, ArrayList<String> list){

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddCarActivity.this, R.layout.spinner_text, list);
        spinner.setAdapter(adapter);
    }

    private void setSpinnerAdapter(AutoCompleteTextView spinner, ArrayList<String> list){

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddCarActivity.this, android.R.layout.simple_spinner_dropdown_item, list);
        spinner.setAdapter(adapter);
    }

    public void getVinFromCar(View view) {
        new CalculateVinTask(AddCarActivity.this, AddCarActivity.this).execute("");
    }

    @Override
    public void onTaskCompleted(String vin) {

        tvGetVinFromCar.setText(vin);
        Log.i("nansjks","Got vin : "+vin);
    }

    /**
     * Uploading asynchronous task
     */
    class AddVehicleRestCall extends AsyncTask<Vehicle, Void, String> {

        private VehicleRepo vehicleRepo;
        @Override
        protected String doInBackground(Vehicle... vehicles) {

            vehicleRepo = new VehicleRepo(getApplicationContext(), Vehicle.class);
            try {
                String response = new VehicleRestService().createVehicle(vehicles[0]);
                if(response != null) {
                    JSONObject responseObj = new JSONObject(response);
                    vehicles[0].setId(responseObj.getString("id"));
                    vehicles[0].setSync(true);
                    vehicleRepo.saveOrUpdate(vehicles[0]);
                } else {
                    vehicles[0].setId(new Date().getTime()+"");
                    vehicles[0].setSync(false);
                    vehicleRepo.saveOrUpdate(vehicles[0]);
                }
                if(vehicleRepo.findAll().size() == 1){
                    Log.d("kaj","First car seleted by default");
                    preferences.edit().putString(Util.CAR_PREFERENCE, vehicles[0].getId()).apply();
                    preferences.edit().putString(Util.CARNAME_PREFERENCE, vehicles[0].getNickName().trim()).apply();
                    preferences.edit().commit();
                }
                vehicleRepo.close();
                Log.d(AddCarActivity.class.getSimpleName(), "Rest Response:-"+response);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogBox.showDailog();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(dialogBox.isShowing()){
                dialogBox.dismissDialog();
            }
            Toast.makeText(AddCarActivity.this,"Car added successfully",Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
    }
}
