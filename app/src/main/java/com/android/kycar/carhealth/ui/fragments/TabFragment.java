package com.android.kycar.carhealth.ui.fragments;

import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.android.kycar.carhealth.R;
import com.google.android.material.tabs.TabLayout;


/**
 * Created by Chetan on 7/27/2015.
 */
public class TabFragment extends Fragment {

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 3 ;

    boolean move = true;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        /**
         *Inflate tab_layout and setup Views.
         */
            View x =  inflater.inflate(R.layout.tab_layout,null);
            tabLayout = (TabLayout) x.findViewById(R.id.tabs);
            viewPager = (ViewPager) x.findViewById(R.id.viewpager);

        /**
         *Set an Apater for the View Pager
         */
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                    tabLayout.setupWithViewPager(viewPager);
                   }
        });

        return x;
    }

    public void switchPage(int i){
        viewPager.setCurrentItem(i);
    }
    public void switchPageNext(int i){
        Log.i("dsd","i "+i+"   current "+viewPager.getCurrentItem());
        if(move) {
            int nextPage = 0;
            if (viewPager.getCurrentItem() == 2) {
                nextPage = 0;
            } else {
                nextPage = viewPager.getCurrentItem() + 1;
            }
            viewPager.setCurrentItem(nextPage);
            move = false;
        }else {
            move = true;
        }
    }

    class MyAdapter extends FragmentPagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position.
         */

        @Override
        public Fragment getItem(int position)
        {
          switch (position){
              case 0 : return new ValuesFragmentUpdated();
              case 1 : return new StatsFragmentUpdated();
              case 2 : return new MapFragment();
              //case 3 : return new BikeFragment();
              //case 4 : return new CarFragment();

          }
        return null;
        }

        @Override
        public int getCount() {
            return int_items;
        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0 :
                    return "Live";
                case 1 :
                    return "OBD";
                case 2 :
                    return "MAP";
                case 3 :
                    return "Bike";
                case 4 :
                    return "Car";
            }
                return null;
        }
    }

}
