package com.android.kycar.carhealth.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.kycar.carhealth.R;
import com.android.kycar.carhealth.config.Util;
import com.android.kycar.carhealth.customfonts.MyTextView;
import com.android.kycar.carhealth.entities.User;
import com.android.kycar.carhealth.entities.UserContact;
import com.android.kycar.carhealth.entities.UserContactSetting;
import com.android.kycar.carhealth.repositories.user.UserContactRepo;
import com.android.kycar.carhealth.repositories.user.UserContactSettingRepo;
import com.android.kycar.carhealth.repositories.user.UserRepo;

import java.util.List;

public class CleanerOverlay extends Overlay {

    private TextView dismiss;
    private LinearLayout sos1, sos2, sos3;
    private MyTextView tvAllergies, tvOngoingMedications, tvAccidentalHistory;
    private MyTextView tvUserName, tvBloodGroup, tvAge;
    private UserRepo userRepo;
    private UserContactRepo userContactRepo;
    private UserContactSettingRepo userContactSettingRepo;
    private SharedPreferences preferences;

    private static final int flags = //WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
             //WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
            //| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
             //WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
            //| WindowManager.LayoutParams.FLAG_FULLSCREEN
             WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
            ;

    public CleanerOverlay(final Context context) {
        super(context, WindowManager.LayoutParams.TYPE_SYSTEM_ERROR, flags, true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cleaner_overlay);
        dismiss = (TextView) findViewById(R.id.dismiss);
        sos1 = (LinearLayout) findViewById(R.id.ll_sos_1);
        sos1.setOnClickListener(onClickListener);
        sos2 = (LinearLayout) findViewById(R.id.ll_sos_2);
        sos2.setOnClickListener(onClickListener);
        sos3 = (LinearLayout) findViewById(R.id.ll_sos_3);
        sos3.setOnClickListener(onClickListener);

        tvAccidentalHistory = (MyTextView) findViewById(R.id.tv_accidental_history);
        tvAllergies = (MyTextView) findViewById(R.id.tv_allergies);
        tvOngoingMedications = (MyTextView) findViewById(R.id.tv_ongoing_medications);
        tvAge = (MyTextView) findViewById(R.id.tv_age);
        tvUserName = (MyTextView) findViewById(R.id.tv_user_name);
        tvBloodGroup = (MyTextView) findViewById(R.id.tv_lock_screen_blood_group);
        preferences = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        userRepo = new UserRepo(getApplicationContext(), User.class);
        userContactRepo = new UserContactRepo(getApplicationContext(), UserContact.class);
        userContactSettingRepo = new UserContactSettingRepo(getApplicationContext(), UserContactSetting.class);

        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        initUi();
    }

    private void initUi(){

        User user = (User) userRepo.findById(preferences.getString(Util.USER_PREFERENCE, ""));
        tvOngoingMedications.setText(user.getOnGoingMedications());
        tvAccidentalHistory.setText(user.getAccidentalHistory());
        tvAllergies.setText(user.getAllergies());
        tvAge.setText(""+46);
        tvUserName.setText(preferences.getString(Util.USERNAME_PREFERENCE, ""));

        if(user.getBloodGrp() != -1) {
            switch (user.getBloodGrp()){
                case 0:
                    tvBloodGroup.setText("A+");
                    break;
                case 1:
                    tvBloodGroup.setText("A-");
                    break;
                case 2:
                    tvBloodGroup.setText("B+");
                    break;
                case 3:
                    tvBloodGroup.setText("B-");
                    break;
                case 4:
                    tvBloodGroup.setText("o+");
                    break;
                case 5:
                    tvBloodGroup.setText("o-");
                    break;
                case 6:
                    tvBloodGroup.setText("AB+");
                    break;
                case 7:
                    tvBloodGroup.setText("AB-");
                    break;
            }

            /*bloodGroupList.add("A+");
            bloodGroupList.add("A-");
            bloodGroupList.add("B+");
            bloodGroupList.add("B-");
            bloodGroupList.add("O+");
            bloodGroupList.add("O-");
            bloodGroupList.add("AB+");
            bloodGroupList.add("AB-");*/
        }

        List<UserContact> userContacts = userContactRepo.findAll();
        for (UserContact userContact : userContacts) {

            UserContactSetting userContactSetting = userContactSettingRepo.findByUserContactId(userContact.getId());
            if(userContactSetting.getIsSos()) {
                if(sos1.getVisibility() == View.GONE) {
                    MyTextView tvName = (MyTextView) sos1.findViewWithTag("tv_name");
                    tvName.setText(userContact.getName());
                    MyTextView tvNumber = (MyTextView) sos1.findViewWithTag("tv_number");
                    tvNumber.setText(userContact.getNumber());
                    sos1.setVisibility(View.VISIBLE);
                } else if(sos2.getVisibility() == View.GONE){
                    MyTextView tvName = (MyTextView) sos2.findViewWithTag("tv_name");
                    tvName.setText(userContact.getName());
                    MyTextView tvNumber = (MyTextView) sos2.findViewWithTag("tv_number");
                    tvNumber.setText(userContact.getNumber());
                    sos2.setVisibility(View.VISIBLE);
                } else if(sos3.getVisibility() == View.GONE){
                    MyTextView tvName = (MyTextView) sos3.findViewWithTag("tv_name");
                    tvName.setText(userContact.getName());
                    MyTextView tvNumber = (MyTextView) sos3.findViewWithTag("tv_number");
                    tvNumber.setText(userContact.getNumber());
                    sos3.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
        Log.d("dwd","sds");
    }

    View.OnClickListener onClickListener  = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.d("dsds","calling");
            /*Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "999999999"));
            startActivity(intent);*/
        }
    };


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d("dsds","onKeyDown");

        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK)
        {
            if (event.getAction() == KeyEvent.ACTION_DOWN  &&  event.getRepeatCount() == 0)
            {
                Log.d("dsds","KEYCODE_BACK");
                return true;

            }

            else if (event.getAction() == KeyEvent.ACTION_UP)
            {
                Log.d("dsds","ACTION_UP");

            }
        }
        return super.onKeyDown(keyCode, event);
    }
}

